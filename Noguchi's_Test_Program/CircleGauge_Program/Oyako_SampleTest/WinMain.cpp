
#include "common.h"

// =========================================================================
//
//
//							メインループ
//
//
// =========================================================================
int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance,
						LPSTR lpCmdLine, int nCmdShow )
{
	// ---------------------------
	//		DXLIB初期化処理
	// ---------------------------
	SetMainWindowText( TEXT( "Texture_Test" ) );
	ChangeWindowMode( TRUE ) ;
	SetGraphMode( 800, 640, 32 ) ;

	if ( DxLib_Init() == -1 )		// ＤＸライブラリ初期化処理
		return -1 ;					// エラーが起きたら直ちに終了

	SetDrawScreen( DX_SCREEN_BACK );

	// ---------------------------
	//		変数初期化処理
	// ---------------------------
	// --- 画像セット
	SetTransColor( 0, 255, 0 ) ;
	int cirGraph = LoadGraph( "..\\image\\HPgauge.png" ) ;			// --- 4/4 メーター
	int fraGraph = LoadGraph( "..\\image\\HPFrame_1harf4.bmp" ) ;	// --- 3/4 メーター

	if ( (cirGraph == -1) || (fraGraph == -1) )
		printf( "ERORR\n" ) ;

	// --- 足場にステータスを付与
	printf( "=======================================================\n" ) ;
	printf( "              足場ステータスセット\n" ) ;
	printf( "=======================================================\n" ) ;
	for ( int i = 0 ; i < ASIBA_MAX ; i++ )
	{
		scaffold *scff = &g_asiba[i] ;

		//					X					Y					Z
		scff->pos	= VGet( (float)(i * 10),	(float)(i * 30),	0.0f ) ;	// --- 位置
		scff->size	= VGet( 1.0f,				1.0f,				1.0f ) ;	// --- サイズ
		scff->dir	= 0.0f ;													// --- 回転	

		if ( OBJID_ASIBA + i <= OBJID_ASIBA_MAX )
			scff->objID = OBJID_ASIBA + i ;				// --- ID付与

		printf( "【g_asiba[%02d]】objID - %02d\n", i, scff->objID ) ;
	}
	printf( "=======================================================\n" ) ;
	printf( "           追従するオブジェのアドレスをセット\n" ) ;
	printf( "=======================================================\n" ) ;
	for ( int i = 0 ; i < ASIBA_MAX ; i++ )
	{
		scaffold	*scff = &g_asiba[i] ;	// --- 足場
		collision	*coll = &g_coll[i] ;	// --- コリジョン

		coll->scfAddr = scff ;				// --- 足場オブジェクトのアドレスを、メンバ変数「scfAddr」にセット
	}

/*
	// --- 追跡オブジェクトのIDをセット
	printf( "=======================================================\n" ) ;
	printf( "              追跡オブジェのＩＤをセット\n" ) ;
	printf( "=======================================================\n" ) ;
	for ( int i = 0 ; i < ASIBA_MAX ; i++ )
	{
		scaffold	*scff = &g_asiba[i] ;	// --- 足場
		collision	*coll = &g_coll[i] ;	// --- コリジョン

		coll->chaseObjID = scff->objID ;	// --- 足場オブジェクトのＩＤをメンバ変数「chaseObjID」にセット
	}
*/

	// -------------------------------------------------------------------------------------------
	//
	//										while
	//
	// -------------------------------------------------------------------------------------------
	printf( "=======================================================\n" ) ;
	printf( "                ループをスタートさせる\n" ) ;
	printf( "=======================================================\n" ) ;

	float num = 0 ;
	float plus = 0.5 ;
	while ( ProcessMessage() == 0 && CheckHitKey(KEY_INPUT_ESCAPE) == 0 )
	{
		ClearDrawScreen() ;		// --- 画面のクリア
		DrawBox( 0, 0, 800, 640, GetColor( 0xff, 0xff, 0xff ), TRUE ) ;

		DrawGraph( 400 - 180, 400 - 180, fraGraph, TRUE ) ;
		DrawCircleGauge( 400, 400, num, cirGraph, 0 ) ;

		if ( num + plus < 75 )
			num += plus ;
		else if ( num + plus >= 75 )
			num = 74.9f ;

		// ----------------------------------------
		// --- オブジェクト(足場)を移動し、格納する
		// ----------------------------------------
		for ( int i = 0 ; i < ASIBA_MAX ; i++ )
		{
			scaffold *scff = &g_asiba[i] ;

			scff->pos.x += (float)(rand() % 7 - 3) ;		// --- 移動処理

			g_cInfo[ scff->objID ].pos	= scff->pos ;		// --- 位置セット
			g_cInfo[ scff->objID ].size	= scff->size ;		// --- サイズセット
			g_cInfo[ scff->objID ].dir	= scff->dir ;		// --- 回転セット
		}

		// ----------------------------------------
		// --- コリジョンモデルにセット
		// ----------------------------------------
		for ( int i = 0 ; i < ASIBA_MAX ; i++ )
			g_coll[i].action() ;

		// ----------------------------------------
		// --- テスト出力
		// ----------------------------------------

		ScreenFlip() ;			// --- 裏画面を表に出力する
	}

	DxLib_End() ;			// ＤＸライブラリ使用の終了処理

	return 0 ;				// ソフトの終了 
}


