
// --- 定義
#define ASIBA_MAX			10			// --- 足場の最大数
#define OBJID_ASIBA			20			// --- 足場のオブジェクトID_最小値
#define OBJID_ASIBA_MAX		120			// --- 足場のオブジェクトID_最大値
#define OBJ_MAX				1028		// --- オブジェクトの最大数

// --- インクルーディング
#include <DxLib.h>

#include "ConsoleWindow.h"	// --- こんそ〜る

#include "object.h"			// --- オブジェクト
#include "scaffold.h"		// --- 足場

#include "collobj_info.h"	// --- コリジョン関連の情報まとめ
#include "collision.h"		// --- コリジョン

// --- 外部宣言

extern ConsoleWindow	g_cWin ;						// --- コンソールウィンドウ
extern scaffold			g_asiba[ ASIBA_MAX ] ;			// --- 足場オブジェ
extern collision		g_coll[ ASIBA_MAX ] ;			// --- コリジョンオブジェクト

extern collobj_info		g_cInfo[ OBJ_MAX ] ;			// --- オブジェクト情報を格納する箇所( pos, size, dirなど )


