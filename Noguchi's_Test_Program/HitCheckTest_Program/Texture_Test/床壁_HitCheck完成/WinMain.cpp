
#include "DxLib.h"
#include "d3d9types.h"
#include "ConsoleWindow.h"
#include <math.h>

#define FLOOR_MAX					17			// --- 床オブジェ最大数

#define PLAYER_MAX_HITCOLL			2058		// --- 察知可能な最大ポリゴン数
#define PLAYER_HIT_HEIGHT			100			// --- プレイヤーのヒット判定 _ 縦
#define PLAYER_HIT_WIDTH			80			// --- プレイヤーのヒット判定 _ 横
#define PLAYER_HIT_TRYNUM			16			// --- 壁押し出し処理の最大試行回数
#define PLAYER_HIT_SLIDE_LENGTH		5.0f		// --- 一度の壁押し出し処理でスライドさせる距離

#define DIR_UP						0x01		// --- 向き
#define DIR_DOWN					0x02		// --- 向き
#define DIR_LEFT					0x04		// --- 向き
#define DIR_RIGHT					0x08		// --- 向き
#define DIR_UPLEFT					0x05		// --- 向き
#define DIR_UPRIGHT					0x09		// --- 向き
#define DIR_DOWNLEFT				0x06		// --- 向き
#define DIR_DOWNRIGHT				0x0a		// --- 向き

// -------------------------
//		    列挙型
// -------------------------
enum PlayerAction
{
	e_Wait_Init,
	e_Wait,
	e_Move_Init,
	e_Move,
	e_Jump_Init,
	e_Jump,
	e_Fall_Init,
	e_Fall,
	e_Total	
} ;

// -------------------------
//			構造体
// -------------------------
typedef struct
{
	int aHandle ;
	float aTotal ;
	float playTime ;

} MODEL_ANIM ;

typedef struct
{
	int		handle ;
	VECTOR	pos ;
	VECTOR	scale ;
	BOOL	useFlg ;

	VECTOR	moveAndMove ;

} OBJ_INFO ;

typedef struct
{
	float up ;
	float down ;
	float left ;
	float right ;

} MOVE_4DIR ;

// -------------------------
//	  プロトタイプ宣言
// -------------------------
template<typename T>int OraclesIN_Modoki( const char*, T cNum, const char* ... ) ;
void HitCheck_And_Move( VECTOR vec, MOVE_4DIR *arg_movePower ) ;
u_char GetDir( ) ;
int GpUpdateKey() ;

// -------------------------
//	 ぐろ〜ばり〜
// -------------------------
// ==================================================================
// --- 位置関連
// ==================================================================
OBJ_INFO oneTime_Floor[ FLOOR_MAX ] ;							// --- Floor 
OBJ_INFO oneTime_SkyBox ;										// --- SkyBox 
VECTOR oneTimePos			= VGet( 0.0f, 0.0f, 0.0f ) ;		// --- 座標
VECTOR oneTimeRot			= VGet( 0.0f, 0.0f, 0.0f ) ;		// --- 回転

// ==================================================================
// --- モデル関連
// ==================================================================
int modelHandle ;			// --- パックマンのモデルハンドル
int cubeHandle ;			// --- 箱のモデルハンドル
int skyBox_BlueHandle ; 	// --- スカイボックスのモデルハンドル

// ==================================================================
// --- その他
// ==================================================================
int g_keyBuf[256] ;		// --- キー情報の格納変数
int g_playerMode ;		// --- プレイヤーのモード


// =========================================================================
//
//
//							メインループ
//
//
// =========================================================================
int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance,
						LPSTR lpCmdLine, int nCmdShow )
{
	ConsoleWindow cWin ;

	// --- 初期準備
	SetMainWindowText( TEXT( "Texture_Test" ) );
	ChangeWindowMode( TRUE ) ;
	SetGraphMode( 1200, 600, 32 ) ;

	if( DxLib_Init() == -1 )		// ＤＸライブラリ初期化処理
		return -1 ;					// エラーが起きたら直ちに終了

	SetDrawScreen( DX_SCREEN_BACK );
	SetUseLighting( TRUE ) ;
	ChangeLightTypeDir( VGet( 1.0f, -1.0f, 0.5f ) ) ;
	SetLightEnable( TRUE ) ;

	SetCameraPositionAndTargetAndUpVec( VGet( 0, 2000, -1800 ),
										VGet( 0, -200, -300 ),
										VGet( 0, 1.0f, 0 )
		) ;

	// ===================================================================================================
	// --- らいとかんれん
	SetLightAmbColor( GetColorF( 0.9f, 0.9f, 0.9f, 1.0f ) ) ;

	// ===================================================================================================
	// --- かげかんれん
	int ShadowMapHandle = MakeShadowMap( 1024, 1024 ) ;
	SetShadowMapLightDirection( ShadowMapHandle, VGet( -0.4f, -0.8f, 0.1f ) ) ;
	SetShadowMapDrawArea( ShadowMapHandle, VGet( -2000.0f, -2000.0f, -2000.0f ), VGet( 2000.0f, 1000.0f, 2000.0f ) ) ;

	// ===================================================================================================
	// --- もでるかんれん
	int attachIdx = 0 ;
	int modelRootFlm = 0 ;
	modelHandle = MV1LoadModel( "..\\Model\\PacMan\\PacMan.mv1" ) ;		// --- パックマンのモデルハンドル

	cubeHandle = MV1LoadModel( "..\\Model\\FBX 2013\\Cube.mv1" ) ;		// --- 箱のモデルハンドル

	for ( int i = 0 ; i < FLOOR_MAX ; i++ )
		oneTime_Floor[i].handle	= MV1DuplicateModel( cubeHandle ) ;		// --- 箱のモデルハンドル_Floor

	skyBox_BlueHandle		= MV1LoadModel( "..\\Model\\bluesky.mv1" ) ;	// --- スカイボックスのモデルハンドル
	oneTime_SkyBox.handle	= skyBox_BlueHandle ;

	// ===================================================================================================
	// --- ざひょうかんれん
	oneTime_Floor[0].pos	= VGet( 0.0f,		-100.0f,	0.0f ) ;		// --- 座標		- Floor 
	oneTime_Floor[0].scale = VGet( 3.0f, 1.0f, 3.0f ) ;					// --- サイズ	- Floor

	oneTime_Floor[1].pos	= VGet( -600.0f,	-1000.0f,	-600.0f ) ;		// --- 座標		- Floor 
	oneTime_Floor[2].pos	= VGet( 0.0f,		-1000.0f,	-600.0f ) ;		// --- 座標		- Floor 
	oneTime_Floor[3].pos	= VGet( 600.0f,		-1000.0f,	-600.0f ) ;		// --- 座標		- Floor 
	oneTime_Floor[4].pos	= VGet( -600.0f,	-1000.0f,	0.0f ) ;		// --- 座標		- Floor 
	oneTime_Floor[5].pos	= VGet( 600.0f,		-1000.0f,	0.0f ) ;		// --- 座標		- Floor 
	oneTime_Floor[6].pos	= VGet( -600.0f,	-1000.0f,	600.0f ) ;		// --- 座標		- Floor 
	oneTime_Floor[7].pos	= VGet( 0.0f,		-1000.0f,	600.0f ) ;		// --- 座標		- Floor 
	oneTime_Floor[8].pos	= VGet( 600.0f,		-1000.0f,	600.0f ) ;		// --- 座標		- Floor 
	
	for ( int i = 1 ; i < 9 ; i++ )
		oneTime_Floor[ i ].scale = VGet( 3.0f, 3.0f, 3.0f ) ;				// --- サイズ	- Floor
	
	oneTime_Floor[9].pos	= VGet( -1200.0f,	-800.0f,	-1200.0f ) ;	// --- 座標		- Floor 
	oneTime_Floor[10].pos	= VGet( -1200.0f,	-800.0f,	-600.0f ) ;		// --- 座標		- Floor 
	oneTime_Floor[11].pos	= VGet( -1200.0f,	-800.0f,	600.0f ) ;		// --- 座標		- Floor 
	oneTime_Floor[12].pos	= VGet( -1200.0f,	-800.0f,	1200.0f ) ;		// --- 座標		- Floor 
	oneTime_Floor[13].pos	= VGet( -600.0f,	-800.0f,	1200.0f ) ;		// --- 座標		- Floor 
	oneTime_Floor[14].pos	= VGet( -600.0f,	-800.0f,	-1200.0f ) ;	// --- 座標		- Floor 

	for ( int i = 9 ; i < FLOOR_MAX ; i++ )
		oneTime_Floor[ i ].scale = VGet( 3.0f, 4.0f, 3.0f ) ;				// --- サイズ	- Floor

	// --- 縦揺れブロック
	oneTime_Floor[15].pos	= VGet( 1200.0f,	-800.0f,	0.0f ) ;		// --- 座標		- Floor 
	oneTime_Floor[15].scale = VGet( 3.0f, 1.0f, 9.0f ) ;					// --- サイズ	- Floor

	// --- 横揺れブロック
	oneTime_Floor[16].pos	= VGet( 600.0f,	-800.0f,	-1200.0f ) ;		// --- 座標		- Floor 
	oneTime_Floor[16].scale = VGet( 3.0f, 3.0f, 3.0f ) ;					// --- サイズ	- Floor

	for ( int i = 0 ; i < FLOOR_MAX ; i++ )
		oneTime_Floor[ i ].moveAndMove = VGet( 0.0f, 0.0f, 0.0f ) ;			// --- サイズ	- Floor

	oneTime_SkyBox.pos		= VGet( 0.0f, -2000.0f, 0.0f );					// --- 座標		- SkyBox
	oneTime_SkyBox.scale	= VGet( 20.0f, 100.0f, 20.0f ) ;				// --- サイズ	- SkyBox

	oneTimePos				= VGet( 0.0f, 0.0f, 0.0f ) ;		// --- 座標		- Player
	oneTimeRot				= VGet( 0.0f, 0.0f, 0.0f ) ;		// --- 回転		- Player

	for ( int i = 0 ; i < FLOOR_MAX ; i++ )
	{
		MV1SetPosition( oneTime_Floor[i].handle, oneTime_Floor[i].pos ) ;
		MV1SetScale( oneTime_Floor[i].handle, oneTime_Floor[i].scale ) ;
	}

	// ------------------------------------------
	//	こりじょん
	// ------------------------------------------
	// ヒットチェックに使用するコリジョン情報のセットアップ
	for ( int i = 0 ; i < FLOOR_MAX ; i++ )
		MV1SetupCollInfo( oneTime_Floor[i].handle, -1 ) ;

	// ===================================================================================================
	// --- てくすちゃかんれん
	int grHandle_Body_Blue		= LoadGraph( "..\\Model\\PacMan\\PacMan.fbm\\pac_Body_Blue.png" ) ;
	int grHandle_SkyDom_Blue	= LoadGraph( "..\\Model\\skydormsun.bmp" ) ;
	int grHandle_Sea			= LoadGraph( "..\\Model\\sea.bmp" ) ;

	printf( "hanldle - %d\n", grHandle_SkyDom_Blue ) ;

	MV1SetTextureGraphHandle( skyBox_BlueHandle, 0, grHandle_SkyDom_Blue, FALSE ) ;

	// ===================================================================================================
	// --- あにめかんれん
	MODEL_ANIM walkM ;
	walkM.playTime = 0.0f ;
	walkM.aHandle = MV1LoadModel( "..\\Model\\PacMan\\PacMan_Walk.mv1" ) ;

	// --- 入らなかった場合、失敗判定
	if( walkM.aHandle == -1 )
		return FALSE ;

	// ---------------------------
	// --- 次にアニメを読み込む
	// ---------------------------
	// --- アタッチINDEXを取得( モデルとアニメーションを結び付ける為に使用する )
	attachIdx = MV1AttachAnim( modelHandle, 0, walkM.aHandle ) ;

	printf( "%d", attachIdx ) ;

	// --- 総時間を取得し、デタッチする( しておかないと死んでしまいます )
	walkM.aTotal = MV1GetAttachAnimTotalTime( modelHandle, attachIdx ) ;
	MV1DetachAnim( modelHandle, attachIdx ) ;

	// アニメーションして動いてもその場で動いてるような状態にセットする。
	MV1SetFrameUserLocalMatrix( walkM.aHandle, modelRootFlm, MGetIdent() ) ;

	printf( "テクスチャ : % d\n", MV1GetTextureNum( modelHandle ) ) ;

	// --- あにめせっと
	MV1DetachAnim( modelHandle, attachIdx ) ;
	attachIdx = MV1AttachAnim( modelHandle, 0, walkM.aHandle ) ;
	
	// ------------------------------------------
	//	オブジェクト関連の変数
	// ------------------------------------------
	int		upperMode = 1 ;		// --- 縦ブロック
	float	upperNum = 18.0f ;	// --- 縦ブロック

	int		siderMode = 1 ;		// --- 横ブロック
	float	siderNum = 0 ;		// --- 横ブロック

	// ------------------------------------------
	//	プレイヤー関連の変数
	// ------------------------------------------
	float	jumpPower = 0.0f ;
	BOOL	jumpFlg = FALSE ;

	MOVE_4DIR	movePower ;
	ZeroMemory( &movePower, sizeof( movePower ) ) ;

	float	oneTimeA = 1.0f ;
	float	col[3] = { 0.0f, 0.0f, 0.0f } ;
	BOOL	mode = FALSE ;

	int		moveDir = 0 ;
	float	moveLength = 10.0f ;

	g_playerMode = e_Wait ;

	if ( 1 == OraclesIN_Modoki( "d", 5, "ddddd", 1, 2, 3, 4, 6 ) )
		printf( "一致！\n" ) ;

	// ============================================================================================================================== while
	// ----------------------------------------------------------------------------------------------------------
	//
	//												while
	//
	// ----------------------------------------------------------------------------------------------------------
	while ( ProcessMessage() == 0 && CheckHitKey(KEY_INPUT_ESCAPE) == 0 && GpUpdateKey() == 0 )
	{
		ClearDrawScreen() ;		// --- 画面のクリア

		// ---------------------------------------------------------------------
		//							アニメーション処理
		// ---------------------------------------------------------------------
		walkM.playTime += 0.5f ;
		if ( walkM.playTime > walkM.aTotal )
			walkM.playTime = 0.0f ;

//		if ( KeyState[ KEY_INPUT_SPACE ] )
//			MV1SetTextureGraphHandle( modelHandle, 0, grHandle_Body_Blue, FALSE ) ;

		// ---------------------------------------------------------------------
		//						オブジェクトアクション
		// ---------------------------------------------------------------------
		// ---------------
		// --- 縦
		// ---------------
		if ( upperMode == 0 )
		{
			upperNum += 0.5f ;
			if ( upperNum >= 18.0f )
				upperMode = 1 ;
		}
		else if ( upperMode == 1 )
		{
			upperNum -= 0.5f ;
			if ( upperNum <= 0.0f )
				upperMode = 2 ;
		}
		else if ( upperMode == 2 )
		{
			upperNum -= 0.5f ;
			if ( upperNum <= -18.0f )
				upperMode = 3 ;
		}
		else
		{
			upperNum += 0.5f ;
			if ( upperNum >= 0.0f )
				upperMode = 0 ;
		}

		oneTime_Floor[15].pos.y += upperNum ;

		// ---------------
		// --- 横
		// ---------------
		if ( siderMode == 0 )
		{	
			oneTime_Floor[16].moveAndMove.x = 5.0f ;
			oneTime_Floor[16].pos.x += 5.0f ;

			if ( oneTime_Floor[16].pos.x >= 600 )
			{
				oneTime_Floor[16].pos.x = 600 ;
				siderMode = 1 ;
			}
		}
		else
		{
			oneTime_Floor[16].moveAndMove.x = -5.0f ;
			oneTime_Floor[16].pos.x -= 5.0f ;

			if ( oneTime_Floor[16].pos.x <= 0 )
			{
				oneTime_Floor[16].pos.x = 0 ;
				siderMode = 0 ;
			}
		}

		// ---------------------------------------------------------------------
		//							プレイヤーアクション
		// ---------------------------------------------------------------------
		VECTOR	moveVec = VGet( 0.0f, 0.0f, 0.0f ) ;
		float	playerMaxSp = 14.0f ;

		// --- Move 処理
		u_char oneTimeDir = GetDir() ;

		switch ( g_playerMode )
		{
			// =============== Wait Init
			case e_Wait_Init :
				jumpPower = 0.0f ;
				g_playerMode = e_Wait ;
				break ;

			// =============== Wait
			case e_Wait :		

				if ( (g_keyBuf[ KEY_INPUT_W ] >= 1) || (g_keyBuf[ KEY_INPUT_A ] >= 1) || (g_keyBuf[ KEY_INPUT_S ] >= 1) || (g_keyBuf[ KEY_INPUT_D ] >= 1) )
					g_playerMode = e_Move_Init ;

				// --- Jump 処理
				if ( g_keyBuf[ KEY_INPUT_SPACE ] >= 1 )
					g_playerMode = e_Jump_Init ;

				break ;

			// =============== Move Init
			case e_Move_Init :
				g_playerMode = e_Move ;
				break ;

			// =============== Move
			case e_Move :

				if ( g_keyBuf[ KEY_INPUT_W ] >= 1 )
				{
					if ( movePower.up <= playerMaxSp )
						movePower.up	+= 1.0f ;
					moveVec.z += movePower.up ;
				}
				else if ( g_keyBuf[ KEY_INPUT_S ] >= 1 )
				{
					if ( movePower.down <= playerMaxSp )
						movePower.down	+= 1.0f ;
					moveVec.z += -1 * movePower.down ;
				}
				if ( g_keyBuf[ KEY_INPUT_A ] >= 1 )
				{
					if ( movePower.left <= playerMaxSp )
						movePower.left	+= 1.0f ;
					moveVec.x += -1 * movePower.left ;
				}
				else if ( g_keyBuf[ KEY_INPUT_D ] >= 1 )
				{
					if ( movePower.right <= playerMaxSp )
						movePower.right	+= 1.0f ;
					moveVec.x += movePower.right ;
				}

				// --- Jump 処理
				if ( g_keyBuf[ KEY_INPUT_SPACE ] >= 1 )
					g_playerMode = e_Jump_Init ;

				break ;

			// =============== Jump Init
			case e_Jump_Init :
				jumpPower	 = 40.0f ;

				movePower.up	= 0.0f ;
				movePower.down	= 0.0f ;
				movePower.left	= 0.0f ;
				movePower.right	= 0.0f ;

				g_playerMode = e_Jump ;
				break ;

			// =============== Jump
			case e_Jump :

				if ( g_keyBuf[ KEY_INPUT_W ] >= 1 )
				{
					if ( movePower.up <= playerMaxSp )
						movePower.up	+= 1.0f ;
					moveVec.z += movePower.up ;
				}
				else if ( g_keyBuf[ KEY_INPUT_S ] >= 1 )
				{
					if ( movePower.down <= playerMaxSp )
						movePower.down	+= 1.0f ;
					moveVec.z += -1 * movePower.down ;
				}
				if ( g_keyBuf[ KEY_INPUT_A ] >= 1 )
				{
					if ( movePower.left <= playerMaxSp )
						movePower.left	+= 1.0f ;
					moveVec.x += -1 * movePower.left ;
				}
				else if ( g_keyBuf[ KEY_INPUT_D ] >= 1 )
				{
					if ( movePower.right <= playerMaxSp )
						movePower.right	+= 1.0f ;
					moveVec.x += movePower.right ;
				}

				moveVec.y += jumpPower ;	// --- 上昇 OR 降下処理
				jumpPower -= 2.0f ;

				if ( jumpPower <= 0.0f )
					g_playerMode = e_Fall_Init ;

				break ;

			// =============== Fall Init
			case e_Fall_Init :
				jumpPower	 = 0.0f ;

//				movePower.up	= 0.0f ;
//				movePower.down	= 0.0f ;
//				movePower.left	= 0.0f ;
//				movePower.right	= 0.0f ;

				g_playerMode = e_Fall ;
				break ;

			// =============== Fall
			case e_Fall :

				if ( g_keyBuf[ KEY_INPUT_W ] >= 1 )
				{
					if ( movePower.up <= playerMaxSp )
						movePower.up	+= 1.0f ;
					moveVec.z += movePower.up ;
				}
				else if ( g_keyBuf[ KEY_INPUT_S ] >= 1 )
				{
					if ( movePower.down <= playerMaxSp )
						movePower.down	+= 1.0f ;
					moveVec.z += -1 * movePower.down ;
				}
				if ( g_keyBuf[ KEY_INPUT_A ] >= 1 )
				{
					if ( movePower.left <= playerMaxSp )
						movePower.left	+= 1.0f ;
					moveVec.x += -1 * movePower.left ;
				}
				else if ( g_keyBuf[ KEY_INPUT_D ] >= 1 )
				{
					if ( movePower.right <= playerMaxSp )
						movePower.right	+= 1.0f ;
					moveVec.x += movePower.right ;
				}

				moveVec.y += jumpPower ;	// --- 上昇 OR 降下処理

				if ( jumpPower >= -40.0f )
					jumpPower -= 2.0f ;

				break ;
		}

		float stopSpeed = 0.25f ;

		if ( (movePower.up > 0) && (g_keyBuf[ KEY_INPUT_W ] == 0) )
		{
			movePower.up -= stopSpeed ;
			moveVec.z += movePower.up ;
			if ( movePower.up < 0.0f )
				movePower.up = 0.0f ;
		}

		if ( (movePower.down > 0) && (g_keyBuf[ KEY_INPUT_S ] == 0) )
		{
			movePower.down -= stopSpeed ;
			moveVec.z -= movePower.down ;
			if ( movePower.down < 0.0f )
				movePower.down = 0.0f ;
		}
		if ( (movePower.left > 0) && (g_keyBuf[ KEY_INPUT_A ] == 0) )
		{
			movePower.left -= stopSpeed ;
			moveVec.x -= movePower.left ;
			if ( movePower.left < 0.0f )
				movePower.left = 0.0f ;
		}
		if ( (movePower.right > 0) && (g_keyBuf[ KEY_INPUT_D ] == 0) )
		{
			movePower.right -= stopSpeed ;
			moveVec.x += movePower.right ;
			if ( movePower.right < 0.0f )
				movePower.right = 0.0f ;
		}

		// ==========================
		// --- 描画開始
		// ==========================
		DrawBox( 0, 0, 1200, 600, RGB( 0xa0, 0x8b, 0x33 ), TRUE ) ;

		ShadowMap_DrawSetup( ShadowMapHandle ) ;
		
		// --- プレイヤー表示
		MV1SetAttachAnimTime( modelHandle, attachIdx, walkM.playTime ) ;
		MV1SetScale( modelHandle, VGet( 1, 1, 1 ) ) ;
		MV1SetPosition( modelHandle, oneTimePos ) ;
		MV1SetRotationXYZ( modelHandle, VGet( oneTimeRot.x, oneTimeRot.y * 1.57f, oneTimeRot.z ) ) ;

		// --- 床設定
		for ( int i = 0 ; i < FLOOR_MAX ; i++ )
		{
			MV1SetPosition( oneTime_Floor[i].handle, oneTime_Floor[i].pos ) ;
			MV1SetScale( oneTime_Floor[i].handle, oneTime_Floor[i].scale ) ;
		}

		// --- スカイボックス設定
		MV1SetPosition( skyBox_BlueHandle, oneTime_SkyBox.pos ) ;
		MV1SetScale( skyBox_BlueHandle, oneTime_SkyBox.scale ) ;
		
		//-------------------------------------------------------
		//						シャドー
		//-------------------------------------------------------
		MV1DrawModel( modelHandle ) ;

		for ( int i = 0 ; i < FLOOR_MAX ; i++ )
			MV1DrawModel( oneTime_Floor[i].handle ) ;

		// シャドウマップへの描画を終了
		ShadowMap_DrawEnd() ;

		// 描画に使用するシャドウマップを設定
		SetUseShadowMap( 0, ShadowMapHandle ) ;

		//--------------------------------------------------------
		//						本描画
		//--------------------------------------------------------
		for ( int i = 0 ; i < FLOOR_MAX ; i++ )
			MV1DrawModel( oneTime_Floor[i].handle ) ;

		MV1DrawModel( skyBox_BlueHandle ) ;

		// ------------------------------------------------------
		//						 ここに設置
		// ------------------------------------------------------
		HitCheck_And_Move( moveVec, &movePower ) ;

		// --- 探索範囲球
//		DrawSphere3D( oneTimePos, 200.0f, 16, GetColor( 0xd2, 0x00, 0x00 ), GetColor( 0xff, 0x12, 0x32 ), FALSE ) ;

		MV1DrawModel( modelHandle ) ;

		// 描画に使用するシャドウマップの設定を解除
		SetUseShadowMap( 0, -1 ) ;

		// 更新
		MV1RefreshCollInfo( cubeHandle, -1 ) ;
		for ( int i = 0 ; i < FLOOR_MAX ; i++ )
			MV1RefreshCollInfo( oneTime_Floor[i].handle ) ;
	
		// --- てすと
		DrawFormatString( 1100, 20,  GetColor( 0xff, 0xff, 0xff ), "上 : %0.2f", movePower.up ) ;
		DrawFormatString( 1100, 50,  GetColor( 0xff, 0xff, 0xff ), "下 : %0.2f", movePower.down ) ;
		DrawFormatString( 1100, 80,  GetColor( 0xff, 0xff, 0xff ), "左 : %0.2f", movePower.left ) ;
		DrawFormatString( 1100, 110, GetColor( 0xff, 0xff, 0xff ), "右 : %0.2f", movePower.right ) ;

		DrawFormatString( 10, 20, GetColor( 0xff, 0xff, 0xff ), "MODE : %d", g_playerMode ) ;

		// ---------------------------------------------------------------------
		//							実移動処理
		// ---------------------------------------------------------------------

		ScreenFlip() ;			// --- 裏画面を表に出力する
	}
	
	DeleteShadowMap( ShadowMapHandle ) ;

	MV1DeleteModel( modelHandle ) ;						// ハンドル削除
	MV1DeleteModel( cubeHandle ) ;						// ハンドル削除

	for ( int i = 0 ; i < FLOOR_MAX ; i++ )
		MV1DeleteModel( oneTime_Floor[i].handle )  ;	// ハンドル削除

	MV1DeleteModel( skyBox_BlueHandle ) ;				// ハンドル削除

	DxLib_End() ;						// ＤＸライブラリ使用の終了処理

	return 0 ;							// ソフトの終了 
}

// ================================================================================================================================= HitCheck
// -----------------------------------------------------------------------------------------
//
//								ヒットチェック & 移動
//
// -----------------------------------------------------------------------------------------
void HitCheck_And_Move( VECTOR moveVec, MOVE_4DIR *arg_movePower )
{
	float size = 10.0f ;

	// --- 足元にポリゴンが存在するか確認する
	// ======================
	//		変数宣言地帯
	// ======================
	int i, j ;
	int footNum = 0 ;
	int HitObjNumber[ FLOOR_MAX ] ;
	int	FootObjNumber[ PLAYER_MAX_HITCOLL ] ;					// --- 足元チェックを行うオブジェクトの番号を格納( 周辺取得時に使用 )
	int	hitNum = 0 ;
	MV1_COLL_RESULT_POLY_DIM	HitDim[ FLOOR_MAX ] ;			// --- プレイヤーの周囲に存在するポリゴンを検出した際の結果を格納する
	MV1_COLL_RESULT_POLY		*Kabe[ PLAYER_MAX_HITCOLL ] ;	// --- 壁
	MV1_COLL_RESULT_POLY		*Yuka[ PLAYER_MAX_HITCOLL ] ;	// --- 床
	MV1_COLL_RESULT_POLY		*Poly ;							// --- ポリゴンの構造体にアクセスするために使用するポインタ
	int	KabeNum = 0 ;											// --- 察知した壁の数
	int	YukaNum = 0 ;											// --- 察知した床の数
	VECTOR	oldPos = oneTimePos ;								// --- 現在位置
	VECTOR	nowPos = VAdd( oneTimePos, moveVec ) ;				// --- 移動後の位置
	BOOL	moveFlg ;											// --- 移動フラグ

	// ======================================================================================
	// ==================
	//		周辺探索
	// ==================
	// プレイヤーの周囲にあるステージポリゴンを取得する
	// ( 検出する範囲は移動距離も考慮する )
	for ( i = 0 ; i < FLOOR_MAX ; i++ )
	{
		HitDim[ hitNum ]		= MV1CollCheck_Sphere( oneTime_Floor[i].handle, -1, oldPos, 400.0f ) ;

		// --- 当たったら次の床ポリチェックにかける
		if( HitDim[ hitNum ].HitNum != 0 )
		{
			// --- 現在踏んでいる足場のオブジェナンバーを取得
			HitObjNumber[ footNum ] = i ;
			footNum++ ;

			hitNum++;
		}
		else
			MV1CollResultPolyDimTerminate( HitDim[ hitNum ] );
	}

	// --- 動いたか動いていないかをチェック
	if ( fabs( moveVec.x ) >= 0.01f || fabs( moveVec.z ) >= 0.01f )
		moveFlg = TRUE ;
	else
		moveFlg = FALSE ;

	// 検出されたポリゴンが壁ポリゴン( ＸＺ平面に垂直なポリゴン )か床ポリゴン( ＸＺ平面に垂直ではないポリゴン )かを判断する
	for ( i = 0 ; i < hitNum ; i++ )
	{
		// 検出されたポリゴンの数だけ繰り返し
		for( j = 0 ; j < HitDim[ i ].HitNum ; j ++ )
		{
			// ＸＺ平面に垂直かどうかはポリゴンの法線のＹ成分が０に限りなく近いかどうかで判断する
			if( HitDim[ i ].Dim[ j ].Normal.y < 0.000001f && HitDim[ i ].Dim[ j ].Normal.y > -0.000001f )
			{
				// 壁ポリゴンと判断された場合でも、プレイヤーのＹ座標＋1.0ｆより高いポリゴンのみ当たり判定を行う
				if( HitDim[ i ].Dim[ j ].Position[ 0 ].y > oldPos.y + 1.0f ||
					HitDim[ i ].Dim[ j ].Position[ 1 ].y > oldPos.y + 1.0f ||
					HitDim[ i ].Dim[ j ].Position[ 2 ].y > oldPos.y + 1.0f )
				{
					// ポリゴンの数が列挙できる限界数に達していなかったらポリゴンを配列に追加
					if( KabeNum < PLAYER_MAX_HITCOLL )
					{
						// ポリゴンの構造体のアドレスを壁ポリゴンポインタ配列に保存する
						Kabe[ KabeNum ] = &HitDim[ i ].Dim[ j ] ;

						// 壁ポリゴンの数を加算する
						KabeNum ++ ;
					}
				}
			}
			else
			{
				// ポリゴンの数が列挙できる限界数に達していなかったらポリゴンを配列に追加
				if( YukaNum < PLAYER_MAX_HITCOLL )
				{
					FootObjNumber[ YukaNum ] = HitObjNumber[ i ] ;

					// ポリゴンの構造体のアドレスを床ポリゴンポインタ配列に保存する
					Yuka[ YukaNum ] = &HitDim[ i ].Dim[ j ] ;

					// 床ポリゴンの数を加算する
					YukaNum++ ;
				}
			}
		}
	}

	// ====================================================================================== FLOOR
	// =======================================
	//		床ポリゴンとのヒットチェック
	// =======================================
	BOOL	hitFootFlg = FALSE ;

	if( YukaNum != 0 )
	{
		BOOL	footMoveFlg = FALSE ;		// --- 動いている足場に乗った時のフラグ
		float	MaxY_Movement = -100000 ;	// --- 最上層位置_動いている足場
		float	MaxY = 0 ;					// --- 最上層位置

		for( i = 0 ; i < YukaNum ; i ++ )
		{
			float sizeP		= 50.0f ;		// --- プレイヤーのヒットチェック部(中心からの距離分)
			float checkDis	= 50.0f ;		// --- 足元チェックの最大距離

			Poly = Yuka[ i ] ;

			// =======================================
			//		落ちるかどうかの判断を行う
			// =======================================
			// --- ヒット情報をチェック
			HITRESULT_LINE oneHR_LL = HitCheck_Line_Triangle( VGet( nowPos.x - sizeP, nowPos.y + 59, nowPos.z - sizeP ),
											VGet( nowPos.x - sizeP, nowPos.y - checkDis, nowPos.z - sizeP ), Poly->Position[0], Poly->Position[1], Poly->Position[2] ) ;
			HITRESULT_LINE oneHR_LR = HitCheck_Line_Triangle( VGet( nowPos.x - sizeP, nowPos.y + 59, nowPos.z + sizeP ),
											VGet( nowPos.x - sizeP, nowPos.y - checkDis, nowPos.z + sizeP ), Poly->Position[0], Poly->Position[1], Poly->Position[2] ) ;
			HITRESULT_LINE oneHR_RL = HitCheck_Line_Triangle( VGet( nowPos.x + sizeP, nowPos.y + 59, nowPos.z - sizeP ),
											VGet( nowPos.x + sizeP, nowPos.y - checkDis, nowPos.z - sizeP ), Poly->Position[0], Poly->Position[1], Poly->Position[2] ) ;
			HITRESULT_LINE oneHR_RR = HitCheck_Line_Triangle( VGet( nowPos.x + sizeP, nowPos.y + 59, nowPos.z + sizeP ),
											VGet( nowPos.x + sizeP, nowPos.y - checkDis, nowPos.z + sizeP ), Poly->Position[0], Poly->Position[1], Poly->Position[2] ) ;

			// --- 当たらなかったら次のループへ
			if ( (!oneHR_LL.HitFlag) && (!oneHR_LR.HitFlag) && (!oneHR_RL.HitFlag) && (!oneHR_RR.HitFlag) )
				continue ;

			// --- 最も高い点を見つける
			float nowMax = -100000 ;

			if ( (nowMax < oneHR_LL.Position.y) && (oneHR_LL.HitFlag) )
				nowMax = oneHR_LL.Position.y ;
			if ( (nowMax < oneHR_LR.Position.y) && (oneHR_LR.HitFlag) )
				nowMax = oneHR_LR.Position.y ;
			if ( (nowMax < oneHR_RL.Position.y) && (oneHR_RL.HitFlag) )
				nowMax = oneHR_RL.Position.y ;		
			if ( (nowMax < oneHR_RR.Position.y) && (oneHR_RR.HitFlag) )
				nowMax = oneHR_RR.Position.y ;
			
			// 既に当たったポリゴンがあり、且つ今まで検出した床ポリゴンより低い場合は何もしない
			if( hitFootFlg == TRUE && MaxY > nowMax )
				continue ;

			// ポリゴンに当たったフラグを立てる
			hitFootFlg = TRUE ;

			// 接触したＹ座標を保存する
			MaxY = nowMax ;

			// =======================================
			//		床オブジェとの併合移動処理
			// =======================================
			if ( footMoveFlg )
				continue ;

			// --- プレイヤーの中心しか判定がないように設計
			HITRESULT_LINE oneHR = HitCheck_Line_Triangle( VGet( nowPos.x, nowPos.y + 59, nowPos.z ),
											VGet( nowPos.x, nowPos.y - 50, nowPos.z ), Poly->Position[0], Poly->Position[1], Poly->Position[2] ) ;

			// --- 当たらなかったら AND 最上層位置より低い場合スルー
			if ( (!oneHR.HitFlag) && (MaxY_Movement > oneHR.Position.y) )
				continue ;

			footMoveFlg = TRUE ;
			MaxY_Movement = oneHR.Position.y ;

			// --- 足場の移動に合わせて移動
			nowPos.x += oneTime_Floor[ FootObjNumber[ i ] ].moveAndMove.x ;
			nowPos.z += oneTime_Floor[ FootObjNumber[ i ] ].moveAndMove.z ;
		}

		// ------------------
		// --- 着地処理
		// ------------------
		if ( hitFootFlg )
		{
			// --- 新たに座標をセット
			if ( g_playerMode == e_Wait || g_playerMode == e_Move )
				nowPos.y = MaxY ;
			if ( g_playerMode == e_Fall )
			{
				nowPos.y = MaxY ;
				g_playerMode = e_Wait_Init ;		
			}
		}

		// ------------------
		// --- 落ち処理
		// ------------------
		if ( !hitFootFlg )
		{
			if ( g_playerMode < e_Jump_Init )
				g_playerMode = e_Fall_Init ;
		}
	}

	// ====================================================================================== WALL
	// =======================================
	//		壁ポリゴンとのヒットチェック
	//			(MoveVec.X のみ変更)
	// =======================================
	if ( KabeNum != 0 )
	{
		for ( i = 0 ; i < KabeNum ; i++ )
		{
			Poly = Kabe[i] ;

			float pW = 60.0f ;
			float pH = 50.0f ;

			// --- 見ているベクトルの線分と壁をヒットチェック
			if ( !HitCheck_Capsule_Triangle( VGet( nowPos.x, nowPos.y + 60.0f, nowPos.z ), 
															VGet( nowPos.x, nowPos.y + 60.0f + pH, nowPos.z ), pW,
															Poly->Position[0], Poly->Position[1], Poly->Position[2] ) )
				continue ;
			
			// --- 動きを止める
			nowPos.x = oldPos.x ;
			nowPos.z = oldPos.z ;

			ZeroMemory( arg_movePower, sizeof( MOVE_4DIR ) ) ;
		}
	}

	// --- 移動
	oneTimePos = nowPos ;

	// --- さようなら
	for ( int i = 0 ; i < hitNum ; i++ )
		MV1CollResultPolyDimTerminate( HitDim[ i ] ) ;
}

// ---------------------------------------------------------------
//
//				オラクルの「IN()」みたいなやつ
//
// ---------------------------------------------------------------
template <typename T>
int OraclesIN_Modoki( const char* oneF, T a, const char* format ... )
{
	va_list args ;							// --- 可変リスト
	int		varFom[2] = { 0, 0 } ;			// --- 引数の型(【1でint】,【2でfloat】)		[0] - 第二引数, [1] - 第四以降の比較する引数
	int		intNum[2] = { 0, 0 } ;			// --- 比較数値_Int								[0] - 第二引数, [1] - 第四以降の比較する引数
	float	floatNum[2] = { 0.0f, 0.0f } ;	// --- 比較数値_Float							[0] - 第二引数, [1] - 第四以降の比較する引数

	// --------------
	//		開始
	// --------------
	va_start( args, oneF ) ;

	const char *p = oneF ;
	if ( *p == 'd' )			// --- 第二引数がint型なら
	{
		intNum[0] = (int)a ;
		varFom[0] = 1 ;
	}
	else if ( *p == 'f' )		// --- 第二引数がfloat型なら
	{
		floatNum[0] = (float)a ;
		varFom[0] = 2 ;
	}
	else
		return -1 ;

	va_end( args ) ;

	// ----------------------------------
	//	第一引数と全ての引数を比較する
	// ----------------------------------
	va_start( args, format ) ;
	p = format ;

	while ( *p != '\0' )
	{
		// --- 比較する引数の型を判別
		if ( *p == 'd' )
			varFom[1] = 1 ;
		else if ( *p == 'f' )
			varFom[1] = 2 ;
		else
			return -1 ;

		// ---------------------------------
		// --- 第一引数がint型だった時の処理
		// ---------------------------------
		if ( varFom[0] == 1 )
		{
			// --- 比較する引数がint型なら
			if ( varFom[1] == 1 )
			{
				intNum[1] = va_arg( args, int ) ;

				if ( intNum[0] == intNum[1] )
					return 1 ;
			}

			// --- 比較する引数がfloat型なら
			if ( varFom[1] == 2 )
			{
				floatNum[1] = va_arg( args, float ) ;

				if ( (float)intNum[0] == floatNum[1] )
					return 1 ;
			}

		}

		// ---------------------------------
		// --- 第一引数がfloat型だった時の処理
		// ---------------------------------
		else if ( varFom[0] == 2 )
		{
			// --- 比較する引数がint型なら
			if ( varFom[1] == 1 )
			{
				intNum[1] = va_arg( args, int ) ;

				if ( floatNum[0] == (float)intNum[1] )
					return 1 ;
			}

			// --- 比較する引数がfloat型なら
			if ( varFom[1] == 2 )
			{
				floatNum[1] = va_arg( args, float ) ;

				if ( floatNum[0] == floatNum[1] )
					return 1 ;
			}
		}

		p++ ;
	}

	// --------------
	//		終了
	// --------------
	va_end( args ) ;

	return 0 ;
}

// ================================================================================================================================= GetDir
// -----------------------------------------------------------------------------------------
//
//									向き取得
//
// -----------------------------------------------------------------------------------------
u_char GetDir()
{
	u_char dir = 0x00 ;		// --- 帰り値用の向き

	if ( g_keyBuf[ KEY_INPUT_W ] >= 1 )
		dir |= DIR_UP ;

	if ( g_keyBuf[ KEY_INPUT_S ] >= 1 )
		dir |= DIR_DOWN ;
	
	if ( g_keyBuf[ KEY_INPUT_A ] >= 1 )
		dir |= DIR_LEFT ;

	if ( g_keyBuf[ KEY_INPUT_D ] >= 1 )
		dir |= DIR_RIGHT ;

	// --- 向きを変える
	if ( dir == DIR_UP )
		oneTimeRot.y = 2.0f ;
	if ( dir == DIR_DOWN )
		oneTimeRot.y = 0.0f ;
	if ( dir == DIR_LEFT )
		oneTimeRot.y = 1.0f ;
	if ( dir == DIR_RIGHT )
		oneTimeRot.y = 3.0f ;

	if ( dir == DIR_UPLEFT )
		oneTimeRot.y = 1.5f ;
	if ( dir == DIR_UPRIGHT )
		oneTimeRot.y = 2.5f ;
	if ( dir == DIR_DOWNLEFT )
		oneTimeRot.y = 0.5f ;
	if ( dir == DIR_DOWNRIGHT )
		oneTimeRot.y = 3.5f ;

	return dir ;
}

// ================================================================================================================================= Gp Update Key
// -----------------------------------------------------------------------------------------
//
//								キー情報更新関数
//
// -----------------------------------------------------------------------------------------
int GpUpdateKey()
{
        char tmpKey[256];				// --- 現在のキーの入力状態を格納する
        GetHitKeyStateAll( tmpKey );	// --- 全てのキーの入力状態を得る

		for( int i = 0 ; i < 256 ; i++ )
		{ 
			// i番のキーコードに対応するキーが押されていたら
			if( tmpKey[i] != 0 )
				g_keyBuf[i]++ ;		// --- 加算処理を行う

			// 押されていなければ
			else
				g_keyBuf[i] = 0;	// --- 0にする
        }
        return 0;
}







