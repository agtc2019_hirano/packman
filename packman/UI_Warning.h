/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: UI_Warning.h
	CREATE	: HIRANO
	
	------ Explanation of file ---------------------------------------------------------------------
       
	UI_Warningクラスの定義部
	
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

class UI_Warning : public UserInterface
{
	public :
		UI_Warning( ) ;			// --- コンストラクタ
		~UI_Warning( ) ;		// --- デストラクタ

		void UIDraw( int ){} ;		// 純粋仮想関数
		void WarInit( ) ;			// 初期セット
		void WarDraw( ) ;			// 表示
		float UI_pos_1 ;			// UIのポジション格納
		float UI_pos_2 ;			// UIのポジション格納
		float UI_pos_3 ;			// UIのポジション格納
		float UI_pos_4 ;			// UIのポジション格納
		float UI_movepos ;

		int ui_alpha ;				// 透明度
		int ui_alphabar ;			// 全体の透明度
		int ui_add ;				// 透明度加算値
		int ui_addbar ;				// 透明度加算値
} ;
