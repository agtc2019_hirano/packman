/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: WinMain.cpp
	
	------ Explanation of file ---------------------------------------------------------------------
       
	WinMain

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
#include "Common.h"		// 共通ヘッダー
#include <time.h>

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	// ----------------------------------
	//		DxLib 初期セット
	// ----------------------------------
	SetOutApplicationLogValidFlag( FALSE ) ;			// --- LOG.txtへの書き込みをしない

	SetMainWindowText( TEXT( "ANIMAL VACATION" ) );
//	ChangeWindowMode( TRUE ) ;							// --- 通常モード
	ChangeWindowMode( FALSE ) ;							// --- フルスクリーンモード

	SetGraphMode( WINDOWSIZE_W, WINDOWSIZE_H, 32 ) ;
//	SetGraphMode( 1280, 720, 32 ) ;						// --- 決めたウィンドウサイズは「g_winSize」に格納される。

	if ( DxLib_Init() == -1 )
		return 0 ;

	SetWindowIconID( 333 ) ;			// --- アイコン読み込み

	SetDrawScreen( DX_SCREEN_BACK );	// --- 裏描画開始

	SetUseZBuffer3D( TRUE ) ;			// --- Ｚバッファを有効にする
	SetWriteZBuffer3D( TRUE ) ;			// --- Ｚバッファへの書き込みを有効にする

	// ----------------------------------
	//		影、光関連の初期セット
	// ----------------------------------
	SetLightDifColor( GetColorF( 1.0f, 1.0f, 1.0f, 0.2f ) ) ;		// --- 標準ライトのディフューズをセット(影色)
	SetLightAmbColor( GetColorF( 1.0f, 1.0f, 1.0f, 1.0f ) ) ;		// --- 標準ライトの色をセット
	SetLightDirection( VGet( 0.0f, -1.0f, 1.0f ) ) ;				// --- 光の方向を決定

	g_shadowMap.handle	= MakeShadowMap( 1024, 1024 ) ;									// --- 影のハンドルを生成
	g_shadowMap.isUse	= TRUE ;														// --- 影の使用フラグをTRUEにする
	SetShadowMapLightDirection( g_shadowMap.handle, VGet( 0.0f, -1.0f, 0.0f ) ) ;		// --- 影の向きをセット
	SetShadowMapDrawArea( g_shadowMap.handle, VGet( -2000.0f, -1000.0f, -2000.0f ),		// --- 影の描画範囲を決定	(第一引数 : MIN)
											VGet( 2000.0f, 1000.0f, 2000.0f ) ) ;		// ---						(第二引数 : MAX)

	// ----------------------------------
	//		変数などの初期セット
	// ----------------------------------
	srand( (unsigned)time(NULL) ) ;		// --- シード値をかき乱すぜぇ

	InitAllStates() ;
	SetCameraPositionAndTargetAndUpVec( VGet( 0, 400, -500 ), VGet( 0, 100, 0 ), VGet( 0, 1.0f, 0 ) ) ;

//	StartFPS() ;					// --- FPS描画のセットアップ

	// ----------------------------------
	//		コリジョンのセットアップ( 一時的にここに置かさせてもらいます	 )
	// ----------------------------------
	for ( int i = 0 ; i < ELE_X_1 * ELE_Y_1 ; i++ )
		MV1SetupCollInfo( Scaf1[ i ].col.handle ) ;		// --- 足場オブジェクト(Stage1)の衝突セットアップ
	for ( int i = 0 ; i < ELE_X_2 * ELE_Y_2 ; i++ )
		MV1SetupCollInfo( Scaf2_t[ i ].col.handle ) ;	// --- 足場オブジェクト(Stage2)の衝突セットアップ
	for ( int i = 0 ; i < ELE_X_2 * ELE_Y_2 ; i++ )
		MV1SetupCollInfo( Scaf2_u[ i ].col.handle ) ;	// --- 足場オブジェクト(Stage2)の衝突セットアップ
	for ( int i = 0 ; i < ELE_X_3 * ELE_Y_3 ; i++ )
		MV1SetupCollInfo( Scaf3[ i ].col.handle ) ;		// --- 足場オブジェクト(Stage3)の衝突セットアップ

	for ( int i = 0 ; i < 2 ; i++ )
		MV1SetupCollInfo(  g_Player[i].modelHandle ) ;	// --- パックマンの衝突セットアップ

	// ============================================================================================
	//
	//										メインループ
	//
	// ============================================================================================
	while (	ProcessMessage() == 0 && CheckHitKey(KEY_INPUT_ESCAPE) == 0 && GpUpdateKey() == 0 )
	{
		ClearDrawScreen() ;		// --- 画面のクリア
		DrawBox( 0, 0, 1920, 1080, RGB( 0xff, 0xd2, 0x55 ), TRUE ) ;
		DrawModelEasy( &g_Object[ OBJID_SKYDOM ] ) ;						// --- スカイドーム

		// ---------------------------------------------
		//				全体のアクション
		// ---------------------------------------------
		int changeModeNum = GameLoop() ;

		// ---------------------------------------------
		//				カメラのアクション
		// ---------------------------------------------
		g_camera.cAction() ;

		// ---------------------------------------------
		//			  オブジェクト(影あり)の描画
		// ---------------------------------------------
		if ( g_shadowMap.isUse )
		{
			ShadowMap_DrawSetup( g_shadowMap.handle ) ;		// --- 影描画のスタートアップ

			DrawLoop( FALSE ) ;								// --- 影用に描画を行う

			ShadowMap_DrawEnd() ;							// --- シャドウマップへの描画を終了
			SetUseShadowMap( 0, g_shadowMap.handle ) ;		// --- 描画に使用するシャドウマップを設定

			DrawLoop( TRUE ) ;								// --- 普通にオブジェクトの描画を行う
			SetUseShadowMap( 0, -1 ) ;						// --- 描画に使用するシャドウマップの設定を解除
		}

		// ---------------------------------------------
		//			 オブジェクト(影無し)の描画
		// ---------------------------------------------
		else
		{
			DrawLoop( TRUE ) ;
		}

		// ---------------------------------------------
		//					その他
		// ---------------------------------------------
		// --- 帰り値に応じてgameMode値を変更する( -1の時はスルー )
		if ( changeModeNum != -1 )
			g_gameMode = changeModeNum ;

//		DebugLoop( TRUE ) ;		// --- デバッグ用。"F1"を押すことでデバッグメニューを起動できる。( Function.cpp内にて )

		// --- 現在のFPSを描画
//		DrawFPS( 10, (int)g_winSize.y - 20 ) ;

		// ---------------------------------------------
		//			 全ての処理を初期化する処理
		// ---------------------------------------------
		if ( g_keyBuf[ KEY_INPUT_F5 ] == 1 )
			InitAllField() ;

		ScreenFlip() ;			// --- 裏画面を表に出力する
	}

	// --- 全ての画像ハンドルを消去
	for ( int i = 0 ; i < e_Image_Total ; i++ )
		DeleteGraph( g_graphHandle[ i ].handle ) ;

	DxLib_End() ;

	return 0 ;
}

