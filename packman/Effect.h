/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: Effect.h
	CREATE	: NOGUCHI

	------ Explanation of file ---------------------------------------------------------------------
       
	エフェクトクラスの定義部。

	このオブジェクトを実際にゲーム内で呼び起こすには「EffectGenerator()」を使う必要がある。
	座標、画像の定数を指定してあげよう。自動的にゼロサーチも行ってくれるぞ。
	
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

enum e_EffectID
{
	e_Effect_Star = 0,				// --- エフェクトID_お星さま
	e_Effect_Push_Star,				// --- エフェクトID_プッシュ時の星エフェクト
	e_Effect_Super_Shine ,			// --- エフェクトID_スーパー時の煌き
	e_Effect_Drop_Coconuts,			// --- エフェクトID_落ちた際に飛び散るココナッツエフェクト
	e_Effect_Result_Coconuts,		// --- エフェクトID_リザルトで飛び出すココナッツエフェクト
	e_Effect_Total					// --- エフェクトID_総計
} ;

// ---------------------------------------
//	生成時にセットされるエフェクトの情報
// ---------------------------------------
typedef struct
{
	float	size ;		// --- サイズ

	VECTOR	moveVec ;		// --- 移動方向
	float	aliveTime ;		// --- 画面に描画される時間

	VECTOR	rotate ;		// --- 回転
	BOOL	dispFlg ;		// --- 表示フラグ
	BOOL	aliveFlg ;		// --- 画面に存在しているかどうかのフラグ

	// +で「エフェクトのID」と「イメージ定数」
	// これらはprotected にしたいのでここには含めない

} EFFECT_INFO ;

// ---------------------------------------------------------
//
//						Effect クラス
//
// ---------------------------------------------------------
class Effect
{
	// ---------
	//	public
	// ---------
	public :

		// --- 以下メソッド
		Effect( ) ;							// --- コンストラクタ
		~Effect( ) ;						// --- デストラクタ

		int	initStates() ;					// --- 初期化関数
		int setHandle( int arg_handle ) ;	// --- ハンドルをセットする関数
		int getHandle()						// --- ハンドルを帰す関数
				{ return handle ; }	
		int setEffectID( int arg_ID ) ;		// --- エフェクトIDをセットする関数
		int getEffectID()					// --- エフェクトIDを帰す関数
				{ return effectID ; }

		virtual int eAction() ;				// --- アクション関数
		virtual int eDraw() ;				// --- 描画のみの記述部。

		int	destroy() ;						// --- 消滅関数

		// --- 以下フィールド
		VECTOR		pos ;					// --- 座標
		c_fPoint	ltSpot ;				// --- 表示する画像の左上座標( Left Top Spot )
		int			mode ;					// --- モード

		float		nowTime ;				// --- 生成されてからの経過時間
		EFFECT_INFO	efInfo ;				// --- エフェクトの各種情報

		int		freeVar_i[16] ;				// --- 自由に使っていい配列( int )
		float	freeVar_f[16] ;				// --- 自由に使っていい配列( float )

		BOOL	isUse2D ;					// --- リザルト時のココナッツに使用
		BOOL	isUse3DModel ;				// --- 3Dモデルであるかどうかのフラグ
		int		use3DModelID ;				// --- 使用するオブジェクトのID

	// ------------
	//	protected
	// ------------
	protected :

		// --- 以下フィールド
		int		handle ;					// --- 画像ハンドル
		int		effectID ;					// --- エフェクトID
} ;



