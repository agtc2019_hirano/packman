/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: Common.h
	
	------ Explanation of file ---------------------------------------------------------------------
       
	全てのファイルで共通のヘッダーファイル

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
#include <DxLib.h>

/* --------------------------------------------------------------------------------------


       列挙型宣言


 --------------------------------------------------------------------------------------- */
enum e_ModelNames
{
	e_Model_Animal_Fox_1P,				// --- モデルハンドル_動物さん_キツネ_1P
	e_Model_Animal_Cat_1P,				// --- モデルハンドル_動物さん_ネコ_1P
	e_Model_Animal_Pig_1P,				// --- モデルハンドル_動物さん_ブタ_1P
	e_Model_Animal_Fox_2P,				// --- モデルハンドル_動物さん_キツネ_2P
	e_Model_Animal_Cat_2P,				// --- モデルハンドル_動物さん_ネコ_2P
	e_Model_Animal_Pig_2P,				// --- モデルハンドル_動物さん_ブタ_2P
	e_Model_Renga,						// --- モデルハンドル_レンガ床
	e_Model_WoodBox,					// --- モデルハンドル_木箱床
	e_Model_Ice,						// --- モデルハンドル_氷床
	e_Model_Col_Scaffold,				// --- モデルハンドル_足場コリジョン
	e_Model_SkyDom,						// --- モデルハンドル_スカイドーム(朝)
	e_Model_Sunset,						// --- モデルハンドル_スカイドーム(夕)
	e_Model_Hunnsui,					// --- モデルハンドル_噴水
	e_Model_Sea,						// --- モデルハンドル_海
	e_Model_Dot,						// --- モデルハンドル_ドット
	e_Model_SuperPacMan	,				// --- モデルハンドル_スーパーパックマン
	e_Model_Dodai,						// --- モデルハンドル_土台( リザルト用 )
	e_Model_BeachChair,					// --- モデルハンドル_ビーチチェア
	e_Model_Table,						// --- モデルハンドル_テーブル
	e_Model_Juice,						// --- モデルハンドル_ココナッツジュース
	e_Model_YashiWood,					// --- モデルハンドル_ヤシの木（ココナッツ付き）
	e_Model_Beach,						// --- モデルハンドル_ビーチ
	e_Model_Nami,						// --- モデルハンドル_波
	e_Model_BSea,						// --- モデルハンドル_ビーチの海
	e_Model_Cloud,						// --- モデルハンドル_雲
	e_Model_Ukiwa,						// --- モデルハンドル_浮き輪
	e_Model_Island,						// --- モデルハンドル_孤島
	e_Model_Total						// --- モデルハンドル_総計
} ;

enum e_Image
{
	e_Image_Skysun,						// --- 画像番号_スカイドーム（朝）
	e_Image_Sunset,						// --- 画像番号_スカイドーム（夕）
	e_Image_Hoshizora,					// --- 画像番号_スカイドーム（夜）
	e_Image_Beach,						// --- 画像番号_ビーチ
	e_Image_Water,						// --- 画像番号_水滴
	e_Image_Time1,						// --- 画像番号_タイマーで使う画像
	e_Image_Time1_FlashBlend,			// --- 画像番号_タイマーで使う画像_発光用ブレンド画像
	e_Image_Time1_RedBlend,				// --- 画像番号_タイマーで使う画像_発光用ブレンド画像_赤
	e_Image_Score,						// --- 画像番号_スコアで使う画像
	e_Image_PlayerID,					// --- 画像番号_プレイヤーのID表示で使う画像
	e_Image_PlayerID_Chase,				// --- 画像番号_プレイヤーのID表示で使う画像_追跡表示用
	e_Image_Line,						// --- 画像番号_１P・２Pのスコアのラインで使う画像
	e_Image_Effect_Star,				// --- 画像番号_テストお星さま
	e_Image_Effect_Shine,				// --- 画像番号_キラキラ(ダイヤ)
	e_Image_Effect_Coconut,				// --- 画像番号_ココナッツ
	e_Image_Effect_Yasinoki,			// --- 画像番号_ヤシの木
	e_Image_Title,						// --- 画像番号_タイトル
	e_Image_PushtoEnter,				// --- 画像番号_PushToEnter
	e_Image_cFrame_OK,					// --- 画像番号_キャラ選択_OKフレーム
	e_Image_cFrame_Str,					// --- 画像番号_キャラ選択_OKフレーム_文字「準備OK!!」「選び直す」
	e_Image_cFrame_Select,				// --- 画像番号_キャラ選択_OKフレーム_選択状況
	e_Image_cIcon_Fox,					// --- 画像番号_キャラアイコン_Cat
	e_Image_cIcon_Cat,					// --- 画像番号_キャラアイコン_Fox
	e_Image_cIcon_Pig,					// --- 画像番号_キャラアイコン_Pig
	e_Image_cIcon_FlashBlend,			// --- 画像番号_キャラアイコン_発光用ブレンド画像
	e_Image_cIcon_Select1P,				// --- 画像番号_キャラアイコンの選択状況可視化UI_1P
	e_Image_cIcon_Select2P,				// --- 画像番号_キャラアイコンの選択状況可視化UI_2P
	e_Image_VS_Icon,					// --- 画像番号_VSのやつ
	e_Image_DotGauge,					// --- 画像番号_ドットゲージ
	e_Image_DotGauge_Max,				// --- 画像番号_ドットゲージ_Max
	e_Image_DotGauge_Waku,				// --- 画像番号_ドットゲージ_枠
	e_Image_DotGauge_Damage,			// --- 画像番号_ドットゲージ_ダメージ分
	e_Image_DotGauge_Flame,				// --- 画像番号_ドットゲージ_ファイア
	e_Image_MiniIcon_Animals,			// --- 画像番号_アニマルアイコン(小)
	e_Image_StateFrame,					// --- 画像番号_ステータスの枠
	e_Image_CountDown_Str,				// --- 画像番号_カウントダウン用文字列「3 2 1 GO!」
	e_Image_TimeUp_Str,					// --- 画像番号_タイムアップ用文字列「TIME UP」
	e_Image_Warning_Str,				// --- 画像番号_警告表示文字列「WARNING」
	e_Image_Warning_Bar,				// --- 画像番号_警告表示バー
	e_Image_Warning_Line,				// --- 画像番号_警告表示ライン
	e_Image_SuperFlash,					// --- 画像番号_スーパーフラッシュ( スーパー状態の時に出現する枠のようなもの )
	e_Image_SceneChange_Bar,			// --- 画像番号_シーンチェンジ素材_バー
	e_Image_SceneChange_Icon,			// --- 画像番号_シーンチェンジ素材_アイコン
	e_Image_Result_ScoreIcon,			// --- 画像番号_リザルト時のスコアアイコン
	e_Image_Result_Bar,					// --- 画像番号_リザルト時の文字列を載せるバー
	e_Image_Result_BarStr_Drop,			// --- 画像番号_リザルト時の文字列_「落とした数」
	e_Image_Result_BarStr_Coconut,		// --- 画像番号_リザルト時の文字列_「獲得ココナッツ数」
	e_Image_Result_WinStr,				// --- 画像番号_リザルト時の文字列_「1PWIN」「2PWIN」
	e_Image_Result_BackFlash,			// --- 画像番号_リザルト時に使うWINの後ろできらめくやつ
	e_Image_Total						// --- 画像番号_総計
} ;

enum e_Title_Object
{
	e_Title_Fox = 0,			// --- タイトルオブジェクト_キツネ
	e_Title_Cat,				// --- タイトルオブジェクト_ネコ
	e_Title_Pig,				// --- タイトルオブジェクト_ブタ
	e_Title_BeachChair,			// --- タイトルオブジェクト_ビーチチェアー
	e_Title_Table,				// --- タイトルオブジェクト_テーブル
	e_Title_Juice,				// --- タイトルオブジェクト_ココナッツジュース
	e_Title_YashiWood_1,		// --- タイトルオブジェクト_ヤシの木(ココナッツ付き)_1 with Cat
	e_Title_YashiWood_2,		// --- タイトルオブジェクト_ヤシの木(ココナッツ付き)_2 not Cat
	e_Title_YashiWood_3,		// --- タイトルオブジェクト_ヤシの木(ココナッツ付き)_3 not Cat
	e_Title_Ukiwa,				// --- 浮き輪
	e_Title_Beach,				// --- ビーチ
	e_Title_Sea, 				// --- 海
	e_Title_Nami, 				// --- 波
	e_Title_Total,		// --- タイトルオブジェクト_総計
} ;

enum e_Image_LoadView
{
	e_imgLoadView_View = 0,				// --- ロード画面用画像_背景
	e_imgLoadView_Gauge,				// --- ロード画面用画像_ゲージ
	e_imgLoadView_Total					// --- ロード画面用画像_総計
} ;

enum e_SoundNames
{
	e_Sound_TitleBGM = 0,				// --- サウンド_タイトルBGM
	e_Sound_SeaWaveSE,					// --- サウンド_タイトルの海の音
	e_Sound_SelectBGM,					// --- サウンド_セレクトBGM
	e_Sound_BattleBGM,					// --- サウンド_バトルBGM
	e_Sound_TitlePushSE,				// --- サウンド_タイトル決定音
	e_Sound_CursolSE,					// --- サウンド_カーソル移動音
	e_Sound_CountdownSE,				// --- サウンド_カウントダウンSE
	e_Sound_GongSE,						// --- サウンド_ゴングSE
	e_Sound_VibrationSE,				// --- サウンド_振動SE
	e_Sound_BattlePushSE,				// --- サウンド_バトルプッシュ音
	e_Sound_DoteatSE,					// --- サウンド_ドット取得音
	e_Sound_PowerUpSE,					// --- サウンド_スペシャル
	e_Sound_ReturnSE,					// --- サウンド_スペシャル戻り
	e_Sound_FallSE,						// --- サウンド_落下
	e_Sound_ChakuchiSE,					// --- サウンド_着地
	e_Sound_BuzzerSE,					// --- サウンド_ステージ切り替えブザー
	e_Sound_WaterSE,					// --- サウンド_水の吹き上げる音
	e_Sound_TimeOutSE,					// --- サウンド_ゲーム終了音
	e_Sound_heartSE,					// --- サウンド_心臓音
	e_Sound_DrumrollSE,					// --- サウンド_ドラムロール音
	e_Sound_rollfinishSE,				// --- サウンド_ドラムロール_シンバル音
	e_Sound_applauseSE,					// --- サウンド_拍手音
	e_Sound_Total						// --- サウンド_総計
} ;

enum e_GameMode
{
	e_Mode_Blank = 0,					// --- モード_ブランク(初期値)
	e_Mode_Title_Init,					// --- モード_タイトル_初期化
	e_Mode_Title,						// --- モード_タイトル
	e_Mode_CharaSelect_Init,			// --- モード_キャラ選択_初期化
	e_Mode_CharaSelect,					// --- モード_キャラ選択
	e_Mode_Game_Init,					// --- モード_ゲーム_初期化
	e_Mode_Game_Start,					// --- モード_ゲーム_スタート
	e_Mode_Game,						// --- モード_ゲーム
	e_Mode_Result_Init,					// --- モード_結果発表_初期化
	e_Mode_Result,						// --- モード_結果発表
} ;

enum e_StageNo
{
	e_stage1 = 0,						// --- ステージ番号_ステージ１
	e_stage2_top,						// --- ステージ番号_ステージ２上部
	e_stage2_under,						// --- ステージ番号_ステージ２下部
	e_stage3,							// --- ステージ番号_ステージ３
} ;

enum e_Timer
{
	e_Timer1 = 0,						// --- タイマー（足場で使用）
	e_Timer2,							// --- ステージ変更までの時間計測に使用
	e_Timer3,							// --- メインタイマー
	e_TimerTotal						// --- タイマー総計
} ;

enum e_PlayerAction
{
	e_PlayerWait = 0 ,					// --- プレイヤー待機アニメ
	e_PlayerWalk ,						// --- プレイヤー歩きアニメ
	e_PlayerPush_In ,					// --- プレイヤー押し始アニメ_始
	e_PlayerPush_Out ,					// --- プレイヤー押し終アニメ_終
	e_PlayerPress_In ,					// --- プレイヤー押され始アニメ_始
	e_PlayerPress_Out ,					// --- プレイヤー押され終アニメ_終
	e_PlayerSpecialAttack_In ,			// --- プレイヤー_スペシャル時の攻撃_始
	e_PlayerSpecialAttack_Loop ,		// --- プレイヤー_スペシャル時の攻撃_繰
	e_PlayerSpecialAttack_Out ,			// --- プレイヤー_スペシャル時の攻撃_終
	e_SuparPacMan_Walk ,				// --- プレイヤースーパー歩行
	e_StageChange_1to2 ,				// --- 1-2ステージ切り替え時アニメ
	e_StageChange_2to3 ,				// --- 2-3ステージ切り替え時終わりアニメ
	e_StageChange_2to3_End ,			// --- 2-3ステージ切り替え時アニメ
	e_PlayerSelectAction ,				// --- プレイヤー_キャラ選択画面での決定時アニメ
	e_PlayerWinEmote_In ,				// --- プレイヤー_勝利エモート_始
	e_PlayerWinEmote_Loop ,				// --- プレイヤー_勝利エモート_繰
	e_PlayerLoseEmote_In ,				// --- プレイヤー_敗北エモート_始
	e_PlayerLoseEmote_Loop ,			// --- プレイヤー_敗北エモート_繰
	e_PlayerTitle_Sleep ,				// --- プレイヤー_タイトルエモート_寝る
	e_PlayerTitle_Jump ,				// --- プレイヤー_タイトルエモート_跳ぶ
	e_PlayerTitle_Flow ,				// --- プレイヤー_タイトルエモート_浮かぶ
	e_PlayerJump_In,					// --- プレイヤー_ジャンプ_始
	e_PlayerJump_Loop,					// --- プレイヤー_ジャンプ_繰
	e_PlayerJump_Out,					// --- プレイヤー_ジャンプ_終
	e_PlayerAnimTotal					// --- プレイヤーアニメ総計
} ;

enum e_PlayerMode
{
	e_pMode_Wait_Init = 0,				// --- プレイヤーモード_待機_初期化
	e_pMode_Wait,						// --- プレイヤーモード_待機
	e_pMode_Move,						// --- プレイヤーモード_歩き
	e_pMode_Push,						// --- プレイヤーモード_押し
	e_pMode_Press,						// --- プレイヤーモード_押され
	e_pMode_Fall_Init,					// --- プレイヤーモード_落下_初期化
	e_pMode_Fall,						// --- プレイヤーモード_落下
	e_pMode_Death,						// --- プレイヤーモード_死亡
	e_pMode_Revive,						// --- プレイヤーモード_復活
	e_pMode_Super,						// --- プレイヤーモード_スーパー
	e_pMode_StageChange_1to2_Init,		// --- ステージ切り替え時アニメ
	e_pMode_StageChange_1to2,			// --- ステージ切り替え時アニメ_初期セット
	e_pMode_StageChange_2to3_Init,		// --- ステージ切り替え時アニメ
	e_pMode_StageChange_2to3,			// --- ステージ切り替え時吹っ飛びアニメ_初期セット
	e_pMode_StageChange_2to3_End		// --- ステージ切り替え着地アニメ
} ;

enum e_StageID
{
	e_Stage_1_OnSky = 1,				// --- ステージ_天空
	e_Stage_2_OnSea,					// --- ステージ_海上
	e_Stage_3_OnIce,					// --- ステージ_氷上
	e_Stage_Total						// --- ステージ_総計
} ;

enum e_Direction
{
	e_Down = 0,							// --- 方向_下
	e_Left,								// --- 方向_左
	e_Up,								// --- 方向_上
	e_Right								// --- 方向_右
} ;

/* --------------------------------------------------------------------------------------


       DEFINE宣言


 --------------------------------------------------------------------------------------- */
#define WINDOWSIZE_W	1920		// ウィンドウ横サイズ_フルスクリーン
#define WINDOWSIZE_H	1080		// ウィンドウ縦サイズ_フルスクリーン
#define WIN_MINISIZE_W	1280		// ウィンドウ横サイズ_通常サイズ
#define WIN_MINISIZE_H	720			// ウィンドウ縦サイズ_通常サイズ

#define BLOCKSIZE_X		300		// ブロックサイズX
#define BLOCKSIZE_Y		300		// ブロックサイズY
#define BLOCKOFSET		-300	// ブロックの高さを変える
#define ELE_X_1			7		// ステージ１X要素数
#define ELE_X_2			13		// ステージ２X要素数
#define ELE_X_3			7		// ステージ３X要素数
#define ELE_Y_1			5		// ステージ１Y要素数
#define ELE_Y_2			3		// ステージ２Y要素数
#define ELE_Y_3			5		// ステージ３Y要素数

#define MAX_TIME		90		// 最大秒数

#define PAD_BUTTON_MAX 28		// ボタン総数
#define KEY_BUFFER_MAX 256		// キーボード入力用バッファサイズ
#define PAD_INPUT_ON 1			// 押された
#define PAD_INPUT_OFF 0			// 押されてない。

#define PLAYER_ONE		0		// 1P
#define PLAYER_TWO		1		// 2P

#define USEOBJ_MAX			160						// 使用するオブジェクトの最大数
#define OBJID_SKYDOM		12						// g_Object[]の要素ID_スカイドーム
	#define SKYDOM_MAX			2					//						MAX_スカイドーム
#define OBJID_HUNSUI		14						// g_Object[]の要素ID_噴水
	#define HUNSUI_MAX			2					//						MAX_噴水
#define OBJID_SEA			16						// g_Object[]の要素ID_海
	#define SEA_MAX				4					//						MAX_海
#define OBJID_DODAI			22						// g_Object[]の要素ID_土台
	#define DODAI_MAX			6					//						MAX_土台
#define OBJID_COCONUT		30						// g_Object[]の要素ID_ココナッツ
	#define COCONUT_MAX			20					//						MAX_ココナッツ
#define OBJID_TITLEOB		55						// g_Object[]の要素ID_タイトルで使用するオブジェクト
	#define TITLEOB_MAX			20					//						MAX_タイトルで使用するオブジェクト
#define OBJID_CLOUD			80						// g_Object[]の要素ID_雲
	#define CLOUD_MAX			2					//						MAX_雲
#define OBJID_GAMEOB		90						// g_Object[]の要素ID_ゲーム画面で使用するオブジェクト
	#define GAMEOB_MAX			30					//						MAX_ゲーム画面で使用するオブジェクト
#define OBJID_RESULTOB		130						// g_Object[]の要素ID_リザルトで使用するオブジェクト
	#define RESULTOB_MAX		15					//						MAX_リザルトで使用するオブジェクト

#define USEANIM_MAX			30						// オブジェクトごとに使用できるアニメーションの最大数
#define USEGRAPH_MAX		50						// 使用するテクスチャ画像の最大数
#define PLAYER_MAX_HITCOLL	2056					// プレイヤーがヒットチェックを同時に行える最大ポリゴン数
#define COLLOBJ_MAX			(ELE_X_1 * ELE_Y_1) + ((ELE_X_2 * ELE_Y_2) * 2) + (ELE_X_3 * ELE_Y_3)	// コリジョンオブジェクトの最大数

#define EFFECT_MAX			256						// 使用するエフェクトの最大数

#define LOADNUM_MAX			e_Image_Total + e_Model_Total + e_Sound_Total + (e_PlayerAnimTotal * 3)	// 読み込むオブジェクトの数

#define CLAPMODE_EXCEED	0		// Check Limit And Plus() のモード。最大値を"上回った"かどうかを見たいときに使用
#define CLAPMODE_BELOW	1		// Check Limit And Plus() のモード。最小値を"下回った"かどうかを見たいときに使用

#define GAMEUI_POS_Y		90	// ゲーム中、上に表示されるUIのY座標。同じ列にするよ

/* --------------------------------------------------------------------------------------


       構造体宣言


 --------------------------------------------------------------------------------------- */
// =====================
//	ジョイパッド情報
// =====================
struct tyAttachment
{
	int intPadNo;			// --- パッド番号
	int intButtonHandle;	// --- ボタンハンドル
} ;

// =====================
//		アニメ情報
// =====================
typedef struct
{
	float	animTotal ;			// --- アニメーション総時間
	int		handle ;			// --- ハンドル

} ANIM_INFO ;

// =====================
//	モデル_アニメ情報
// =====================
typedef struct
{
	int			modelHandle ;			// --- モデルハンドル
	int			rootFlm ;				// --- フレームルート
	int			animCount ;				// --- 使用アニメーションの個数
	ANIM_INFO	anim[ USEANIM_MAX ] ;	// --- アニメハンドル

} MODEL_ANIM_INFO ;

// =====================
//	   サウンド情報
// =====================
typedef struct
{
	int		soundHandle ;			// --- サウンドのハンドル
	int		playType ;				// --- プレイタイプ(NORMAL, LOOP, BACK)
	int		volume ;				// --- ボリューム

} SOUND_INFO ;

// =====================
//	     画像情報
// =====================
typedef struct
{
	int		handle ;				// --- ハンドル
	POINT	fullSize ;				// --- サイズ全体のサイズ
	POINT	chipSize ;				// --- チップごとのサイズ
	POINT	offSet ;				// --- オフセット( 中心 )

} GRAPH_INFO ;

// =====================
//	      影情報
// =====================
typedef struct
{
	int		handle ;			// --- ハンドル
	BOOL	isUse ;				// --- 影を使用するかどうかのフラグ

} SHADOW_INFO ;

// =====================
//	  コリジョン情報
// =====================
typedef struct
{
	BOOL	isValid ;			// --- コリジョン有効フラグ
	int		handle ;			// --- コリジョンモデルハンドル

} COL_INFO ;

// =====================
//		FPS情報
// =====================
typedef struct
{
	LONGLONG	NowTime ;			// --- 現在のシステム時間
	LONGLONG	Time ;				// --- システム時間の格納先
	float		DeltaTime ;			// --- 加工後の数値格納先
	int			FPS ;				// --- カウンターの一時的な保存場所
	int			FPSCounter ;		// --- カウンター
	LONGLONG	FPSCheckTime ;		// --- チェックタイム

	BOOL		isSetupOK ;			// --- セットアップ完了フラグ

} FPS_INFO ;

// =====================
//	  デバッグ情報
// =====================
typedef struct
{
	BOOL		isCameramode ;		// --- カメラモードの有効
	BOOL		dispBar ;			// --- デバッグバーを表示するかどうかのフラグ
	int			nowSelect ;			// --- 現在カーソルを合わせているモード
	int			mapSwitch ;			// --- マップ変更すいっち
	FPS_INFO	fps ;				// --- FPS情報

} DEBUGMODE_INFO ;

/* --------------------------------------------------------------------------------------


       共通ヘッダー


 --------------------------------------------------------------------------------------- */
#include "StructAndClasses.h"	// 便利な構造体、クラスの定義部

#include "Object.h"				// オブジェクトクラス
#include "Effect.h"				// エフェクトクラス
#include "CameraMovement.h"		// カメラ本体のクラス
#include "ConsoleWindow.h"		// コンソール
#include "Scaffold.h"			// 足場クラス
#include "Mapgeneration.h"		// マップ生成クラス
#include "PadData.h"			// パッド入力クラス
#include "Player.h"				// プレイヤークラス
#include "Item.h"				// アイテムクラス
#include "DotItem.h"			// ドットアイテムクラス
#include "Timer.h"				// タイマークラス
#include "Ghost.h"				// ゴーストクラス
#include "WaterSurface.h"		// 水面クラス

#include "UserInterface.h"			// UIクラス
#include "TimerUI.h"				// UIクラス_タイマー
#include "PlayerId.h"				// UIクラス_追跡プレイヤーID
#include "ScoreUI.h"				// UIクラス_スコア
#include "UI_DotGaugeAndPlayerID.h"	// UIクラス_ドットゲージとプレイヤーID
#include "UI_CountDownNumber.h"		// UIクラス_カウントダウンNo
#include "UI_Warning.h"				// UIクラス_ワーニング
#include "UI_SceneChange.h"			// UIクラス_シーン遷移

/* --------------------------------------------------------------------------------------


       型宣言


 --------------------------------------------------------------------------------------- */

typedef int ( *TBLJP )( void ) ;		// ←これなに？

/* --------------------------------------------------------------------------------------


       変数宣言


 --------------------------------------------------------------------------------------- */



/* --------------------------------------------------------------------------------------


       プロトタイプ宣言


 --------------------------------------------------------------------------------------- */

int InitAllField() ;					// --- 初期セット関数_フィールド関連

int InitAllStates() ;					// --- 初期化関数_メイン
int InitVarStates() ;					// --- 初期化関数_変数
int InitSoundStates() ;					// --- 初期化関数_サウンド
int InitGraphStates() ;					// --- 初期化関数_画像
int InitModelAndAnimStates() ;			// --- 初期化関数_モデル
int InitObjectStates() ;				// --- 初期化関数_オブジェクト
int InitAttachmentPad() ;				// --- 初期化関数_ゲームパッド

int  DebugLoop( BOOL isUse ) ;					// --- デバッグ用のループ
void StartFPS() ;								// --- FPS描画のセットアップ
void DrawFPS( int arg_pos_x, int arg_pos_y ) ;	// --- 現在FPSの描画

int DrawLoop( BOOL isUIDisp ) ;			// --- 描画の管理を行う関数
int GameLoop() ;						// --- ゲーム全体のメインループ

void LoadGaugeDisp_FillOK( int arg_OKnum ) ;	// --- 完了分加算 + ロードゲージ描画
int  GpUpdateKey() ;							// --- キー入力情報取得関数
int	 SetWinLoseInfo() ;							// --- プレイヤー同士の勝敗判定を調べ、各フィールドにその状態をセットする。

int EffectGenerator( VECTOR arg_setPos, int arg_effectID, int arg_info = 0 ) ;											// --- エフェクトを生成する関数
int GetGraphSizeAndOffSet( GRAPH_INFO* arg_g ) ;																		// --- 画像サイズ、オフセットをセットする関数
int GetAnimEasy ( MODEL_ANIM_INFO *arg_MAInfo, const int arg_actionNo, const TCHAR *FileName, const int arg_No = 0 ) ;	// --- アニメハンドルと総時間を取得する関数
int AnimCopyTo( MODEL_ANIM_INFO *arg_MAInfo, const int arg_action, 
					const MODEL_ANIM_INFO *arg_copyMAInfo, const int arg_No = 0 ) ;			// --- アニメ情報のコピーを行う関数
int SetAllSoundVolume100() ;																// --- 全てのサウンドの音量を100にする関数
int SetSoundVolumePlusNum( int a_plusNum, SOUND_INFO *a_sHandle ) ;							// --- 指定サウンドハンドルのボリュームに指定数値分加算する。
int SetSoundVolumeNum( int a_volNum, SOUND_INFO *a_sHandle ) ;								// --- 指定サウンドハンドルのボリュームに指定数値セットする。

int		CountArrayInBool( BOOL arg_array[], BOOL arg_checkBool = TRUE )	;					// --- BOOL配列における中身の同一データ数を調べる
VECTOR	SetVector( float arg_num ) ;														// --- 規定の数値をセットしたVECTORを帰す関数
void	ResetVector( VECTOR &pos ) ;														// --- VECTOR型変数の中身をリセットする関数
float	GetTriangleSide( const float side_Length_1 , const float side_Length_2 ) ;			// --- ２辺から、辺の長さを求める関数

template <class T>
	int CheckLimitAndPlus( int mode, T *useNum, T plusNum, T MaxMinCheck, T overSetNum ) ;	// --- AにBを足した数が、Cを超過しない場合加算を行う。
template<class T>
	T GetAverage_InArray( const T *arg_Array , int arg_Num ) ;								// --- 配列の要素から平均を求める関数
template <class T>
	void DrawModelEasy( T *arg_obj ) ;														// --- オブジェクトに応じた描画関数

// ----------------------
//		以下UI関連
// ----------------------
int UIDraw_DotGauge( int arg_playerNo, int arg_pos_x, int arg_pos_y ) ;						// --- ドットゲージ描画

/* --------------------------------------------------------------------------------------


       外部参照宣言


 --------------------------------------------------------------------------------------- */
//extern ConsoleWindow		g_cWin ;										// --- コンソールウィンドウ
extern POINT				g_winSize ;										// --- 現在のウィンドウサイズの大きさ
extern c_countGroup<float>	g_loadCnt ;										// --- ロード完了のパーセンテージを格納した変数

extern PUSH_STATE			g_dataPadInput[4] ;								// --- パッドの入力情報( 一応要素は4つ作っておく )
extern SHADOW_INFO			g_shadowMap ;									// --- 影のハンドル

extern CameraMovement		g_camera ;										// --- カメラオブジェクト

extern float				g_gameSpeed ;									// --- ゲームスピード変数( デフォルトが 1.0 )

extern int					g_gameMode ;									// --- ゲームモード管理変数
extern int					g_keyBuf[ 256 ] ;								// --- キーの入力情報( 常時更新 )
extern MODEL_ANIM_INFO		g_mv1Handle[ e_Model_Total ] ;					// --- モデルとアニメーションのハンドル格納構造体
extern GRAPH_INFO			g_graphHandle[ e_Image_Total ] ;				// --- 画像のハンドル
extern SOUND_INFO			g_soundHandle[ e_Sound_Total ] ;				// --- サウンドのハンドル格納構造体

extern GRAPH_INFO			g_graphHandle_LoadView[ e_imgLoadView_Total ] ;	// --- ロード画面用の画像ハンドル格納変数

extern Object g_Object[ USEOBJ_MAX ] ;										// --- 使用するオブジェクトを格納する配列

extern PadData Padinput ;										// --- パッド入力情報
extern Player  g_Player[2] ;									// --- プレイヤーオブジェクト
extern Player  *p_Player ;										// --- プレイヤーのポインタ

extern DotItem Dot1stg[ELE_X_1 * ELE_Y_1] ;
extern DotItem Dot2stg_t[ELE_X_2 * ELE_Y_2] ;
extern DotItem Dot2stg_u[ELE_X_2 * ELE_Y_2] ;
extern DotItem Dot3stg[ELE_X_3 * ELE_Y_1] ;

extern MapGeneration Map ;										// --- マップ生成
extern Scaffold Scaf1[ELE_X_1 * ELE_Y_1] ;						// --- 足場１
extern Scaffold Scaf2_t[ELE_X_2 * ELE_Y_2] ;					// --- 足場２上部
extern Scaffold Scaf2_u[ELE_X_2 * ELE_Y_2] ;					// --- 足場２下部
extern Scaffold Scaf3[ELE_X_3 * ELE_Y_3] ;						// --- 足場３

extern Timer Time[e_TimerTotal] ;								// --- タイマー

extern TimerUI					TimeUI[2] ;						// --- タイマーのUI
extern PlayerId					PlayerUI[2] ;					// --- プレイヤーの１P・２P表示
extern ScoreUI					ScoreBoard[2] ;					// --- スコアの１P・２P表示
extern UI_DotGaugeAndPlayerID	UI_DGauge_ID[2] ;				// --- ドットゲージ_プレイヤーIDのUI
extern UI_CountDownNumber		UI_CntDown ;					// --- カウントダウン用UI
extern UI_Warning				UI_War ;						// --- ワーニングUI
extern UI_SceneChange			UI_SChange ;					// --- シーンチェンジUI

extern int map1[ELE_X_1 * ELE_Y_1] ;							// --- ステージ1マップ
extern int map2_top[ELE_X_2 * ELE_Y_2] ;						// --- ステージ2上部マップ
extern int map2_under[ELE_X_2 * ELE_Y_2] ;						// --- ステージ2下部マップ
extern int map3[ELE_X_3 * ELE_Y_3] ;							// --- ステージ3マップ

extern Effect		g_effect[ EFFECT_MAX ] ;					// --- 使用するエフェクトの格納先

extern DEBUGMODE_INFO	g_debInfo ;								// --- デバッグステータス

