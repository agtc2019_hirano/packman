/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: StructAndClasses.h
	CREATE	: NOGUCHI

	------------------------------------------------------------------------------------------------------------------------------------
	------ Explanation of file ---------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------
       
		様々な構造体、クラスの定義部。
		コンストラクタを上手く使えば多くの変数の初期化を一行で済ますことができる！
			
		(例)	c_fPoint myPos ;							// [x - 0.0f]         [y - 0.0f]			が初期値として入る			  
				myPos += c_fPoint( 5.0f, 8.0f ) ;			// [x - (0.0f + 5.0f)][y - (0.0f + 8.0f)]	という計算がされる

				c_fPoint youPos = c_fPoint( 1.0f, 2.0f ) ;	// [x - 1.0f]         [y - 2.0f]			がそれぞれ初期値として入る
				youPos *= 3.0f ;							// [x - (1.0f * 3.0f)][y - (2.0f * 3.0f)]	という計算がされる

				c_fPoint newPos = myPos + youPos ;			// [x - (5.0f + 3.0f)][y - (8.0f + 6.0f)]	という計算のもと初期値が入る

		テンプレートクラスは好きな型に当てはめることができる！便利！

		(例)	c_4Direction<int> cI ;												// int型として使える
				c_4Direction<float> cF_A = cI ;										// やめて！
				c_4Direction<float> cF_B = c_4Direction<float> ( (float)cI.up, (略	// やめて！


	-----------------------------------------------------------------------------------------------------------------------------------
	------ Kind of structs and classes ------------------------------------------------------------------------------------------------
	-----------------------------------------------------------------------------------------------------------------------------------

	＜クラス＞
		・c_fPoint				- x, y の情報を持ったクラス。全てfloat型。
		・c_4Direction			- up, down, left, right の4情報を持ったクラス。	[テンプレートクラス]
		・c_countGroup			- now, plus, max, min の4情報を持ったクラス。	[テンプレートクラス]

	＜構造体＞
		・S_PLAYER_INPUTINFO	- シーン毎のプレイヤー入力情報( c_ImgInfoなどはこの構造体限定のもの( 使ってもいいけど使い勝手は最悪 ) )

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */



// ================================================================================================================ c_fPoint
// ------------------------------------------------------------------------------
//								POINTのfloat版
// ------------------------------------------------------------------------------
class c_fPoint
{
	public :
		c_fPoint() {								// --- コンストラクタ( 引数０ )
			this->x = this->y = 0.0f ;
		}
		c_fPoint( float arg_f ) {					// --- コンストラクタ( 引数１ )
			this->x = this->y = arg_f ;
		}
		c_fPoint( float arg_x, float arg_y ) {		// --- コンストラクタ( 引数２ )
			this->x = arg_x ;
			this->y = arg_y ;
		}

		void operator= ( c_fPoint arg_p ) {			// --- オペレーター( ＝ )
			this->x = arg_p.x ;
			this->y = arg_p.y ;
		}
		c_fPoint operator+ ( c_fPoint arg_p ) {		// --- オペレーター( ＋ )
			c_fPoint ret ;
			ret.x = this->x + arg_p.x ;
			ret.y = this->y + arg_p.y ;
			return ret ;
		}
		c_fPoint operator- ( c_fPoint arg_p ) {		// --- オペレーター( − )
			c_fPoint ret ;
			ret.x = this->x - arg_p.x ;
			ret.y = this->y - arg_p.y ;
			return ret ;
		}
		c_fPoint operator* ( c_fPoint arg_p ) {		// --- オペレーター( ＊ )
			c_fPoint ret ;
			ret.x = this->x * arg_p.x ;
			ret.y = this->y * arg_p.y ;
			return ret ;
		}
		c_fPoint operator/ ( c_fPoint arg_p ) {		// --- オペレーター( ／ )
			c_fPoint ret ;
			ret.x = this->x / arg_p.x ;
			ret.y = this->y / arg_p.y ;
			return ret ;
		}
		void operator+= ( c_fPoint arg_p ) {		// --- オペレーター( ＋＝ )
			this->x += arg_p.x ;
			this->y += arg_p.y ;
		}
		void operator-= ( float arg_f ) {			// --- オペレーター( −＝ )
			this->x -= arg_f ;
			this->y -= arg_f ;
		}
		void operator*= ( float arg_f ) {			// --- オペレーター( ＊＝ )
			this->x *= arg_f ;
			this->y *= arg_f ;
		}
		void operator/= ( float arg_f ) {			// --- オペレーター( ／＝ )
			this->x /= arg_f ;
			this->y /= arg_f ;
		}

		float	x ;				// --- 座標_x
		float	y ;				// --- 座標_y
} ;

// ================================================================================================================ c_4Direction
// ------------------------------------------------------------------------------
//							4方向の属性を持つクラス
// ------------------------------------------------------------------------------
template <class TC>
class c_4Direction
{
	public :
		c_4Direction() {												// --- コンストラクタ( 引数０ )
			this->up = this->down = this->left = this->right = 0 ;
		}
		c_4Direction( int arg_d ) {										// --- コンストラクタ( 引数１ )
			this->up = this->down = this->left = this->right = arg_d ;
		}
		c_4Direction( TC arg_U, TC arg_D, TC arg_L, TC arg_R ) {		// --- コンストラクタ( 引数４ )
			this->up	= arg_U ;
			this->down	= arg_D ;
			this->left	= arg_L ;
			this->right	= arg_R ;
		}

		void operator= ( c_4Direction arg_d ) {			// --- オペレーター( ＝ )
			this->up	= arg_d.up ;
			this->down	= arg_d.down ;
			this->left	= arg_d.left ;
			this->right	= arg_d.right ;
		}
		c_4Direction operator+ ( c_4Direction arg_d ) {	// --- オペレーター( ＋ )
			c_4Direction ret ;
			ret = this->up    + arg_d.up ;
			ret = this->down  + arg_d.down ;
			ret = this->left  + arg_d.left ;
			ret = this->right + arg_d.right ;
			return ret ;
		}
		c_4Direction operator- ( c_4Direction arg_d ) {	// --- オペレーター( − )
			c_4Direction ret ;
			ret = this->up    - arg_d.up ;
			ret = this->down  - arg_d.down ;
			ret = this->left  - arg_d.left ;
			ret = this->right - arg_d.right ;
			return ret ;
		}
		c_4Direction operator* ( c_4Direction arg_d ) {	// --- オペレーター( ＊ )
			c_4Direction ret ;
			ret = this->up    * arg_d.up ;
			ret = this->down  * arg_d.down ;
			ret = this->left  * arg_d.left ;
			ret = this->right * arg_d.right ;
			return ret ;
		}
		c_4Direction operator/ ( c_4Direction arg_d ) {	// --- オペレーター( ／ )
			c_4Direction ret ;
			ret = this->up    / arg_d.up ;
			ret = this->down  / arg_d.down ;
			ret = this->left  / arg_d.left ;
			ret = this->right / arg_d.right ;
			return ret ;
		}
		void operator+= ( c_4Direction arg_d ) {		// --- オペレーター( ＋＝ )
			this->up	+= arg_d.up ;
			this->down	+= arg_d.down ;
			this->left	+= arg_d.left ;
			this->right	+= arg_d.right ;
		}
		void operator-= ( c_4Direction arg_d ) {		// --- オペレーター( −＝ )
			this->up	-= arg_d.up ;
			this->down	-= arg_d.down ;
			this->left	-= arg_d.left ;
			this->right	-= arg_d.right ;
		}
		void operator*= ( c_4Direction arg_d ) {		// --- オペレーター( ＊＝ )
			this->up	*= arg_d.up ;
			this->down	*= arg_d.down ;
			this->left	*= arg_d.left ;
			this->right	*= arg_d.right ;
		}
		void operator/= ( c_4Direction arg_d ) {		// --- オペレーター( ／＝ )
			this->up	/= arg_d.up ;
			this->down	/= arg_d.down ;
			this->left	/= arg_d.left ;
			this->right	/= arg_d.right ;
		}
		BOOL CheckAllZero()								// --- 要素全て「0」の場合のみTRUEを帰す
		{
			if ( (this->up == 0) && (this->down == 0) &&
					(this->left == 0) && (this->right == 0) )
				return TRUE ;

			return FALSE ;
		}

		TC up ;					// --- 上
		TC down ;				// --- 下
		TC left ;				// --- 左
		TC right ;				// --- 右
} ;

// ================================================================================================================ c_countGroup
// ------------------------------------------------------------------------------
//					now, plus, max, min の４情報を持ったクラス。
// ------------------------------------------------------------------------------
template <class TC>
class c_countGroup
{
	public :
		c_countGroup() {														// --- コンストラクタ( 引数０ )
			this->now	= 0 ;
			this->plus	= 0 ;
			memset( max, 0, sizeof( TC [8] ) ) ;
			memset( min, 0, sizeof( TC [8] ) ) ;
		}
		c_countGroup( TC arg_Now, TC arg_Plus, TC arg_Max, TC arg_Min = 0 ) {	// --- コンストラクタ( 引数３ )
			this->now		= arg_Now ;
			this->plus		= arg_Plus ;
			this->max[0]	= arg_Max ;
			this->min[0]	= arg_Min ;
		}
		void operator= ( c_countGroup arg_nmm ) {								// --- オペレーター( ＝ )
			this->now	= arg_nmm.now ;
			this->plus	= arg_nmm.plus ;
			memcpy( this->max, arg_nmm.max, sizeof( TC [8] ) ) ;
			memcpy( this->min, arg_nmm.min, sizeof( TC [8] ) ) ;
		}
		BOOL plus_RetMaxOver( TC arg_plus, int arg_arrayNo = 0 ) {				// --- 減算処理( 指定した配列の値を上回ると TRUE )
			this->now += arg_plus ;
			if ( this->now > this->max[ arg_arrayNo ] )
			{
				this->now = this->max[ arg_arrayNo ] ;
				return TRUE ;
			}

			return FALSE ;
		}
		BOOL min_RetMinBelow( TC arg_min, int arg_arrayNo = 0 ) {				// --- 減算処理( 指定した配列の値を下回ると TRUE )
			this->now -= arg_min ;
			if ( this->now < this->min[ arg_arrayNo ] )
			{
				this->now = this->min[ arg_arrayNo ] ;
				return TRUE ;
			}

			return FALSE ;
		}

		TC now ;		// --- 現在値
		TC plus ;		// --- 加算分
		TC max[8] ;		// --- 最大_8つあるのは、目標地点が複数あっても対応できるよう
		TC min[8] ;		// --- 最小_8つあるのは、目標地点が複数あっても対応できるよう
} ;

// ================================================================================================================ S_PLAYER_INPUTINFO
// ------------------------------------------------------------------------------
//						シーン毎のプレイヤー入力状況
// ------------------------------------------------------------------------------
class c_ImgInfo
{
	public :
		c_ImgInfo() { isSizeChange = FALSE ; changeNum = 0 ; }	// --- 初期値セット( 構造体クンさぁ... )

		POINT	sizeChange ;	// --- 可変サイズ( 100 で 通常サイズ )
		POINT	pos ;			// --- 座標

		BOOL	isSizeChange ;	// --- サイズ変更フラグ
		int		changeNum ;		// --- サイズ変更分の値
} ;

typedef struct
{
	int nowTime ;				// --- 現在の経過時間( 0スタート )
	int totalTime ;				// --- 表示する総時間
	int inTime ;				// --- totalTimeの内、フェードインに使用する時間
	int outTime ;				// --- totalTimeの内、フェードアウトに使用する時間

} s_FinInfo ;

enum e_ResultMode
{
	e_RSMode_BlackChange = 0,				// --- リザルトモード_暗転時
	e_RSMode_Chara_SpawnIn,					// --- リザルトモード_キャラが登場するシーン
	e_RSMode_PresenBefore_Str_Push,			// --- リザルトモード_発表前_押し出しスコア発表タグ
	e_RSMode_PresenBefore_Push,				// --- リザルトモード_発表前_押し出しスコア(どぅるるるる...)
	e_RSMode_PresenAfter_Push,				// --- リザルトモード_発表後_押し出しスコア(でぇぇえぇぇん)
	e_RSMode_PresenBefore_Str_Dot,			// --- リザルトモード_発表前_ドットスコア発表タグ
	e_RSMode_PresenBefore_Dot,				// --- リザルトモード_発表前_ドットスコア(どぅるるるる...)
	e_RSMode_PresenAfter_Dot,				// --- リザルトモード_発表後_ドットスコア(でぇぇえぇぇん)
	e_RSMode_DispString,					// --- リザルトモード_勝利、敗北文字描画
} ;

typedef struct
{
	BOOL		title_PushFlg ; 			// --- タイトル_押下フラグ

	int					cSel_IconNum ;				// --- キャラ選択_アイコンの数
	char				cSel_Icon[2] ;				// --- キャラ選択_アイコン選択状況
	BOOL				cSel_CharaSetFlg[2] ;		// --- キャラ選択_キャラOKフラグ
	BOOL				cSel_StartOKFlg[2] ;		// --- キャラ選択_スタートフラグ
	int					cSel_OKToBlackInterval ;	// --- キャラ選択_選択から、暗転までのインターバル
	BOOL				cSel_IsBlackStart ;			// --- キャラ選択_暗転開始フラグ
	c_ImgInfo			cSel_IconState[3] ;			// --- キャラ選択_アイコンのステータス
	c_countGroup<float>	cSel_WhiteBox ;				// --- キャラ選択_白い壁の透明度カウンタ
	float				cSel_CloudMovePow ;			// --- キャラ選択_雲の移動量を表す

	VECTOR		game_pFallPos[2] ;			// --- ゲーム画面_プレイヤーが落下し始める場所
	VECTOR		game_MovePower_Cloud[2] ;	// --- ゲーム画面_雲の移動量
	VECTOR		game_MovePower_Player[2] ;	// --- ゲーム画面_プレイヤーの移動量
	BOOL		game_IsCountStart ;			// --- ゲーム画面_カウントダウンが始まったフラグ
	BOOL		game_IsFin ;				// --- ゲーム画面_90秒経過フラグ
	s_FinInfo	game_FinishDisp ;			// --- ゲーム画面_「終了」を表示する時間( 例 totalTime[50], inTime[10], outTime[10] - 30フレームだけ通常表示 )
	int			game_ChangeResTime ;		// --- ゲーム画面_90秒経過からリザルトに移行するまでの総時間(フレーム数)

	int					res_Mode ;			// --- リザルト_モード(内部詳細は上記に)
	int					res_WinnerID ;		// --- リザルト_勝利キャラのID
	BOOL				res_isCameraMove ;	// --- リザルト_カメラ移動完了フラグ
	float				res_rad[3] ;		// --- リザルト_回転時に使用するラジアン
	float				res_fallSpeed[2] ;	// --- リザルト_落下速度( キャラ )
	float				res_flashRotPower ;	// --- リザルト_フラッシュ画像_回転加算分数値
	float				res_flashRot ;		// --- リザルト_フラッシュ画像_回転数値
	c_countGroup<float>	res_flashSize ;		// --- リザルト_フラッシュ画像_サイズ数値
	float				res_barSChange ;	// --- リザルト_数字のサイズ変更用カウンタ( 0 〜 1 )
	c_countGroup<float>	res_strSize ;		// --- リザルト_バーのサイズ変更用カウンタ( 0 〜 1 )

	float		setBlackCount_Max ;			// --- 暗転時間_最大値
	float		setBlackCount ;				// --- 暗転時間_カウントダウン用
	BOOL		isBlackStart ;				// --- 暗転から開始した際に、晴れるまでTRUEにするフラグ

	char		any_select[16] ;			// --- 自由に使える選択状況
	BOOL		any_flg[16] ;				// --- 自由に使えるフラグ
	int			any_counter[16] ;			// --- 自由に使えるカウンター
	int			any_number[16] ;			// --- 自由に使える数値

} S_PLAYER_INPUTINFO ;

