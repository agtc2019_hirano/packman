/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: Object.h
	
	------ Explanation of file ---------------------------------------------------------------------
       
	オブジェクトクラスの宣言

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

// ---------------------------------------------------------
//
//						Object クラス
//
// ---------------------------------------------------------
class Object
{
	// --------------
	// --- public
	// --------------
	public :
		// --- 以下メソッド
		Object( ) ;						// --- コンストラクタ
		~Object( ) ;					// --- デストラクタ

		virtual void initStates() ;		// --- ステータス初期化メソッド

		void	setModelAndAnim( MODEL_ANIM_INFO arg_MAInfo, BOOL arg_isDuplicate, 
									VECTOR arg_emiPower = VGet( 0.0f, 0.0f, 0.0f ) ) ;				// --- モデルとアニメのセッター

		int		getObjID() { return objID ; }														// --- ObjID のゲッター
		void	setObjID( int arg_ID ) { objID = arg_ID ; }											// --- ObjID のセッター

		int		getMode() { return mode ; }															// --- Mode のゲッター
		void	setMode( int arg_mode ) { mode = arg_mode ; }										// --- Mode のセッター
		
		int		setAnim( int arg_anim ) ;															// --- モーションセットメソッド
		int		setStatesFromParent() ;																// --- コリジョンのステータスを親と一致させる
		
		inline	void	 set_DispAndColValid( BOOL a_set ) { dispFlg = col.isValid = a_set ; } ;	// --- 表示、当たり判定有効フラグを同時に数値セットする

		// --- 以下フィールド
		VECTOR		pos ;						// --- 座標
		VECTOR		movepos ;					// --- 移動座標
		VECTOR		size ;						// --- サイズ
		VECTOR		sizePow ;					// --- サイズ可変分数値
		VECTOR		dir ;						// --- 向いている方向
		VECTOR		dirPow ;					// --- 向き可変分数値

		BOOL		dispFlg ;					// --- 表示フラグ

		int			modelHandle ;				// --- モデルのハンドル

		int			nowanim ;					// --- 現時点のアニメの習得

		BOOL		animUseFlg ;				// --- アニメーションを使用しているどうかのフラグ
		ANIM_INFO	anim[e_PlayerAnimTotal] ;	// --- アニメ情報
		float		playTime ;					// --- アニメーション時間
		int			rootFlm ;					// --- 骨組みの位置情報
		int			attachIdx ;					// --- モデルとアニメーションのアタッチ情報(Detachの際に必要になる)

		COL_INFO	col ;						// --- コリジョン関連

	// --------------
	// --- protected
	// --------------
	protected :
		int	mode ;				// --- モード
		int objID ;				// --- オブジェクトID
} ;


	