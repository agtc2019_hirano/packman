/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: UI_CountDownNumber.cpp
	CREATE	: NOGUCHI
	
	------ Explanation of file ---------------------------------------------------------------------
       
	UI_CountDownNumberクラスの実処理記述部
	
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

#include "Common.h"

UI_Warning::UI_Warning( )
{
}
UI_Warning::~UI_Warning( ){
}

/* ------------------------------------------------------------
					初期セット関数
===============================================================
	ワーニングＵＩの初期セット
	引数：なし
	戻り値：なし
--------------------------------------------------------------- */
void UI_Warning::WarInit( )
{
	UI_pos_1 = (float)g_winSize.x / 3 * 1 ;
	UI_pos_2 = (float)g_winSize.x / 3 * 2 ;
	UI_pos_3 = (float)g_winSize.x / 3 * 3 ;
	UI_pos_4 = (float)g_winSize.x / 3 * 4 ;
	UI_movepos = -5.0f ;
	ui_alpha = 255 ;
	ui_alphabar = 1 ;
	ui_add = -4 ;
	ui_addbar = 4 ;
	f_disp = FALSE ;
}

/* ------------------------------------------------------------
					Warning関数
===============================================================
	ワーニングＵＩの表示
	引数：なし
	戻り値：なし
--------------------------------------------------------------- */
void UI_Warning::WarDraw( )
{			 
	if ( UI_pos_1 <= -624.0f )	
		UI_pos_1 = (float)g_winSize.x ;	// UI座標を右端に
	if ( UI_pos_2 <= -624.0f )	
		UI_pos_2 = (float)g_winSize.x ;	// UI座標を右端に
	if ( UI_pos_3 <= -624.0f )	
		UI_pos_3 = (float)g_winSize.x ;	// UI座標を右端に
	if ( UI_pos_4 <= -624.0f )	
		UI_pos_4 = (float)g_winSize.x ;	// UI座標を右端に

	UI_pos_1 += UI_movepos ;				// UI座標に移動値足す
	UI_pos_2 += UI_movepos ;				// UI座標に移動値足す
	UI_pos_3 += UI_movepos ;				// UI座標に移動値足す
	UI_pos_4 += UI_movepos ;				// UI座標に移動値足す

	ui_alpha += ui_add ;						// 透明度足す
	if( ui_alpha <= 128 || ui_alpha == 255 )
	{
		ui_add = -ui_add ;						// 加算値逆転
	}

	if ( ui_alphabar >= 255 )
		ui_addbar = -ui_addbar ;

	if ( (ui_alphabar < 128) && (ui_addbar < 0) ){		// 全体の表示に合わせてワーニング表示フェードアウト
		ui_add = -4 ;
		ui_alpha += ui_add ;						// 透明度足す
	}

	if ( ui_alphabar < 0 )
		f_disp = FALSE ;

	if ( f_disp == TRUE ){
		ui_alphabar+= ui_addbar ;

		// 全体の透明度
		SetDrawBlendMode( DX_BLENDMODE_ALPHA, ui_alphabar ) ;

		DrawGraph( 0, (int)g_winSize.y / 2, g_graphHandle[ e_Image_Warning_Bar ].handle, TRUE ) ;				// 黒いバー表示
		// 透明にする( 描画した後ブレンドモードを元に戻す )
		SetDrawBlendMode( DX_BLENDMODE_ALPHA, ui_alpha ) ;
		DrawGraph( 0, (int)g_winSize.y / 2, g_graphHandle[ e_Image_Warning_Line ].handle, TRUE ) ;				// ライン表示
		DrawGraph( (int)UI_pos_1, (int)g_winSize.y / 2, g_graphHandle[ e_Image_Warning_Str ].handle, TRUE ) ;	// ワーニング表示
		DrawGraph( (int)UI_pos_2, (int)g_winSize.y / 2, g_graphHandle[ e_Image_Warning_Str ].handle, TRUE ) ;	// ワーニング表示
		DrawGraph( (int)UI_pos_3, (int)g_winSize.y / 2, g_graphHandle[ e_Image_Warning_Str ].handle, TRUE ) ;	// ワーニング表示
		DrawGraph( (int)UI_pos_4, (int)g_winSize.y / 2, g_graphHandle[ e_Image_Warning_Str ].handle, TRUE ) ;	// ワーニング表示
	
		SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;
	}


}

