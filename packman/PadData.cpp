/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: PadData.cpp
	CREATE	: HIRANO & NOGUCHI( 微調整 )
	
	------ Explanation of file ---------------------------------------------------------------------
       
	PadDateクラス

	<対応キー>
		・PAD_INPUT_A		- △
		・PAD_INPUT_B		- 〇
		・PAD_INPUT_C		- ×
		・PAD_INPUT_D		- □
		・PAD_INPUT_START	- SELECT( !? )

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

#include "Common.h"

PadData::PadData( ){
}
PadData::~PadData( ){
}

PUSH_STATE PadData::m_button[4] ;		// パッド毎のボタン情報格納

char PadData::m_padNum = -1 ;	// パッド番号


/* ------------------------------------------------------------
					UpDate関数
===============================================================
	パッドごとに情報を格納する
	引数：なし
	戻り値：なし
--------------------------------------------------------------- */
void PadData::UpDate()
{
	// ゲームパッドの取得出来ているか
	if ( m_padNum == -1 ) return;

	int	 Pad ;

	// ゲームパッドの個数だけ処理
	for( int padNumber = 0 ; padNumber < static_cast<int>(m_padNum) ; padNumber++ )
	{
		// 入力状態を取得
		Pad = GetJoypadInputState( padNumber+1 ) ;

		/// ボタン関係-----------------------------------------------------------
		{
			if ( Pad != 0 )
			{
				// --- 十字キーの情報を変更する
				if ( !( ((Pad & 0x0f) == 0x01) || ((Pad & 0x0f) == 0x02) || ((Pad & 0x0f) == 0x04) || ((Pad & 0x0f) == 0x08) ) )
					Pad &= 0xf0 ;

				SetPushTime( padNumber, Pad ) ;

				m_button[padNumber].pushKey = Pad ;
			}
			else
			{
				m_button[padNumber].pushTime	= 0 ;
				m_button[padNumber].pushKey		= 0 ;
			}

			//switch( Pad & 0xff )
			//{
			//	case PAD_INPUT_A :
			//		printf( "PAD_INPUT_A\n" ) ;
			//		break ;
			//
			//	case PAD_INPUT_B :
			//		printf( "PAD_INPUT_B\n" ) ;
			//		break ;
			//
			//	case PAD_INPUT_C :
			//		printf( "PAD_INPUT_C\n" ) ;
			//		break ;
			//
			//	case PAD_INPUT_X :
			//		printf( "PAD_INPUT_X\n" ) ;
			//		break ;
			//
			//	case PAD_INPUT_Y :
			//		printf( "PAD_INPUT_Y\n" ) ;
			//		break ;
			//	case PAD_INPUT_Z :
			//		printf( "PAD_INPUT_Z\n" ) ;
			//		break ;
			//	case PAD_INPUT_R :
			//		printf( "PAD_INPUT_R\n" ) ;
			//		break ;
			//	case PAD_INPUT_START :
			//		printf( "PAD_INPUT_START\n" ) ;
			//		break ;
			//}
			
//			DrawFormatString( 350, 530,  GetColor( 0xff, 0xff, 0xff ), "Pad : 0x%3x", m_trigger[0] ) ;	// デバック表示
//			DrawFormatString( 350, 590,  GetColor( 0xff, 0xff, 0xff ), "Pad : 0x%3x", m_button[padNumber] ) ;	// デバック表示
		}
		/// ---------------------------------------------------------------------
	}
}

/* ------------------------------------------------------------
					GetButton関数
===============================================================
	パッドのボタン情報取得
	引数：なし
	戻り値：int
--------------------------------------------------------------- */
const PUSH_STATE& PadData::GetButton(const int& t_padNum)
{
	return m_button[t_padNum] ;		// ボタン情報
}

/* ------------------------------------------------------------
				Check1FramePressed関数
===============================================================
	ボタン入力が１フレーム目であるかどうかを調べる関数
	引数：プレイヤーのナンバー, 入力キー
	戻り値：BOOL
--------------------------------------------------------------- */
BOOL PadData::Check1FramePressed( const int a_pNum, const int a_checkKey )
{
	if ( (m_button[ a_pNum ].pushKey & a_checkKey) && 
		(m_button[ a_pNum ].pushTime == 1) )
		return TRUE ;

	return FALSE ;
}




