/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: Functions.cpp
	
	------ Explanation of file ---------------------------------------------------------------------
       
	色々な関数置き場

	--------------------------------------------------------------------------------------
							使用用途が限りなく限定される関数群
	--------------------------------------------------------------------------------------
		・Init All Field()
				すべてのフィールドを初期化する関数

		・Debug Loop()
				デバッグ用ループ。本提出時は引数を"FALSE"にしてね。

		・Draw FPS()
				FPS( Frame Percent Second )を描画する。

		・Get New Obj ID()
				全てのオブジェクトのIDに被らない、新たなObjIDを生成する。

		・Set Win Lose Info()
				プレイヤーの勝敗状況をチェックし、フィールド「isWin_now」にセットする。

		・Load Gauge Disp_ Fill OK ()
				ゲージカウンタに数値を足しこみ、描画する。( ロード画面用 )

		・Gp Update Key ()
				キーの入力情報、押下時間を算出する。
					 
		・Get Anim Easy ( MODEL_ANIM_INFO*, int, TCHAR* )
				アニメのハンドル、総時間を取得。

		・Anim Copy To ( MODEL_ANIM_INFO*, int, MODEL_ANIM_INFO* )
				第三引数のモデル、アニメ情報を第一引数にコピーする。

		・Get Graph Size And OffSet( GRAPH_INFO* )
				画像のサイズ、オフセットを求める。

	--------------------------------------------------------------------------------------
								  サウンド関連の関数群
	--------------------------------------------------------------------------------------
		・Set All Sound Volume 100
				全てのサウンドハンドルのボリュームを100にする。

		・Set Sound Volume Plus Num
				指定サウンドハンドルのボリュームに指定数値分加算する。(0 〜 100)

		・Set Sound Volume Num
				指定サウンドハンドルのボリュームに指定数値をセットする。(0 〜 100)

	--------------------------------------------------------------------------------------
								以下、便利(そう)な関数群
	--------------------------------------------------------------------------------------
		・Count Array In Bool( BOOL *[], int, BOOL = TRUE )
				配列内のBOOL変数がいくつ TRUEであるか調べる。
				第三引数でFALSEを指定すれば、FALSEを見ることもできる。

		・Check Limit And Plus ( int mode, T A, T B, T C, T setNum )
				AにBを足した数が、Cを超過しない場合加算を行う。
				超過する場合は第5引数の数値をAにセットする。

		・Set Vector ( int )
				引数の値を持つVECTORを帰す関数。
	
		・Reset Vector ( VECTOR )
				VECTOR型変数の中身を全て０にする。
		
		・Get Triangle Side ( float, float )
				辺と辺の長さを基に、辺の長さを求める。
		
		・Get Average_ In Array ( T *[], int )
				【テンプレート関数】配列の要素から平均を求める。

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

#include "Common.h"
#include <math.h>

// ============================================================================= Init All Field
// -----------------------------------------------------------------
//
//					全てのフィールドを初期化する関数
//
// -----------------------------------------------------------------
int InitAllField()
{
	g_gameSpeed	= 1.0f ;					// --- ゲームスピード
	g_gameMode	= e_Mode_Blank ;			// --- ゲームモード

	// --- 背景セット
	MV1SetTextureGraphHandle( g_mv1Handle[ e_Model_SkyDom ].modelHandle, 0, g_graphHandle[ e_Image_Skysun ].handle, FALSE ) ;

	SetAllSoundVolume100() ;								// --- ボリューム 100セット

	// ----------------------------------
	//			プレイヤーの初期化
	// ----------------------------------
	for ( int i = 0 ; i < 2 ; i++ )
	{
		g_Player[ i ].initStates_Player() ;								// --- ステータス初期化
		g_Player[ i ].size = SetVector( 1.0f ) ;						// --- サイズセット

		MV1SetOpacityRate( g_Player[ i ].modelHandle , 1.0f ) ;			// --- 不透明度セット
	}

	for ( int i = 0 ; i < e_TimerTotal ; i++ )
		Time[ i ].ResetTimer() ;

	// ----------------------------------
	//			 UIの初期化
	// ----------------------------------
	for ( int i = 0 ; i < 2 ; i++ )
	{
		UI_DGauge_ID[ i ].initStates() ;
	}

	UI_SChange.initStates() ;

	// ----------------------------------
	//		足場、ドットを初期化する
	// ----------------------------------
	Map.initStates() ;

	for ( int i = 0 ; i < (ELE_X_1 * ELE_Y_1) ; i++ )
	{
		Dot1stg[ i ].DotInit() ;
		Scaf1[ i ].initStates() ;
	}
	for ( int i = 0 ; i < (ELE_X_2 * ELE_Y_2) ; i++ )
	{
		Dot2stg_t[ i ].DotInit() ;
		Scaf2_t[ i ].initStates() ;
	}
	for ( int i = 0 ; i < (ELE_X_2 * ELE_Y_2) ; i++ )
	{
		Dot2stg_u[ i ].DotInit() ;
		Scaf2_u[ i ].initStates() ;
	}
	for ( int i = 0 ; i < (ELE_X_3 * ELE_Y_3) ; i++ )
	{
		Dot3stg[ i ].DotInit() ;
		Scaf3[ i ].initStates() ;
	}

	return 0 ;
}

// ============================================================================= Debug Loop
// -----------------------------------------------------------------
//
//			デバッグ用。"F1"を押すことでデバッグメニューが開く。
//
//					成功で1、引き分けで-1を帰す。
//
// -----------------------------------------------------------------
//			・引数１ - この関数の処理を使用するかのフラグ
// -----------------------------------------------------------------
int DebugLoop( BOOL isUse )
{
	if ( !isUse )
		return -1 ;

	int selectMax = 6 ;

	// --- F1キープッシュでデバッグメニュー描画
	if ( g_keyBuf[ KEY_INPUT_F1 ] == 1 )
		g_debInfo.dispBar ^= 0x01 ;

	if ( g_debInfo.dispBar )
	{
		int pInterval = 15 ;		// --- 長押しの有効間隔

		// --- プレイヤーのアクション(?)
		if ( (g_keyBuf[ KEY_INPUT_DOWN ] == 1) || (g_keyBuf[ KEY_INPUT_DOWN ] % pInterval == (pInterval - 1)) )
			CheckLimitAndPlus( CLAPMODE_BELOW, &g_debInfo.nowSelect, 1, selectMax, 0 ) ;
		if ( (g_keyBuf[ KEY_INPUT_UP ] == 1) || (g_keyBuf[ KEY_INPUT_UP ] % pInterval == (pInterval - 1)) )
			CheckLimitAndPlus( CLAPMODE_EXCEED, &g_debInfo.nowSelect, -1, 0, selectMax ) ;

		// --- 決定でモードセット
		if ( g_keyBuf[ KEY_INPUT_RETURN ] == 1 )
		{
			int otMode = g_gameMode ;

			if ( g_debInfo.nowSelect == 0 )			// --- タイトル
				otMode = e_Mode_Title_Init ;
			if ( g_debInfo.nowSelect == 1 )			// --- キャラ選択
				otMode = e_Mode_CharaSelect_Init ;
			if ( g_debInfo.nowSelect == 2 )			// --- ゲーム_1ステージ
			{
				otMode = e_Mode_Game_Init ;
				g_debInfo.mapSwitch = 1 ;
			}
			if ( g_debInfo.nowSelect == 3 )			// --- ゲーム_2ステージ
			{
				otMode = e_Mode_Game_Init ;
				g_debInfo.mapSwitch = 2 ;
			}
			if ( g_debInfo.nowSelect == 4 )			// --- ゲーム_3ステージ
			{
				otMode = e_Mode_Game_Init ;
				g_debInfo.mapSwitch = 3 ;
			}
			if ( g_debInfo.nowSelect == 5 )			// --- リザルト
				otMode = e_Mode_Result_Init ;
			if ( g_debInfo.nowSelect == 6 )			// --- カメラモード
				g_debInfo.isCameramode ^= 0x01 ;

			g_gameMode = otMode ;
		}

		// --- 枠
		SetDrawBlendMode( DX_BLENDMODE_ALPHA, 160 ) ;
		DrawBox( g_winSize.x / 4 * 3, 0, g_winSize.x, g_winSize.y / 2, GetColor( 0xff, 0xff, 0xff ), TRUE ) ;
		SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;
	
		DrawBox( g_winSize.x / 4 * 3, 0, g_winSize.x, g_winSize.y / 2, GetColor( 0x00, 0x00, 0x00 ), FALSE ) ;

		// --- 文字の内側におくやつ
		if ( !g_debInfo.isCameramode )
		{	
			DrawBox( g_winSize.x / 4 * 3 + 30, (g_winSize.y / 2) / 8 * 7, 
					g_winSize.x - 30, ((g_winSize.y / 2) / 8 * 7) + 30, GetColor( 0xdd, 0xdd, 0xdd ), TRUE ) ;
		}
		else
		{
			DrawBox( g_winSize.x / 4 * 3 + 30, (g_winSize.y / 2) / 8 * 7, 
					g_winSize.x - 30, ((g_winSize.y / 2) / 8 * 7) + 30, GetColor( 0x44, 0x44, 0x44 ), TRUE ) ;
		}

		// --- 選択肢
		DrawString( g_winSize.x / 4 * 3 + 30, (g_winSize.y / 2) / 8 * 1,	"e_gameMode_Title",			GetColor( 0x22, 0x22, 0x22 ) ) ;
		DrawString( g_winSize.x / 4 * 3 + 30, (g_winSize.y / 2) / 8 * 2,	"e_gameMode_CharaSelect",	GetColor( 0x22, 0x22, 0x22 ) ) ;
		DrawString( g_winSize.x / 5 * 4 + 30, (g_winSize.y / 2) / 8 * 3,	"e_gameMode_Game_1",		GetColor( 0x22, 0x22, 0x22 ) ) ;
		DrawString( g_winSize.x / 5 * 4 + 30, (g_winSize.y / 2) / 8 * 4,	"e_gameMode_Game_2",		GetColor( 0x22, 0x22, 0x22 ) ) ;
		DrawString( g_winSize.x / 5 * 4 + 30, (g_winSize.y / 2) / 8 * 5,	"e_gameMode_Game_3",		GetColor( 0x22, 0x22, 0x22 ) ) ;
		DrawString( g_winSize.x / 4 * 3 + 30, (g_winSize.y / 2) / 8 * 6,	"e_gameMode_Result",		GetColor( 0x22, 0x22, 0x22 ) ) ;
		DrawString( g_winSize.x / 4 * 3 + 30, (g_winSize.y / 2) / 8 * 7,	" --- CameraMode --- ",		GetColor( 0xff, 0x00, 0x00 ) ) ;

		// --- カーソル
		DrawBox( g_winSize.x / 4 * 3 + 30, (g_winSize.y / 2) / 8 * (g_debInfo.nowSelect + 1), 
					g_winSize.x - 30, ((g_winSize.y / 2) / 8 * (g_debInfo.nowSelect + 1)) + 30, GetColor( 0xff, 0x22, 0x22 ), FALSE ) ;
	}

	// --- カメラモードでの処理
	if ( g_debInfo.isCameramode )
	{
		float	movePow = 20.0f ;

		// --- WASD -> 位置移動
		if ( !g_debInfo.dispBar )
		{
			if ( g_keyBuf[ KEY_INPUT_W ] > 0 )
				g_camera.pos.y += movePow ;
			if ( g_keyBuf[ KEY_INPUT_S ] > 0 )
				g_camera.pos.y -= movePow ;
			if ( g_keyBuf[ KEY_INPUT_A ] > 0 )
				g_camera.pos.x -= movePow ;
			if ( g_keyBuf[ KEY_INPUT_D ] > 0 )
				g_camera.pos.x += movePow ;

			// --- 左右上下 -> 注視点移動
			if ( g_keyBuf[ KEY_INPUT_UP ] > 0 )
				g_camera.lookPos.y += movePow ;
			if ( g_keyBuf[ KEY_INPUT_DOWN ] > 0 )
				g_camera.lookPos.y -= movePow ;
			if ( g_keyBuf[ KEY_INPUT_LEFT ] > 0 )
				g_camera.lookPos.x -= movePow ;
			if ( g_keyBuf[ KEY_INPUT_RIGHT ] > 0 )
				g_camera.lookPos.x += movePow ;

			// --- ZX -> Z位置移動
			if ( g_keyBuf[ KEY_INPUT_Z ] > 0 )
				g_camera.pos.z += movePow ;
			if ( g_keyBuf[ KEY_INPUT_X ] > 0 )
				g_camera.pos.z -= movePow ;

			// --- NM -> Z注視点移動
			if ( g_keyBuf[ KEY_INPUT_N ] > 0 )
				g_camera.lookPos.z -= movePow ;
			if ( g_keyBuf[ KEY_INPUT_M ] > 0 )
				g_camera.lookPos.z += movePow ;
		}

		// --- 枠
		SetDrawBlendMode( DX_BLENDMODE_ALPHA, 160 ) ;
		DrawBox( 0, 0, 280, 220, GetColor( 0xff, 0xff, 0xff ), TRUE ) ;
		SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;
	
		DrawBox( 0, 0, 280, 220, GetColor( 0x00, 0x00, 0x00 ), FALSE ) ;

		// --- 文字
		DrawString( 20, 20, " 【Ａ】【Ｄ】- 位置X", GetColor( 0x22, 0x22, 0x22 ) ) ;
		DrawString( 20, 50, " 【Ｗ】【Ｓ】- 位置Y", GetColor( 0x22, 0x22, 0x22 ) ) ;
		DrawString( 20, 80, " 【Ｚ】【Ｘ】- 位置Z", GetColor( 0x22, 0x22, 0x22 ) ) ;

		DrawString( 20, 120, " 【←】【→】- 注視点X", GetColor( 0x22, 0x22, 0x22 ) ) ;
		DrawString( 20, 150, " 【↑】【下】- 注視点Y", GetColor( 0x22, 0x22, 0x22 ) ) ;
		DrawString( 20, 180, " 【Ｎ】【Ｍ】- 注視点Z", GetColor( 0x22, 0x22, 0x22 ) ) ;

	}

	return 0 ;
}

// ============================================================================= Start FPS
// -----------------------------------------------------------------
//
//	現在のFPS( Frame Percent Second )を描画する為のセットアップ
//
//		DrawFPS() を使用する前にこれを必ず呼び出してください。
//
// -----------------------------------------------------------------
void StartFPS()
{
	g_debInfo.fps.Time = GetNowHiPerformanceCount() ;	// --- システム時間を取得しておく

	g_debInfo.fps.DeltaTime = 0.000001f ;				// ---  最初の経過時間は仮に0.000001f 秒にしておく

	// FPS計測関係の初期化
	g_debInfo.fps.FPSCheckTime = GetNowHiPerformanceCount() ;
	g_debInfo.fps.FPS = 0 ;
	g_debInfo.fps.FPSCounter = 0 ;

	g_debInfo.fps.isSetupOK = TRUE ;
}

// ============================================================================= Draw FPS
// -----------------------------------------------------------------
//
//			現在のFPS( Frame Percent Second )を描画する関数
//
// -----------------------------------------------------------------
//					・引数１ - 描画座標（Ｘ）
//					・引数２ - 描画座標（Ｙ）
// -----------------------------------------------------------------
void DrawFPS( int arg_pos_x, int arg_pos_y )
{
	// --- セットアップがされていない場合、即終了
	if ( !g_debInfo.fps.isSetupOK )
		return ;

	// FPSの描画
	DrawFormatString( arg_pos_x, arg_pos_y, GetColor( 0x00, 0x00, 0x00 ), "FPS:%d", g_debInfo.fps.FPS ) ;

	g_debInfo.fps.NowTime = GetNowHiPerformanceCount() ;

	// 前回取得した時間からの経過時間を秒に変換してセット
	// ( GetNowHiPerformanceCount で取得できる値はマイクロ秒単位なので 1000000 で割ることで秒単位になる )
	g_debInfo.fps.DeltaTime = ( g_debInfo.fps.NowTime - g_debInfo.fps.Time ) / 1000000.0f ;

	// 今回取得した時間を保存
	g_debInfo.fps.Time = g_debInfo.fps.NowTime ;

	// FPS関係の処理( 1秒経過する間に実行されたメインループの回数を FPS とする )
	g_debInfo.fps.FPSCounter++ ;
	if ( g_debInfo.fps.NowTime - g_debInfo.fps.FPSCheckTime > 1000000 )
	{
		g_debInfo.fps.FPS			= g_debInfo.fps.FPSCounter ;
		g_debInfo.fps.FPSCounter	= 0 ;
		g_debInfo.fps.FPSCheckTime	= g_debInfo.fps.NowTime ;
	}

}

// ============================================================================= Get New Obj ID
// -----------------------------------------------------------------
//
//	どのオブジェクトのObjIDとも被らない、新たなObjIDを生成する関数
//						帰り値が -1 だとエラー。
//
// -----------------------------------------------------------------
int GetNewObjID()
{
	int retID = -1 ;

	for ( int i = 0 ; i < 2 ; i++ )
		g_Player[i].getObjID() ;

	/* --------------------	//
	//						//
	//		 作りかけ		//
	//						//
	// --------------------	//
				‖
			   ‖
			  ‖			
	*/

	return retID ;
}

// ============================================================================= Set Win Lose Info
// -----------------------------------------------------------------
//
//	  プレイヤー同士の勝敗状況を確認し、フィールド「isWin_now」
//						  にセットする関数
//
//					成功で1、引き分けで-1を帰す。
//
// -----------------------------------------------------------------
int SetWinLoseInfo()
{
	// =================================================
	// --- 落とした回数をチェックし、この時点で差があるなら即終了
	if ( g_Player[0].pushscore < g_Player[1].pushscore )
	{
		g_Player[0].isWin_now = TRUE ;
		g_Player[1].isWin_now = FALSE ;

		return 1 ;
	}
	else if ( g_Player[0].pushscore > g_Player[1].pushscore )
	{
		g_Player[0].isWin_now = FALSE ;
		g_Player[1].isWin_now = TRUE ;

		return 1 ;
	}

	// =================================================
	// --- もし落とした数が同じであれば、スコアの数を見る
	if ( g_Player[0].dotscore > g_Player[1].dotscore )
	{
		g_Player[0].isWin_now = TRUE ;
		g_Player[1].isWin_now = FALSE ;

		return 1 ;
	}
	else if ( g_Player[0].dotscore < g_Player[1].dotscore )
	{
		g_Player[0].isWin_now = FALSE ;
		g_Player[1].isWin_now = TRUE ;

		return 1 ;
	}

	// =================================================
	// --- 完全に引き分けであれば、数値は変えない。
	return -1 ;
}

// ============================================================================= Load Gauge Disp_ Fill OK
// -----------------------------------------------------------------
//
//		ロード完了分の数値を足しこみ、ゲージを即描画する関数
//					( ロード画面専用関数 )
//
// -----------------------------------------------------------------
//			・引数１ - 読み込み完了分の足しこみ数値
// -----------------------------------------------------------------
void LoadGaugeDisp_FillOK( int arg_OKnum )
{
	int		 percent ;						// --- ロード完了の％表示		
	c_fPoint graphSize ;					// --- ロードゲージの表示サイズ(切り取り地点)
	
	g_loadCnt.now += arg_OKnum ;			// --- ロード完了分の数値をインクリメント

	graphSize.x = 1800.0f * (g_loadCnt.now / g_loadCnt.max[0]) ;
	graphSize.y = 150 ;

	percent		= (int)(100 * (g_loadCnt.now / g_loadCnt.max[0])) ;

	// ---------------
	// --- 描画部 
	// ---------------
	DrawRectGraph( 60, 880, 0, 0, (int)graphSize.x, (int)graphSize.y, g_graphHandle_LoadView[ e_imgLoadView_Gauge ].handle, TRUE ) ;

	ScreenFlip() ;		// --- すぐ画面に描画
}

// ============================================================================= Gp Update Key
// -----------------------------------------------------------------
//
//		キー入力のチェック、及びその押下時間を加算する関数
//
//		( 例 )	Aキーを30フレーム分押し続けると
//					g_keyBuf[ KEY_INPUT_A ] -> 30
//
//				Aキーを離すと
//					g_keyBuf[ KEY_INPUT_A ] -> 0
//
//		( 例 )	押した最初の瞬間のみ判定したい・・・
//					if ( g_keyBuf[ KEY_INPUT_A ] == 1 )
//
//				押されている限り処理をさせ続けたい・・・
//					if ( g_keyBuf[ KEY_INPUT_A ] >= 1 )
//
// -----------------------------------------------------------------
int GpUpdateKey()
{
    char tmpKey[256] ;				// --- 現在のキーの入力状態を格納する
    GetHitKeyStateAll( tmpKey ) ;	// --- 全てのキーの入力状態を得る

	for( int i = 0 ; i < 256 ; i++ )
	{ 
		// i番のキーコードに対応するキーが押されていたら
		if( tmpKey[i] != 0 )
			g_keyBuf[i]++ ;		// --- 加算処理を行う

		// 押されていなければ
		else
			g_keyBuf[i] = 0 ;	// --- 0にする
    }

    return 0 ;
}

// ============================================================================= Get Graph Size And OffSet
// -----------------------------------------------------------------
//
//				画像のサイズ、オフセットを取得する。
//			オフセットは中心になる。(カスタマイズは好みで)
//		チップサイズは登録できないので、直接登録する必要がある。
//
//					成功で1以上, 失敗で-1を帰す。
//
// -----------------------------------------------------------------
//				・引数１ - 画像構造体 ( ポインタ渡し )
// -----------------------------------------------------------------
int GetGraphSizeAndOffSet( GRAPH_INFO* arg_g )
{
	int otSize_x, otSize_y ;
	GetGraphSize( arg_g->handle, &otSize_x, &otSize_y ) ;	// --- 画像サイズ取得

	arg_g->fullSize.x = otSize_x ;				// --- 画像サイズセット
	arg_g->fullSize.y = otSize_y ;				// --- 画像サイズセット

	arg_g->offSet.x = arg_g->fullSize.x / 2 ;	// --- オフセットのセット
	arg_g->offSet.y = arg_g->fullSize.y / 2 ;	// --- オフセットのセット

	return 1 ;
}

// ============================================================================= Get Anim Easy
// -----------------------------------------------------------------
//
//							アニメのゲッター
//
//					成功でTRUE, 失敗でFALSEを帰す。
//	この関数を経て取得した各種ハンドルは、オブジェクトに入れることで
//						初めて使えるようになる。
//
// -----------------------------------------------------------------
//				・引数１ - 格納構造体 ( ポインタ渡し )
//				・引数２ - アクションNo
//				・引数３ - ファイルパス( TEXTマクロ必須 )
// -----------------------------------------------------------------
int GetAnimEasy( MODEL_ANIM_INFO *arg_MAInfo, const int arg_action, const TCHAR *FileName, const int arg_No )
{
	int oneTime_attachIdx ;		// --- 一時的なアタッチ情報格納変数

	// ------------------------------------
	// --- アニメーション使用フラグ、ＯＮ
	// ------------------------------------
	arg_MAInfo->animCount++ ;

	// ---------------------------
	// --- まずモデルを読み込む
	// ---------------------------
	arg_MAInfo->anim[ arg_action ].handle = MV1LoadModel( FileName ) ;

	// --- 入らなかった場合、失敗判定
	if( arg_MAInfo->anim[ arg_action ].handle == -1 )
		return FALSE ;

	// ---------------------------
	// --- 次にアニメを読み込む
	// ---------------------------
	// --- アタッチINDEXを取得( モデルとアニメーションを結び付ける為に使用する )
	oneTime_attachIdx = MV1AttachAnim( arg_MAInfo->modelHandle, 0, arg_MAInfo->anim[ arg_action ].handle ) ;

	// --- 総時間を取得し、デタッチする( しておかないと死んでしまいます )
	arg_MAInfo->anim[ arg_action ].animTotal = MV1GetAttachAnimTotalTime( arg_MAInfo->modelHandle, oneTime_attachIdx ) ;
	MV1DetachAnim( arg_MAInfo->modelHandle, oneTime_attachIdx ) ;

	// アニメーションして動いてもその場で動いてるような状態にセットする。
//	arg_MAInfo->rootFlm = MV1SearchFrame( arg_MAInfo->anim[ arg_action ].handle, "foot_R_end" ) ;
//	MV1SetFrameUserLocalMatrix( arg_MAInfo->anim[ arg_action ].handle, arg_MAInfo->rootFlm, MGetIdent() ) ;

	return TRUE ;
}

// ============================================================================= Anim Copy To
// -----------------------------------------------------------------
//
//							アニメ情報のコピー
//
//					成功でTRUE, 失敗でFALSEを帰す。
//		同じアニメーションを読み込むと遅くなってしまうので、
//					これを使って短縮化できる。
//
// -----------------------------------------------------------------
//				・引数１ - コピー先格納構造体 ( ポインタ渡し )
//				・引数２ - アクションNo
//				・引数３ - コピー元のハンドル
// -----------------------------------------------------------------
int AnimCopyTo( MODEL_ANIM_INFO *arg_MAInfo, const int arg_action, const MODEL_ANIM_INFO *arg_copyMAInfo, const int arg_No )
{
	int oneTime_attachIdx ;		// --- 一時的なアタッチ情報格納変数

	// ------------------------------------
	// --- アニメーション使用フラグ、ＯＮ
	// ------------------------------------
	arg_MAInfo->animCount++ ;

	arg_MAInfo->anim[ arg_action ].handle = arg_copyMAInfo->anim[ arg_action ].handle ;

	// ---------------------------
	// --- 次にアニメを読み込む
	// ---------------------------
	// --- アタッチINDEXを取得( モデルとアニメーションを結び付ける為に使用する )
	oneTime_attachIdx = MV1AttachAnim( arg_MAInfo->modelHandle, 0, arg_MAInfo->anim[ arg_action ].handle ) ;

	// --- 総時間を取得し、デタッチする( しておかないと死んでしまいます )
	arg_MAInfo->anim[ arg_action ].animTotal = MV1GetAttachAnimTotalTime( arg_MAInfo->modelHandle, oneTime_attachIdx ) ;
	MV1DetachAnim( arg_MAInfo->modelHandle, oneTime_attachIdx ) ;

	// アニメーションして動いてもその場で動いてるような状態にセットする。
	MV1SetFrameUserLocalMatrix( arg_MAInfo->anim[ arg_action ].handle, arg_MAInfo->rootFlm, MGetIdent() ) ;

	return TRUE ;
}

// ============================================================================= Set All Sound Volume 100
// -----------------------------------------------------------------
//
//		全てのサウンドハンドルのボリュームを100にする関数
//
// -----------------------------------------------------------------
int SetAllSoundVolume100()
{
	SOUND_INFO *otSP = &g_soundHandle[0] ;
	for ( int i = 0 ; i < e_Sound_Total ; i++ )
	{
		SetSoundVolumeNum( 100, otSP ) ;				// --- ボリューム 100セット
		otSP++ ;
	}

	return 1 ;
}

// ============================================================================= Set Sound Volume Plus Num
// -----------------------------------------------------------------
//
//	引数で指定した数値をサウンドハンドルのボリュームに加算する関数
//						帰り値 1 で成功判定
//						帰り値 0 で失敗判定
//
// -----------------------------------------------------------------
//				第一引数	: 加算したい数値
//				第一引数	: サウンドハンドル
// -----------------------------------------------------------------
int SetSoundVolumePlusNum( int a_plusNum, SOUND_INFO *a_sHandle )
{
	int newVolume = a_sHandle->volume + a_plusNum ;

	// --- ボリューム数値は0 〜 100 の範囲内に留める
	if ( !(newVolume >= 0 && newVolume <= 100) )
		return 0 ;

	a_sHandle->volume = newVolume ;

	ChangeVolumeSoundMem( (255 * a_sHandle->volume / 100), a_sHandle->soundHandle ) ;

	return 1 ;
}

// ============================================================================= Set Sound Volume Num
// -----------------------------------------------------------------
//
//	引数で指定した数値をサウンドハンドルのボリュームにセットする関数
//						帰り値 1 で成功判定
//						帰り値 0 で失敗判定
//
// -----------------------------------------------------------------
//				第一引数	: ボリューム数値( 0 〜 100 )
//				第一引数	: サウンドハンドル
// -----------------------------------------------------------------
int SetSoundVolumeNum( int a_volNum, SOUND_INFO *a_sHandle )
{
	// --- ボリューム数値は0 〜 100 の範囲内に留める
	if ( !(a_volNum >= 0 && a_volNum <= 100) )
		return 0 ;

	a_sHandle->volume = a_volNum ;

	ChangeVolumeSoundMem( (255 * a_sHandle->volume / 100), a_sHandle->soundHandle ) ;

	return 1 ;
}

// ============================================================================= Count Array In Bool
// -----------------------------------------------------------------
//
//		配列内のBOOL変数がいくつ TRUEであるか調べる。
//		第三引数でFALSEを指定すれば、FALSEを見ることもできる。
//
//						帰り値はその個数
//
// -----------------------------------------------------------------
//	第一引数	: BOOL型の配列
//	第二引数	: 調べる内容 ( デフォルト - TRUE )
// -----------------------------------------------------------------
int CountArrayInBool( BOOL arg_array[], BOOL arg_checkBool )
{
	int maxLoop	= sizeof( arg_array ) / sizeof( arg_array[0] ) ;	// --- 要素数を求める
	int i		= 0 ;												// --- ループ用( 見れば分かるね )
	int retCnt	= 0; 

	while ( i < maxLoop )
	{
		if ( arg_array[ i ] == arg_checkBool )
			retCnt++ ;

		i++ ;
	}

	return retCnt ;
}

// ============================================================================= Check Limit And Plus
// -----------------------------------------------------------------
//
//		AにBを足した数が、Cを超過しない場合加算を行う。
//		超過する場合は第5引数の数値をAにセットする。
//
//	  	  帰り値が0で未加工、1で加工済み、-1で失敗。
//
// -----------------------------------------------------------------
//	
//			この関数、結局なにができるかというと、
//
//		if ( now + plus < max )
//			now += plus ;
//		else
//			now = max ;
//
//			という処理を1行にできるのだ。
//
//	(例) CheckLimitAndPlus( CLAPMODE_EXCEED, 10, 20, 25, 0 ) ;
//			→	10 + 20 を行う。結果は「30」。
//				第四引数で指定した数値「25」を超過しているので
//				結果として「0」が入る。
//	
//				もし第四引数が「35」なら結果は「30」になる。
//				(未超過である為)
//
// -----------------------------------------------------------------
//	第一引数	: モード( CLAPMODE_EXCEED, CLAPMODE_BELOW )
//	第二引数	: 実際に数値をセットする変数( ポインタ渡し )
//	第三引数	: 加算数値
//	第四引数	: 第二、三引数を計算した際に見たい最大値、又は最小値
//	第五引数	: 第四引数を超過したときにセットする数値
// -----------------------------------------------------------------
template <class T>
int CheckLimitAndPlus( int mode, T *useNum, T plusNum, T MaxMinCheck, T overSetNum )
{
	int iRet = 0 ;		// --- 帰り値用

	switch ( mode )
	{
		// --------------------------------------
		//　最大値を "下回った" ら規定値にセット
		// --------------------------------------
		case CLAPMODE_EXCEED :
			if ( MaxMinCheck <= *useNum + plusNum )
			{
				*useNum += plusNum ;
				iRet = 1 ;
			}
			else
				*useNum = overSetNum ;

			break ;

		// --------------------------------------
		//　最小値を "上回った" ら規定値にセット
		// --------------------------------------
		case CLAPMODE_BELOW :
			if ( MaxMinCheck >= *useNum + plusNum )
			{
				*useNum += plusNum ;
				iRet = 1 ;
			}
			else
				*useNum = overSetNum ;

			break ;

		// --------------------------------------
		//　？？？？
		// --------------------------------------
		default :
			iRet = -1 ;

			break ;
	}

	return iRet ;
}
template int CheckLimitAndPlus<int>( int mode, int *useNum, int plusNum, int MaxMinCheck, int overSetNum ) ;
template int CheckLimitAndPlus<float>( int mode, float *useNum, float plusNum, float MaxMinCheck, float overSetNum ) ;

// ============================================================================= Set Vector
// -----------------------------------------------------------------
//
//		引数で指定した数値を全データに格納したVECTORを帰す関数。
//
// -----------------------------------------------------------------
//				第一引数	: セットしたい数値
// -----------------------------------------------------------------
VECTOR SetVector( float arg_num )
{
	// --- 全て規定の数値にする
	return VGet( arg_num, arg_num, arg_num ) ;
}

// ============================================================================= Reset Vector
// -----------------------------------------------------------------
//
//			受け取ったVECTOR構造体の変数を初期化する
//
//					参照渡しで行うものとする。
//
// -----------------------------------------------------------------
//			第一引数	: 全ての要素を0にしたいVECTOR型変数
// -----------------------------------------------------------------
void ResetVector( VECTOR &arg_Vec )
{
	// --- 全て0にする
	arg_Vec = VGet( 0.0f, 0.0f, 0.0f ) ;
}

// ============================================================================= Get Triangle Side
// -----------------------------------------------------------------
//
//		 三角形のサイン、コスの間の辺の長さを手に入れる関数
//					帰り値はその算出結果となる
//
//		   
//			・
//			┃＼
//			┃  ＼		←要はここの長さを計算で求めてくれる。
//	  side1 ┃    ＼
//			┃      ＼
//			・━━━━・
//				side2
//
// -----------------------------------------------------------------
//						第一引数	: 辺の長さ_1
//						第二引数	: 辺の長さ_2
// -----------------------------------------------------------------
float GetTriangleSide( const float side_Length_1 , const float side_Length_2 )
{
	float result ;

	// --- 両方0なら0を帰す
	if ( (side_Length_1 == 0) && (side_Length_2 == 0) )
		return 0 ;

	result = (float)(sqrtl( powf( side_Length_1, 2.0 ) + powf( side_Length_2, 2.0 ) ));

	return result ;
}

// ============================================================================= Get Average In Array
// -----------------------------------------------------------------
//
//				 要素全ての平均値を求める関数
//					算出結果が帰り値となる
//		
// -----------------------------------------------------------------
//			第一引数	: 要素が入った配列のアドレス
//			第二引数	: 要素の個数
// -----------------------------------------------------------------
template<class T>
T GetAverage_InArray( const T *arg_Array , int arg_Num )
{
	T total = 0 ;		// --- 合計

	// --- 例外処理
	if ( arg_Num <= 0 )
		return 0 ;
	if ( arg_Num == 1 )
		return *arg_Array ;

	// --- 配列の中のものを、totalに全て足す
	for ( int i = 0 ; i < arg_Num ; i++ )
	{
		total += *arg_Array ;
		arg_Array++ ;
	}

	return (total / ( T )arg_Num) ;
}

template int GetAverage_InArray<int>( const int *arg_Array, int arg_Num ) ;				// --- int 対応
template float GetAverage_InArray<float>( const float *arg_Array, int arg_Num ) ;		// --- float 対応



