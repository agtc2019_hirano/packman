/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: TimerUI.h
	CREATE	: HIRANO
	
	------ Explanation of file ---------------------------------------------------------------------
       
	TimerUIクラスの宣言

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

class TimerUI : public UserInterface
{
	public :
		TimerUI( ) ;		// コンストラクタ
		~TimerUI( ) ;		// デストラクタ

		void initStates() ;			// --- ステータス初期

		void UIDraw( int ) ;		// ここでタイマーに合わせて時間表示
		int time ;					// 経過秒数入れる

		int time_beforeOneSec ;		// 一秒前のタイマー数値( 切り替わった瞬間を取得する為 )
		float changeSize ;			// 画像表示倍率
		float defaultSize ;			// 画像表示倍率_デフォルト

		int		redFlashLimit ;		// --- タイマーが赤く光りだす時間
} ;			


	