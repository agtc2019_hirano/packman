/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: EffectSetter.cpp
	CREATE	: NOGUCHI

	------ Explanation of file ---------------------------------------------------------------------
       
	エフェクトをセットする為の関数が揃っている。

			・EffectGenerator( VECTOR, int )
			
					-	エフェクトの初期情報セッター。
						呼び出し時の引数には座標とエフェクトIDをセットしよう。
	
			・Effect_EmptySearch()
		
					-	エフェクトの空き配列を探索する関数。	   
	
			・SetEffectInitInfo( Effect, Effect )
	
					-	エフェクトの情報をセットする関数。

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

#include "Common.h"
#include <math.h>

int Effect_EmptySearch() ;												// --- 空配列を探索する関数
void SetEffectInitInfo( Effect *setOut, Effect *setIn, int id ) ;		// --- セット情報を組み込む関数
void SetEffectInitInfo_2D( Effect *setOut, Effect *setIn, int id ) ;	// --- セット情報を組み込む関数（リザルト用に作成）

// ============================================================================= Effect Generator
// -----------------------------------------------------------------
//
//					エフェクトを生成する関数
//		正確にはエフェクトの初期情報を入れてあげる、というもの
//				(あとはEffectクラス内で処理を行う)
//
//					成功で0以上, 失敗で-1を帰す。
//
// -----------------------------------------------------------------
//				・引数１ - 出力座標
//				・引数２ - 発生エフェクトのＩＤ
//				・引数３ - 何かしらの情報( デフォルト - 0 )
// -----------------------------------------------------------------
int EffectGenerator( VECTOR arg_setPos, int arg_effectID, int arg_info )
{
	int			useNum = -1 ;		// --- 使用する枠の要素数
	int			objNum = -1 ;		// --- g_Objecct[] の使用要素数
	Effect		setEInfo  ;			// --- セットするエフェクトの情報
	Effect		*eP = &setEInfo ;	// --- ↑のポインタ

	eP->pos	= arg_setPos ;		// --- セット情報に座標を格納

	// --- 初期情報のみをセットしてあげる
	switch ( arg_effectID )
	{
		// --------------------------------------------------------------------------------------------- Effect_Star
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
		//			エフェクト_お星さま				//
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
		case e_Effect_Star :
			// loopNumは全てのエフェクトで使うわけではないのでブロック内限定にした。
			// このかぎかっこは記述ミスではなく意図的なものである。
			{	
				int loopNum = 12 ;					// --- 射出量(360度回転)
				int randomTan = rand() % 360 ;		// --- 射出角度(360度)

				for ( int i = 0 ; i < loopNum ; i++ )
				{
					useNum = Effect_EmptySearch() ;
					if ( useNum == -1 )
						break ;

					eP->efInfo.moveVec.x	= (float)(cos( (i * (360 / loopNum)) * 3.14 / 180)) * 5.0f ;
					eP->efInfo.moveVec.z	= (float)(sin( (i * (360 / loopNum)) * 3.14 / 180)) * 5.0f ;
					eP->efInfo.moveVec.y	= 10.0f ;

					eP->efInfo.size			= (float)(rand() % 60 + 4) ;

					eP->efInfo.rotate.z		= 0 ;
					eP->efInfo.aliveTime	= 20 ;

					eP->setHandle( e_Image_Effect_Star ) ;		// --- ハンドルセット

					SetEffectInitInfo( &g_effect[ useNum ], eP, arg_effectID ) ;		// --- 指定のEffect Objectに情報セット
				}
			}

			break ;

		// --------------------------------------------------------------------------------------------- Effect_Push_Star
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
		//	  エフェクト_プッシュ時の星エフェクト	//
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
		case e_Effect_Push_Star :
			{	
				// プッシュされているプレイヤーを探す。
				int		pNumber = -1 ;				// --- プレイヤーID
				float	pDir	= -1 ;				// --- プレイヤー角度
				
				for ( int i = 0 ; i < 2 ; i++ )
				{
					if ( e_pMode_Press ==  g_Player[i].getMode() )
						pNumber = i ;
				}

				// --- のけぞりプレイヤー未発見で強制終了
				if ( pNumber == -1 )
					break ;
				else
					pDir = g_Player[ pNumber ].dir.y ;

				int loopNum = 8 ;					// --- 射出量(360度回転)				
				for ( int i = 0 ; i < 360 ; i += (360 / loopNum) )
				{
					useNum = Effect_EmptySearch() ;
					if ( useNum == -1 )
						break ;

					switch ( (int)pDir )
					{
						// --- 手前方向
						case e_Down :
							eP->efInfo.moveVec.x	= (float)(cos( i * 3.14 / 180)) * 20.0f ;
							eP->efInfo.moveVec.z	= -5.0f ;

							break ;

						// --- 左向き( つまり右にエフェクトは向かう )
						case e_Left :
							eP->efInfo.moveVec.x	= 5.0f ;
							eP->efInfo.moveVec.z	= (float)(cos( i * 3.14 / 180)) * 20.0f ;

							break ;

						// --- 奥方向
						case e_Up :
							eP->efInfo.moveVec.x	= (float)(cos( i * 3.14 / 180)) * 20.0f ;
							eP->efInfo.moveVec.z	= 5.0f ;

							break ;

						// --- 右向き( つまり左にエフェクトは向かう )
						case e_Right :
							eP->efInfo.moveVec.x	= -5.0f ;
							eP->efInfo.moveVec.z	= (float)(cos( i * 3.14 / 180)) * 20.0f ;

							break ;
					}

					eP->efInfo.moveVec.y	= (float)(sin( i * 3.14 / 180)) * 20.0f ;
					eP->pos.y				+= 50.0f ;

					eP->efInfo.size			= (float)(rand() % 80 + 84) ;

					eP->efInfo.rotate.z		= 0 ;			// --- 回転率セット
					eP->efInfo.aliveTime	= 16 ;			// --- 生成時間セット
						
					eP->setHandle( e_Image_Effect_Star ) ;	// --- ハンドルセット

					SetEffectInitInfo( &g_effect[ useNum ], eP, arg_effectID ) ;		// --- 指定のEffect Objectに情報セット
				}
			}

			break ;

		case e_Effect_Super_Shine :
			{	
				// スーパー状態プレイヤーを探す。
				int		pNumber = -1 ;				// --- プレイヤーID
				float	pDir	= -1 ;				// --- プレイヤー角度
				
				for ( int i = 0 ; i < 2 ; i++ )
				{
					if ( g_Player[i].superflg == TRUE )
						pNumber = i ;
				}

				// --- スーパープレイヤー未発見で強制終了
					pDir = g_Player[ pNumber ].dir.y ;

				int loopNum = 6 ;					// --- 射出量(360度回転)				
				for ( int i = 30 ; i < 390 ; i += (360 / loopNum) )
				{
					useNum = Effect_EmptySearch() ;
					if ( useNum == -1 )
						break ;

					switch ( (int)pDir )
					{
						// --- 手前方向
						case e_Down :
							eP->pos.z -= 60.0f ;

							eP->efInfo.moveVec.x	= (float)(cos( i * 3.14 / 180)) * (float)(rand() % 20) ;
							eP->efInfo.moveVec.z	= (float)(rand() % 10) ;

							break ;

						// --- 左向き( つまり右にエフェクトは向かう )
						case e_Left :
							eP->pos.x -= 60.0f ;

							eP->efInfo.moveVec.x	= (float)(rand() % 10) ;
							eP->efInfo.moveVec.z	= (float)(cos( i * 3.14 / 180)) * (float)(rand() % 20) ;

							break ;

						// --- 奥方向
						case e_Up :
							eP->pos.z += 60.0f ;

							eP->efInfo.moveVec.x	= (float)(cos( i * 3.14 / 180)) * (float)(rand() % 20) ;
							eP->efInfo.moveVec.z	= -(float)(rand() % 10) ;

							break ;

						// --- 右向き( つまり左にエフェクトは向かう )
						case e_Right :
							eP->pos.x += 60.0f ;

							eP->efInfo.moveVec.x	= -(float)(rand() % 10) ;
							eP->efInfo.moveVec.z	= (float)(cos( i * 3.14 / 180)) * (float)(rand() % 20) ;

							break ;
					}

					eP->efInfo.moveVec.y	= (float)(sin( i * 3.14 / 180)) * (float)(rand() % 10) ;
					eP->pos.y				+= (float)( (rand() % 75) + 50 ) ;

					eP->efInfo.size			= (float)(rand() % 80 + 84) ;

					eP->efInfo.rotate.z		= (float)(rand() % 10) ;			// --- 回転率セット
					eP->efInfo.aliveTime	= (float)(rand() % 10) + 40 ;		// --- 生成時間セット
						
					eP->setHandle( e_Image_Effect_Shine ) ;	// --- ハンドルセット

					SetEffectInitInfo( &g_effect[ useNum ], eP, arg_effectID ) ;		// --- 指定のEffect Objectに情報セット
				}
			}

			break ;

		// --------------------------------------------------------------------------------- Effect_Drop_Coconuts
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
		//		エフェクト_落としたココナッツ		//
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
		case e_Effect_Drop_Coconuts :
			{
				// --- "arg_info" は落とすココナッツエフェクトの数
				int loopNum = arg_info ;

				for ( int i = 0 ; i < loopNum ; i++ )
				{
					// --- ZERO SEARCH
					useNum = Effect_EmptySearch() ;
					if ( useNum == -1 )
						break ;

					// --- ZERO SEARCH( 関数化はあとでします )
					for ( int j = OBJID_COCONUT ; j < OBJID_COCONUT + COCONUT_MAX ; j++ )
					{
						// --- 使用されていなかったらIDをそれにすうる
						if ( g_Object[ j ].modelHandle <= 0 )
						{
							objNum = j ;
							break ;
						}
					}

					if ( objNum == -1 )
						break ;

					eP->efInfo.moveVec.x	= (float)(cos( (i * (360 / loopNum)) * 3.14 / 180)) * 5.0f ;
					eP->efInfo.moveVec.z	= (float)(sin( (i * (360 / loopNum)) * 3.14 / 180)) * 5.0f ;
					eP->efInfo.moveVec.y	= 22.0f ;

					eP->efInfo.size			= 0.35f ;

					eP->efInfo.rotate.x		= (float)(rand() % 50) / 10 ;	// --- 回転率セット
					eP->efInfo.rotate.y		= (float)(rand() % 50) / 10 ;	// --- 回転率セット
					eP->efInfo.rotate.z		= (float)(rand() % 50) / 10 ;	// --- 回転率セット
					eP->efInfo.aliveTime	= 60 ;							// --- 生成時間セット

					eP->isUse3DModel		= TRUE ;						// --- 3DモデルOFF
					eP->setHandle( e_Model_Dot ) ;							// --- ハンドルセット

					eP->use3DModelID		= objNum ;						// --- ココナッツOBJID セット

					g_Object[ eP->use3DModelID ].dispFlg = TRUE ;																	// --- 可視化
					g_Object[ eP->use3DModelID ].setModelAndAnim( g_mv1Handle[ e_Model_Dot ], TRUE, VGet( 0.5f, 0.5f, 0.5f ) ) ;	// --- モデルセット

					SetEffectInitInfo( &g_effect[ useNum ], eP, arg_effectID ) ;		// --- 指定のEffect Objectに情報セット
				}
			}

			break ;

		// --------------------------------------------------------------------------------------------- Effect_Result_Coconuts
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
		//			リザルトココナッツ				//
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
		case e_Effect_Result_Coconuts :
			int loopNum = 5 ;					// --- 射出量(360度回転)

			for ( int i = 0 ; i < 360 ;  i += (360 / loopNum)  )
			{
				// --- 空き要素サーチ
				useNum = Effect_EmptySearch() ;
				if ( useNum == -1 )
					break ;

				eP->efInfo.moveVec.y	= -(float)((rand() % 20)+30) ;								// --- ｙ座標の移動値セット
				eP->ltSpot.y			= eP->pos.y + 50 ;											// --- ｙ座標値セット

				eP->efInfo.size			= (float)1 / (rand() % 3) ;									// --- サイズランダムセット

				eP->efInfo.moveVec.x	= (float)(sin( i * 3.14 / 180)) * (float)(rand() % 20) ;	// --- ｘ座標の移動値セット
				eP->ltSpot.x			= (float)( rand() % (int)eP->pos.x ) ;						// --- ｘ座標値セット
				eP->efInfo.rotate.x		= (float)(rand() % 1) ;										// --- ｘ方向に反転させるかTRUE・FALSE
				eP->efInfo.rotate.y		= (float)(rand() % 1) ;										// --- ｙ方向に反転させるかTRUE・FALSE
				eP->efInfo.rotate.z		= (float)(rand() % 5) ;										// --- 画像の回転値
				eP->efInfo.aliveTime	= 200 ;														// --- 表示時間

				eP->isUse2D				= TRUE ;					// --- 2D画面に表示

				// --- ランダムでココナッツ・ヤシの木の画像セット
				if ((rand() % 2) == 0 )
					eP->setHandle( e_Image_Effect_Yasinoki ) ;		// --- ヤシの木ハンドルセット
				else
					eP->setHandle( e_Image_Effect_Coconut ) ;		// --- ココナッツハンドルセット

				SetEffectInitInfo_2D( &g_effect[ useNum ], eP, arg_effectID ) ;		// --- 指定のEffect Objectに情報セット
			}

			break ;
	}

	return 0 ;
}

// ============================================================================= Effect Empty Search
// -----------------------------------------------------------------
//
//				エフェクト配列の空きを見つける関数
//
//			帰り値0以上で発見した要素、帰り値-1で未発見。
//
// -----------------------------------------------------------------
int Effect_EmptySearch()
{
	Effect	*oneTimeEP ;		// --- エフェクトポインタ

	for ( int i = 0 ; i < EFFECT_MAX ; i++ )
	{
		oneTimeEP = &g_effect[i] ;

		// --- 空いていたらその枠の番号を使用
		if ( !oneTimeEP->efInfo.aliveFlg )
			return i ;
	}

	// --- ここに来たということは未発見。帰り値 -1
	return -1 ;
}

// ============================================================================= Set Effect Init Info
// -----------------------------------------------------------------
//
//			エフェクトオブジェクトに初期情報をセットする関数
//
// -----------------------------------------------------------------
void SetEffectInitInfo( Effect *setOut, Effect *setIn, int arg_effectID )
{
	setOut->pos					= setIn->pos ;					// --- 座標セット
	setOut->efInfo.rotate		= setIn->efInfo.rotate ;		// --- 回転
	setOut->efInfo.moveVec		= setIn->efInfo.moveVec ;		// --- 初動ベクトルセット
	setOut->efInfo.aliveTime	= setIn->efInfo.aliveTime ;		// --- 生存時間セット
	setOut->efInfo.size			= setIn->efInfo.size ;			// --- 生存時間セット
	setOut->efInfo.aliveFlg		= TRUE ;						// --- 使用フラグON
	setOut->efInfo.dispFlg		= TRUE ;						// --- 表示フラグON
	setOut->isUse3DModel		= setIn->isUse3DModel ;			// --- 3Dモデル使用情報セット
	setOut->use3DModelID		= setIn->use3DModelID ;			// --- 3Dモデル_ID

	setOut->nowTime				= 0 ;							// --- 現在の時間セット
	setOut->setHandle( setIn->getHandle() ) ;					// --- 画像ハンドルの定数セット

	setOut->setEffectID( arg_effectID ) ;						// --- 使うエフェクトのＩＤをセット
}

// ============================================================================= Set Effect Init Info 2D
// -----------------------------------------------------------------
//
//			エフェクトオブジェクトに初期情報をセットする関数
//				リザルト時のココナッツなど2D座標に表示する
//
// -----------------------------------------------------------------
void SetEffectInitInfo_2D( Effect *setOut, Effect *setIn, int arg_effectID )
{
	setOut->ltSpot				= setIn->ltSpot ;				// --- 座標セット
	setOut->efInfo.rotate		= setIn->efInfo.rotate ;		// --- 回転
	setOut->efInfo.moveVec		= setIn->efInfo.moveVec ;		// --- 初動ベクトルセット
	setOut->efInfo.aliveTime	= setIn->efInfo.aliveTime ;		// --- 生存時間セット
	setOut->efInfo.size			= setIn->efInfo.size ;			// --- 生存時間セット
	setOut->efInfo.aliveFlg		= TRUE ;						// --- 使用フラグON
	setOut->efInfo.dispFlg		= TRUE ;						// --- 表示フラグON
	setOut->isUse2D				= setIn->isUse2D ;				// --- 2D画面表示使用情報セット
	setOut->isUse3DModel		= setIn->isUse3DModel ;			// --- 3Dモデル使用情報セット
	setOut->use3DModelID		= setIn->use3DModelID ;			// --- 3Dモデル_ID

	setOut->nowTime				= 0 ;							// --- 現在の時間セット
	setOut->setHandle( setIn->getHandle() ) ;					// --- 画像ハンドルの定数セット

	setOut->setEffectID( arg_effectID ) ;						// --- 使うエフェクトのＩＤをセット
}


