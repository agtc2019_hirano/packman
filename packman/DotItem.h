/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: DotItem.h
	
	------ Explanation of file ---------------------------------------------------------------------
       
	ドットアイテムクラスの宣言

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

class DotItem : public Item
{
	public :
		DotItem( ) ;						// コンストラクタ
		~DotItem( ) ;						// デストラクタ

		void DotInit() ;					// --- 初期化

		BOOL DotHitCheck() ;				// ヒットチェック
		int DotGenerate( int ) ;			// ドット生成
		int DotDelete( int ) ;				// ドット非表示
		int DotMapSelect() ;

		BOOL dotonly ;				// --- ドット描画判定

	private :
		// === メンバ関数
		int DotRevive() ;			// --- ドット復活

		// === メンバ変数
		BOOL dothitflg ;			// --- ドットヒット判定
		BOOL dotrevnot ;			// --- 復活不可フラグ
		int dotrevcnt ;				// --- ドット復活タイム
		int dotcngtim ;				// --- ドットチェンジタイム

		int nowmap ;				// --- マップ情報の取得用

} ;


	