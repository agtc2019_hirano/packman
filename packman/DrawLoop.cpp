/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: DrawLoop.cpp
	CREATE	: NOGUCHI

	------ Explanation of file ---------------------------------------------------------------------
       
	描画の全体的な流れを管理するクラス。
	Objectクラスの派生先であるオブジェクトなら、DrawModelEasy()を使用できる。
	モデルの描画を行うには
		「DrawModelEasy( &Pac ) ;」のように記述すればオーケー。

	ここではシューティングゲームの時に使用した「テーブルジャンプ」を扱っている。

		・Draw Loop				- 描画関連のメインループ。
		
		・Draw Func 〇〇		- DrawLoopで呼び出される関数。シーンごとに対応。
		
		・Draw Model Easy		- モデルの描画を行う。アニメーション非対応も可能。

	なお、引数の「isUIDisp」はその名の通りUIを描画するかしないかのフラグ。
	UIの影を描画させたくない為、DrawGraph()系の関数はこれがTRUEである時のみ行ってほしい。

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

#include "Common.h"

extern S_PLAYER_INPUTINFO player_PSInfo ;			// --- プレイヤーの入力情報
extern S_PLAYER_INPUTINFO *piP ;					// --- プレイヤーの入力情報

Object OTPac_cSel[2] ;

// ---------------------------------
//		テーブルジャンプ実装部
// ---------------------------------
void DrawFunc_Blank( BOOL isUIDisp ) ;				// --- ブランク				描画関数
void DrawFunc_TitleInit( BOOL isUIDisp ) ;			// --- タイトル_初期化		描画関数
void DrawFunc_Title( BOOL isUIDisp ) ;				// --- タイトル				描画関数
void DrawFunc_CharaSelectInit( BOOL isUIDisp ) ;	// --- キャラ選択_初期化	描画関数
void DrawFunc_CharaSelect( BOOL isUIDisp ) ;		// --- キャラ選択			描画関数
void DrawFunc_GameInit( BOOL isUIDisp ) ;			// --- ゲーム_初期化		描画関数
void DrawFunc_GameStart( BOOL isUIDisp ) ;			// --- ゲーム_スタート		描画関数
void DrawFunc_Game( BOOL isUIDisp ) ;				// --- ゲーム				描画関数
void DrawFunc_ResultInit( BOOL isUIDisp ) ;			// --- 結果発表_初期化		描画関数
void DrawFunc_Result( BOOL isUIDisp ) ;				// --- 結果発表				描画関数

void ( *DrawValues[] )( BOOL ) =	
	{	DrawFunc_Blank,
		DrawFunc_TitleInit,
		DrawFunc_Title,
		DrawFunc_CharaSelectInit,
		DrawFunc_CharaSelect,
		DrawFunc_GameInit,
		DrawFunc_GameStart,
		DrawFunc_Game,
		DrawFunc_ResultInit,
		DrawFunc_Result,
	} ;

// ============================================================================= Draw Loop
/* --------------------------------------------------------------

					全体の描画を管理する関数

 ---------------------------------------------------------------- */
int DrawLoop( BOOL isUIDisp )
{
	// --- 関数ポインタでシーンに応じた関数を呼び出す
	( *DrawValues[ g_gameMode ] )( isUIDisp ) ;

	return 0 ;
}

// ============================================================================================================================================== 【ブランク】
// -----------------------------------------------------------------
//
//					DrawLoop関数_ブランク
//
// -----------------------------------------------------------------
void DrawFunc_Blank( BOOL isUIDisp )
{

}

// ============================================================================================================================================== 【タイトル部】
// -----------------------------------------------------------------
//					DrawLoop関数_タイトル初期化
// -----------------------------------------------------------------
void DrawFunc_TitleInit( BOOL isUIDisp )
{

}

// -----------------------------------------------------------------
//
//						DrawLoop関数_タイトル
//
// -----------------------------------------------------------------
void DrawFunc_Title( BOOL isUIDisp )
{
	// --- タイトルロゴ表示
	c_fPoint rogoSize( 600, 300 ) ;								// --- もともとのサイズ
	c_fPoint newSize( rogoSize.x * 1.5f, rogoSize.y * 1.5f ) ;	// --- 新サイズ

	//DrawModelEasy( &g_Object[ OBJID_BEACH ] ) ;		// --- ビーチ
	//DrawModelEasy( &g_Object[ OBJID_NAMI ] ) ;		// --- 波
	//DrawModelEasy( &g_Object[ OBJID_BSEA ] ) ;		// --- ビーチの海

	// --- タイトルで使用オブジェクト表示
	for ( int i = OBJID_TITLEOB ; i < OBJID_TITLEOB + e_Title_Total ; i++ )
		DrawModelEasy( &g_Object[ i ] ) ;

	//DrawModelEasy( &g_Object[ OBJID_BEACH ] ) ;

	if ( isUIDisp )
	{
		// --- タイトル画像描画
		DrawGraph( 50, 30, g_graphHandle[ e_Image_Title ].handle, TRUE ) ;

		// --- [Enter To Start] 表示
		if ( piP->any_flg[0] )
			DrawRectGraph( g_winSize.x / 2 - 450, (int)g_winSize.y / 5 * 4, 0, 0, 900, 400, g_graphHandle[ e_Image_PushtoEnter ].handle, TRUE ) ;			// --- PushToEnter
		else
			DrawRectGraph( g_winSize.x / 2 - 450, (int)g_winSize.y / 5 * 4, 0, 400, 900, 400, g_graphHandle[ e_Image_PushtoEnter ].handle, TRUE ) ;

		// --- 押下フラグがONの時、暗転用のBoxを描画する
		if ( piP->title_PushFlg )
		{
			SetDrawBlendMode( DX_BLENDMODE_ALPHA, (int)( piP->setBlackCount * (255.0f / piP->setBlackCount_Max) ) ) ;
			DrawBox( 0, 0, g_winSize.x, g_winSize.y, GetColor( 0x00, 0x00, 0x00 ), TRUE ) ;	
			SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;
		}

	}

}

// ============================================================================================================================================== 【キャラ選択部】
// -----------------------------------------------------------------
//					DrawLoop関数_キャラ選択初期化
// -----------------------------------------------------------------
void DrawFunc_CharaSelectInit( BOOL isUIDisp )
{
	POINT iSize		= { 360, 360 } ;																// --- アイコンのサイズ( x, y )
	POINT iPos[3]	= { {(int)(WINDOWSIZE_W / 2 - iSize.x / 2 * 2.6),	WINDOWSIZE_H / 4 * 3},
						{(int)(WINDOWSIZE_W / 2),						WINDOWSIZE_H / 4 * 3},
						{(int)(WINDOWSIZE_W / 2 + iSize.x / 2 * 2.6),	WINDOWSIZE_H / 4 * 3}, } ;	// --- アイコンの座標

	// --- 画像ステータスセット
	piP->cSel_IconNum = 3 ;
	for ( int i = 0 ; i < piP->cSel_IconNum ; i++ )
	{
		piP->cSel_IconState[ i ].pos.x = iPos[ i ].x ;									// --- 座標
		piP->cSel_IconState[ i ].pos.y = iPos[ i ].y ;									// --- 座標

		piP->cSel_IconState[ i ].sizeChange.x = 100 ;									// --- 可変サイズ
		piP->cSel_IconState[ i ].sizeChange.y = 100 ;									// --- 可変サイズ
	}
}

// -----------------------------------------------------------------
//
//						DrawLoop関数_キャラ選択
//
// -----------------------------------------------------------------
void DrawFunc_CharaSelect( BOOL isUIDisp )
{
	// --------------------------
	// --- 雲とキャラの描画
	// --------------------------
	for ( char i = 0 ; i < 2 ; i++ )
	{
		DrawModelEasy( &g_Player[ i ] ) ;
		DrawModelEasy( &g_Object[ OBJID_CLOUD + i ] ) ;
	}

	if ( isUIDisp )
	{
		// --------------------------
		// --- アイコンステータス
		// --------------------------
		// アイコンの座標、サイズセットは↑のInitで行っているよ

		float newIconSize[3] ;	// --- サイズ変更分をプラスした分のサイズ
		for ( int i = 0 ; i < piP->cSel_IconNum ; i++ )
		{
			if ( (piP->cSel_IconState[ i ].sizeChange.x == 100) && (piP->cSel_IconState[ i ].sizeChange.y == 100) )
			{
				newIconSize[ i ] = 1.0f ;
				continue ;
			}

			newIconSize[ i ] = (float)piP->cSel_IconState[ i ].sizeChange.x / 100.0f ;
		}

		POINT newIconPos[3] ;	// --- オフセットをプラスした分の座標	
		for ( int i = 0 ; i < piP->cSel_IconNum ; i++ )
		{
			newIconPos[ i ].x =	piP->cSel_IconState[ i ].pos.x ;
			newIconPos[ i ].y =	piP->cSel_IconState[ i ].pos.y ;
		}

		// ---- "VS"
		POINT	VSPos		= { WINDOWSIZE_W / 2, 360 } ;

		DrawRotaGraph( VSPos.x, VSPos.y, 1.0, 0.0, g_graphHandle[ e_Image_VS_Icon ].handle, TRUE ) ;

		// --------------------------
		// --- キャラアイコン描画
		// --------------------------
		for ( int i = 0 ; i < piP->cSel_IconNum ; i++ )
		{
			DrawRotaGraph( newIconPos[ i ].x, newIconPos[ i ].y,
							newIconSize[ i ], 0.0, g_graphHandle[ e_Image_cIcon_Fox + i ].handle, TRUE ) ;
		}

		// --- キャラアイコン発光ブレンド描画
		for ( int i = 0 ; i < 2 ; i++ )
		{
			SetDrawBlendMode( DX_BLENDMODE_ADD, piP->any_counter[2 + i] ) ;

			// --- もしキャラ選択が完了していたら・・・
			if ( piP->cSel_CharaSetFlg[ i ] )
			{
				char cNum = piP->cSel_Icon[ i ] ;							// --- 選択している枠の数値を取得
			
				DrawRotaGraph( newIconPos[ cNum ].x, newIconPos[ cNum ].y,
								newIconSize[ cNum ], 0.0, g_graphHandle[ e_Image_cIcon_FlashBlend ].handle, TRUE ) ;
			}

			SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;
		}

		// --------------------------
		// --- 選択状況可視化枠描画
		// --------------------------
		for ( char i = 0 ; i < 2 ; i++ )
		{
			char	cNum	= piP->cSel_Icon[ i ] ;					// --- 選択している枠の数値を取得

			// --- 枠描画
			SetDrawBright( piP->any_counter[1], piP->any_counter[1], piP->any_counter[1] ) ;	// --- 明るさを常時変化させる

			if ( piP->cSel_CharaSetFlg[ i ] )
				SetDrawBright( 0x50, 0x50, 0x50 ) ;												// --- 決定状態であるならば、色を黒くする

			DrawRotaGraph( newIconPos[ cNum ].x, newIconPos[ cNum ].y,
							newIconSize[ cNum ], 0.0, g_graphHandle[ e_Image_cIcon_Select1P + i ].handle, TRUE ) ;

			SetDrawBright( 0xff, 0xff, 0xff ) ;
		}

		// --------------------------
		// ---		ID描画
		// --------------------------
		int			otIDHandle[2]	= { 0, 0 } ;
		POINT		otIDPos[2]		= { { g_winSize.x / 10, 160 }, { g_winSize.x / 10 * 9, 160 } } ;
		GRAPH_INFO	*otIDHP			= &g_graphHandle[ e_Image_PlayerID ] ;

		for ( int i = 0 ; i < 2 ; i++ )
			otIDHandle[ i ] = DerivationGraph( 0, otIDHP->chipSize.y * i, otIDHP->chipSize.x, otIDHP->chipSize.y,  otIDHP->handle ) ;

		DrawRotaGraph( otIDPos[0].x, otIDPos[0].y, 0.5, 0.0, otIDHandle[0], TRUE ) ;
		DrawRotaGraph( otIDPos[1].x, otIDPos[1].y, 0.5, 0.0, otIDHandle[1], TRUE ) ;

		// --- ハンドルの消去
		for ( int i = 0 ; i < 2 ; i++ )
			DeleteGraph( otIDHandle[ i ] ) ;

		// ---------------------
		// 「ＯＫ？」部分描画
		// ---------------------
		// --- 白い四角形を全体に描画し、やや色合いを薄くする
		if ( piP->cSel_WhiteBox.now != piP->cSel_WhiteBox.min[0] )
		{
			SetDrawBlendMode( DX_BLENDMODE_ALPHA, (int)(255 * ( piP->cSel_WhiteBox.now / piP->cSel_WhiteBox.max[1])) ) ;
			DrawBox( 0, 0, g_winSize.x, g_winSize.y, GetColor( 0xff, 0xff, 0xff ), TRUE ) ;	
			SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;
		}

		// --- 本描画部分
		if ( (piP->any_number[0] == 1) || (piP->cSel_WhiteBox.now != piP->cSel_WhiteBox.min[0]) )
		{
			POINT fraPos	= { g_winSize.x / 2, g_winSize.y / 2 } ;													// --- 枠座標
			POINT sYNPos[2]	= { {g_winSize.x / 2, g_winSize.y / 2 + 100}, {g_winSize.x / 2, g_winSize.y / 2 - 100} } ;	// --- 選択肢_座標

			int			newSentakuHandle[2] ;		// --- 選択肢の切り出しハンドル
			int			newPSelectHandle[2] ;		// --- プレイヤーごとの選択状況切り出しハンドル
			GRAPH_INFO	*otGP ;						// --- 画像情報のポインタ
			
			otGP = &g_graphHandle[ e_Image_cFrame_Str ] ;
			newSentakuHandle[0] = DerivationGraph( 0, otGP->chipSize.y * 0, otGP->chipSize.x, otGP->chipSize.y, otGP->handle ) ;	// --- 画像切り出し_選択肢
			newSentakuHandle[1] = DerivationGraph( 0, otGP->chipSize.y * 1, otGP->chipSize.x, otGP->chipSize.y, otGP->handle ) ;	// --- 画像切り出し_選択肢

			otGP = &g_graphHandle[ e_Image_cFrame_Select ] ;
			newPSelectHandle[0] = DerivationGraph( 0, otGP->chipSize.y * 0, otGP->chipSize.x, otGP->chipSize.y, otGP->handle ) ;	// --- 画像切り出し_選択状況
			newPSelectHandle[1] = DerivationGraph( 0, otGP->chipSize.y * 1, otGP->chipSize.x, otGP->chipSize.y, otGP->handle ) ;	// --- 画像切り出し_選択状況

			// --- 座標セット
			int zurashiPos_Y = (int)( (g_winSize.y / 3 * 2) * ((piP->cSel_WhiteBox.max[0] - piP->cSel_WhiteBox.now) / piP->cSel_WhiteBox.max[0]) ) ;
			fraPos.y	-= zurashiPos_Y ;
			sYNPos[0].y	-= zurashiPos_Y ;
			sYNPos[1].y	-= zurashiPos_Y ;

			// --- OK枠、選択肢描画
			SetDrawBlendMode( DX_BLENDMODE_ALPHA, 230 ) ;
			DrawRotaGraph( fraPos.x, fraPos.y, 1.2, 0.0, g_graphHandle[ e_Image_cFrame_OK ].handle, TRUE ) ;

			DrawRotaGraph( sYNPos[1].x, sYNPos[1].y, 1.0, 0.0, newSentakuHandle[ 0 ], TRUE ) ;		// --- 「準備ＯＫ」
			DrawRotaGraph( sYNPos[0].x, sYNPos[0].y, 1.0, 0.0, newSentakuHandle[ 1 ], TRUE ) ;		// --- 「選び直す」

			SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;

			// --- プレイヤーの選択肢に応じて枠を描画
			for ( int i = PLAYER_ONE ; i <= PLAYER_TWO ; i++ )
			{
				// --- プッシュ済みであった場合、黒くする
				if ( piP->cSel_StartOKFlg[ i ] )
					SetDrawBright( 0x50, 0x50, 0x50 ) ;

				DrawRotaGraph( sYNPos[ piP->any_select[ i ] ].x, sYNPos[ piP->any_select[ i ] ].y, 1.0, 0.0, newPSelectHandle[ i ], TRUE ) ; 

				SetDrawBright( 0xff, 0xff, 0xff ) ;
			}

			// --- ハンドルの消去
			for ( int i = 0 ; i < 2 ; i++ )
			{
				DeleteGraph( newPSelectHandle[ i ] ) ;
				DeleteGraph( newSentakuHandle[ i ] ) ;
			}
		}

		// --- 押下フラグがONの時、暗転用のBoxを描画する
		if ( (piP->cSel_IsBlackStart) || (piP->isBlackStart) )
		{
			SetDrawBlendMode( DX_BLENDMODE_ALPHA, (int)( piP->setBlackCount * (255.0f / piP->setBlackCount_Max) ) ) ;
			DrawBox( 0, 0, WINDOWSIZE_W, WINDOWSIZE_H, GetColor( 0x00, 0x00, 0x00 ), TRUE ) ;	
			SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;
		}
	}
}

// ============================================================================================================================================== 【メインのゲーム部】
// -----------------------------------------------------------------
//					DrawLoop関数_ゲーム初期化
// -----------------------------------------------------------------
void DrawFunc_GameInit( BOOL isUIDisp )
{
}

// -----------------------------------------------------------------
//
//					DrawLoop関数_ゲームスタート
//
// -----------------------------------------------------------------
void DrawFunc_GameStart( BOOL isUIDisp )
{
	/* ------------------------------------
				オブジェクト関連
	--------------------------------------- */
	Map.MapStart( ) ;
	for ( int i = 0 ; i < ELE_X_1 * ELE_Y_1 ; i++ )
		DrawModelEasy( &Scaf1[i] ) ;						// --- 足元s_1描画

	for ( int i = 0 ; i < 2 ; i++ )
	{
		DrawModelEasy( & g_Player[ i ] ) ;					// --- プレイヤー描画
		DrawModelEasy( &g_Object[ OBJID_CLOUD + i ] ) ;		// --- くも描画
	}

	// --- 周りの雲
	for ( int i = OBJID_GAMEOB ; i < OBJID_GAMEOB + GAMEOB_MAX ; i++ )
		DrawModelEasy( &g_Object[ i ] ) ;									// --- 使用オブジェクト


	/* ------------------------------------
					UI関連
	--------------------------------------- */

	// カウントの表示
	if ( isUIDisp )
	{
		UI_CntDown.CountDraw() ;			// --- カウントダウン

		// --- 暗転処理
		if ( piP->isBlackStart )
		{
			SetDrawBlendMode( DX_BLENDMODE_ALPHA, (int)( piP->setBlackCount * (255.0f / piP->setBlackCount_Max) ) ) ;
			DrawBox( 0, 0, WINDOWSIZE_W, WINDOWSIZE_H, GetColor( 0x00, 0x00, 0x00 ), TRUE ) ;	
			SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;
		}
	}

//	DrawFormatString( 350, 500,  GetColor( 0xff, 0xff, 0xff ), "現経過時間 : %3d", piP->any_counter[0] ) ;
}

// -----------------------------------------------------------------
//
//						DrawLoop関数_ゲーム
//
// -----------------------------------------------------------------
void DrawFunc_Game( BOOL isUIDisp )
{
	/* ------------------------------------
				オブジェクト関連
	--------------------------------------- */
	for ( int i = 0 ; i < ELE_X_1 * ELE_Y_1 ; i++ )
		DrawModelEasy( &Scaf1[i] ) ;					// --- 足元s_1描画
	for ( int i = 0 ; i < ELE_X_2 * ELE_Y_2 ; i++ )
		DrawModelEasy( &Scaf2_t[i] ) ;					// --- 足元s_2top描画
	for ( int i = 0 ; i < ELE_X_2 * ELE_Y_2 ; i++ )
		DrawModelEasy( &Scaf2_u[i] ) ;					// --- 足元s_2under描画
	for ( int i = 0 ; i < ELE_X_3 * ELE_Y_3 ; i++ )
		DrawModelEasy( &Scaf3[i] ) ;					// --- 足元s_3描画

	for ( int i = 0 ; i < ELE_X_1 * ELE_Y_1 ; i++ )
		DrawModelEasy( &Dot1stg[i] ) ;					// --- ドット描画
	for ( int i = 0 ; i < ELE_X_2 * ELE_Y_2 ; i++ )
		DrawModelEasy( &Dot2stg_t[i] ) ;				// --- ドット描画
	for ( int i = 0 ; i < ELE_X_2 * ELE_Y_2 ; i++ )
		DrawModelEasy( &Dot2stg_u[i] ) ;				// --- ドット描画
	for ( int i = 0 ; i < ELE_X_3 * ELE_Y_3 ; i++ )
		DrawModelEasy( &Dot3stg[i] ) ;					// --- ドット描画

	DrawModelEasy( & g_Player[0] ) ;					// --- プレイヤー描画
	DrawModelEasy( & g_Player[1] ) ;					// --- プレイヤー描画

	DrawModelEasy( & g_Object[ OBJID_HUNSUI ] ) ;		// --- 噴水

	DrawModelEasy( &g_Object[ OBJID_SEA ] ) ;							// --- 海

	// --- 周りの雲
	if ( Map.mapswitch == 1 )
	{
		for ( int i = OBJID_GAMEOB ; i < OBJID_GAMEOB + GAMEOB_MAX ; i++ )
			DrawModelEasy( &g_Object[ i ] ) ;									// --- 使用オブジェクト
	}

	/* ------------------------------------
			スーパー時の暗くするやつ
	--------------------------------------- */
	if ( isUIDisp )
	{
		if ( g_Player[ PLAYER_ONE ].superflg || g_Player[ PLAYER_TWO ].superflg )
		{
			SetDrawBlendMode( DX_BLENDMODE_ALPHA, 10 ) ;
			DrawBox( 0, 0, g_winSize.x, g_winSize.y, GetColor( 0x00, 0x00, 0x00 ), TRUE ) ;	
			SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;
		}
	}

	/* ------------------------------------
				エフェクト関連
	--------------------------------------- */
	Effect *oneTimeEP ;
	for ( int i = 0 ; i < EFFECT_MAX ; i++ )
	{
		oneTimeEP = &g_effect[i] ;

		if ( oneTimeEP->efInfo.aliveFlg )
			oneTimeEP->eDraw() ;				// --- エフェクト描画
	}

	// すこあ
//	DrawExtendGraph( 0,    0, 400,  300, g_graphHandle[e_Image_Score_1P_Test], TRUE ) ;
//	DrawExtendGraph( 1520, 0, 1920, 300, g_graphHandle[e_Image_Score_2P_Test], TRUE ) ;	

	/* ------------------------------------
					UI関連
	--------------------------------------- */
	if ( isUIDisp )
	{
		// --- UI
		UI_DGauge_ID[ 0 ].DrawSuperFlash() ;			// --- UI表示_スーパーフラッシュ

		PlayerUI[ 0 ].UIDraw( PLAYER_ONE ) ;			// --- UI表示_プレイヤーID
		PlayerUI[ 1 ].UIDraw( PLAYER_TWO ) ;			// --- UI表示_プレイヤーID
//		UI_DGauge_ID[ 0 ].DrawFrame( PLAYER_ONE ) ;		// --- UI表示_枠
//		UI_DGauge_ID[ 1 ].DrawFrame( PLAYER_TWO ) ;		// --- UI表示_枠
		TimeUI[ 0 ].UIDraw( PLAYER_ONE ) ;				// --- UI表示_タイマー
		TimeUI[ 1 ].UIDraw( PLAYER_TWO ) ;				// --- UI表示_タイマー
		ScoreBoard[ 0 ].UIDraw( PLAYER_ONE ) ;			// --- UI表示_スコア
		ScoreBoard[ 1 ].UIDraw( PLAYER_TWO ) ;			// --- UI表示_スコア
		UI_DGauge_ID[ 0 ].UIDraw( PLAYER_ONE ) ;		// --- UI表示_ドットゲージ、プレイヤーID
		UI_DGauge_ID[ 1 ].UIDraw( PLAYER_TWO ) ;		// --- UI表示_ドットゲージ、プレイヤーID

		UI_War.WarDraw() ;								// --- UI表示_ワーニング表示

		// ---「終了」描画
		POINT TUpSize = { 480, 480 } ;
		POINT otPlus = { 0, 0 } ;

		if ( piP->game_IsFin )
		{
			s_FinInfo *otFP = &piP->game_FinishDisp ;

			// --- フェードイン
			if ( otFP->nowTime < otFP->inTime )
				SetDrawBlendMode( DX_BLENDMODE_ALPHA, (int)(255 * ( (float)otFP->nowTime / (float)otFP->inTime) ) ) ;
			// --- フェードアウト
			if ( (otFP->nowTime > (otFP->totalTime - otFP->outTime)) && (otFP->nowTime < otFP->totalTime) )
				SetDrawBlendMode( DX_BLENDMODE_ALPHA, (int)(255 * ( (float)otFP->totalTime - (float)otFP->nowTime) / (float)otFP->outTime ) ) ;
		
			// --- 徐々にサイズを大きくする
			otPlus.x = otPlus.y = otFP->nowTime ;

			// --- 時間内の時のみ表示をする
			if ( otFP->nowTime < otFP->totalTime )
			{
				DrawExtendGraph( ((g_winSize.x / 2) - (TUpSize.x / 2) - otPlus.x), ((g_winSize.y / 2) - (TUpSize.y / 2) - otPlus.y),
					((g_winSize.x / 2) + (TUpSize.x / 2) + otPlus.x), ((g_winSize.y / 2) + (TUpSize.y / 2) + otPlus.y),
						g_graphHandle[ e_Image_TimeUp_Str ].handle, TRUE ) ;	// --- 文字描画
			}

			SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;
		}

		// --- 暗転処理
		if ( Map.c_collapse == 1 )		// 暗転のフラグが立ったら
		{
			SetDrawBlendMode( DX_BLENDMODE_ALPHA, (int)( Map.darken_screen * (255.0f / 100.0f) ) ) ;
			DrawBox( 0, 0, WINDOWSIZE_W, WINDOWSIZE_H, GetColor( 0x00, 0x00, 0x00 ), TRUE ) ;	
			SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;
		}
		// --- マップ切り替え時の暗転
		if ( Map.c_collapse == 2 )
		{
			SetDrawBlendMode( DX_BLENDMODE_ALPHA, (int)( Map.darken_screen * (255.0f / 100.0f) ) ) ;
			DrawBox( 0, (int)Map.size, WINDOWSIZE_W, WINDOWSIZE_H, GetColor( 0x04, 0xff, 0xff ), TRUE ) ;	// 水色のBox描画	
			SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;
		}

		// --- シーンチェンジ
		UI_SChange.UIDraw( 0 ) ;
	}

}


// ============================================================================================================================================== 【リザルト部】
// -----------------------------------------------------------------
//					DrawLoop関数_結果発表初期化
// -----------------------------------------------------------------
void DrawFunc_ResultInit( BOOL isUIDisp )
{
	// --- シーンチェンジ
	UI_SChange.UIDraw( 1 ) ;
}

// -----------------------------------------------------------------
//
//						DrawLoop関数_結果発表
//
// -----------------------------------------------------------------
void DrawFunc_Result( BOOL isUIDisp )
{
	int		otScore[2]  ;		// --- 各スコア
	POINT	scorePos[2]		= { { g_winSize.x / 8 * 3, 100 }, { g_winSize.x / 8 * 6, 100 } } ;			// --- スコア_2D空間に表示する座標
	POINT	scoreLU[2][2]	= { {{ 0, 0 }, { 0, 0 }}, {{ 0, 0 }, { 0, 0 }} } ;							// --- スコア_表示画像の左上座標

	POINT	dScorePos[2]	= { { g_winSize.x / 16 * 7, 200 }, { g_winSize.x / 16 * 13, 200 } } ;		// --- ドットスコア_2D空間に表示する座標
	POINT	dScoreLU[2][2]	= { {{ 0, 0 }, { 0, 0 }}, {{ 0, 0 }, { 0, 0 }} } ;							// --- ドットスコア_表示画像の左上座標

	POINT	stringPos[2]	= { { 500, 300 }, { 1360, 300 } } ;						// --- 文字列_2D空間に表示する座標

	otScore[ PLAYER_ONE ] = g_Player[ PLAYER_TWO ].pushscore ;						// --- スコアをセット( 1Pの落ちた数が事実上2Pの獲得スコア(なぜこうした？) )
	otScore[ PLAYER_TWO ] = g_Player[ PLAYER_ONE ].pushscore ;						// --- スコアをセット( 1Pの落ちた数が事実上2Pの獲得スコア(なぜこうした？) )

	/* ------------------------------------
				  3Dモデル描画
	--------------------------------------- */
	for ( int i = 0 ; i < 2 ; i++ )
	{
		DrawModelEasy( &g_Object[ OBJID_DODAI + i ] ) ;
		DrawModelEasy( &g_Player[ i ] ) ;
	}

	for ( int i = OBJID_RESULTOB ; i < OBJID_RESULTOB + RESULTOB_MAX ; i++ )
		DrawModelEasy( &g_Object[ i ] ) ;

	/* ------------------------------------
					UI関連
	--------------------------------------- */
	if ( isUIDisp )
	{
		/* ------------------------------------
					エフェクト関連
		--------------------------------------- */
		Effect *oneTimeEP ;
		for ( int i = 0 ; i < 150 ; i++ )
		{
			oneTimeEP = &g_effect[i] ;

			if ( oneTimeEP->efInfo.aliveFlg )
				oneTimeEP->eDraw() ;				// --- エフェクト描画
		}
		// ----------------------------------------
		//	  どぅるるる切り出し( ランダム数値 )
		// ----------------------------------------
		if ( (piP->res_Mode == e_RSMode_PresenBefore_Push) || (piP->res_Mode == e_RSMode_PresenBefore_Dot) )
		{
			for ( int i = 0 ; i < 2 ; i++ )
			{
				scoreLU[ i ][1].x = (piP->any_number[ 0 + i ] / 10) * g_graphHandle[ e_Image_Time1 ].chipSize.x ;	// ランダム_10の位の計算_X
				scoreLU[ i ][0].x = (piP->any_number[ 0 + i ] % 10) * g_graphHandle[ e_Image_Time1 ].chipSize.x ;	// ランダム_1の位の計算_X
			}
		}

		// ----------------------------------------
		//	 プッシュ切り出し( 押し出し数値 )
		// ----------------------------------------
		else if ( (piP->res_Mode == e_RSMode_PresenAfter_Push) )
		{
			for ( int i = 0 ; i < 2 ; i++ )
			{
				scoreLU[ i ][1].x = (otScore[ i ] / 10) * g_graphHandle[ e_Image_Time1 ].chipSize.x ;				// 落_10の位の計算_X
				scoreLU[ i ][0].x = (otScore[ i ] % 10) * g_graphHandle[ e_Image_Time1 ].chipSize.x ;				// 落_1の位の計算_X
			}
		}

		// ----------------------------------------
		//	 ドット切り出し( 獲得ココナッツ数値 )
		// ----------------------------------------
		else if ( (piP->res_Mode == e_RSMode_PresenAfter_Dot) )
		{
			for ( int i = 0 ; i < 2 ; i++ )
			{
				scoreLU[ i ][1].x = (g_Player[ i ].dotscore / 10) * g_graphHandle[ e_Image_Time1 ].chipSize.x ;		// ドット_10の位の計算_X
				scoreLU[ i ][0].x = (g_Player[ i ].dotscore % 10) * g_graphHandle[ e_Image_Time1 ].chipSize.x ;		// ドット_1の位の計算_X
			}
		}

		// ----------------------------------------
		//		プッシュ、ドット数値切り出し
		// ----------------------------------------
		else if ( (piP->res_Mode == e_RSMode_DispString) )
		{
			for ( int i = 0 ; i < 2 ; i++ )
			{
				scoreLU[ i ][1].x = (otScore[ i ] / 10) * g_graphHandle[ e_Image_Time1 ].chipSize.x ;				// 落_10の位の計算_X
				scoreLU[ i ][0].x = (otScore[ i ] % 10) * g_graphHandle[ e_Image_Time1 ].chipSize.x ;				// 落_1の位の計算_X

				dScoreLU[ i ][1].x = (g_Player[ i ].dotscore / 10) * g_graphHandle[ e_Image_Time1 ].chipSize.x ;	// ドット_10の位の計算_X
				dScoreLU[ i ][0].x = (g_Player[ i ].dotscore % 10) * g_graphHandle[ e_Image_Time1 ].chipSize.x ;	// ドット_1の位の計算_X
			}
		}

		// --- 切り取り位置をセット
		for ( int i = 0 ; i < 2 ; i++ )
		{
			scoreLU[ i ][1].y = g_graphHandle[ e_Image_Time1 ].chipSize.y * (i + 1) ;		// 落_10の位の計算_Y
			scoreLU[ i ][0].y = g_graphHandle[ e_Image_Time1 ].chipSize.y * (i + 1) ;		// 落_1の位の計算_Y
		}
		for ( int i = 0 ; i < 2 ; i++ )
		{
			dScoreLU[ i ][1].y = g_graphHandle[ e_Image_Time1 ].chipSize.y * (i + 1) ;		// ドット_10の位の計算_Y
			dScoreLU[ i ][0].y = g_graphHandle[ e_Image_Time1 ].chipSize.y * (i + 1) ;		// ドット_1の位の計算_Y
		}

		/* ------------------------------------

					  画像切り出し
		
		--------------------------------------- */

		// --- 数字ごとに切り取った画像ハンドルをセット
		GRAPH_INFO *gNumP = &g_graphHandle[ e_Image_Time1 ] ;

		// --- 落としたスコア( 新たな画像ハンドルを生成 )
		int newGraphHandle[8] ;
		newGraphHandle[0] = DerivationGraph( scoreLU[0][1].x, scoreLU[ 0 ][1].y, gNumP->chipSize.x, gNumP->chipSize.y, gNumP->handle ) ;	// --- 1P_落スコア_2桁目
		newGraphHandle[1] = DerivationGraph( scoreLU[0][0].x, scoreLU[ 0 ][0].y, gNumP->chipSize.x, gNumP->chipSize.y, gNumP->handle ) ;	// --- 1P_落スコア_1桁目
		newGraphHandle[2] = DerivationGraph( scoreLU[1][1].x, scoreLU[ 1 ][1].y, gNumP->chipSize.x, gNumP->chipSize.y, gNumP->handle ) ;	// --- 2P_落スコア_2桁目
		newGraphHandle[3] = DerivationGraph( scoreLU[1][0].x, scoreLU[ 1 ][0].y, gNumP->chipSize.x, gNumP->chipSize.y, gNumP->handle ) ;	// --- 2P_落スコア_1桁目

		if ( piP->res_Mode == e_RSMode_DispString )
		{
			newGraphHandle[4] = DerivationGraph( dScoreLU[0][1].x, dScoreLU[ 0 ][1].y, gNumP->chipSize.x, gNumP->chipSize.y, gNumP->handle ) ;	// --- 1P_ﾄﾞｯﾄｽｺｱ_2桁目
			newGraphHandle[5] = DerivationGraph( dScoreLU[0][0].x, dScoreLU[ 0 ][0].y, gNumP->chipSize.x, gNumP->chipSize.y, gNumP->handle ) ;	// --- 1P_ﾄﾞｯﾄｽｺｱ_1桁目
			newGraphHandle[6] = DerivationGraph( dScoreLU[1][1].x, dScoreLU[ 1 ][1].y, gNumP->chipSize.x, gNumP->chipSize.y, gNumP->handle ) ;	// --- 2P_ﾄﾞｯﾄｽｺｱ_2桁目
			newGraphHandle[7] = DerivationGraph( dScoreLU[1][0].x, dScoreLU[ 1 ][0].y, gNumP->chipSize.x, gNumP->chipSize.y, gNumP->handle ) ;	// --- 2P_ﾄﾞｯﾄｽｺｱ_1桁目
		}

		// --- フラッシュブレンド
		gNumP = &g_graphHandle[ e_Image_Time1_FlashBlend ] ;
		int newFlashHandle[8] ;

		newFlashHandle[0] = DerivationGraph( scoreLU[0][1].x, 0, gNumP->chipSize.x, gNumP->chipSize.y, gNumP->handle ) ;	// --- 1P_スコア_2桁目_Flash
		newFlashHandle[1] = DerivationGraph( scoreLU[0][0].x, 0, gNumP->chipSize.x, gNumP->chipSize.y, gNumP->handle ) ;	// --- 1P_スコア_1桁目_Flash
		newFlashHandle[2] = DerivationGraph( scoreLU[1][1].x, 0, gNumP->chipSize.x, gNumP->chipSize.y, gNumP->handle ) ;	// --- 2P_スコア_2桁目_Flash
		newFlashHandle[3] = DerivationGraph( scoreLU[1][0].x, 0, gNumP->chipSize.x, gNumP->chipSize.y, gNumP->handle ) ;	// --- 2P_スコア_1桁目_Flash
/*
		if ( piP->res_Mode == e_RSMode_DispString )
		{
			newFlashHandle[4] = DerivationGraph( dScoreLU[0][1].x, 0, gNumP->chipSize.x, gNumP->chipSize.y, gNumP->handle ) ;		// --- 1P_ﾄﾞｯﾄｽｺｱ_2桁目_Flash
			newFlashHandle[5] = DerivationGraph( dScoreLU[0][0].x, 0, gNumP->chipSize.x, gNumP->chipSize.y, gNumP->handle ) ;		// --- 1P_ﾄﾞｯﾄｽｺｱ_1桁目_Flash
			newFlashHandle[6] = DerivationGraph( dScoreLU[1][1].x, 0, gNumP->chipSize.x, gNumP->chipSize.y, gNumP->handle ) ;		// --- 2P_ﾄﾞｯﾄｽｺｱ_2桁目_Flash
			newFlashHandle[7] = DerivationGraph( dScoreLU[1][0].x, 0, gNumP->chipSize.x, gNumP->chipSize.y, gNumP->handle ) ;		// --- 2P_ﾄﾞｯﾄｽｺｱ_1桁目_Flash
		}
*/
		// --- スコアアイコン( 新たな画像ハンドルを生成 )
		gNumP = &g_graphHandle[ e_Image_Result_ScoreIcon ] ;

		int newSIconHandle[2] ;
		newSIconHandle[0] = DerivationGraph( 0, 0,					gNumP->chipSize.x, gNumP->chipSize.y, gNumP->handle ) ;		// --- アイコン_プッシュ
		newSIconHandle[1] = DerivationGraph( 0, gNumP->chipSize.y,	gNumP->chipSize.x, gNumP->chipSize.y, gNumP->handle ) ;		// --- アイコン_ドット

		// ^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~
		// ^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~
		/* ------------------------------------

					  本描画
		
		--------------------------------------- */
		float		otSizeY ;																// --- piP->res_barSChange の数値とサイズを掛け合わせた数値
		int			INPos_X[2]		= { g_winSize.x / 2 - 450, g_winSize.x / 2 + 350, } ;	// --- Icon,Numberの座標_X
		int			ALLPos_Y		= g_winSize.y / 4 * 3 ;									// --- Icon,Number,Bar全てのUI座標_Y
		int			plusSize		= (int)(piP->res_strSize.now * 100)  ;					// --- でぇぇん！の瞬間の文字サイズ加算分
		int			plusSize_F		= plusSize * 2 ;										// --- でぇぇん！の瞬間の文字サイズ加算分_FlashBlend
		int			otGenten ;																// --- 一時的な原点
		int			icon_P0_D1		= 0 ;													// --- 表示するアイコンNo 【Push - 0】【Dot - 1】

		// -------------------------
		//
		//  ●  09 - ●  10
		//	   ↑この辺がINPos_X[0]
		// -------------------------

		// -----------------------------------------------
		// --- 「落とした数」「獲得ココナッツ数」文字描画
		// -----------------------------------------------
		if ( (piP->res_Mode == e_RSMode_PresenBefore_Str_Push) || (piP->res_Mode == e_RSMode_PresenBefore_Str_Dot) )
		{
			otSizeY = piP->res_barSChange * g_graphHandle[ e_Image_Result_Bar ].fullSize.y ;
				
			// --- 枠描画
			DrawExtendGraph( 0, (int)(ALLPos_Y - otSizeY / 2), g_winSize.x, (int)(ALLPos_Y + otSizeY / 2),
								g_graphHandle[ e_Image_Result_Bar ].handle, TRUE ) ;

			// --- 文字描画( 落とした数 )
			if ( piP->res_Mode == e_RSMode_PresenBefore_Str_Push )
			{
				DrawExtendGraph( 0, (int)(ALLPos_Y - otSizeY / 2), g_winSize.x, (int)(ALLPos_Y + otSizeY / 2),
									g_graphHandle[ e_Image_Result_BarStr_Drop ].handle, TRUE ) ;
			}
			// --- 文字描画( 獲得ココナッツ数 )
			else if ( piP->res_Mode == e_RSMode_PresenBefore_Str_Dot )
			{
				DrawExtendGraph( 0, (int)(ALLPos_Y - otSizeY / 2), g_winSize.x, (int)(ALLPos_Y + otSizeY / 2),
									g_graphHandle[ e_Image_Result_BarStr_Coconut ].handle, TRUE ) ;
			}
		}

		// -----------------------------------------------
		// --- どぅるるるるる -> でぇぇん 描画
		// -----------------------------------------------
		if ( (piP->res_Mode == e_RSMode_PresenBefore_Push) || (piP->res_Mode == e_RSMode_PresenAfter_Push) ||
			(piP->res_Mode == e_RSMode_PresenBefore_Dot) || (piP->res_Mode == e_RSMode_PresenAfter_Dot) )
		{
			// --- 枠描画
			otSizeY = g_graphHandle[ e_Image_Result_Bar ].fullSize.y * piP->res_barSChange ;	// 0 〜 1の間で変わるサイズ変数を有効活用
		
			DrawExtendGraph(	(int)(g_winSize.x / 2 - g_graphHandle[ e_Image_Result_Bar ].fullSize.x / 2), (int)(ALLPos_Y - otSizeY / 2),
								(int)(g_winSize.x / 2 + g_graphHandle[ e_Image_Result_Bar ].fullSize.x / 2), (int)(ALLPos_Y + otSizeY / 2),
								g_graphHandle[ e_Image_Result_Bar ].handle, TRUE ) ;

			// --- 数字描画
			if ( (piP->res_Mode == e_RSMode_PresenBefore_Dot) || (piP->res_Mode == e_RSMode_PresenAfter_Dot) )
				icon_P0_D1 = 1 ;

			for ( int i = 0 ; i < 2 ; i++ )
			{
				// --- スコアアイコン
				otSizeY	= piP->res_barSChange * g_graphHandle[ e_Image_Result_ScoreIcon ].chipSize.y ;
				DrawExtendGraph((int)(INPos_X[ i ] - g_graphHandle[ e_Image_Result_ScoreIcon ].fullSize.x * 3.5f), (int)(ALLPos_Y - otSizeY),
								(int)(INPos_X[ i ] - g_graphHandle[ e_Image_Result_ScoreIcon ].fullSize.x * 1.5f), (int)(ALLPos_Y + otSizeY),
								newSIconHandle[ icon_P0_D1 ], TRUE ) ;

				// --- 落としたスコア数値描画( 上が2桁目、下が1桁目 )
				otSizeY		= piP->res_barSChange * g_graphHandle[ e_Image_Time1 ].chipSize.y ;				// --- 伸ばし分のYサイズ
				otGenten	= INPos_X[ i ] + g_graphHandle[ e_Image_Result_ScoreIcon ].fullSize.x ;			// --- 原点

				DrawExtendGraph(otGenten - (int)(g_graphHandle[ e_Image_Time1 ].chipSize.x * 2 * 0.8f) - plusSize, (int)(ALLPos_Y - otSizeY * 0.8f) - plusSize,
								otGenten + (int)(g_graphHandle[ e_Image_Time1 ].chipSize.x / 4 * 0.8f) + plusSize, (int)(ALLPos_Y + otSizeY * 0.8f) + plusSize,
								newGraphHandle[ (i * 2) + 0 ], TRUE ) ;	

				DrawExtendGraph(otGenten - (int)(g_graphHandle[ e_Image_Time1 ].chipSize.x / 4 * 0.8f) - plusSize, (int)(ALLPos_Y - otSizeY * 0.8f) - plusSize,
								otGenten + (int)(g_graphHandle[ e_Image_Time1 ].chipSize.x * 2 * 0.8f) + plusSize, (int)(ALLPos_Y + otSizeY * 0.8f) + plusSize,
								newGraphHandle[ (i * 2) + 1 ], TRUE ) ;	

				// --- アルファブレンド描画
				if ( piP->res_strSize.now > 0.0f )
				{

					SetDrawBlendMode( DX_BLENDMODE_ADD, (int)(255.0f * (piP->res_strSize.now / piP->res_strSize.max[0])) ) ;

					DrawExtendGraph(otGenten - (int)(g_graphHandle[ e_Image_Time1 ].chipSize.x * 2 * 0.8f) - plusSize_F, (int)(ALLPos_Y - otSizeY * 0.8f) - plusSize_F,
									otGenten + (int)(g_graphHandle[ e_Image_Time1 ].chipSize.x / 4 * 0.8f) + plusSize_F, (int)(ALLPos_Y + otSizeY * 0.8f) + plusSize_F,
									newFlashHandle[ (i * 2) + 0 ], TRUE ) ;	

					DrawExtendGraph(otGenten - (int)(g_graphHandle[ e_Image_Time1 ].chipSize.x / 4 * 0.8f) - plusSize_F, (int)(ALLPos_Y - otSizeY * 0.8f) - plusSize_F,
									otGenten + (int)(g_graphHandle[ e_Image_Time1 ].chipSize.x * 2 * 0.8f) + plusSize_F, (int)(ALLPos_Y + otSizeY * 0.8f) + plusSize_F,
									newFlashHandle[ (i * 2) + 1 ], TRUE ) ;	

					SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;
				}
			}
		}

		// -----------------------------------------------
		// ---				WIN, Flash 描画
		// -----------------------------------------------
		if ( piP->res_Mode == e_RSMode_DispString )
		{
			POINT otWPos[2] = { { g_winSize.x - 450, 250 }, { 450, 250 } } ;	// --- 表示座標

			GRAPH_INFO	*gIDP = &g_graphHandle[ e_Image_Result_WinStr ] ;		// --- Win文字 のハンドル
			int			newWinStrHandle[2] ;									// --- 切り出し画像ハンドル

			newWinStrHandle[0]= DerivationGraph( 0, gIDP->chipSize.y * 0, gIDP->chipSize.x, gIDP->chipSize.y, gIDP->handle ) ;
			newWinStrHandle[1]= DerivationGraph( 0, gIDP->chipSize.y * 1, gIDP->chipSize.x, gIDP->chipSize.y, gIDP->handle ) ;

			// --- フラッシュ描画
			DrawRotaGraph( otWPos[ piP->res_WinnerID ].x, otWPos[ piP->res_WinnerID ].y, 1.5 + piP->res_flashSize.now, piP->res_flashRot,
							g_graphHandle[ e_Image_Result_BackFlash ].handle, TRUE ) ;

			// --- 1P or 2PWIN! 描画
			DrawRotaGraph( otWPos[ piP->res_WinnerID ].x, otWPos[ piP->res_WinnerID ].y, 1.5, 0.0, newWinStrHandle[ piP->res_WinnerID ], TRUE ) ;

			// --- ハンドル消去
			for ( int i = 0 ; i < 2 ; i++ )
				DeleteGraph( newWinStrHandle[ i ] ) ;
		}

		// -----------------------------------------------
		// ---				1P 2P 描画
		// -----------------------------------------------
		if ( piP->res_Mode < e_RSMode_DispString )
		{
			GRAPH_INFO	*gIDP = &g_graphHandle[ e_Image_PlayerID_Chase ] ;	// --- プレイヤーID のハンドル
			int			newIDHandle[2] ;									// --- 切り出し画像ハンドル

			newIDHandle[0]= DerivationGraph( 0, gIDP->chipSize.y * 0, gIDP->chipSize.x, gIDP->chipSize.y, gIDP->handle ) ;
			newIDHandle[1]= DerivationGraph( 0, gIDP->chipSize.y * 1, gIDP->chipSize.x, gIDP->chipSize.y, gIDP->handle ) ;

			for ( int i = PLAYER_ONE ; i <= PLAYER_TWO ; i++ )
			{
				DrawModiBillboard3D( VGet( g_Player[ i ].pos.x - 120, g_Player[ i ].pos.y + 500, g_Player[ i ].pos.z ),
										0,128, 256,128, 256,0, 0,0, newIDHandle[ i ], TRUE ) ;
			}

			// --- ハンドル消去
			for ( int i = 0 ; i < 2 ; i++ )
				DeleteGraph( newIDHandle[ i ] ) ;
		}

		// -------------------------------
		// ---	シーンチェンジ関連
		// -------------------------------
		UI_SChange.UIDraw( 1 ) ;

		// ^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~
		// ^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~

		for ( int i = 0 ; i < 8 ; i++ )
			DeleteGraph( newGraphHandle[ i ] ) ;
		for ( int i = 0 ; i < 8 ; i++ )
			DeleteGraph( newFlashHandle[ i ] ) ;
		for ( int i = 0 ; i < 2 ; i++ )
			DeleteGraph( newSIconHandle[ i ] ) ;
	}
}

// ============================================================================================================================================== Draw Model Easy
// -----------------------------------------------------------------
//
//				オブジェクトのクラスに応じた描画関数
//
// -----------------------------------------------------------------
//			・引数１ - Objectオブジェクト ( ポインタ渡し )
// -----------------------------------------------------------------
template <class T> 
void DrawModelEasy( T *arg_obj )
{
	// アニメ情報に基づいて、アニメーションを付与
	if ( arg_obj->animUseFlg == TRUE )
		MV1SetAttachAnimTime( arg_obj->modelHandle, arg_obj->attachIdx, arg_obj->playTime ) ;

	// モデルの回転
	MV1SetRotationXYZ( arg_obj->modelHandle, VGet( 1.57f * arg_obj->dir.x , 1.57f * arg_obj->dir.y, 1.57f * arg_obj->dir.z ) ) ;

	// モデルの拡縮
	MV1SetScale( arg_obj->modelHandle, arg_obj->size ) ;

	// モデルの移動
	MV1SetPosition( arg_obj->modelHandle, arg_obj->pos );

	// 描画フラグが偽だと、描画を行わない
	if ( !arg_obj->dispFlg )
		return ;
	// モデルの描画
	MV1DrawModel( arg_obj->modelHandle ) ;
}

template void DrawModelEasy<Object>( Object *arg_obj ) ;			// --- Object 対応
template void DrawModelEasy<Player>( Player *arg_obj ) ;			// --- Player 対応
template void DrawModelEasy<DotItem>( DotItem *arg_obj ) ;			// --- DotItem 対応
template void DrawModelEasy<Scaffold>( Scaffold *arg_obj ) ;		// --- Scaffold 対応


