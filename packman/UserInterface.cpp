/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: UserInterface.cpp
	CREATE	: HIRANO
	
	------ Explanation of file ---------------------------------------------------------------------
       
	UIクラス

	ここではUIの描画を主に行う( 派生は必須 )。
	画像のアニメーションや、連続した数値は別の画像として保管すると整理整頓されていない
	部屋みたいになっちゃうので、

	「DerivationGraph()」を使って画像を切り取って新たなハンドルを生成しよう。
	変形させられるメリットがある。

	また、これを使った後は必ず「DeleteGraph()」で生成したハンドルを消してほしい。
	生成できるハンドルには限界があるので、重くなってしまうよ。

	(例)
		c_fPoint cut = c_fPoint( 20.0f, 20.0f ) ;

		int newHandle = DerivationGraph( (int)cut.x, (int)cut,y, 20, 20, imageHandle ) ;
		DrawRotaGraph( 0, 0, 1.0, 1.0, newHandle, TRUE ) ;

		DeleteGraph( newHandle ) ;

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

#include "Common.h"

UserInterface::UserInterface( ){
	image_w = 0 ;
	image_h = 0 ;
	f_disp = TRUE ;
}
UserInterface::~UserInterface( ){
}




