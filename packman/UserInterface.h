/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: UserInterface.h
	CREATE	: HIRANO
	
	------ Explanation of file ---------------------------------------------------------------------
       
	UIクラスの宣言

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

class UserInterface
{
	public :
		UserInterface( ) ;		// コンストラクタ
		~UserInterface( ) ;		// デストラクタ

		virtual  void UIDraw( int ) = 0 ;		// 純粋仮想関数絶対作ろうね

		c_fPoint cord ;			// 描画する座標
		c_fPoint draw ;			// 画像の描画したい左上座標
		int image_w ;			// 表示する幅
		int image_h ;			// 表示する高さ
		bool f_disp ;			// 表示させるか
} ;


	