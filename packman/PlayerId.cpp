/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: PlayerId.cpp
	CREATE	: HIRANO
	
	------ Explanation of file ---------------------------------------------------------------------
       
	PlayerIdクラス

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

#include "Common.h"

PlayerId::PlayerId( ){

	floatPow.plus	= 2.0f ;			// --- 初速
	floatPow.max[0]	= floatPow.plus ;	// --- 初速_バックアップ
	isFloatUp		= TRUE ;			// --- 上昇フラグ

}
PlayerId::~PlayerId( ){
}

/* ------------------------------------------------------------
					PlayerId関数
===============================================================
	プレイヤーIDＵＩの表示
	引数：なし
	戻り値：int
--------------------------------------------------------------- */
void PlayerId::UIDraw( int eleno )
{
	// ------------------------------------
	// --- 現在位置指定
	// ------------------------------------
	//            □
	//       □         □
	//    □               □
	//	 □                 □
	//  □                   □
	//                          □
	//                              □
	//		↑こういう動きにしたい
	// ------------------------------------
	BOOL	isHanten = FALSE ;
	float	otChange = 0.2f ;	// --- 変化量
	float	offSet_Y = 500 ;	// --- オフセット( プレイヤーの頭上に表示する為の数値 )

	floatPow.now += floatPow.plus ;

	// --- 上昇処理
	if ( isFloatUp )
	{
		floatPow.plus -= otChange ;

		if ( floatPow.now <= 0 )
		{
			floatPow.plus = -floatPow.max[0] ;
			isHanten = TRUE ;
		}
	}
	// --- 下降処理
	else
	{
		floatPow.plus += otChange ;

		if ( floatPow.now >= 0 )
		{
			floatPow.plus = floatPow.max[0] ;
			isHanten = TRUE ;
		}
	}

	if ( isHanten )				// --- 反転フラグが立ったら数値を反転する
	{
		floatPow.now = 0.0f ;
		isFloatUp ^= 0x01 ;
	}

	// --- 対象プレイヤーがスーパー状態だと、オフセットの位置が上がる
	if ( g_Player[ eleno ].superflg )
		offSet_Y += 100.0f ;

	// ------------------------------------
	// --- 実描画
	// ------------------------------------
	GRAPH_INFO *gIDP = &g_graphHandle[ e_Image_PlayerID_Chase ] ;
	int newIDHandle = DerivationGraph( 0, eleno * gIDP->chipSize.y, gIDP->chipSize.x, gIDP->chipSize.y, gIDP->handle ) ;

	DrawModiBillboard3D( VGet( g_Player[eleno].pos.x - 120, g_Player[eleno].pos.y + offSet_Y + floatPow.now, g_Player[eleno].pos.z ),
							0,128, 256,128, 256,0, 0,0, newIDHandle, TRUE ) ;

	DeleteGraph( newIDHandle ) ;

//	DrawModiBillboard3D(  g_Player[0].pos, 0,0, 64,0, 64,64,0,64, g_graphHandle[e_Image_Player1P], TRUE ) ;
//	DrawRectGraph( (int)cord.x, (int)cord.y, (int)draw.x, (int)draw.y, image_w, image_h, g_graphHandle[e_Image_PlayerId], TRUE, FALSE ) ;		// 画像の表示
}
