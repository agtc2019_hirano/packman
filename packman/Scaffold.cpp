/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: Scaffold.cpp
	CREATE	: HIRANO
	
	------ Explanation of file ---------------------------------------------------------------------
       
	ステージにあったマップデータの保持、足場の演出など

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

#include "Common.h"

Scaffold::Scaffold( ){
	this->initStates() ;
}
Scaffold::~Scaffold( ){
}

/* ------------------------------------------------------------
					init States
===============================================================
	フィールドを初期化する
	引数：
	戻り値：
--------------------------------------------------------------- */
void Scaffold::initStates()
{
	mapmode = 0 ;
	f_move = 0 ;
	Map.mapcount = 0 ;
	dispFlg = 0 ;
	count = 0 ;
	Map.darken_screen = 0 ;
}

/* ------------------------------------------------------------
					MapSelect関数
===============================================================
	マップを選択する
	引数：
	戻り値：
--------------------------------------------------------------- */
int Scaffold::MapSelect( )
{
	if ( Time[e_Timer1].c_timer == 0 )	// c_timerが０の時
	{
		Time[e_Timer1].ResetTimer( ) ;			// タイマー１リセット
	}

	if ( (count == 0) && (Map.mapswitch == 2) )	// c_timerが０の時かつmapswitchが２の時
	{
		Time[e_Timer2].ResetTimer( ) ;			// タイマー２リセット
		printf( "e_Time2リセット\n" ) ;
		count = 1 ;
	}

	if ( Time[e_Timer3].c_timer == 0 )			// c_timerが０の時
	{
		Time[e_Timer3].ResetTimer( ) ;			// タイマー３リセット
	}

	Map.mapcount = Time[e_Timer3].CountSecond( ) ;	// Map.mapcountに経過秒数入れる

	if ( (Map.mapcount / 29) == Map.mapswitch )			// ３０秒経ったらマップ切り替え
	{
		if ( Map.mapswitch < 3)	// マップ３超えないようにする
			Map.mapswitch++ ;		// インクリメント
		else
			Map.mapswitch = 3 ;

		// --- 背景の見た目を変化させる( 海は第三ステージで暗くする )
		if ( Map.mapswitch == 2 )
			MV1SetMaterialEmiColor( g_Object[ OBJID_SEA ].modelHandle, 0, GetColorF( 0.5f, 0.5f, 0.5f, 1.0f ) ) ;
		if ( Map.mapswitch == 3 )
		{
			MV1SetMaterialEmiColor( g_Object[ OBJID_SEA ].modelHandle, 0, GetColorF( 0.1f, 0.1f, 0.1f, 1.0f ) ) ;
			MV1SetTextureGraphHandle( g_mv1Handle[ e_Model_SkyDom ].modelHandle, 0, g_graphHandle[ e_Image_Hoshizora ].handle, FALSE ) ;	// --- 星空背景
		}
	}

	switch( Map.mapswitch )
	{
		case 1 :		// ステージ１
			Map.LoadMap( e_stage1, mapdate1 ) ;		// ステージ１読み込み
			if ( Map.mapcount == 24 )
				UI_War.f_disp = TRUE ;				// ワーニング表示

			if ( Map.mapcount == 26 )				// タイマーが26秒だったら
			{
				Map.MapChangeInitSet1to2() ;		// マップ切り替え処理
				TimeUI[0].f_disp = FALSE ;			// タイマーUI非表示
				TimeUI[1].f_disp = FALSE ;			// タイマーUI非表示

				ScoreBoard[0].f_disp = FALSE ;		// タイマーUI非表示
				ScoreBoard[1].f_disp = FALSE ;		// タイマーUI非表示

				UI_DGauge_ID[0].f_disp = FALSE ;	// --- UI表示_ドットゲージ、プレイヤーID非表示
				UI_DGauge_ID[1].f_disp = FALSE ;	// --- UI表示_ドットゲージ、プレイヤーID非表示

				Map.f_move = 1 ;

				// --- カタカタ音を再生
				if( CheckSoundMem( g_soundHandle[e_Sound_VibrationSE].soundHandle ) == 0 )
				{
					SOUND_INFO *otSP = &g_soundHandle[ e_Sound_VibrationSE ] ;
					PlaySoundMem( otSP->soundHandle, otSP->playType ) ;
				}
			}
			if ( Map.mapcount == 28 )						// タイマーが28秒だったら
			{
				if( CheckSoundMem(g_soundHandle[e_Sound_FallSE].soundHandle) == 0 &&
					Map.c_collapse == 0 )
				{
					SOUND_INFO *otSP = &g_soundHandle[ e_Sound_FallSE ] ;
					PlaySoundMem( otSP->soundHandle, otSP->playType ) ;
				}

				Map.c_collapse = 1 ;				// マップ切り替えフラグ立てる
//				g_shadowMap.isUse = FALSE ;

				// --- カタカタ音を止める
				StopSoundMem( g_soundHandle[ e_Sound_VibrationSE ].soundHandle ) ;
			}

			Map.HideMap( e_stage2_top ) ;
			Map.HideMap( e_stage2_under ) ;
			Map.HideMap( e_stage3 ) ;

			break ;

		case 2 :		// ステージ２
			if ( Map.mapcount == 29 )
			{
				Map.size = (float)g_winSize.y ;
				g_Player[0].pos.x = 0.0f ;		// プレイヤー１のｘ座標指定
				g_Player[0].pos.y = 400.0f ;	// プレイヤー１のｙ座標指定
				g_Player[0].pos.z = 0.0f ;		// プレイヤー１のｚ座標指定
				g_Player[0].dir.y = 3 ;			// プレイヤー１の方向：右向き

				g_Player[1].pos.x = 1200.0f ;	// プレイヤー２のｘ座標指定
				g_Player[1].pos.y = 400.0f ;	// プレイヤー２のｙ座標指定
				g_Player[1].pos.z = -1500.0f ;	// プレイヤー２のｚ座標指定
				g_Player[1].dir.y = 1 ;			// プレイヤー１の方向：左向き

				UI_War.WarInit() ;				// UI初期セット

				g_Player[0].PlayerAction_AfterStage1() ;	// プレイヤー１の移動値リセット
				g_Player[1].PlayerAction_AfterStage1() ;	// プレイヤー２の移動値リセット
			}
			if ( (Map.c_collapse != 0) && (Map.mapcount == 30) )			// マップ切り替えフラグ
			{
//				Map.p_move = 1 ;				// プレイヤー着地アニメ
				//g_Player[0].land = TRUE ;	// プレイヤー１の移動値リセット
				//g_Player[1].land = TRUE ;	// プレイヤー２の移動値リセット
				Map.size = 500 ;
				Map.AfterMapInit() ;			// マップ切り替え後初期セット
				printf( "tyakutisitao\n" ) ;
			}
			if ( Map.mapcount > 32 )			// タイマーが32秒を超えたら
				Map.f_move = 1 ;				// ムーブフラグ立てる

			Map.HideMap( e_stage1 ) ;						// ステージ１非表示
			Map.HideMap( e_stage3 ) ;

			Map.LoadMap( e_stage2_top, mapdate2_t ) ;		// ステージ２読み込み
			Map.LoadMap( e_stage2_under, mapdate2_u ) ;		// ステージ２読み込み
			if ( Map.mapcount == 52 )
				UI_War.f_disp = TRUE ;				// ワーニング表示
			if ( Map.mapcount == 55 )
			{
				Map.darken_screen = 0 ;						// 暗転0から
				Map.MapChangeInitSet2to3( ) ;				// マップ切り替え時のプレイヤーセット
				Map.size = (float)g_winSize.y ;				// マップの暗転y座標
				Map.c_collapse = 2 ;						// マップ切り替えフラグ
				g_Object[ OBJID_HUNSUI ].dispFlg = TRUE ;	// 噴水表示
				if( CheckSoundMem( g_soundHandle[ e_Sound_WaterSE ].soundHandle ) == 0 )
				{
					// --- 音量セット
					SetAllSoundVolume100() ;								// --- ボリューム 100セット

					// --- 水の音再生
					SOUND_INFO *otSP = &g_soundHandle[ e_Sound_WaterSE ] ;// --- ウォーター
					PlaySoundMem( otSP->soundHandle, otSP->playType ) ;
				}
			}
			break ;

		case 3 :		// ステージ３
			if ( Map.mapcount == 58 )
			{
				g_Player[0].pos.x = 0.0f ;					// プレイヤー１のｘ座標指定
				g_Player[0].pos.y = 700.0f ;				// プレイヤー１のｙ座標指定
				g_Player[0].pos.z = -600.0f ;				// プレイヤー１のｚ座標指定
				g_Player[0].dir.y = 3 ;						// プレイヤー１の方向：右向き

				g_Player[1].pos.x = 1800.0f ;				// プレイヤー２のｘ座標指定
				g_Player[1].pos.y = 700.0f ;				// プレイヤー２のｙ座標指定
				g_Player[1].pos.z = -600.0f ;				// プレイヤー２のｚ座標指定
				g_Player[1].dir.y = 1 ;						// プレイヤー２の方向：左向き

				g_Player[0].PlayerAction_AfterStage1() ;	// プレイヤー１の移動値リセット
				g_Player[1].PlayerAction_AfterStage1() ;	// プレイヤー２の移動値リセット
				g_Object[ OBJID_HUNSUI ].dispFlg = FALSE ;			// 噴水非表示がトリガー
			}
			// 暗転解除合わせる
			if ( Map.mapcount == 59 ){
				//g_Player[0].land = TRUE ;	// プレイヤー１の移動値リセット
				//g_Player[1].land = TRUE ;	// プレイヤー２の移動値リセット
			}
			if ( (Map.c_collapse != 0) && (Map.mapcount == 60) )	// マップ切り替えフラグ
			{
//				Map.p_move = 1 ;				// 着地アニメ
				//g_Player[0].land = TRUE ;	// プレイヤー１の移動値リセット
				//g_Player[1].land = TRUE ;	// プレイヤー２の移動値リセット
				
				Map.AfterMapInit() ;			// マップ切り替え後初期セット
				printf( "tyakutisitao2-3\n" ) ;
			}
			Map.HideMap( e_stage1 ) ;						// ステージ１非表示
			Map.HideMap( e_stage2_top ) ;					// ステージ２非表示
			Map.HideMap( e_stage2_under ) ;					// ステージ２非表示
			Map.LoadMap( e_stage3, mapdate3 ) ;				// ステージ３読み込み
//			Map.p_move = 0 ;								// 着地アニメフラグ落とす
			break ;
	}
	if ( (Map.c_collapse == 1) && (Map.mapswitch == 1) ){			// ステージ移行フラグが立ったら
		if ( (Map.darken_screen + 1.87) <= 100 )
			Map.darken_screen += 1.5f ;		// マップ暗転カウント
		else
			Map.darken_screen = 100 ;
	}
	if ( (Map.c_collapse == 1) && (Map.mapswitch == 2) ){			// ステージ移行フラグが立ったら
		if ( Map.darken_screen >= 0 )
			Map.darken_screen -= 0.7f ;		// マップ暗転明度
	}
	
	if ( (Map.c_collapse == 2) && (Map.mapswitch == 2) )						// タイマーが28秒だったら
	{
		if ( (Map.darken_screen + 1.5) <= 100 )
			Map.darken_screen += 1.5f ;		// マップ暗転明度
		else
			Map.darken_screen = 100 ;
		Map.size -= 30 ;					// マップ暗転サイズ
	}
	if ( (Map.c_collapse == 2) && (Map.mapswitch == 3) && (g_Object[ OBJID_HUNSUI ].dispFlg == FALSE) )		// タイマーが28秒だったら
	{
//		printf( "出現するよ【%d秒】\n", Map.mapcount) ;
		if ( Map.darken_screen >= 0 )
			Map.darken_screen -= 0.7f ;		// マップ切り替えフラグ立てる
	}

	// -------------------------------------------------------------------------
	// --- 足場オブジェクトのコリジョンモデルに毎カウントステータスを一致させる
	// -------------------------------------------------------------------------
	for ( int i = 0 ; i < ELE_X_1 * ELE_Y_1 ; i++ )
		Scaf1[ i ].setStatesFromParent() ;
	for ( int i = 0 ; i < ELE_X_2 * ELE_Y_2 ; i++ )
		Scaf2_t[ i ].setStatesFromParent() ;
	for ( int i = 0 ; i < ELE_X_2 * ELE_Y_2 ; i++ )
		Scaf2_u[ i ].setStatesFromParent() ;
	for ( int i = 0 ; i < ELE_X_3 * ELE_Y_3 ; i++ )
		Scaf3[ i ].setStatesFromParent() ;

	// デバッグ用ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
//	DrawFormatString( 350, 500,  GetColor( 0xff, 0xff, 0xff ), "現経過時間 : %3d", Map.mapcount ) ;
//	DrawFormatString( 350, 530,  GetColor( 0xff, 0xff, 0xff ), "マップ : %3d", movepos.x ) ;
//	DrawFormatString( 350, 560,  GetColor( 0xff, 0xff, 0xff ), "f_move : %3d", Map.f_move ) ;

	return 0 ;
}






