/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: MapGeneration.cpp
	CREATE	: HIRANO
	
	------ Explanation of file ---------------------------------------------------------------------
       
	マップ生成クラス

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

#include "Common.h"

MapGeneration::MapGeneration( ){

	initStates() ;
}
MapGeneration::~MapGeneration( ){
}

/* ------------------------------------------------------------
					MapStart関数
===============================================================
	ゲームスタート時のマップ表示
	引数：mapno..ステージ番号
	戻り値：int
--------------------------------------------------------------- */
void MapGeneration::initStates()
{
	f_move = 0 ;		// 初期化
	c_collapse = 0 ;
	p_move = 0 ;
	mapswitch = 0 ;	
	mapcount = 0 ;
	darken_screen = 0 ;
	size = 0 ;
}

/* ------------------------------------------------------------
					MapStart関数
===============================================================
	ゲームスタート時のマップ表示
	引数：mapno..ステージ番号
	戻り値：int
--------------------------------------------------------------- */
int MapGeneration::MapStart( )
{
	int c_xel = 0 ;			// ｘ要素数
	int c_yel = 0 ;			// ｙ要素数

	scafP = &Scaf1[0] ;

	for( int i = 0 ; i < ELE_X_1 * ELE_Y_1 ; i++ )
	{
		if ( map1[i] == 1 )	// マップデータが１
		{
			scafP->dispFlg = TRUE ;							// 表示させる
			if ( scafP->f_move == 0 )		// 足場それぞれのf_moveをチェック
			{
				scafP->pos.x = (float)BLOCKSIZE_X * c_xel ;		// x座標
				scafP->pos.y = (float)BLOCKOFSET ; 				// y座標
				scafP->pos.z = (float)-( BLOCKSIZE_Y * c_yel ) ;	// z座標
				scafP->dir.y = 1 ;									// 回転率
				scafP->c_scaf = 0 ;								// カウンター初期値
			}
		}else{
			scafP->dispFlg = FALSE ;								// 非表示
		}

		c_xel++ ;					// ｘ要素数インクリメント
		if ( c_xel == ELE_X_1 )		// ｘ要素数がELE_X_1だったら
		{
			c_xel = 0 ;		// Xリセット
			c_yel++ ;		// Yカウント
		}

		scafP++;
	}
	return 0 ;
}

/* ------------------------------------------------------------
					LoadMap関数
===============================================================
	マップの読み込み
	引数：mapno..ステージ番号
		  *map..マップデータを入れる配列
	戻り値：int
--------------------------------------------------------------- */
int MapGeneration::LoadMap( int mapno, int *map )
{
	int c_xel = 0 ;			// ｘ要素数
	int c_yel = 0 ;			// ｙ要素数

	const int Llimit_t = 1200 ;
	const int Rlimit_t = 2100 ;
	const int Llimit_u = -900 ;
	const int Rlimit_u = 350 ;
	const int yunder = -600 ;

	switch( mapno )		// mapnoに合わせてマップの読み込み
	{
		case e_stage1 :		// ステージ１
			scafP = &Scaf1[0] ;

			for( int i = 0 ; i < ELE_X_1 * ELE_Y_1 ; i++ )
			{
				map[i] = map1[i] ;										// マップデータの代入
				if ( map[i] == 1 )	// マップデータが１
				{
					scafP->set_DispAndColValid( TRUE ) ;				// 表示させる
					if ( scafP->f_move == 0 )		// 足場それぞれのf_moveをチェック
					{
						scafP->pos.x = (float)BLOCKSIZE_X * c_xel ;		// x座標
						scafP->pos.y = (float)BLOCKOFSET ; 				// y座標
						scafP->pos.z = (float)-( BLOCKSIZE_Y * c_yel ) ;	// z座標
						scafP->dir.y = 1 ;									// 回転率
						scafP->c_scaf = 0 ;								// カウンター初期値
					}
				}else{
					scafP->set_DispAndColValid( FALSE ) ;					// 非表示
				}

				c_xel++ ;					// ｘ要素数インクリメント
				if ( c_xel == ELE_X_1 )		// ｘ要素数がELE_X_1だったら
				{
					c_xel = 0 ;		// Xリセット
					c_yel++ ;		// Yカウント
				}

				if ( (f_move!= 0) && (scafP->f_move == 0) )	// タイマーが２５秒経過かつフラグが０
				{
					scafP->f_move++ ;			// インクリメント
				}
				if ( scafP->f_move == 1 )		// 足場それぞれのフラグが１
				{
					scafP->mapmode = 1 ;		//マップモードを１
					scafP->f_move++ ;			// インクリメント
				}
				if ( scafP->pos.y < -1000 )		// ｙ座標が-1000より下になったら
				{
					scafP->set_DispAndColValid( FALSE ) ;	// 非表示
					f_move = 0 ;							// moveフラグ落とす
				}

				AnimMap1( ) ;	// アニメーション関数
				scafP++ ;
			}

			break ;

		case e_stage2_top :		// ステージ２上部
			scafP = &Scaf2_t[0] ;

			for( int i = 0 ; i < ELE_X_2 * ELE_Y_2 ; i++ )
			{
				map[i] = map2_top[i] ;									// マップデータの代入
				if ( map[i] == 1 )	// マップデータが１
				{
					scafP->set_DispAndColValid( TRUE ) ;				// 表示させる
					if ( f_move == 0 )									// moveフラグが0だったら
					{
						if ( (c_xel % 25) > 6 )		// マップデータの５列以降の足場
						{
							scafP->pos.x = (float)BLOCKSIZE_X * c_xel ;			// x座標
							scafP->pos.y = (float)BLOCKOFSET * 5 ; 				// y座標
							scafP->pos.z = (float)-( BLOCKSIZE_Y * c_yel ) ;	// z座標
							scafP->dir.y = 1 ;									// 回転率
						}else{
							scafP->pos.x = (float)BLOCKSIZE_X * c_xel ;			// x座標
							scafP->pos.y = (float)BLOCKOFSET ; 					// y座標
							scafP->pos.z = (float)-( BLOCKSIZE_Y * c_yel ) ;	// z座標
							scafP->dir.y = 1 ;									// 回転率
						}
					}else{
						if ( c_collapse == 0 )
						{
							if ( (scafP->pos.y < BLOCKOFSET) && (scafP->pos.x > Llimit_t) && (scafP->pos.x < Rlimit_t) )	// 足場がBLOCKOFSETより下に位置しｘ座標がLlimit_tからRlimit_tの時
							{
								scafP->mapmode = 1 ;			// モード１：ブロック上昇
							}else if ( scafP->pos.y >= BLOCKOFSET ){
								scafP->mapmode = 2 ;			// モード２：ブロックの高さ一定
							}
						}else if( (c_collapse == 2) && (scafP->mapmode == 2)){
							scafP->mapmode = 4 ;				// モード４：ステージ変更時アクション
						}else if( scafP->mapmode != 4 ){
							scafP->mapmode = 5 ;				// モード５：足場停止
						}
						if ( scafP->pos.x < -250 )						// 足場のｘ座標が-250になったら
						{
							scafP->mapmode = 3 ;						// モード３：ブロック下移動
							if ( scafP->pos.y < yunder )				// 足場のｙ座標がyunderになったら
							{
								scafP->set_DispAndColValid( FALSE ) ;	// 非表示にする
							}
						}
					}
				}else{
					scafP->set_DispAndColValid( FALSE ) ;				// 非表示
				}
				c_xel++ ;												// ｘ要素数インクリメント		
				if ( c_xel == ELE_X_2 )									// ｘ要素数がELE_X_2だったら
				{
					c_xel = 0 ;		// Xリセット
					c_yel++ ;		// Yカウント
				}

				scafP++ ;
			}
			AnimMap2_t( ) ;	// アニメーション関数

			break ;

		case e_stage2_under :		// ステージ２下部
			scafP = &Scaf2_u[0] ;

			for( int i = 0 ; i < ELE_X_2 * ELE_Y_2 ; i++ )
			{
				map[i] = map2_under[i] ;	// マップデータの代入
				if ( map[i] == 1 )	// マップデータが１
				{
					scafP->set_DispAndColValid( TRUE ) ;				// 表示させる
					if ( f_move == 0 )									// moveフラグが0だったら
					{
						if ( (c_xel % 25) > 6 )		// マップデータの５列以降の足場
						{
							scafP->pos.x = (float)-(BLOCKSIZE_X * c_xel) + 1200 ;			// x座標
							scafP->pos.y = (float)BLOCKOFSET * 5 ; 							// y座標
							scafP->pos.z = (float)-( (BLOCKSIZE_Y * c_yel) + 900 ) ;		// z座標
							scafP->dir.y = 1 ;												// 回転率
						}else{
							scafP->pos.x = (float)-(BLOCKSIZE_X * c_xel) + 1200 ;			// x座標
							scafP->pos.y = (float)BLOCKOFSET ; 								// y座標
							scafP->pos.z = (float)-( (BLOCKSIZE_Y * c_yel) + 900 ) ;		// z座標
							scafP->dir.y = 1 ;												// 回転率
						}
					}else{
						if ( c_collapse == 0 )
						{
							if ( (scafP->pos.y < BLOCKOFSET) && (scafP->pos.x < Rlimit_u) && (scafP->pos.x > Llimit_u) )	// 足場がBLOCKOFSETより下に位置しｘ座標が250から-700の時
							{
								scafP->mapmode = 1 ;			// モード１：ブロック上昇
							}else if ( scafP->pos.y >= BLOCKOFSET ){
								scafP->mapmode = 2 ;			// モード２：ブロックの高さ一定
							}
						}else if( (c_collapse == 2) && (scafP->mapmode == 2)){
							scafP->mapmode = 4 ;				// モード４：ステージ変更時アクション
						}else if( scafP->mapmode != 4 ){
							scafP->mapmode = 5 ;				// モード５：足場停止
						}
						if ( scafP->pos.x > 1450 )						// 足場のｘ座標が1450になったら
						{
							scafP->mapmode = 3 ;						// モードを３：ブロック下移動
							if ( scafP->pos.y < yunder )				// 足場のｙ座標がyunderになったら
							{
								scafP->set_DispAndColValid( FALSE ) ;	// 非表示にする
							}
						}
					}
				}else{
					scafP->set_DispAndColValid( FALSE ) ;				// 非表示
				}
				c_xel++ ;													// ｘ要素数インクリメント		
				if ( c_xel == ELE_X_2 )										// ｘ要素数がELE_X_2だったら
				{
					c_xel = 0 ;		// Xリセット
					c_yel++ ;		// Yカウント
				}

				scafP++ ;
			}

			AnimMap2_u( ) ;	// アニメーション関数

			break ;

		case e_stage3 :		// ステージ３
			scafP = &Scaf3[0] ;

			for( int i = 0 ; i < ELE_X_3 * ELE_Y_3 ; i++ )
			{
				map[i] = map3[i] ;										// マップデータの代入
				if ( map[i] == 1 )	// マップデータが１
				{
					scafP->pos.x = (float)BLOCKSIZE_X * c_xel ;		// x座標
					scafP->pos.y = (float)BLOCKOFSET ; 				// y座標
					scafP->pos.z = (float)-( BLOCKSIZE_Y * c_yel ) ;	// z座標
					scafP->dir.y = 1 ;								// 回転率
					scafP->set_DispAndColValid( TRUE ) ;				// 表示させる
				}else{
					scafP->set_DispAndColValid( FALSE ) ;				// 非表示
				}
				c_xel++ ;						// ｘ要素数インクリメント	
				if ( c_xel == ELE_X_3 )			// ｘ要素数がELE_X_2だったら
				{
					c_xel = 0 ;		// Xリセット
					c_yel++ ;		// Yカウント
				}

				scafP++ ;
			}
			g_Object[ OBJID_SEA ].pos.x += 1.0f ;
			break ;
	}
	g_Object[ OBJID_HUNSUI ].dir.y += 0.01f ;		// 噴水回転

	return 0 ;
}

/* ------------------------------------------------------------
					AnimMap1関数
===============================================================
	マップ1変更時のアニメーションScafのモードで管理

	引数：なし
	戻り値：int
--------------------------------------------------------------- */
int MapGeneration::AnimMap1( )
{
	scafAnimP = &Scaf1[0] ;

	for( int i = 0 ; i < ELE_X_1 * ELE_Y_1 ; i++ )
	{
		switch( scafAnimP->mapmode )
		{
			case 0 :		// モード０：待機
				break ;

			case 1 :		// モード１：ブロック右に移動
				if ( (Time[e_Timer1].CountDesignation(0.05) != 0) && (scafAnimP->c_scaf < 150) )		// タイマーが0.05秒経過してカウンターが300未満
				{
					scafAnimP->pos.x += (0.2f * i) ;	// ｘ座標に移動値足す
					scafAnimP->mapmode++ ;				// モードインクリメント
					scafAnimP->c_scaf++ ;				// カウンターインクリメント
				}
				if ( scafAnimP->c_scaf >= 300 )			// カウンターが１０より大きくなったら
				{
					if ( Time[e_Timer1].CountDesignation(0.5) != 0 )		// タイマーが0.5秒経過したら
					{
						scafAnimP->c_scaf = 0 ;			// カウンターリセット
						scafAnimP->mapmode++ ;			// モードインクリメント
						Time[e_Timer1].ResetTimer( ) ;	// これないと綺麗に落ちない
					}
				}
				break ;

			case 2 :		// モード２：ブロック左に移動
				if ( (Time[e_Timer1].CountDesignation(0.05) != 0) && (scafAnimP->c_scaf < 150) )		// タイマーが0.05秒経過してカウンターが300未満
				{
					scafAnimP->pos.x -= (0.2f * i) ;	// ｘ座標に移動値足す
					scafAnimP->mapmode-- ;				// モードデクリメント
					scafAnimP->c_scaf++ ;				// カウンターインクリメント
				}
				if ( scafAnimP->c_scaf >= 150 )			// カウンターが300より大きくなったら
				{
					if ( Time[e_Timer1].CountDesignation(0.5) != 0 )		// タイマーが0.5秒経過したら
					{
						scafAnimP->c_scaf = 0 ;			// カウンターリセット
						scafAnimP->mapmode++ ;			// モードインクリメント
					}
				}
				break ;

			case 3 :		// モード３：ブロック落下
				scafAnimP->pos.y -= 1.0f ;			// ｙ座標に移動値足す
				break ;

			case 4 :		// モード４：ブロック消す
				scafAnimP->dispFlg = FALSE ;		// 非表示
				break ;
		}
		scafAnimP++ ;
	}

	return 0 ;
}

/* ------------------------------------------------------------
					AnimMap2_t関数
===============================================================
	マップ2変更時のアニメーションScafのモードで管理

	引数：なし
	戻り値：int
--------------------------------------------------------------- */
int MapGeneration::AnimMap2_t( )
{
	scafAnimP = &Scaf2_t[0] ;

	for( int i = 0 ; i < ELE_X_2 * ELE_Y_2 ; i++ )
	{
		if ( (scafAnimP->mapmode != 0) && (scafAnimP->mapmode != 4) && (scafAnimP->mapmode != 5) && (scafAnimP->mapmode != 6) )
		{
			scafAnimP->movepos.x = -0.8f ;				// 足場のｘ移動距離代入
		}
		scafAnimP->pos.x += scafAnimP->movepos.x ;		// 足場のｘ座標に移動距離足しこむ

		switch( scafAnimP->mapmode )
		{
			case 0 :		// モード０：待機
				if ( f_move == 1 )
					scafAnimP->movepos.x = -0.8f ;		// 足場フラグが立ったら移動値入れる
				break ;

			case 1 :		// モード１：ブロック上昇
				scafAnimP->movepos.y = 1.5f ;					// 足場のｙ移動距離代入
				scafAnimP->pos.y += scafAnimP->movepos.y ;		// 足場のｙ座標に移動距離足しこむ
				break ;

			case 2 :		// モード２：ブロック高さそろえる
				scafAnimP->pos.y = BLOCKOFSET ;					// x座標が一定の位置に行ったら高さそろえる
				scafP->dispFlg = TRUE ;
				break ;

			case 3 :		// モード３：ブロック下降
				scafAnimP->movepos.y = -1.5f ;					// 足場のｙ移動距離代入
				scafAnimP->pos.y += scafAnimP->movepos.y ;		// 足場のｙ座標に移動距離足しこむ
				break ;

			case 4 :		// モード４：ステージ変更時アクション
				scafAnimP->movepos.x = 0.0f ;						// 足場のｘ移動距離代入
				if ( Time[e_Timer1].CountDesignation(0.05) != 0 )	// タイマーが0.05秒経過してカウンターが300未満
				{
					if ( scafAnimP->pos.y < BLOCKOFSET + 200 )
						scafAnimP->movepos.y += (0.5f * i) ;		// 足場のｙ移動距離代入
					scafAnimP->pos.y += scafAnimP->movepos.y ;		// 足場のｙ座標に移動距離足しこむ
					scafAnimP->c_scaf++ ;		// カウンターインクリメント
				}
				g_Object[ OBJID_HUNSUI ].movepos.y = 0.1f ;
				break ;

			case 5 :		// モード５：足場停止
				scafAnimP->movepos.x = 0.0f ;		// マップの移動値０
				scafAnimP->movepos.y = 0.0f ;		// マップの移動値０
				scafAnimP->movepos.z = 0.0f ;		// マップの移動値０
				break ;
		}

		scafAnimP++ ;
	}

	// ---------------------
	//		噴水の動き
	// ---------------------
	objP = &g_Object[ OBJID_HUNSUI ] ;

	if ( objP->size.x < 13.0f )
	{
		objP->size.x += objP->movepos.y ;		// 噴水のｘサイズ13.0fまで拡大
		objP->size.z += objP->movepos.y ;		// 噴水のｚサイズ13.0fまで拡大
	}
	if ( objP->size.y <= 18.0f )
	{
		objP->size.y += objP->movepos.y ;		// 噴水のｙサイズ18.0fまで拡大
	}else{
		objP->size.y = 18.0f ;		// ｙサイズ固定
	}
	objP->pos.y += (objP->movepos.y * 1.2f) ;	// 噴水上昇
	objP->dir.y += 0.1f ;						// 噴水回転

	g_Object[ OBJID_SEA ].dispFlg = TRUE ;
	g_Object[ OBJID_SEA ].pos.x += 1.5f ;
	return 0 ;
}

/* ------------------------------------------------------------
					AnimMap2_u関数
===============================================================
	マップ2変更時のアニメーションScafのモードで管理

	引数：なし
	戻り値：int
--------------------------------------------------------------- */
int MapGeneration::AnimMap2_u( )
{
	scafAnimP = &Scaf2_u[0] ;

	for( int i = 0 ; i < ELE_X_2 * ELE_Y_2 ; i++ )
	{
		if ( (scafAnimP->mapmode != 0) && (scafAnimP->mapmode != 4) && (scafAnimP->mapmode != 5) && (scafAnimP->mapmode != 6) )
		{
			scafAnimP->movepos.x = 0.8f ;				// 足場のｘ移動距離代入
		}
		scafAnimP->pos.x += scafAnimP->movepos.x ;		// 足場のｘ座標に移動距離足しこむ

		switch( scafAnimP->mapmode )
		{
			case 0 :		// モード０：待機
				if ( f_move == 1 )
					scafAnimP->movepos.x = 0.8f ;		// 足場フラグが立ったら移動値入れる
				break ;

			case 1 :		// モード１：ブロック上昇
				scafAnimP->movepos.y = 1.5f ;					// 足場のｙ移動距離代入
				scafAnimP->pos.y += scafAnimP->movepos.y ;		// 足場のｙ座標に移動距離足しこむ
				break ;

			case 2 :		// モード２：ブロック高さそろえる
				scafAnimP->pos.y = BLOCKOFSET ;					// x座標が一定の位置に行ったら高さそろえる
				break ;

			case 3 :		// モード３：ブロック下降
				scafAnimP->movepos.y = -1.5f ;					// 足場のｙ移動距離代入
				scafAnimP->pos.y += scafAnimP->movepos.y ;		// 足場のｙ座標に移動距離足しこむ
				break ;

			case 4 :		// モード４：ステージ変更時アクション
				scafAnimP->movepos.x = 0.0f ;						// 足場のｘ移動距離代入
				if ( Time[e_Timer1].CountDesignation(0.05) != 0 )	// タイマーが0.05秒経過してカウンターが300未満
				{
					if ( scafAnimP->pos.y < BLOCKOFSET + 200 )
						scafAnimP->movepos.y += (0.5f * i) ;		// 足場のｙ移動距離代入
					scafAnimP->pos.y += scafAnimP->movepos.y ;		// 足場のｙ座標に移動距離足しこむ
					scafAnimP->c_scaf++ ;		// カウンターインクリメント
				}
				break ;

			case 5 :		// モード５：足場停止
				scafAnimP->movepos.x = 0.0f ;		// マップの移動値０
				scafAnimP->movepos.y = 0.0f ;		// マップの移動値０
				scafAnimP->movepos.z = 0.0f ;		// マップの移動値０
				break ;
		}
		scafAnimP++ ;
	}

	return 0 ;
}

/* ------------------------------------------------------------
					HideMap関数
===============================================================
	指定したマップを非表示にする

	引数：int
	戻り値：なし
--------------------------------------------------------------- */
void MapGeneration::HideMap( int hideno )
{
	switch( hideno )
	{
		case e_stage1 :			// ステージ１
			for( int i = 0 ; i < ELE_X_1 * ELE_Y_1 ; i++ )
			{
				Scaf1[ i ].set_DispAndColValid( FALSE ) ;
			}
			break ;

		case e_stage2_top :		// ステージ２上
			for( int i = 0 ; i < ELE_X_2 * ELE_Y_2 ; i++ )
			{
				Scaf2_t[ i ].set_DispAndColValid( FALSE ) ;
			}
			break ;

		case e_stage2_under :	// ステージ２下
			for( int i = 0 ; i < ELE_X_2 * ELE_Y_2 ; i++ )
			{
				Scaf2_u[ i ].set_DispAndColValid( FALSE ) ;
			}
			break ;

		case e_stage3 :		// ステージ３
			for( int i = 0 ; i < ELE_X_3 * ELE_Y_3 ; i++ )
			{
				Scaf3[ i ].set_DispAndColValid( FALSE ) ;
			}
			break ;
	}
}

/* ------------------------------------------------------------
					AfterMapInit関数
===============================================================
	マップ切り替え後の初期セット

	引数：なし
	戻り値：int
--------------------------------------------------------------- */
int MapGeneration::AfterMapInit( )
{
	c_collapse = 0 ;	// カウンターリセット

	TimeUI[0].f_disp = TRUE ;			// タイマーUI非表示
	TimeUI[1].f_disp = TRUE ;			// タイマーUI非表示

	ScoreBoard[0].f_disp = TRUE ;		// タイマーUI非表示
	ScoreBoard[1].f_disp = TRUE ;		// タイマーUI非表示

	UI_DGauge_ID[0].f_disp = TRUE ;		// --- UI表示_ドットゲージ、プレイヤーID表示
	UI_DGauge_ID[1].f_disp = TRUE ;		// --- UI表示_ドットゲージ、プレイヤーID表示

	return 0 ;
}

/* ------------------------------------------------------------
					MapChangeInitSet1to2関数
===============================================================
	1から2のマップ切り替え時の初期セットをする

	引数：なし
	戻り値：int
--------------------------------------------------------------- */
int MapGeneration::MapChangeInitSet1to2( )
{
	g_Player[0].PlayerAction_Invisible1to2() ;		// プレイヤーステージ移行初期セット
	g_Player[1].PlayerAction_Invisible1to2() ;		// プレイヤーステージ移行初期セット

	return 0 ;
}

/* ------------------------------------------------------------
					MapChangeInitSet2to3数
===============================================================
	2から3のマップ切り替え時の初期セットをする

	引数：なし
	戻り値：int
--------------------------------------------------------------- */
int MapGeneration::MapChangeInitSet2to3( )
{
	g_Player[0].PlayerAction_Invisible2to3() ;		// プレイヤーステージ移行初期セット
	g_Player[1].PlayerAction_Invisible2to3() ;		// プレイヤーステージ移行初期セット

	return 0 ;
}


