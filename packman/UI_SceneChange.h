/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: UI_SceneChange.h
	CREATE	: NOGUCHI
	
	------ Explanation of file ---------------------------------------------------------------------
       
	UI_SceneChangeクラスの宣言。ぶわぁぁぁって落ちるやつを作りたい！

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

class UI_SceneChange : public UserInterface
{
	public :
		UI_SceneChange( ) ;			// コンストラクタ
		~UI_SceneChange( ) ;		// デストラクタ

		void initStates() ;			// --- ステータス初期化

		int		UIAction( int ) ;	// --- シーン遷移アクション
		void	UIDraw( int ) ;		// --- シーン遷移描画

		// --- 以下フィールド
		int			BarNum ;		// --- バーの必要な数
		c_fPoint	BarPos[6] ;		// --- バー座標
		float		BarMoveSpeed ;	// --- バー移動速度

		c_fPoint	BarInitPos[3] ;	// --- 移動の際のチェック位置( 画面端＿上、真ん中、画面端＿下 )

		c_countGroup<int>	moveCnt ;	// --- 移動カウンタ
} ;
