/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: UI_DotGaugeAndPlayerID.h
	CREATE	: NOGUCHI
	
	------ Explanation of file ---------------------------------------------------------------------
       
	UI_DotGaugeAndPlayerIDクラスの宣言

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

class UI_DotGaugeAndPlayerID : public UserInterface
{
	public :
		UI_DotGaugeAndPlayerID( ) ;			// コンストラクタ
		~UI_DotGaugeAndPlayerID( ) ;		// デストラクタ

		void initStates() ;					// --- ステータス初期化

		void UIDraw( int ) ;				// --- プレイヤーの１P・２P表示

		void DrawFrame( int ) ;				// --- 枠描画
		void DrawGauge( int ) ;				// --- ゲージの描画のみ行う
		void DrawPlayerID( int ) ;			// --- プレイヤーIDの描画のみ行う
		void DrawSuperFlash() ;				// --- スーパー状態の可視化フレームの描画のみ行う

		c_countGroup<int>	superTime ;		// --- スーパータイムのカウンタ( 大元が使いづらいのでデータを変える )

		c_countGroup<float>	flamePower ;	// --- ドットゲージの後ろに描画する「炎」の大きさカウンタ
		BOOL				isDispFlame ;	// --- 炎の表示フラグ
		double				flameSize ;		// --- 炎の実際の大きさ( 1.0が等倍 )

		c_countGroup<float>	testLimit ;		// --- スーパーパックマン_メーターリミット( TEST )

		c_countGroup<float>	gaugeDisp_Damage ;	// --- ダメージ表記のメーターカウンタ
		c_countGroup<float>	damage_dispTime ;	// --- ダメージゲージを表示しておく時間
		int					gauge_Before ;		// --- 前回( 1フレーム前 )のゲージ分
		int					maxZurashi ;		// --- 被弾時のゲージ揺れ幅

		c_countGroup<int>	flashAlpha ;		// --- フラッシュの透明値
		c_countGroup<float>	flashSize ;			// --- フラッシュのサイズ
		c_countGroup<float>	flashRot ;			// --- フラッシュの回転率
} ;

