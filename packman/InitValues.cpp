/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: InitValues.cpp
	CREATE	: NOGUCHI

	------ Explanation of file ---------------------------------------------------------------------
       
	   初期化関連の関数をまとめたクラス。

	   ・Init All States				- いわゆるInit関連のメイン部分。
	   ・Init Var States				- 変数の初期セット部分。
	   ・Init Graph States				- 画像関連の初期セット部分
	   ・Init Model And Anim States		- モデル、アニメの初期セット部分。
	   ・Init Object States				- オブジェクトの初期セット部分。
	   ・Init Attachment Gamepad		- ゲームパッド系の初期セット部分。

	------------------------------------------------------------------------------------------------

	   今回、InitAllStates()で行うLoad系の関数を上手く利用して「ロード画面」を作ろうと思う。 
	   ロード完了分の数値を表す「g_load.now」にLoad系関数が正常終了する度に加算し、
	   その数値を用いてゲージ画像の右端を切り取っている。

	   描画は数値加算のタイミングで行っており、ScreenFlip()を使用することで即描画が実現している。
	   Load系が終了する度に、下記の「LF_SETUP」を記述して関数を呼び出そう。

	------------------------------------------------------------------------------------------------

		そのほか、画像の読み込みを行うと同時に「画像サイズ、オフセット(初期は中心点)」を
		セットしている。g_graphHandle[].offSet, fullSizeがそれに該当する。

		chipSizeだけはセットできないので、initGraphStates()内でやっちゃってください。
	
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

#include "Common.h"

#define LF_SETUP	LoadGaugeDisp_FillOK( 1 ) ;		// --- 関数名が長ったらしいので短縮の意を込めてDEFINE
#define IN_DATA		"..\\ANIMAL VACATION\\Data\\"			// --- データへのパス

// =====================================================================================================================Init All States
/* --------------------------------------------------------------

					Init 関連のメイン実装部
		関数内がぐっちょりするのが嫌だったので、分けた。

---------------------------------------------------------------- */
int InitAllStates()
{
	InitVarStates() ;					// --- 変数関連の初期化
	InitSoundStates() ;					// --- サウンド関連の初期化
	InitGraphStates() ;					// --- 画像関連の初期化
	InitModelAndAnimStates() ;			// --- モデル、アニメ関連の初期化
	InitObjectStates() ;				// --- オブジェクト関連の初期化
	InitAttachmentPad() ;				// --- ゲームパッドの初期化

	return 1 ;
}

// ===================================================================================================================== Init Var States
/* --------------------------------------------------------------

						変数の初期化関数
					  「Var」=「Variable」

---------------------------------------------------------------- */
int InitVarStates()
{
	// -------------
	//	カメラ関連
	// -------------
	//							X			Y			Z
	g_camera.pos		= VGet( 400.0f,		400.0f,		-2200.0f ) ;		// --- 位置
	g_camera.lookPos	= VGet( 400.0f,		0.0f,		-100.0f ) ;			// --- 注視点
	g_camera.upDir		= VGet( 0.0f,		1.0f,		0.0f ) ;			// --- 上方向

	// -------------
	//	 重要変数
	// -------------
	int otSX, otSY, emptyDummy ;						// --- サイズの格納用変数。カラービット情報は不要なので最後の変数ははダミー。
	
	GetScreenState( &otSX, &otSY, &emptyDummy ) ;		// --- ウィンドウサイズを取得。変動する可能性があるので変数に入れるよ。
	g_winSize.x = otSX ;
	g_winSize.y = otSY ;

	g_gameSpeed	= 1.0f ;					// --- ゲームスピード
	g_gameMode	= e_Mode_Blank ;			// --- ゲームモード

	g_loadCnt.max[0] = LOADNUM_MAX - 2 ;	// --- 読み込む画像、モデルの統計

	// -------------
	//  その他変数
	// -------------
	SecureZeroMemory( &g_debInfo, sizeof( DEBUGMODE_INFO ) ) ;

	return 1 ;
}
// ===================================================================================================================== Init Sound States
/* --------------------------------------------------------------

						サウンドの初期化関数

---------------------------------------------------------------- */
int InitSoundStates()
{
	SOUND_INFO *otSP ;

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//		   ハンドルセット部
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	otSP = &g_soundHandle[ e_Sound_TitleBGM ] ;
	otSP->soundHandle		= LoadSoundMem( IN_DATA "sound\\BGM\\Title_BGM.mp3" ) ;		LF_SETUP	// --- タイトルBGM
	otSP->playType			= DX_PLAYTYPE_LOOP ;																// --- タイトルBGM
	
	otSP = &g_soundHandle[ e_Sound_SeaWaveSE ] ;
	otSP->soundHandle		= LoadSoundMem( IN_DATA "sound\\SE\\se_seawave.wav" ) ;		LF_SETUP	// --- 海の音
	otSP->playType			= DX_PLAYTYPE_BACK ;												// --- 海の音
	
	otSP = &g_soundHandle[ e_Sound_SelectBGM ] ;
	otSP->soundHandle		= LoadSoundMem( IN_DATA "sound\\BGM\\Select_BGM.mp3" ) ;	LF_SETUP	// --- セレクトBGM
	otSP->playType			= DX_PLAYTYPE_LOOP ;												// --- セレクトBGM
	
	otSP = &g_soundHandle[ e_Sound_BattleBGM ] ;
	otSP->soundHandle		= LoadSoundMem( IN_DATA "sound\\BGM\\Battle_BGM.mp3" ) ;	LF_SETUP	// --- バトルBGM
	otSP->playType			= DX_PLAYTYPE_LOOP ;												// --- バトルBGM
	
	otSP = &g_soundHandle[ e_Sound_TitlePushSE ] ;
	otSP->soundHandle		= LoadSoundMem( IN_DATA "sound\\SE\\se_pirori.mp3" ) ;			LF_SETUP	// --- タイトル_プッシュ音
	otSP->playType			= DX_PLAYTYPE_BACK ;													// --- タイトル_プッシュ音

	otSP = &g_soundHandle[ e_Sound_CursolSE ] ;
	otSP->soundHandle		= LoadSoundMem( IN_DATA "sound\\SE\\se_cursol.mp3" ) ;			LF_SETUP	// --- タイトル_カーソル音
	otSP->playType			= DX_PLAYTYPE_BACK ;													// --- タイトル_カーソル音

	otSP = &g_soundHandle[ e_Sound_CountdownSE ] ;
	otSP->soundHandle		= LoadSoundMem( IN_DATA "sound\\SE\\se_Countdown.wav" ) ;		LF_SETUP	// --- バトル_カウントダウン
	otSP->playType			= DX_PLAYTYPE_BACK ;													// --- バトル_カウントダウン

	otSP = &g_soundHandle[ e_Sound_GongSE ] ;
	otSP->soundHandle		= LoadSoundMem( IN_DATA "sound\\SE\\se_gong.mp3" ) ;			LF_SETUP	// --- バトル_ゴング音
	otSP->playType			= DX_PLAYTYPE_BACK ;													// --- バトル_ゴング音

	otSP = &g_soundHandle[ e_Sound_VibrationSE ] ;
	otSP->soundHandle		= LoadSoundMem( IN_DATA "sound\\SE\\se_katakata.wav" ) ;		LF_SETUP	// --- バトル_ゴング音
	otSP->playType			= DX_PLAYTYPE_BACK ;													// --- バトル_ゴング音

	otSP = &g_soundHandle[ e_Sound_BattlePushSE ] ;
	otSP->soundHandle		= LoadSoundMem( IN_DATA "sound\\SE\\se_battle_push.mp3" ) ;		LF_SETUP	// --- バトル_プッシュ音
	otSP->playType			= DX_PLAYTYPE_BACK ;													// --- バトル_プッシュ音

	otSP = &g_soundHandle[ e_Sound_DoteatSE ] ;
	otSP->soundHandle		= LoadSoundMem( IN_DATA "sound\\SE\\se_eat.mp3" ) ;				LF_SETUP	// --- ドット食べる音
	otSP->playType			= DX_PLAYTYPE_BACK ;													// --- ドット食べる音

	otSP = &g_soundHandle[ e_Sound_PowerUpSE ] ;
	otSP->soundHandle		= LoadSoundMem( IN_DATA "sound\\SE\\se_powerup.mp3" ) ;			LF_SETUP	// --- パワーアップ音
	otSP->playType			= DX_PLAYTYPE_BACK ;													// --- パワーアップ音

	otSP = &g_soundHandle[ e_Sound_ReturnSE ] ;
	otSP->soundHandle		= LoadSoundMem( IN_DATA "sound\\SE\\se_powerdown.wav" ) ;		LF_SETUP	// --- パワーダウン音
	otSP->playType			= DX_PLAYTYPE_BACK ;													// --- パワーダウン音

	otSP = &g_soundHandle[ e_Sound_FallSE ] ;
	otSP->soundHandle		= LoadSoundMem( IN_DATA "sound\\SE\\se_fall.wav" ) ;			LF_SETUP	// --- 落下音
	otSP->playType			= DX_PLAYTYPE_BACK ;													// --- 落下音

	otSP = &g_soundHandle[ e_Sound_ChakuchiSE ] ;
	otSP->soundHandle		= LoadSoundMem( IN_DATA "sound\\SE\\se_Chakuchi.mp3" ) ;		LF_SETUP	// --- 着地音
	otSP->playType			= DX_PLAYTYPE_BACK ;													// --- 着地音

	otSP = &g_soundHandle[ e_Sound_BuzzerSE ] ;
	otSP->soundHandle		= LoadSoundMem( IN_DATA "sound\\SE\\se_buzzer.wav" ) ;			LF_SETUP	// --- ステージ切り替えブザー
	otSP->playType			= DX_PLAYTYPE_BACK ;													// --- ステージ切り替えブザー

	otSP = &g_soundHandle[ e_Sound_WaterSE ] ;
	otSP->soundHandle		= LoadSoundMem( IN_DATA "sound\\SE\\se_water.mp3" ) ;			LF_SETUP	// --- 水の吹き上げる音
	otSP->playType			= DX_PLAYTYPE_BACK ;													// --- 水素の音ォ〜

	otSP = &g_soundHandle[ e_Sound_TimeOutSE ] ;
	otSP->soundHandle		= LoadSoundMem( IN_DATA "sound\\SE\\se_whistle.mp3" ) ;			LF_SETUP	// --- ホイッスル
	otSP->playType			= DX_PLAYTYPE_BACK ;													// --- ホイッスル

	otSP = &g_soundHandle[ e_Sound_heartSE ] ;
	otSP->soundHandle		= LoadSoundMem( IN_DATA "sound\\SE\\heart.wav" ) ;				LF_SETUP	// --- 心臓音
	otSP->playType			= DX_PLAYTYPE_LOOP ;													// --- 心臓音

	otSP = &g_soundHandle[ e_Sound_DrumrollSE ] ;
	otSP->soundHandle		= LoadSoundMem( IN_DATA "sound\\SE\\drum-roll.wav" ) ;			LF_SETUP	// --- ドラムロール音
	otSP->playType			= DX_PLAYTYPE_LOOP ;													// --- ドラムロール音

	otSP = &g_soundHandle[ e_Sound_rollfinishSE ] ;
	otSP->soundHandle		= LoadSoundMem( IN_DATA "sound\\SE\\roll-finish.mp3" ) ;		LF_SETUP	// --- ドラムロール(終了)音
	otSP->playType			= DX_PLAYTYPE_BACK ;													// --- ドラムロール(終了)音

	otSP = &g_soundHandle[ e_Sound_applauseSE ] ;
	otSP->soundHandle		= LoadSoundMem( IN_DATA "sound\\SE\\cheer.wav" ) ;				LF_SETUP	// --- タイトル_拍手音
	otSP->playType			= DX_PLAYTYPE_LOOP ;													// --- タイトル_拍手音

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//			音量セット部
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	otSP = &g_soundHandle[0] ;
	for ( int i = 0 ; i < e_Sound_Total ; i++ )
	{
		otSP->volume = 100 ;
		otSP++ ;
	}

	/* ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~= */
	/*		読み込みエラーのチェック		*/
	/* ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~= */
	otSP = &g_soundHandle[0] ;
	for ( int i = 0 ; i < e_Sound_Total ; i++ )
	{
		if ( otSP->soundHandle == -1 )
			printf( "失敗【%02d番】のサウンドハンドルが不明です。\n", i ) ;

		otSP++ ;
	}

	return 1 ;
}

// ===================================================================================================================== Init Graph States
/* --------------------------------------------------------------

						画像の初期化関数

---------------------------------------------------------------- */
int InitGraphStates()
{
	// 透過色を設定
    SetTransColor( 0, 255, 0 ) ;		// 緑色が透過色

	/* ----------------------------------------------------
					画像の登録( ロード画面用 )
	------------------------------------------------------- */
	#define UI_PATH	"..\\ANIMAL VACATION\\Data\\image\\UI_matome\\"
	#define EF_PATH	"..\\ANIMAL VACATION\\Data\\image\\Effect_matome\\"

	g_graphHandle_LoadView[ e_imgLoadView_View ].handle		= LoadGraph( UI_PATH "Loading_View.png" ) ;		// --- ロード用_背景
	g_graphHandle_LoadView[ e_imgLoadView_Gauge ].handle	= LoadGraph( UI_PATH "Loading_Gauge.png" ) ;	// --- ロード用_ゲージ_余白透過済み

	DrawGraph( 0, 0, g_graphHandle_LoadView[ e_imgLoadView_View ].handle, TRUE ) ;							// --- ロ〜ド画面描画( このInitAllStates() 内限定 )
	ScreenFlip() ;

	/* ----------------------------------------------------
			画像の登録（画像はＢＭＰを使う！！！！）
	------------------------------------------------------- */
	g_graphHandle[e_Image_Skysun].handle			= LoadGraph( IN_DATA "3Dmodels\\object\\skydormsun.bmp" ) ;			LF_SETUP	// スカイドーム（朝）
	g_graphHandle[e_Image_Sunset].handle			= LoadGraph( IN_DATA "3Dmodels\\object\\skydormsunset.png" ) ;		LF_SETUP	// スカイドーム（夕）
	g_graphHandle[e_Image_Hoshizora].handle			= LoadGraph( IN_DATA "image\\hosizora.png" ) ;						LF_SETUP	// スカイドーム（夜）
	g_graphHandle[e_Image_Beach].handle				= LoadGraph( IN_DATA "3Dmodels\\object\\Beach\\beach_new.png" ) ;	LF_SETUP	// ビーチ
	g_graphHandle[e_Image_Water].handle				= LoadGraph( EF_PATH "woter.png" ) ;								LF_SETUP	// 水滴
	g_graphHandle[e_Image_Time1].handle				= LoadGraph( UI_PATH "GameNumber.png" ) ;							LF_SETUP	// タイマーのUI
	g_graphHandle[e_Image_Time1_FlashBlend].handle	= LoadGraph( UI_PATH "GameNumber_FlashBlend.png" ) ;				LF_SETUP	// タイマーのUI_発光ブレンド
	g_graphHandle[e_Image_Time1_RedBlend].handle	= LoadGraph( UI_PATH "GameNumber_RedFlashBlend.png" ) ;				LF_SETUP	// タイマーのUI_発光ブレンド_赤
	g_graphHandle[e_Image_Score].handle				= LoadGraph( UI_PATH "number2nd.bmp" ) ;							LF_SETUP	// スコアのUI
	g_graphHandle[e_Image_PlayerID].handle			= LoadGraph( UI_PATH "String_Player_ID.png" ) ;						LF_SETUP	// 文字_プレイヤーID表示
	g_graphHandle[e_Image_PlayerID_Chase].handle	= LoadGraph( UI_PATH "String_Player_ID_Chase.png" ) ;				LF_SETUP	// 文字_プレイヤーID表示
	g_graphHandle[e_Image_Line].handle				= LoadGraph( UI_PATH "Pacman_Logo.bmp" ) ;							LF_SETUP	// スコア関連
	g_graphHandle[e_Image_Effect_Star].handle		= LoadGraph( EF_PATH "effect_Star.bmp" ) ;							LF_SETUP	// エフェクト_お星さま
	g_graphHandle[e_Image_Effect_Shine].handle		= LoadGraph( EF_PATH "Effect_Shine.png" ) ;							LF_SETUP	// エフェクト_煌めき
	g_graphHandle[e_Image_Effect_Coconut].handle	= LoadGraph( EF_PATH "effect_coco.png" ) ;							LF_SETUP	// エフェクト_リザルトココナッツ
	g_graphHandle[e_Image_Effect_Yasinoki].handle	= LoadGraph( EF_PATH "effect_yashinoki.png" ) ;						LF_SETUP	// エフェクト_リザルトヤシの木
	g_graphHandle[e_Image_Title].handle				= LoadGraph( UI_PATH "Title.png" ) ;								LF_SETUP	// タイトル_テスト
	g_graphHandle[e_Image_PushtoEnter].handle		= LoadGraph( UI_PATH "PressMaru_New.png" ) ;						LF_SETUP	// タイトル_PushToEnter
	g_graphHandle[e_Image_cFrame_OK].handle			= LoadGraph( UI_PATH "CharaSelect_OKFrame.png" ) ;					LF_SETUP	// OKフレーム
	g_graphHandle[e_Image_cFrame_Str].handle		= LoadGraph( UI_PATH "CharaSelect_String.png" ) ;					LF_SETUP	// OKフレーム_文字
	g_graphHandle[e_Image_cFrame_Select].handle		= LoadGraph( UI_PATH "CharaSelect_SelectFrame.png" ) ;				LF_SETUP	// OKフレーム_選択状況
	g_graphHandle[e_Image_cIcon_Cat].handle			= LoadGraph( UI_PATH "CharaIcon_Cat.png" ) ;						LF_SETUP	// キャラアイコン_ネコ
	g_graphHandle[e_Image_cIcon_Fox].handle			= LoadGraph( UI_PATH "CharaIcon_Fox.png" ) ;						LF_SETUP	// キャラアイコン_キツネ
	g_graphHandle[e_Image_cIcon_Pig].handle			= LoadGraph( UI_PATH "CharaIcon_Pig.png" ) ;						LF_SETUP	// キャラアイコン_ブタ
	g_graphHandle[e_Image_cIcon_FlashBlend].handle	= LoadGraph( UI_PATH "CharaIcon_FlashBlend.bmp" ) ;					LF_SETUP	// キャラアイコン_発光ブレンド
	g_graphHandle[e_Image_cIcon_Select1P].handle	= LoadGraph( UI_PATH "1P_SelectFrame.png" ) ;						LF_SETUP	// キャラアイコン選択状況可視化UI_1P
	g_graphHandle[e_Image_cIcon_Select2P].handle	= LoadGraph( UI_PATH "2P_SelectFrame.png" ) ;						LF_SETUP	// キャラアイコン選択状況可視化UI_2P
	g_graphHandle[e_Image_VS_Icon].handle			= LoadGraph( UI_PATH "VS_Icon.png" ) ;								LF_SETUP	// VSのアイコン
	g_graphHandle[e_Image_DotGauge].handle			= LoadGraph( UI_PATH "DotGauge.png" ) ;								LF_SETUP	// ドットゲージ
	g_graphHandle[e_Image_DotGauge_Max].handle		= LoadGraph( UI_PATH "DotGauge_Max.png" ) ;							LF_SETUP	// ドットゲージ_MAX
	g_graphHandle[e_Image_DotGauge_Waku].handle		= LoadGraph( UI_PATH "DotGauge_Waku.png" ) ;						LF_SETUP	// ドットゲージ_枠
	g_graphHandle[e_Image_DotGauge_Damage].handle	= LoadGraph( UI_PATH "DotGauge_Damage.png" ) ;						LF_SETUP	// ドットゲージ_ダメージ分
	g_graphHandle[e_Image_DotGauge_Flame].handle	= LoadGraph( UI_PATH "DotGauge_Flame.png" ) ;						LF_SETUP	// ドットゲージ_ファイア
	g_graphHandle[e_Image_MiniIcon_Animals].handle	= LoadGraph( UI_PATH "Icon_Animals.png" ) ;							LF_SETUP	// アニマルアイコン(小)
	g_graphHandle[e_Image_StateFrame].handle		= LoadGraph( UI_PATH "Game_State_Waku.png" ) ;						LF_SETUP	// ステータス枠
	g_graphHandle[e_Image_CountDown_Str].handle		= LoadGraph( UI_PATH "CountDOWN.png" ) ;							LF_SETUP	// カウントダウン用文字列「3 2 1 GO!」
	g_graphHandle[e_Image_TimeUp_Str].handle		= LoadGraph( UI_PATH "TimeUp.png" ) ;								LF_SETUP	// タイムアップ用文字列「TIME UP」
	g_graphHandle[e_Image_Warning_Str].handle		= LoadGraph( UI_PATH "WARNING.png" ) ;								LF_SETUP	// 警告表示文字列「WARNING」
	g_graphHandle[e_Image_Warning_Bar].handle		= LoadGraph( UI_PATH "WARNING_BAR.png" ) ;							LF_SETUP	// 警告表示バー
	g_graphHandle[e_Image_Warning_Line].handle		= LoadGraph( UI_PATH "WARNING_Line.png" ) ;							LF_SETUP	// 警告表示ライン
	g_graphHandle[e_Image_SuperFlash].handle		= LoadGraph( UI_PATH "SuperFlash.png" ) ;							LF_SETUP	// スーパー状態の可視化フレーム
	g_graphHandle[e_Image_SceneChange_Bar].handle	= LoadGraph( UI_PATH "SceneChange_Bar.png" ) ;						LF_SETUP	// シーンチェンジ_バー
	g_graphHandle[e_Image_SceneChange_Icon].handle	= LoadGraph( UI_PATH "SceneChange_Icon.png" ) ;						LF_SETUP	// シーンチェンジ_アイコン	
	g_graphHandle[e_Image_Result_ScoreIcon].handle	= LoadGraph( UI_PATH "Result_ScoreIcon_Only.png" ) ;				LF_SETUP	// スコアアイコン
	g_graphHandle[e_Image_Result_WinStr].handle		= LoadGraph( UI_PATH "String_Win.png" ) ;							LF_SETUP	// 文字_1PWIN 2PWIN 
	g_graphHandle[e_Image_Result_BackFlash].handle	= LoadGraph( UI_PATH "BackFlash.png" ) ;							LF_SETUP	// 「WIN」の後ろで光ってるやつ 
	
	/* ----------------------------------------------------
					分ける場合・・・
	------------------------------------------------------- */
	GRAPH_INFO otHandle ;
	
	// --- リザルトバー関連
	otHandle.handle = LoadGraph( UI_PATH "Result_Bar_Str.png" ) ;
	GetGraphSizeAndOffSet( &otHandle ) ;

	g_graphHandle[ e_Image_Result_Bar ].handle				= DerivationGraph( 0, 0,							otHandle.fullSize.x, otHandle.fullSize.y / 3,
																					otHandle.handle ) ;	LF_SETUP		// リザルトバー
	g_graphHandle[ e_Image_Result_BarStr_Drop ].handle		= DerivationGraph( 0, otHandle.fullSize.y / 3 * 1,	otHandle.fullSize.x, otHandle.fullSize.y / 3,
																					otHandle.handle ) ;	LF_SETUP		// リズストリ( 落とした数 )
	g_graphHandle[ e_Image_Result_BarStr_Coconut ].handle	= DerivationGraph( 0, otHandle.fullSize.y / 3 * 2,	otHandle.fullSize.x, otHandle.fullSize.y / 3,
																					otHandle.handle ) ;	LF_SETUP		// リズストリ( 獲得ココナッツ数 )

	/* ----------------------------------------------------
					サイズ、オフセットをセット
	------------------------------------------------------- */
	for ( int i = 0 ; i < e_Image_Total ; i++ )
		GetGraphSizeAndOffSet( &g_graphHandle[ i ] ) ;

	g_graphHandle[ e_Image_DotGauge_Flame ].chipSize.x = g_graphHandle[ e_Image_DotGauge_Flame ].fullSize.y ;		// --- チップサイズセット_ドットゲージX
	g_graphHandle[ e_Image_DotGauge_Flame ].chipSize.y = g_graphHandle[ e_Image_DotGauge_Flame ].fullSize.y ;		// --- チップサイズセット_ドットゲージY

	g_graphHandle[ e_Image_MiniIcon_Animals ].chipSize.x = g_graphHandle[ e_Image_MiniIcon_Animals ].fullSize.y ;	// --- チップサイズセット_動物ミニアイコンX
	g_graphHandle[ e_Image_MiniIcon_Animals ].chipSize.y = g_graphHandle[ e_Image_MiniIcon_Animals ].fullSize.y ;	// --- チップサイズセット_動物ミニアイコンY

	g_graphHandle[ e_Image_CountDown_Str ].chipSize.x = g_graphHandle[ e_Image_CountDown_Str ].fullSize.x ;			// --- チップサイズセット_カウントダウン文字X
	g_graphHandle[ e_Image_CountDown_Str ].chipSize.y = g_graphHandle[ e_Image_CountDown_Str ].fullSize.y / 4 ;		// --- チップサイズセット_カウントダウン文字Y

	g_graphHandle[ e_Image_cFrame_Str ].chipSize.x = g_graphHandle[ e_Image_cFrame_Str ].fullSize.x ;				// --- チップサイズセット_OK確認文字X
	g_graphHandle[ e_Image_cFrame_Str ].chipSize.y = g_graphHandle[ e_Image_cFrame_Str ].fullSize.y / 2 ;			// --- チップサイズセット_OK確認文字Y
	g_graphHandle[ e_Image_cFrame_Select ].chipSize.x = g_graphHandle[e_Image_cFrame_Select].fullSize.x ;			// --- チップサイズセット_OK選択状況
	g_graphHandle[ e_Image_cFrame_Select ].chipSize.y = g_graphHandle[e_Image_cFrame_Select].fullSize.y / 2 ;		// --- チップサイズセット_OK選択状況

	g_graphHandle[ e_Image_PlayerID ].chipSize.x = g_graphHandle[ e_Image_PlayerID ].fullSize.x ;					// --- チップサイズセット_IDのUIX
	g_graphHandle[ e_Image_PlayerID ].chipSize.y = g_graphHandle[ e_Image_PlayerID ].fullSize.y / 2 ;				// --- チップサイズセット_IDのUIY
	g_graphHandle[ e_Image_PlayerID_Chase ].chipSize.x = g_graphHandle[ e_Image_PlayerID_Chase ].fullSize.x ;		// --- チップサイズセット_IDのUI_追跡X
	g_graphHandle[ e_Image_PlayerID_Chase ].chipSize.y = g_graphHandle[ e_Image_PlayerID_Chase ].fullSize.y / 2 ;	// --- チップサイズセット_IDのUI_追跡Y

	g_graphHandle[ e_Image_Time1 ].chipSize.x = g_graphHandle[ e_Image_Time1 ].fullSize.y / 3 ;						// --- チップサイズセット_数字X
	g_graphHandle[ e_Image_Time1 ].chipSize.y = g_graphHandle[ e_Image_Time1 ].fullSize.y / 3 ;						// --- チップサイズセット_数字Y
	g_graphHandle[ e_Image_Time1_FlashBlend ].chipSize.x = g_graphHandle[ e_Image_Time1_FlashBlend ].fullSize.y ;	// --- チップサイズセット_数字_発光X
	g_graphHandle[ e_Image_Time1_FlashBlend ].chipSize.y = g_graphHandle[ e_Image_Time1_FlashBlend ].fullSize.y ;	// --- チップサイズセット_数字_発光Y
	g_graphHandle[ e_Image_Time1_RedBlend ].chipSize.x = g_graphHandle[ e_Image_Time1_RedBlend ].fullSize.y ;		// --- チップサイズセット_数字_赤発光X
	g_graphHandle[ e_Image_Time1_RedBlend ].chipSize.y = g_graphHandle[ e_Image_Time1_RedBlend ].fullSize.y ;		// --- チップサイズセット_数字_赤発光Y

	g_graphHandle[ e_Image_Result_ScoreIcon ].chipSize.x = g_graphHandle[ e_Image_Result_ScoreIcon ].fullSize.x ;		// --- チップサイズセット_リズアイコンX
	g_graphHandle[ e_Image_Result_ScoreIcon ].chipSize.y = g_graphHandle[ e_Image_Result_ScoreIcon ].fullSize.y / 2 ;	// --- チップサイズセット_リズアイコンY

	g_graphHandle[ e_Image_Result_WinStr ].chipSize.x = g_graphHandle[ e_Image_Result_WinStr ].fullSize.x ;				// --- チップサイズセット_Win文字X
	g_graphHandle[ e_Image_Result_WinStr ].chipSize.y = g_graphHandle[ e_Image_Result_WinStr ].fullSize.y / 2 ;			// --- チップサイズセット_Win文字Y

	/* ----------------------------------------------------
						テクスチャ登録
	------------------------------------------------------- */
	// --- ここに書いてね


	/* ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~= */
	/*		読み込みエラーのチェック		*/
	/* ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~= */
	for ( int i = 0 ; i < e_Image_Total ; i++ )
	{
		if ( g_graphHandle[ i ].handle == -1 )
			printf( "失敗【%d番】の画像ハンドルが不明です。\n", i ) ;
	}

	return 1 ;
}

// ===================================================================================================================== Init Model And Anim States
/* ------------------------------------------------------------------

					モデル、アニメの初期化関数

---------------------------------------------------------------------
							＜使用方法＞
	１．g_mv1Handle[  ].modelHandle にモデルのハンドルを入れよう！
	
	２．GetAnimEasy() の引数に以下のものを指定しよう！
			・g_mv1Handle[  ] (ポインタ渡し)
			・アニメーションNo
			・ファイルパス

	３．g_mv1Handle[  ] にモデルハンドル、アニメーションハンドルなどが
		入ったよ。これで終わり。

-------------------------------------------------------------------- */
int InitModelAndAnimStates()
{
	// ----------------------
	//	モデル情報のセット
	// ----------------------
	#define PAC_PATH	"..\\ANIMAL VACATION\\Data\\3Dmodels\\pacman\\"
	#define ANI_PATH	"..\\ANIMAL VACATION\\Data\\3Dmodels\\player\\"
	#define MODEL_PATH	"..\\ANIMAL VACATION\\Data\\3Dmodels\\"
	#define OBJ_PATH	"..\\ANIMAL VACATION\\Data\\3Dmodels\\object\\"

	g_mv1Handle[ e_Model_Animal_Cat_1P ].modelHandle	= MV1LoadModel( ANI_PATH	"animal_Cat.mv1" ) ;								LF_SETUP	// --- PacMan_1P_
	g_mv1Handle[ e_Model_Animal_Fox_1P ].modelHandle	= MV1LoadModel( ANI_PATH	"Fox.mv1" ) ;										LF_SETUP	// --- PacMan_1Pクマ
	g_mv1Handle[ e_Model_Animal_Pig_1P ].modelHandle	= MV1LoadModel( ANI_PATH	"Pig_Mesh.mv1" ) ;									LF_SETUP	// --- PacMan_1Pニワトリ
	g_mv1Handle[ e_Model_Animal_Cat_2P ].modelHandle	= MV1DuplicateModel( g_mv1Handle[ e_Model_Animal_Cat_1P ].modelHandle ) ;		LF_SETUP	// --- PacMan_1P_
	g_mv1Handle[ e_Model_Animal_Fox_2P ].modelHandle	= MV1DuplicateModel( g_mv1Handle[ e_Model_Animal_Fox_1P ].modelHandle ) ;		LF_SETUP	// --- PacMan_1Pクマ
	g_mv1Handle[ e_Model_Animal_Pig_2P ].modelHandle	= MV1DuplicateModel( g_mv1Handle[ e_Model_Animal_Pig_1P ].modelHandle ) ;		LF_SETUP	// --- PacMan_1Pニワトリ
	g_mv1Handle[ e_Model_SuperPacMan ].modelHandle		= MV1LoadModel( PAC_PATH	"PacMan_Mesh_Super.mv1" ) ;							LF_SETUP	// --- スーパーパックマン
	g_mv1Handle[ e_Model_Renga ].modelHandle			= MV1LoadModel( MODEL_PATH	"block\\Block_Grass.mv1" ) ;						LF_SETUP	// --- 足元ステージ１
	g_mv1Handle[ e_Model_WoodBox ].modelHandle			= MV1LoadModel( MODEL_PATH	"block\\WoodBox.mv1" ) ;							LF_SETUP	// --- 足元ステージ２
	g_mv1Handle[ e_Model_Ice ].modelHandle				= MV1LoadModel( MODEL_PATH	"block\\ice.mv1" ) ;								LF_SETUP	// --- 足元ステージ３
	g_mv1Handle[ e_Model_Col_Scaffold ].modelHandle		= MV1LoadModel( MODEL_PATH	"block\\collision.mv1" ) ;							LF_SETUP	// --- コリジョン(足場)
	g_mv1Handle[ e_Model_Dot ].modelHandle				= MV1LoadModel( OBJ_PATH	"Coconuts\\Mesh_Coconut.mv1" ) ;					LF_SETUP	// --- ドット
	g_mv1Handle[ e_Model_SkyDom ].modelHandle			= MV1LoadModel( MODEL_PATH	"object\\skydorm_morning.mv1" ) ;					LF_SETUP	// --- スカイドーム（朝）
	g_mv1Handle[ e_Model_Sunset ].modelHandle			= MV1DuplicateModel( g_mv1Handle[ e_Model_SkyDom ].modelHandle ) ;				LF_SETUP	// --- スカイドーム（夕）
	g_mv1Handle[ e_Model_Hunnsui ].modelHandle			= MV1LoadModel( MODEL_PATH	"object\\hunnsui.mv1" ) ;							LF_SETUP	// --- 噴水
	g_mv1Handle[ e_Model_Sea ].modelHandle				= MV1LoadModel( MODEL_PATH	"object\\AoiUmi.mv1" ) ;							LF_SETUP	// --- 海面
	g_mv1Handle[ e_Model_Dodai ].modelHandle			= MV1LoadModel( MODEL_PATH	"block\\Dodai.mv1" ) ;								LF_SETUP	// --- 土台
	g_mv1Handle[ e_Model_BeachChair ].modelHandle		= MV1LoadModel( OBJ_PATH	"BeachChair\\Mesh_Beach_Chair.mv1" ) ;				LF_SETUP	// --- ビーチチェア
	g_mv1Handle[ e_Model_Table ].modelHandle			= MV1LoadModel( OBJ_PATH	"Table\\Mesh_Table.mv1" ) ;							LF_SETUP	// --- テーブル
	g_mv1Handle[ e_Model_Juice ].modelHandle			= MV1LoadModel( OBJ_PATH	"Coconuts\\Mesh_Coconut_Juice.mv1" ) ;				LF_SETUP	// --- ココナッツジュース
	g_mv1Handle[ e_Model_YashiWood ].modelHandle		= MV1LoadModel( OBJ_PATH	"YashiWood\\Mesh_YashiWood_WithCoconut.mv1" ) ;		LF_SETUP	// --- ビーチ
	g_mv1Handle[ e_Model_Beach ].modelHandle			= MV1LoadModel( OBJ_PATH	"Beach\\beach.mv1" ) ;								LF_SETUP	// --- ビーチ
	g_mv1Handle[ e_Model_Nami ].modelHandle				= MV1LoadModel( OBJ_PATH	"Beach\\nami.mv1" ) ;								LF_SETUP	// --- 波
	g_mv1Handle[ e_Model_BSea ].modelHandle				= MV1LoadModel( OBJ_PATH	"Beach\\beach_sea.mv1" ) ;							LF_SETUP	// --- ビーチの海
	g_mv1Handle[ e_Model_Cloud ].modelHandle			= MV1LoadModel( OBJ_PATH	"Cloud\\cloud.mv1" ) ;								LF_SETUP	// --- 雲
	g_mv1Handle[ e_Model_Ukiwa ].modelHandle			= MV1LoadModel( OBJ_PATH	"Ukiwa\\Ukiwa.mv1" ) ;								LF_SETUP	// --- 雲
	g_mv1Handle[ e_Model_Island ].modelHandle			= MV1LoadModel( OBJ_PATH	"Island\\Island.mv1" ) ;							LF_SETUP	// --- 孤島

	/* ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~= */
	/*		読み込みエラーのチェック		*/
	/* ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~= */
	for ( int i = 0 ; i <= e_Model_Total ; i++ )
	{
		if ( g_mv1Handle[ i ].modelHandle == -1 )
			printf( "失敗【%02d番】のモデルハンドルが不明です。\n", i ) ;
	}

	// ----------------------------------------------------------------------
	//	アニメ情報のセット( 最初に個別のものを読み込み、最後に共通のアニメ )
	// ----------------------------------------------------------------------
	MODEL_ANIM_INFO *mv1P = &g_mv1Handle[ e_Model_Animal_Cat_1P ] ;
	
	// ----------------------------------
	// --- 個別の専用アニメをセット

	// --- キツネ
	mv1P = &g_mv1Handle[ e_Model_Animal_Fox_1P ] ;
	GetAnimEasy( mv1P, e_PlayerSpecialAttack_In,	ANI_PATH "Fox_SuperAttack_In.mv1" ) ;			LF_SETUP	// --- スペシャルアタック_始
	GetAnimEasy( mv1P, e_PlayerSpecialAttack_Loop,	ANI_PATH "Fox_SuperAttack_Loop.mv1" ) ;			LF_SETUP	// --- スペシャルアタック_繰
	GetAnimEasy( mv1P, e_PlayerSpecialAttack_Out,	ANI_PATH "Fox_SuperAttack_Out.mv1" ) ;			LF_SETUP	// --- スペシャルアタック_終
	GetAnimEasy( mv1P, e_PlayerSelectAction,		ANI_PATH "Fox_select.mv1" ) ;					LF_SETUP	// --- キャラ選択での決定アクション
	GetAnimEasy( mv1P, e_PlayerWinEmote_In,			ANI_PATH "Fox_win.mv1" ) ;						LF_SETUP	// --- 勝利エモート
	GetAnimEasy( mv1P, e_PlayerWinEmote_Loop,		ANI_PATH "Fox_win_Loop.mv1" ) ;						LF_SETUP	// --- 勝利エモート
	GetAnimEasy( mv1P, e_PlayerLoseEmote_In,		ANI_PATH "Fox_Lose.mv1" ) ;						LF_SETUP	// --- 敗北エモート始
	GetAnimEasy( mv1P, e_PlayerLoseEmote_Loop,		ANI_PATH "Fox_Lose_Loop.mv1" ) ;				LF_SETUP	// --- 敗北エモートループ	

	// --- ネコ
	mv1P = &g_mv1Handle[ e_Model_Animal_Cat_1P ] ;
	GetAnimEasy( mv1P, e_PlayerSpecialAttack_In,	ANI_PATH "Cat_Special_In.mv1" ) ;				LF_SETUP	// --- スペシャルアタック_始
	GetAnimEasy( mv1P, e_PlayerSpecialAttack_Loop,	ANI_PATH "Cat_Special_Loop.mv1" ) ;				LF_SETUP	// --- スペシャルアタック_繰
	GetAnimEasy( mv1P, e_PlayerSpecialAttack_Out,	ANI_PATH "Cat_Special_Out.mv1" ) ;				LF_SETUP	// --- スペシャルアタック_終
	GetAnimEasy( mv1P, e_PlayerSelectAction,		ANI_PATH "Cat_Select.mv1" ) ;					LF_SETUP	// --- キャラ選択での決定アクション
	GetAnimEasy( mv1P, e_PlayerWinEmote_In,			ANI_PATH "Cat_Win_In.mv1" ) ;					LF_SETUP	// --- 勝利エモート
	GetAnimEasy( mv1P, e_PlayerWinEmote_Loop,		ANI_PATH "Cat_Win_Loop.mv1" ) ;					LF_SETUP	// --- 勝利エモート
	GetAnimEasy( mv1P, e_PlayerLoseEmote_In,		ANI_PATH "Cat_Lose_In.mv1" ) ;					LF_SETUP	// --- 敗北エモート始
	GetAnimEasy( mv1P, e_PlayerLoseEmote_Loop,		ANI_PATH "Cat_Lose_Loop.mv1" ) ;				LF_SETUP	// --- 敗北エモートループ

	// --- ブタ
	mv1P = &g_mv1Handle[ e_Model_Animal_Pig_1P ] ;
	GetAnimEasy( mv1P, e_PlayerSpecialAttack_In,	ANI_PATH "Pig_Anim_SpecialAttack_In.mv1" ) ;	LF_SETUP	// --- スペシャルアタック_始
	GetAnimEasy( mv1P, e_PlayerSpecialAttack_Loop,	ANI_PATH "Pig_Anim_SpecialAttack_Loop.mv1" ) ;	LF_SETUP	// --- スペシャルアタック_繰
	GetAnimEasy( mv1P, e_PlayerSpecialAttack_Out,	ANI_PATH "Pig_Anim_SpecialAttack_Out.mv1" ) ;	LF_SETUP	// --- スペシャルアタック_終
	GetAnimEasy( mv1P, e_PlayerSelectAction,		ANI_PATH "Pig_Anim_Select.mv1" ) ;				LF_SETUP	// --- キャラ選択での決定アクション
	GetAnimEasy( mv1P, e_PlayerWinEmote_In,			ANI_PATH "Pig_Anim_Win_In.mv1" ) ;				LF_SETUP	// --- 勝利エモート
	GetAnimEasy( mv1P, e_PlayerWinEmote_Loop,		ANI_PATH "Pig_Anim_Win_Loop.mv1" ) ;			LF_SETUP	// --- 勝利エモート
	GetAnimEasy( mv1P, e_PlayerLoseEmote_In,		ANI_PATH "Pig_Anim_Lose_In.mv1" ) ;				LF_SETUP	// --- 敗北エモート始
	GetAnimEasy( mv1P, e_PlayerLoseEmote_Loop,		ANI_PATH "Pig_Anim_Lose_Loop.mv1" ) ;			LF_SETUP	// --- 敗北エモートループ

	// ----------------------------------
	// --- 使い回しアニメーションをセット
	mv1P = &g_mv1Handle[ e_Model_Animal_Fox_1P ] ;

	GetAnimEasy( mv1P, e_PlayerWait,				ANI_PATH "Fox_Wait.mv1" ) ;						LF_SETUP	// --- 待機
	GetAnimEasy( mv1P, e_PlayerWalk,				ANI_PATH "Cat_Walk.mv1" ) ;						LF_SETUP	// --- 歩行
	GetAnimEasy( mv1P, e_PlayerPush_In,				ANI_PATH "Any_Anim_Push_In.mv1" ) ;				LF_SETUP	// --- 押し始
	GetAnimEasy( mv1P, e_PlayerPush_Out,			ANI_PATH "Any_Anim_Push_Out.mv1" ) ;			LF_SETUP	// --- 押し終
	GetAnimEasy( mv1P, e_PlayerPress_In,			ANI_PATH "Any_Anim_Nokezori_In.mv1" ) ;			LF_SETUP	// --- 押され始
	GetAnimEasy( mv1P, e_PlayerPress_Out,			ANI_PATH "Any_Anim_Nokezori_Out.mv1" ) ;		LF_SETUP	// --- 押され終
	GetAnimEasy( mv1P, e_SuparPacMan_Walk,			PAC_PATH "PacMan_Anim_Super_Walk.mv1" ) ;		LF_SETUP	// --- スーパー歩行
	GetAnimEasy( mv1P, e_StageChange_1to2,			ANI_PATH "Fall_Anim.mv1" ) ;					LF_SETUP	// --- イベント落下
	GetAnimEasy( mv1P, e_StageChange_2to3,			ANI_PATH "Any_Away_In.mv1" ) ;					LF_SETUP	// --- 回転
	GetAnimEasy( mv1P, e_StageChange_2to3_End,		ANI_PATH "Any_Away_Out.mv1" ) ;					LF_SETUP	// --- 回転終わり
	GetAnimEasy( mv1P, e_PlayerTitle_Sleep,			ANI_PATH "Any_Anim_TItleSleep.mv1" ) ;			LF_SETUP	// --- タイトル_寝る
	GetAnimEasy( mv1P, e_PlayerTitle_Jump,			ANI_PATH "Any_Anim_TItleJump.mv1" ) ;			LF_SETUP	// --- タイトル_ジャンプ
	GetAnimEasy( mv1P, e_PlayerTitle_Flow,			ANI_PATH "Any_Anim_TItleFlow.mv1" ) ;			LF_SETUP	// --- タイトル_浮かぶ
	GetAnimEasy( mv1P, e_PlayerJump_In,				ANI_PATH "Any_Anim_Jump_In.mv1" ) ;				LF_SETUP	// --- ジャンプ始
	GetAnimEasy( mv1P, e_PlayerJump_Loop,			ANI_PATH "Any_Anim_Jump_Loop.mv1" ) ;			LF_SETUP	// --- ジャンプ繰
	GetAnimEasy( mv1P, e_PlayerJump_Out,			ANI_PATH "Any_Anim_Jump_Out.mv1" ) ;			LF_SETUP	// --- ジャンプ終
	
	mv1P = &g_mv1Handle[ e_Model_Animal_Cat_1P ] ;
	for ( int i = e_Model_Animal_Cat_1P ; i <= e_Model_Animal_Pig_1P ; i++ )
	{
		AnimCopyTo( mv1P,  e_PlayerWait,				&g_mv1Handle[ e_Model_Animal_Fox_1P ] ) ;		LF_SETUP	// --- 待機
		AnimCopyTo( mv1P,  e_PlayerWalk,				&g_mv1Handle[ e_Model_Animal_Fox_1P ] ) ;		LF_SETUP	// --- 歩行
		AnimCopyTo( mv1P,  e_PlayerPush_In,				&g_mv1Handle[ e_Model_Animal_Fox_1P ] ) ;		LF_SETUP	// --- 押し始
		AnimCopyTo( mv1P,  e_PlayerPush_Out,			&g_mv1Handle[ e_Model_Animal_Fox_1P ] ) ;		LF_SETUP	// --- 押し終
		AnimCopyTo( mv1P,  e_PlayerPress_In,			&g_mv1Handle[ e_Model_Animal_Fox_1P ] ) ;		LF_SETUP	// --- 押され始
		AnimCopyTo( mv1P,  e_PlayerPress_Out,			&g_mv1Handle[ e_Model_Animal_Fox_1P ] ) ;		LF_SETUP	// --- 押され終
		AnimCopyTo( mv1P,  e_SuparPacMan_Walk,			&g_mv1Handle[ e_Model_Animal_Fox_1P ] ) ;		LF_SETUP	// --- スーパー歩行
		AnimCopyTo( mv1P,  e_StageChange_1to2,			&g_mv1Handle[ e_Model_Animal_Fox_1P ] ) ;		LF_SETUP	// --- イベント落下
		AnimCopyTo( mv1P,  e_StageChange_2to3,			&g_mv1Handle[ e_Model_Animal_Fox_1P ] ) ;		LF_SETUP	// --- 回転
		AnimCopyTo( mv1P,  e_StageChange_2to3_End,		&g_mv1Handle[ e_Model_Animal_Fox_1P ] ) ;		LF_SETUP	// --- 回転終わり
		AnimCopyTo( mv1P,  e_PlayerTitle_Sleep,			&g_mv1Handle[ e_Model_Animal_Fox_1P ] ) ;		LF_SETUP	// --- タイトル_寝る
		AnimCopyTo( mv1P,  e_PlayerTitle_Jump,			&g_mv1Handle[ e_Model_Animal_Fox_1P ] ) ;		LF_SETUP	// --- タイトル_ジャンプ
		AnimCopyTo( mv1P,  e_PlayerTitle_Flow,			&g_mv1Handle[ e_Model_Animal_Fox_1P ] ) ;		LF_SETUP	// --- タイトル_浮かぶ
		AnimCopyTo( mv1P, e_PlayerJump_In,				&g_mv1Handle[ e_Model_Animal_Fox_1P ] ) ;		LF_SETUP	// --- ジャンプ始
		AnimCopyTo( mv1P, e_PlayerJump_Loop,			&g_mv1Handle[ e_Model_Animal_Fox_1P ] ) ;		LF_SETUP	// --- ジャンプ繰
		AnimCopyTo( mv1P, e_PlayerJump_Out,				&g_mv1Handle[ e_Model_Animal_Fox_1P ] ) ;		LF_SETUP	// --- ジャンプ終

		mv1P++ ;
	}

	// ----------------------------------
	// --- アニメ情報を2P用にもセット
	for ( int i = e_Model_Animal_Fox_2P ; i <= e_Model_Animal_Pig_2P ; i++ )
	{
		// --- 全てのアニメーションをコピー
		for ( int j = 0 ; j < e_PlayerAnimTotal ; j++ )		
			AnimCopyTo( &g_mv1Handle[ i ], j, &g_mv1Handle[ i - 3 ] ) ;
	}

	/* ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~= */
	/*		読み込みエラーのチェック		*/
	/* ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~= */
	for ( int i = e_Model_Animal_Cat_1P ; i <= e_Model_Animal_Pig_2P ; i++ )
	{
		for ( int j = e_PlayerWait ; j < e_PlayerAnimTotal ; j++ )
		{
			if ( g_mv1Handle[ j ].anim[ i ].handle == -1 )
				printf( "失敗【キャラ - %d】【%02d番】のアニメハンドルが不明です。\n", i, j ) ;
		}
	}

	// ----------------------
	//	テクスチャ情報のセット
	// ----------------------
	MV1SetTextureGraphHandle( g_mv1Handle[ e_Model_SkyDom ].modelHandle, 0, g_graphHandle[ e_Image_Skysun ].handle, FALSE ) ;		// --- 青空背景
	MV1SetTextureGraphHandle( g_mv1Handle[ e_Model_Beach ].modelHandle, 0, g_graphHandle[ e_Image_Skysun/*e_Image_Beach */].handle, FALSE ) ;			// --- ビーチ

	return 1 ;
}

// ===================================================================================================================== Init Object States
/* --------------------------------------------------------------

					オブジェクトごとの初期化関数

-----------------------------------------------------------------
							＜使用方法＞
 １．InitModelAndAnimStates() でデータをセットした変数を用意。

 ２．Objectクラスの setModelAndAnimメソッドにそれを引数として渡す。

 ３．すると、そのオブジェクトに各種ハンドルがセットされるよ。

---------------------------------------------------------------- */
int InitObjectStates()
{
	// ------------------------------------------
	//	モデルハンドル、アニメーションのセット
	// ------------------------------------------
	g_Object[ OBJID_SKYDOM ].setModelAndAnim( g_mv1Handle[ e_Model_SkyDom ], FALSE, VGet( 1.5f, 1.5f, 1.0f ) ) ;				// --- スカイドーム
	g_Object[ OBJID_HUNSUI ].setModelAndAnim( g_mv1Handle[ e_Model_Hunnsui ], FALSE, VGet( 0.5f, 0.5f, 0.5f ) ) ;				// --- 噴水	
	g_Object[ OBJID_SEA ].setModelAndAnim( g_mv1Handle[ e_Model_Sea ], FALSE, VGet( 0.5f, 0.5f, 0.5f ) ) ;						// --- 海面
	g_Object[ OBJID_CLOUD + 0 ].setModelAndAnim( g_mv1Handle[ e_Model_Cloud ], TRUE, VGet( 0.5f, 0.5f, 0.5f ) ) ;				// --- 雲
	g_Object[ OBJID_CLOUD + 1 ].setModelAndAnim( g_mv1Handle[ e_Model_Cloud ], TRUE, VGet( 0.5f, 0.5f, 0.5f ) ) ;				// --- 雲

	g_Player[0].setModelAndAnim( g_mv1Handle[ e_Model_Animal_Pig_1P ], FALSE, VGet( 0.3f, 0.3f, 0.3f ) ) ;		// --- プレイヤー情報_1P
	g_Player[1].setModelAndAnim( g_mv1Handle[ e_Model_Animal_Cat_2P ], FALSE, VGet( 0.3f, 0.3f, 0.3f ) ) ;		// --- プレイヤー情報_2P
	 
	Dot1stg[35].setModelAndAnim( g_mv1Handle[ e_Model_Dot ], TRUE ) ;
	Dot2stg_t[75].setModelAndAnim( g_mv1Handle[ e_Model_Dot ], TRUE ) ;
	Dot2stg_u[75].setModelAndAnim( g_mv1Handle[ e_Model_Dot ], TRUE ) ;
	Dot3stg[25].setModelAndAnim( g_mv1Handle[ e_Model_Dot ], TRUE ) ;
	
	for ( int i = 0 ; i < 2 ; i++ )
		g_Object[ OBJID_DODAI + i ].setModelAndAnim( g_mv1Handle[ e_Model_Dodai ], TRUE, VGet( 0.5f, 0.5f, 0.5f ) ) ;	// --- 土台

	for ( int i = 0 ; i < ELE_X_1 * ELE_Y_1 ; i++ ){
		Scaf1[i].size.x = 1.5f ;		// --- スケール変更1.5倍
		Scaf1[i].size.y = 1.5f ;		// --- スケール変更1.5倍
		Scaf1[i].size.z = 1.5f ;		// --- スケール変更1.5倍
		//											Renga
		Scaf1[i].setModelAndAnim( g_mv1Handle[ e_Model_Renga ], TRUE, VGet( 0.5f, 0.5f, 0.5f ) ) ;			// --- 草
	}

	for ( int i = 0 ; i < ELE_X_2 * ELE_Y_2 ; i++ ){
		Scaf2_t[i].size.x = 1.5f ;		// --- スケール変更1.5倍
		Scaf2_t[i].size.y = 1.5f ;		// --- スケール変更1.5倍
		Scaf2_t[i].size.z = 1.5f ;		// --- スケール変更1.5倍
		//												WoodBox
		Scaf2_t[i].setModelAndAnim( g_mv1Handle[ e_Model_WoodBox ], TRUE, VGet( 0.5f, 0.5f, 0.5f ) ) ;			// --- 木箱
	}

	for ( int i = 0 ; i < ELE_X_2 * ELE_Y_2 ; i++ ){
		Scaf2_u[i].size.x = 1.5f ;		// --- スケール変更1.5倍
		Scaf2_u[i].size.y = 1.5f ;		// --- スケール変更1.5倍
		Scaf2_u[i].size.z = 1.5f ;		// --- スケール変更1.5倍
		//												WoodBox
		Scaf2_u[i].setModelAndAnim( g_mv1Handle[ e_Model_WoodBox ], TRUE, VGet( 0.5f, 0.5f, 0.5f ) ) ;			// --- 木箱
	}

	for ( int i = 0 ; i < ELE_X_3 * ELE_Y_3 ; i++ ){
		Scaf3[i].size.x = 1.5f ;		// --- スケール変更1.5倍
		Scaf3[i].size.y = 1.5f ;		// --- スケール変更1.5倍
		Scaf3[i].size.z = 1.5f ;		// --- スケール変更1.5倍
		Scaf3[i].setModelAndAnim( g_mv1Handle[ e_Model_Ice ], TRUE, VGet( 0.5f, 0.5f, 0.5f ) ) ;			// --- 氷
	}

	VECTOR dotSize = VGet( 0.35f, 0.35f, 0.35f ) ;

	for ( int i = 0 ; i < ELE_X_1 * ELE_Y_1 ; i++ )
	{
		Dot1stg[i].setModelAndAnim( g_mv1Handle[ e_Model_Dot ], TRUE, VGet( 0.5f, 0.5f, 0.5f ) ) ;			// --- ドット1
		Dot1stg[i].size = dotSize ;
		Dot1stg[i].dir.x = 0.3f ;
	}

	for ( int i = 0 ; i < ELE_X_2 * ELE_Y_2 ; i++ )
	{
		Dot2stg_t[i].setModelAndAnim( g_mv1Handle[ e_Model_Dot ], TRUE, VGet( 0.5f, 0.5f, 0.5f ) ) ;		// --- ドット2_t
		Dot2stg_t[i].size = dotSize ;
		Dot2stg_t[i].dir.x = 0.3f ;
	}

	for ( int i = 0 ; i < ELE_X_2 * ELE_Y_2 ; i++ )
	{
		Dot2stg_u[i].setModelAndAnim( g_mv1Handle[ e_Model_Dot ], TRUE, VGet( 0.5f, 0.5f, 0.5f ) ) ;		// --- ドット2_u
		Dot2stg_u[i].size = dotSize ;
		Dot2stg_u[i].dir.x = 0.3f ;
	}

	for ( int i = 0 ; i < ELE_X_3 * ELE_Y_3 ; i++ )
	{
		Dot3stg[i].setModelAndAnim( g_mv1Handle[ e_Model_Dot ], TRUE, VGet( 0.5f, 0.5f, 0.5f ) ) ;			// --- ドット3
		Dot3stg[i].size = dotSize ;
		Dot3stg[i].dir.x = 0.3f ;
	}

	// ----------------------------------------------------------------------
	//						コリジョンモデルのセット
	// ----------------------------------------------------------------------
	for ( int i = 0 ; i < ELE_X_1 * ELE_Y_1 ; i++ )
		Scaf1[ i ].col.handle = MV1DuplicateModel( g_mv1Handle[ e_Model_Col_Scaffold ].modelHandle ) ;

	for ( int i = 0 ; i < ELE_X_2 * ELE_Y_2 ; i++ )
		Scaf2_t[ i ].col.handle = MV1DuplicateModel( g_mv1Handle[ e_Model_Col_Scaffold ].modelHandle ) ;

	for ( int i = 0 ; i < ELE_X_2 * ELE_Y_2 ; i++ )
		Scaf2_u[ i ].col.handle = MV1DuplicateModel( g_mv1Handle[ e_Model_Col_Scaffold ].modelHandle ) ;

	for ( int i = 0 ; i < ELE_X_3 * ELE_Y_3 ; i++ )
		Scaf3[ i ].col.handle = MV1DuplicateModel( g_mv1Handle[ e_Model_Col_Scaffold ].modelHandle ) ;

	// ------------------------------------------
	//			各種メンバのセット
	// ------------------------------------------
	g_Object[ OBJID_SKYDOM ].pos	= VGet( 700.0f, -2500.0f, 2000.0f ) ;			// --- スカイドームのポジションセット
	g_Object[ OBJID_SKYDOM ].size	= VGet( 70.0f, 50.0f, 70.0f ) ;					// --- スカイドームのサイズセット

	g_Object[ OBJID_HUNSUI ].pos = VGet( 750.0f, -500.0f, 700.0f ) ;
	g_Object[ OBJID_HUNSUI ].size	= VGet( 2.0f, 2.0f, 2.0f ) ;					// --- スカイドームのサイズセット
	g_Object[ OBJID_HUNSUI ].movepos.y = 0.0f ;
	g_Object[ OBJID_HUNSUI ].dispFlg = FALSE ;

//	g_Object[ OBJID_SEA ].pos = VGet( 700.0f, -2500.0f, 0.0f ) ;
//	g_Object[ OBJID_SEA ].pos = VGet( 700.0f, -250.0f, 0.0f ) ;
	g_Object[ OBJID_SEA ].pos = VGet( 700.0f, -250.0f, 0.0f ) ;
	g_Object[ OBJID_SEA ].dispFlg = FALSE ;
	
	//					   ↓定数にして！
	g_Player[0].PlayerId( PLAYER_ONE ) ;							// --- IDセット
	g_Player[1].PlayerId( PLAYER_TWO ) ;							// --- IDセット

	g_Player[0].pos = VGet( 0.0f, 0.0f, 0.0f ) ;					// --- 座標セット
	g_Player[1].pos = VGet( 1000.0f, 0.0f, 0.0f ) ;					// --- 座標セット

	UI_SChange.initStates() ;										// --- シーンチェンジ用UIのステータスセット

	return 1 ;
}

// ===================================================================================================================== Init Attachment Gamepad
/* --------------------------------------------------------------

					ゲームパッドの初期化関数

---------------------------------------------------------------- */
int InitAttachmentPad()
{
	// ゲームパッドの初期化
	PadData::SetPadNum( );		// ゲームパッドの接続数取得
	PadData::UpDate( );			// ゲームパッドの情報更新

	return 1 ;
}

