/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: UI_DotGaugeAndPlayerID.cpp
	CREATE	: NOGUCHI
	
	------ Explanation of file ---------------------------------------------------------------------
       
	UI_DotGaugeAndPlayerIDクラスの実処理記述部
	
	ドットゲージ用の DrawGauge()
	プレイヤーID用の DrawPlayerID()

	の２つに分かれている。ドットゲージは処理がやや複雑なのでこうした。

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

#include "Common.h"

// ---------------------------------------------------------------------------------
//									コンストラクタ
// ---------------------------------------------------------------------------------
UI_DotGaugeAndPlayerID::UI_DotGaugeAndPlayerID( )
{
	this->initStates() ;
}

// ---------------------------------------------------------------------------------
//									デストラクタ
// ---------------------------------------------------------------------------------
UI_DotGaugeAndPlayerID::~UI_DotGaugeAndPlayerID( )
{

}

// ---------------------------------------------------------------------------------
//								フィールド初期化メソッド
// ---------------------------------------------------------------------------------
void UI_DotGaugeAndPlayerID::initStates()
{
	flamePower.max[0]	= 100 ;		// --- DotGauge_Frame_メラメラの大きさMAX
	flamePower.min[0]	= 0 ;		// --- DotGauge_Frame_メラメラの大きさMIN
	flamePower.plus		= 15 ;		// --- DotGauge_Frame_メラメラ加算分

	testLimit.now		= 100 ;		// --- テスト
	testLimit.max[0]	= 100 ;		// --- テスト

	isDispFlame			= FALSE ;	// --- 炎を描画するかどうかのフラグ
	flameSize			= 0.0 ;		// --- 炎の大きさ倍率

	gauge_Before			= 0 ;		// --- 前回のゲージ分数値
	gaugeDisp_Damage.now	= -1 ;		// --- ダメージ分のゲージ
	gaugeDisp_Damage.plus	= 0.5f ;	// --- ダメージ分のゲージ_減少分

	damage_dispTime.now		= 30 ;						// --- ダメージ表示時間
	damage_dispTime.min[0]	= 0 ;						// --- ダメージ表示時間_下限
	damage_dispTime.max[0]	= damage_dispTime.now ;		// --- ダメージ表示時間_BUCK UP

	maxZurashi				= 15 ;						// --- 揺れた時に座標がずれる数値分_MAX( 30 の場合、左に30, 右に30 の可能性がある )

	flashSize.now			= 1.0f ;					// --- フラッシュサイズ_現在
	flashSize.max[0]		= 1.3f ;					// --- フラッシュサイズ_最大値
	flashSize.min[0]		= 1.0f ;					// --- フラッシュサイズ_最小値
	flashSize.plus			= 0.01f ;					// --- フラッシュサイズ_加算値

	flashAlpha.now			= 0 ;						// --- フラッシュアルファ値_現在
	flashAlpha.max[0]		= 100 ;						// --- フラッシュアルファ値_最大
	flashAlpha.plus			= 2 ;						// --- フラッシュアルファ値_加算値
}

// ================================================================================================================ UI Draw
// ---------------------------------------------------------------------------------
//
//
//								所謂Ｌｏｏｐである。
//
//
// ---------------------------------------------------------------------------------
//							・引数１ - プレイヤーの番号
// ---------------------------------------------------------------------------------
void UI_DotGaugeAndPlayerID::UIDraw( int arg_playerNo )
{
	this->DrawGauge( arg_playerNo ) ;			// --- ゲージ描画
	this->DrawPlayerID( arg_playerNo ) ;		// --- プレイヤーID描画
}

// ================================================================================================================ Draw Frame
// ---------------------------------------------------------------------------------
//
//
//						プレイヤーごとの枠を描画する関数
//
//
// ---------------------------------------------------------------------------------
//							・引数１ - プレイヤーの番号
// ---------------------------------------------------------------------------------
void UI_DotGaugeAndPlayerID::DrawFrame( int arg_playerNo )
{
	// --------------------------------------------------
	// --- ポジションセット
	// --------------------------------------------------
	if ( arg_playerNo == PLAYER_ONE )
		cord.x = ((float)g_winSize.x / 2) - 580 ;				// --- X軸
	else if ( arg_playerNo == PLAYER_TWO )
		cord.x = ((float)g_winSize.x / 2) + 580 ;				// --- X軸
	else
		return ;

	cord.y = GAMEUI_POS_Y ;					// --- Y軸(これは共通)

	// --------------------------------------------------
	// ---				本描画
	// --------------------------------------------------
	if ( this->f_disp )
		DrawRotaGraph( (int)cord.x, (int)cord.y, 1.0, 0.0, g_graphHandle[ e_Image_StateFrame ].handle, TRUE ) ;
}

// ================================================================================================================ Draw Gauge
// ---------------------------------------------------------------------------------
//
//
//					プレイヤーごとのドットゲージを描画する関数
//
//
// ---------------------------------------------------------------------------------
//							・引数１ - プレイヤーの番号
// ---------------------------------------------------------------------------------
void UI_DotGaugeAndPlayerID::DrawGauge( int arg_playerNo )
{
	// --------------------------------------------------
	//					ポジションセット
	// --------------------------------------------------
	if ( arg_playerNo == PLAYER_ONE )
		cord.x = ((float)g_winSize.x / 2) - 210 ;				// --- X軸
	else if ( arg_playerNo == PLAYER_TWO )
		cord.x = ((float)g_winSize.x / 2) + 210 ;				// --- X軸
	else
		return ;

	cord.y = GAMEUI_POS_Y ;					// --- Y軸(これは共通)

	// --------------------------------------------------
	//			スーパー状態であるかどうかを検知
	// --------------------------------------------------
	Player	*otPly = &g_Player[ arg_playerNo ] ;
	if ( otPly->superflg && !isDispFlame )
	{
		// --- 初期タイムをセット
		superTime.max[0]	= g_Player[ arg_playerNo ].superendtime - GetNowCount() ;

		flamePower.now	= 0 ;
		isDispFlame		= TRUE ;
	}

	// --------------------------------------------------
	//	ドット、ドットMAXに応じたゲージの表示分を求める
	// --------------------------------------------------
	//
	//		SuperTime		→		SuperEndTime
	//		67700000				67710000
	//
	//	こういうカウンタの仕様なのだが、あまりにも使い勝手
	//	が悪い。
	//	それぞれの数値にGetNowCount() を絡めることで
	//
	//		SuperTime		→		SuperEndTime
	//		0						10000
	//
	//	という数値に仕立てている。
	//
	// --------------------------------------------------
	c_fPoint	zurashiPos ;		// --- 揺れるときに、ズラす座標分の数値
	c_fPoint	newPos ;			// --- ↑を足した分の数値
	int			otGaugeDisp	;		// --- 表示するゲージのサイズ( MAX-100 MIN-0 )

	// ----------------------------
	//			通常ゲージ
	// ----------------------------
	if ( !otPly->superflg )		// --- 非スーパー時は100
		otGaugeDisp		= (int)(100.0f * ((float)otPly->dotcharge / (float)otPly->dotChargeMax)) ;
	else
	{
		superTime.now	= superTime.max[0] - (GetNowCount() - g_Player[ arg_playerNo ].supertime)	;	// --- 現在のカウントを取得
		otGaugeDisp		= (int)(100.0f * ((float)superTime.now / (float)superTime.max[0])) ;			// --- MAX - 100, MIN - 0
	}

	c_fPoint otSpot		= c_fPoint( (float)(cord.x - g_graphHandle[ e_Image_DotGauge ].offSet.x),
									(float)(cord.y - g_graphHandle[ e_Image_DotGauge ].offSet.y) ) ;		// --- 描画位置の決定_ゲージ
	c_fPoint otFlaSpot	= c_fPoint( (float)(cord.x - g_graphHandle[ e_Image_DotGauge_Flame ].offSet.x),
									(float)(cord.y - g_graphHandle[ e_Image_DotGauge_Flame ].offSet.y) ) ;	// --- 描画位置の決定_ファイア

	// ----------------------------
	//		 ダメージ分ゲージ
	// ----------------------------
	// --- ゲージの縮小を感知
	if ( gaugeDisp_Damage.now == -1 )
	{
		if ( (gauge_Before > otGaugeDisp) && (!otPly->superflg) )
		{
			gaugeDisp_Damage.now	= (float)gauge_Before ;

			damage_dispTime.now		= damage_dispTime.max[0] ;	// --- 表示開始
		}
	}
	// --- 減少させる処理
	else
	{
		// --- 少しだけMAX表示で滞在させる
		if ( damage_dispTime.now > damage_dispTime.min[0] )
		{
			// --- 表示時間の 1/3分の時間だけ揺らす
			if ( damage_dispTime.now > (damage_dispTime.max[0] / 3 * 2) )
			{
				zurashiPos.x += (rand() % (maxZurashi * 2)) - maxZurashi ;
				zurashiPos.y += (rand() % (maxZurashi * 2)) - maxZurashi ;

				// --- ここで求めた数値は、あとでセットする
			}

			damage_dispTime.now -= (1 * g_gameSpeed) ;
		}
		else
		{
			// --- 減少しきったら非表示にする
			if ( !CheckLimitAndPlus<float>( CLAPMODE_EXCEED, &gaugeDisp_Damage.now, -(gaugeDisp_Damage.plus * g_gameSpeed), (float)otGaugeDisp, (float)otGaugeDisp ) )
				gaugeDisp_Damage.now = -1 ;
		}
	}

	gauge_Before = otGaugeDisp ;		// --- 前回のゲージ数値を更新

	// --------------------------------------------------
	//					ファイアを描画
	// --------------------------------------------------
	if ( this->isDispFlame )
	{
		int		otImgCut ;	// --- 画像カット位置
		float	otPos_Y ;	// --- 位置ずらし(Y座標)

		GRAPH_INFO *otGP = &g_graphHandle[ e_Image_DotGauge_Flame ] ;

		// --- 「flamePower.now」は0〜100を行き来する
		if ( flamePower.plus > 0 )
		{
			if ( TRUE == flamePower.plus_RetMaxOver( flamePower.plus ) )
				flamePower.plus *= -1 ;
		}
		else
		{
			if ( TRUE == flamePower.min_RetMinBelow( -flamePower.plus ) )
				flamePower.plus *= -1 ;
		}

		// --- 画像の切り取り位置を決定
		if ( flamePower.now < flamePower.max[0] / 2 )
			otImgCut = 0 ;
		else
			otImgCut = otGP->chipSize.x ;

		int newFHandle = DerivationGraph( otImgCut, 0, otGP->chipSize.x, otGP->chipSize.y, otGP->handle ) ;

		if ( otPly->superflg )
		{
			// --- サイズ 位置決定
			flameSize	= 1.0 + ((double)(flamePower.now / flamePower.max[0] / 4)) ;
			otPos_Y		= cord.y - (otGP->chipSize.y / 10 * (flamePower.now / flamePower.max[0])) ;

		}
		else
		{
			// --- サイズは一定の値まで徐々に下げる。
			if ( flameSize - 0.01 > 0.5 )
				flameSize	-= 0.01 ;
			else
			{
				isDispFlame = FALSE ;		// --- 非表示にする
				flameSize	= 0 ;
			}

			otPos_Y		= cord.y ;
		}

		// --- 炎描画
		if ( this->f_disp )
			DrawRotaGraph( (int)cord.x + 10, (int)otPos_Y - 10, flameSize, 0.0, newFHandle, TRUE ) ;
	
		DeleteGraph( newFHandle ) ;
	}

	// --- newPos セット
	newPos = cord + zurashiPos ;

	// --------------------------------------------------
	//					枠、ゲージの描画
	// --------------------------------------------------
	if ( this->f_disp )
	{
		DrawRotaGraph( (int)newPos.x, (int)newPos.y, 1.0, 0.0, g_graphHandle[ e_Image_DotGauge_Waku ].handle, TRUE ) ;		// --- 枠描画
	
		// --- 通常時は数値によって描画サイズを変更
		if ( !otPly->superflg )
		{
			// --- 落ちたとき、減少分を表示するようにする
			if ( gaugeDisp_Damage.now != -1 )
				DrawCircleGauge( (int)newPos.x, (int)newPos.y, (int)gaugeDisp_Damage.now, g_graphHandle[ e_Image_DotGauge_Damage ].handle, 0 ) ;

			DrawCircleGauge( (int)newPos.x, (int)newPos.y, otGaugeDisp, g_graphHandle[ e_Image_DotGauge ].handle, 0 ) ;
		}
		
		// --- スーパー時は固定のゲージを描画
		else
			DrawCircleGauge( (int)newPos.x, (int)newPos.y, otGaugeDisp, g_graphHandle[ e_Image_DotGauge_Max ].handle, 0 ) ;
	}

	// --------------------------------------------------
	//				ミニアイコンの描画
	// --------------------------------------------------
	GRAPH_INFO otCIP = g_graphHandle[ e_Image_MiniIcon_Animals ] ;

	int		otCharaImgCut	= (otPly->selmodel - e_Model_Animal_Fox_1P) % 3 ;
	int		newIconHandle	= DerivationGraph( otCharaImgCut * otCIP.chipSize.x, 0, otCIP.chipSize.x, otCIP.chipSize.y, otCIP.handle ) ;

	if ( this->f_disp )
		DrawRotaGraph( (int)newPos.x, (int)newPos.y, 0.5, 0.0, newIconHandle, TRUE ) ;

	DeleteGraph( newIconHandle ) ;
}

// ================================================================================================================ Draw Player ID
// ---------------------------------------------------------------------------------
//
//
//						  プレイヤーごとのIDを描画する関数
//
//
// ---------------------------------------------------------------------------------
//							・引数１ - プレイヤーの番号
// ---------------------------------------------------------------------------------
void UI_DotGaugeAndPlayerID::DrawPlayerID( int arg_playerNo )
{
	int			otPos_X[2] = { (200), (g_winSize.x - 200) } ;
	GRAPH_INFO	*otIDP = &g_graphHandle[ e_Image_PlayerID ] ;

	// --- 描画位置を表す"cord"は、DrawGauge()にて指定済み
	int		newIDHandle		= DerivationGraph( 0, arg_playerNo * otIDP->chipSize.y, otIDP->chipSize.x, otIDP->chipSize.y, otIDP->handle ) ;
	POINT	newIDPos		= { otPos_X[ arg_playerNo ], (int)cord.y } ;

	if ( this->f_disp )
		DrawRotaGraph( newIDPos.x, newIDPos.y, 0.6, 0.0, newIDHandle, TRUE ) ;

	DeleteGraph( newIDHandle ) ;
}

// ================================================================================================================ Draw Super Flash
// ---------------------------------------------------------------------------------
//
//
//					プレイヤーごとのスーパーフラッシュを描画する
//
//
// ---------------------------------------------------------------------------------
//							・引数１ - プレイヤーの番号
// ---------------------------------------------------------------------------------
void UI_DotGaugeAndPlayerID::DrawSuperFlash()
{
	// --- スーパー状態であるプレイヤーを検知
	Player	*otPP		= &g_Player[0] ;	// --- プレイヤーポインタ
	BOOL	isSuper		= FALSE ;			// --- スーパー状態であるかどうかのフラグ

	for ( int i = PLAYER_ONE ; i <= PLAYER_TWO ; i++ )
	{
		if ( otPP->superflg )
		{
			isSuper = TRUE ;
			break ;
		}

		otPP++ ;
	}

	// --- スーパー状態なのにアルファ値が0の場合は徐々に上げる
	if ( (isSuper) && (flashAlpha.now < 100) )
		CheckLimitAndPlus<int>( CLAPMODE_BELOW, &flashAlpha.now, flashAlpha.plus, flashAlpha.max[0], flashAlpha.max[0] ) ;

	// --- スーパー状態ではなく、アルファ値が100の場合は徐々に上げる
	if ( (!isSuper) && (flashAlpha.now > 0) )
		CheckLimitAndPlus<int>( CLAPMODE_EXCEED, &flashAlpha.now, -flashAlpha.plus, 0, 0 ) ;

	// --- サイズ上昇処理( ほわんほわん )
	if ( flashSize.plus > 0 )
	{
		if ( !CheckLimitAndPlus<float>( CLAPMODE_BELOW, &flashSize.now, flashSize.plus, flashSize.max[0], flashSize.max[0] ) )
			flashSize.plus *= -1.0f ;
	}
	// --- サイズ下降処理( ほわんほわん )
	if ( flashSize.plus < 0 )
	{
		if ( !CheckLimitAndPlus<float>( CLAPMODE_EXCEED, &flashSize.now, flashSize.plus, flashSize.min[0], flashSize.min[0] ) )
			flashSize.plus *= -1.0f ;
	}

	// --- 有効である場合のみ描画を行う
	if ( this->f_disp )
	{
		SetDrawBlendMode( DX_BLENDMODE_ALPHA, (int)( flashAlpha.now * (255.0f / flashAlpha.max[0]) ) ) ;

		DrawRotaGraph( g_winSize.x / 2, g_winSize.y / 2, flashSize.now, 0.0, g_graphHandle[ e_Image_SuperFlash ].handle, TRUE ) ;

		SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;
	}
}


