/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: Object.cpp
	
	------ Explanation of file ---------------------------------------------------------------------
       
	全オブジェクトの親となるクラス

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

#include "Common.h"

// ---------------------------------------------------------------------------------
//								   コンストラクタ
// ---------------------------------------------------------------------------------
Object::Object( )
{
	initStates() ;
}

// ---------------------------------------------------------------------------------
//									デストラクタ
// ---------------------------------------------------------------------------------
Object::~Object( )
{

}

// ========================================================================================================== Init States
// ---------------------------------------------------------------------------------
//
//							フィールドを初期化するメソッド
//
// ---------------------------------------------------------------------------------
void Object::initStates( )
{
	ResetVector( pos ) ;
	ResetVector( dir ) ;

	size		= VGet( 1.0f, 1.0f, 1.0f ) ;
	set_DispAndColValid( TRUE ) ;							// --- dispFlg, col.isValid をTRUEにする

	mode		= 0 ;
	modelHandle = 0 ;
	objID		= 0 ;

	animUseFlg  = FALSE ;

	SecureZeroMemory( &anim, sizeof( ANIM_INFO[2] ) ) ;		// --- 構造体のメモリをクリアしている
	playTime	= 0.0f ;
	rootFlm		= 0 ;
	attachIdx	= 0 ;
}

// ========================================================================================================== Set Model And Anim
// ---------------------------------------------------------------------------------
//
//							モデルとアニメのセッター
//
//			第２引数はMV1DuplicateModel()を使用するかどうかのフラグなのだが
//			以下の２パターンの内、１つでも当てはまったらTRUEにしておこう。
//
//		【同じモデルを使うキャラクターがそれぞれ違うアニメーションをする】
//			【同じモデルを使うキャラクターが静止していることがある】
//
// ---------------------------------------------------------------------------------
//	引数１ :	使用するモデル、アニメの情報
//	引数２ :	MV1DuplicateModel()を使用するかどうかのフラグ
//	引数３ :	エミッションのステータス( デフォルト - VGet( 0.0f, 0.0f 0.0f ) )
// ---------------------------------------------------------------------------------
void Object::setModelAndAnim( MODEL_ANIM_INFO arg_MAInfo, BOOL arg_isDuplicate, VECTOR arg_emiPower )
{
	// --- Duplicateでのハンドル取得
	if ( arg_isDuplicate )
		this->modelHandle	= MV1DuplicateModel( arg_MAInfo.modelHandle ) ;
	// --- 普通にハンドルを取得
	else
		this->modelHandle	= arg_MAInfo.modelHandle ;		

	this->rootFlm	= arg_MAInfo.rootFlm ;						// --- 骨組み情報

	// --------------------------------------
	//		エミッションセット
	// --------------------------------------
	int mNum = MV1GetMaterialNum( this->modelHandle ) ;			// --- マテリアルの数を取得

	for ( int i = 0 ; i < mNum ; i++ )
	{
		MV1SetMaterialEmiColor( this->modelHandle,	i, 
									GetColorF( arg_emiPower.x, arg_emiPower.y, arg_emiPower.z, 1.0f ) ) ;		// 発光ステータスをセット
	}
	// --------------------------------------
	//		ここからアニメーション関連
	// --------------------------------------
	
	// --- アニメ―ション数が0でなければアニメも引き継ぐ
	if ( arg_MAInfo.animCount > 0 )
		this->animUseFlg = TRUE ;
	else
		return ;

	int i = 0 ;

	// --- 使用アニメーション分セットする
	while ( i < arg_MAInfo.animCount )
	{
		this->anim[i].handle	= arg_MAInfo.anim[i].handle ;
		this->anim[i].animTotal	= arg_MAInfo.anim[i].animTotal ;

		i++ ;
	} 
}

// ========================================================================================================== Set Anim
// ---------------------------------------------------------------------------------
//
//						アニメーションセットメソッド
//
//		引数にアニメーションの数値を指定することで、そのモーションをセットする。
//						帰り値がTRUEで成功、FALSEで失敗
//
// ---------------------------------------------------------------------------------
int Object::setAnim( int arg_anim )
{
	// --- ハンドルが空だとFALSEを帰す
	if ( anim[ arg_anim ].handle == -1 )
		return FALSE ;

	MV1DetachAnim( modelHandle, attachIdx ) ;
	attachIdx = MV1AttachAnim( modelHandle, 0, anim[ arg_anim ].handle ) ;

	// --- エラーメッセージ取得で FALSEを帰す
	if ( attachIdx == -1 )
		return FALSE ;

	return TRUE ;
}

// ========================================================================================================== Set States From Parent
// ---------------------------------------------------------------------------------
//
//				親のステータスをコリジョン情報にもセットする
//
//						帰り値がTRUEで成功、FALSEで失敗
//
// ---------------------------------------------------------------------------------
int Object::setStatesFromParent()
{
	// --- コリジョンモデルが空なら、この関数を即終了させる
	if ( (this->col.handle == -1) || (this->col.handle == 0) )
		return FALSE ;

	MV1SetRotationXYZ( this->col.handle, VGet( 1.57f * this->dir.x, 1.57f * this->dir.y, 1.57f * this->dir.z ) ) ;		// --- モデルの回転
	MV1SetScale( this->col.handle, this->size ) ;																		// --- モデルの拡縮
	MV1SetPosition( this->col.handle, this->pos ) ;																		// --- モデルの移動

	return TRUE ;
}


