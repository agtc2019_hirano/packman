/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: Player.h
	
	------ Explanation of file ---------------------------------------------------------------------
       
	プレイヤークラスの宣言（オブジェクトクラス継承）

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

enum speedID
{
	e_Speed_Normal = 0,		// --- 通常時
	e_Speed_Super			// --- スーパー時
} ;

// ---------------------------------------------------------
//					攻撃判定関連の構造体
// ---------------------------------------------------------
typedef struct
{
	BOOL	isUse ;		// --- 使用フラグ
	BOOL	isValid ;	// --- 有効フラグ( TRUEで攻撃中 )
	float	validTime ;	// --- 有効時間( 判定が画面に残り続ける時間 )
	float	nowTime ;	// --- 生成されてからの経過時間

	VECTOR	atkPos ;	// --- 攻撃判定の中心座標
	float	atkR ;		// --- 半径( 範囲は球であるため )

} PACK_ATK ;

// ---------------------------------------------------------
//			アクセル＿ブレーキステータス関連
// ---------------------------------------------------------
typedef struct
{
	float	accele ;		// --- 加速度
	float	fliction ;		// --- キーを離してから、移動量を減衰させる数値

} ACCELE_FLICK ;

// ---------------------------------------------------------
//					移動ステータス関連
// ---------------------------------------------------------
typedef struct
{
	ACCELE_FLICK	power[2] ;		// --- 移動制御
	VECTOR			moveSpeed ;		// --- 現在移動速度

	float			moveMax[ 2 ] ;	// --- MAX移動速度

} PMOVE_STATE ;

// ---------------------------------------------------------
//				氷上の移動制御ステータス関連
// ---------------------------------------------------------
typedef struct
{
	c_countGroup< float >	start ;		// --- 走り出し弱体化数値( 【0.0f - 停止】, 【1.0f - 変化なし】 )
	c_countGroup< float >	init ;		// ---	→バックアップ
	float					fliction ;	// --- 摩擦弱体化分数値( 【0.0f - 摩擦0】, 【1.0f - 変化なし】 )

} PMOVE_ONICE ;

// ---------------------------------------------------------
//
//						Player クラス
//
// ---------------------------------------------------------
class Player : public Object
{
	// --------------
	// --- public
	// --------------
	public :
		Player( ) ;										// --- コンストラクタ
		~Player( ) ;									// --- デストラクタ

		PACK_ATK getAtkInfo(){ return this->atkInfo ; }	// --- AtkInfo 取得関数

		void initStates_Player() ;						// --- ステータス初期化メソッド

		int PlayerId( int arg_id ) ;					// --- プレイヤーの識別を行うメソッド

		int PlayerAction( int ) ;						// --- アクションの検知
		int PlayerAction_AfterStage1() ;				// --- アクション_切替後のアクション(1)
		int PlayerAction_Invisible1to2() ;				// --- アクション_フェーズ切替(1〜2)
		int PlayerAction_Invisible2to3() ;				// --- アクション_フェーズ切替(2〜3)

		int ChangeEmiPower() ;									// --- エミッションパゥワをいじる関数

		int pAttackZone_HitPowerSet( const int arg_pNo ) ;		// --- 攻撃判定に当たった際の、吹っ飛びパワーを決定するメソッド
		int pAttackZone_Func() ;								// --- 攻撃判定の処理を行うメソッド
		int PlayerAnim() ;										// --- アニメ再生関数
		int HitCheck_pAttack() ;								// --- プレイヤーの攻撃判定とヒットチェックを行うメソッド
		int HitCheck_Player_To_Player() ;
		VECTOR HitCheck_ReturnMove( VECTOR arg_move ) ;			// --- ヒットチェック後、実移動量を帰すメソッド

		// =============================== すべてアニメ識別子(列挙型)を返す
		int PlayerMove() ;			// --- 動きのアニメ
		int PlayerWait() ;			// --- 待機のアニメ
		int PlayerPush() ;			// --- 押すアニメ
		int PlayerPress() ;			// --- 押されるアニメ
		int PlayerFallInit() ;		// --- 落ちるアニメ(初期化)
		int PlayerFall() ;			// --- 落ちるアニメ
		int PlayerDeath() ;			// --- 死亡処理
		int PlayerRevive() ;		// --- 復活処理
		int PlayerCatch() ;			// --- ドット取得処理
		int PlayerSuper() ;			// --- スーパーパックマン処理
		PUSH_STATE pad ;			// --- パッドの入力情報入れる

		int selmodel ;				// --- 選んだモデル情報の格納

		int fallDotMinus ;			// --- 落ちた時のドット減少分
		int dotscore ;				// --- Dotスコア
		int dotcharge ;				// --- Dotゲージ貯め
		int dotChargeMax ;			// --- スーパー化するのに必要なDotの数

		int pushscore ;				// --- 落とした数のスコア

		float		deathHeight ;	// --- 死ぬ高さ( -700くらい？ )

		float		fallSpeed ;		// --- 落下速度
		PMOVE_STATE	moveInfo ;		// --- 移動関連
		PMOVE_ONICE	move_onIce ;	// --- 移動制御_氷上

		BOOL		superflg ;		// --- スーパーフラグ
		int			superendtime ;	// --- スーパー終了時間
		int			supertime ;		// --- スーパータイマー

		PACK_ATK	atkInfo ;		// --- 攻撃判定関連

		BOOL		land ;			// --- 着地アニメーションフラグ

		BOOL		isWin_now ;		// --- 現時点で相手プレイヤーに優位であるかのフラグ

		c_countGroup<float>	emiPower ;	// --- エミッションパゥワ

	private :
		//　---- メンバ関数
		void PlayerActionChange() ;	// --- プレイヤー行動切り替え

		// ---- メンバ変数
		int playerno ;						// --- プレイヤー識別
		int key ;							// --- キーボード取得用変数
		int clearflg ;						// --- 透明化フラグ
		int keyflg_1 ;						// --- キーボードで入力分けるのに使う
		float speed ;						// --- ステージ切り替え時プレイヤー落下速度
		c_countGroup<int> timeCount ;		// --- スーパー状態の秒数

		BOOL animendflg	;			// --- アニメ終了フラグ

		int superAttacktime ;

		int effectcnt ;				// --- エフェクト用タイムカウンター

		float clearmodel ;			// --- 透明度

		BOOL	isSlide ;			// --- 滑りフラグ
		VECTOR	moveCut_Slide ;		// --- 移動量を減らす分の数値( 氷ステージで使用 )

		c_countGroup<float>	floatCnt ;			// --- 復活時の浮遊関連
		int					flashtime ;			// --- 点滅用タイマー
		int					flashcnt ;			// --- 点滅のカウント
		BOOL				reviveflg ;			// --- 復活後の秒数カウント用フラグ
		BOOL				isInvincible ;		// --- 無敵かどうかを表すフラグ
		BOOL				isAnimChange ;		// --- アニメ―ション切替フラグ

		float				size_fallWidth ;	// --- プレイヤーのサイズ( 落下ヒットチェックの時に使用 )

		int nowmap ;							// --- マップ情報の取得用

		int notmyid ;

		float nokezoriFirstPower ;	// --- 仰け反り時の初速
} ;


	