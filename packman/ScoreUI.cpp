/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: ScoreUI.cpp
	CREATE	: HIRANO & NOGUCHI
	
	------ Explanation of file ---------------------------------------------------------------------
       
	ScoreUIクラス

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

#include "Common.h"

ScoreUI::ScoreUI( ){
	this->initStates() ;
}
ScoreUI::~ScoreUI( ){
}

/* ------------------------------------------------------------
					ScoreUI関数
===============================================================
	プレイヤーIDＵＩの表示
	引数：なし
	戻り値：int
--------------------------------------------------------------- */
void ScoreUI::initStates()
{
	plyScore_BeforeOneSec = 0 ;
	sizeChange.now		= 0.8f ;			// --- サイズ変更カウンタ_現在地
	sizeChange.plus		= 0.15f ; 			// --- サイズ変更カウンタ_加算地
	sizeChange.max[0]	= 1.5f ;			// --- サイズ変更カウンタ_最大値
	sizeChange.max[1]	= sizeChange.now ;	// --- サイズ変更カウンタ_最大値_BuckUp

	isSizeChangeNum		= FALSE ;			// --- 数字のサイズ変更フラグ

	isLastSpurt			= FALSE ;			// --- ラストスパートフラグ
}

/* ------------------------------------------------------------
					ScoreUI関数
===============================================================
	プレイヤーIDＵＩの表示
	引数：なし
	戻り値：int
--------------------------------------------------------------- */
void ScoreUI::UIDraw( int eleno )
{
	// ------------------------------------------------
	// ---				変数宣言地帯
	// ------------------------------------------------
	c_fPoint	scoreDispPos[2] ;															// --- 表示座標
	float		chipSize_half = g_graphHandle[ e_Image_Time1 ].chipSize.x / 2 * 0.8f ;		// --- 桁同士の隙間

	int			score = 0 ;							// --- 経過秒数入れる
	int			s1_score, s2_score ;				// --- 計算結果入れる

	POINT		cutSpot[2] ;						// --- 画像切り取り位置

	scoreDispPos[0] = c_fPoint( (float)(g_winSize.x / 10) * 7.5f, GAMEUI_POS_Y ) ;
	scoreDispPos[1] = c_fPoint( (float)(g_winSize.x / 10) * 2.5f, GAMEUI_POS_Y ) ;

	// ------------------------------------------------
	// ---				必要な数値を算出
	// ------------------------------------------------
	score = g_Player[ eleno ].pushscore ;		// スコア格納

	s2_score = score / 10 ;		// 10の位の計算
	s1_score = score % 10 ;		// 1の位の計算

	// --- 前回のスコアと異なっていた場合( カウントアップの瞬間を得る )
	if ( (plyScore_BeforeOneSec != score) && (!isLastSpurt) )
	{
		plyScore_BeforeOneSec = score ;				// 現在のスコアを取得
		isSizeChangeNum = TRUE ;					// サイズ変更フラグTRUE
	}

	switch( eleno )
	{
		// --------------------
		// --- プレイヤー１
		case PLAYER_ONE :
			cord.x = scoreDispPos[0].x ;											// 描画するｘ座標
			cord.y = scoreDispPos[0].y ;											// 描画するｙ座標
			cutSpot[0].x = g_graphHandle[ e_Image_Time1 ].chipSize.x * s1_score ;	// 画像の描画したい左上ｘ座標_1桁目
			cutSpot[1].x = g_graphHandle[ e_Image_Time1 ].chipSize.x * s2_score ;	// 画像の描画したい左上ｘ座標_2桁目
			cutSpot[0].y = g_graphHandle[ e_Image_Time1 ].chipSize.y * 2 ;			// 画像の描画したい左上ｙ座標_1桁目
			cutSpot[1].y = g_graphHandle[ e_Image_Time1 ].chipSize.y * 2 ;			// 画像の描画したい左上ｙ座標_2桁目

			break ;

		// --------------------
		// --- プレイヤー２
		case PLAYER_TWO :
			cord.x = scoreDispPos[1].x ;											// 描画するｘ座標
			cord.y = scoreDispPos[1].y ;											// 描画するｙ座標
			cutSpot[0].x = g_graphHandle[ e_Image_Time1 ].chipSize.x * s1_score ;	// 画像の描画したい左上ｘ座標_1桁目
			cutSpot[1].x = g_graphHandle[ e_Image_Time1 ].chipSize.x * s2_score ;	// 画像の描画したい左上ｘ座標_2桁目
			cutSpot[0].y = g_graphHandle[ e_Image_Time1 ].chipSize.y ;				// 画像の描画したい左上ｙ座標_1桁目
			cutSpot[1].y = g_graphHandle[ e_Image_Time1 ].chipSize.y ;				// 画像の描画したい左上ｙ座標_2桁目

			break ;
	}

	image_w = g_graphHandle[ e_Image_Time1 ].chipSize.x ;		// 表示する幅
	image_h = g_graphHandle[ e_Image_Time1 ].chipSize.y ;		// 表示する高さ

	// ------------------------------------------------
	// ---		ステータス変更( 文字サイズ )
	// ------------------------------------------------
	if ( isSizeChangeNum )
	{
		// --- 0.8からMAXに遷移
		if ( sizeChange.plus > 0.0f )
		{
			if ( FALSE == CheckLimitAndPlus<float>( CLAPMODE_BELOW, &sizeChange.now, sizeChange.plus, sizeChange.max[0], sizeChange.max[0] ) )
			{
				sizeChange.now = sizeChange.max[0] ;
				sizeChange.plus *= -1 ;
			}
		}
		// --- MAXから0.8に遷移
		else
		{
			if ( FALSE == CheckLimitAndPlus<float>( CLAPMODE_EXCEED, &sizeChange.now, sizeChange.plus, sizeChange.max[1], sizeChange.max[1] ) )
			{
				sizeChange.now = sizeChange.max[1] ;
				sizeChange.plus *= -1 ;
				isSizeChangeNum = FALSE ;
			}
		}
	}

	// ------------------------------------------------
	// ---					本描画
	// ------------------------------------------------
	if( (90 - Time[ e_Timer3 ].CountSecond( )) < 32 )
	{
		cutSpot[0].x = g_graphHandle[ e_Image_Time1 ].chipSize.x * 10 ;		// --- カウントが残り僅かだと「？？」表記になる
		cutSpot[1].x = g_graphHandle[ e_Image_Time1 ].chipSize.x * 10 ;		// --- カウントが残り僅かだと「？？」表記になる
		isLastSpurt = TRUE ;												// --- ラストスパート( 残り僅かだとスコア加算の発光を行わない )
	}

	if ( ((ScoreBoard[0].f_disp == TRUE) && (eleno == 0)) || ((ScoreBoard[1].f_disp == TRUE) && (eleno == 1)) ||
		 ((ScoreBoard[2].f_disp == TRUE) && (eleno == 2)) || ((ScoreBoard[3].f_disp == TRUE) && (eleno == 3)))
	{
		// --- 画像の切り出しを行い、新たなハンドルで描画を行う( 拡大したいため )
		int newImgHandle[2] ;
		newImgHandle[0] = DerivationGraph( (int)cutSpot[0].x, (int)cutSpot[0].y, image_w, image_h, g_graphHandle[ e_Image_Time1 ].handle ) ;	// --- 一桁目
		newImgHandle[1] = DerivationGraph( (int)cutSpot[1].x, (int)cutSpot[1].y, image_w, image_h, g_graphHandle[ e_Image_Time1 ].handle ) ;	// --- 二桁目

		DrawRotaGraph( (int)(cord.x - chipSize_half), (int)cord.y, sizeChange.now, 0.0, newImgHandle[1], TRUE ) ;
		DrawRotaGraph( (int)(cord.x + chipSize_half), (int)cord.y, sizeChange.now, 0.0, newImgHandle[0], TRUE ) ;

		DeleteGraph( newImgHandle[0] ) ;
		DeleteGraph( newImgHandle[1] ) ;
	}

	// ------------------------------------------------
	// --- サイズが変わっている場合、色を変更にする
	// ------------------------------------------------
	if ( (isSizeChangeNum) && (!isLastSpurt) )
	{
		int newFlaHandle[2] ;
		newFlaHandle[0] = DerivationGraph( (int)cutSpot[0].x, 0, image_w, image_h, g_graphHandle[ e_Image_Time1_FlashBlend ].handle ) ;	// --- 一桁目
		newFlaHandle[1] = DerivationGraph( (int)cutSpot[1].x, 0, image_w, image_h, g_graphHandle[ e_Image_Time1_FlashBlend ].handle ) ;	// --- 二桁目

		// --- 発光用のカウントを取得
		// --- MIN - 1.8, MAX - 2.5 だった場合、割合で【0〜1】を取るのが難しいため、それぞれ1.8を引いた数値で計算する
		float nowFlash = sizeChange.now - sizeChange.max[1] ;
		float maxFlash = sizeChange.max[0] - sizeChange.max[1] ;

		// --- 発光用の画像を描画
		SetDrawBlendMode( DX_BLENDMODE_ADD, (int)(255.0f * (nowFlash / maxFlash)) ) ;
		DrawRotaGraph( (int)(cord.x - chipSize_half), (int)cord.y, sizeChange.now, 0.0, newFlaHandle[1], TRUE ) ;
		DrawRotaGraph( (int)(cord.x + chipSize_half), (int)cord.y, sizeChange.now, 0.0, newFlaHandle[0], TRUE ) ;

		SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;

		DeleteGraph( newFlaHandle[0] ) ;
		DeleteGraph( newFlaHandle[1] ) ;
	}
}


