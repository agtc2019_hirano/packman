/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: Scaffold.h
	CREATE	: HIRANO
	
	------ Explanation of file ---------------------------------------------------------------------
       
	足場クラスの宣言

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

class Scaffold : public Object
{
	public :
		Scaffold( ) ;		// コンストラクタ
		~Scaffold( ) ;		// デストラクタ

		void initStates() ;	// --- 初期化

		int MapSelect( ) ;	// マップを選択する

		int mapdate1[ELE_X_1 * ELE_Y_1] ;		// マップ情報を保持する
		int mapdate2_t[ELE_X_2 * ELE_Y_2] ;		// マップ情報を保持する
		int mapdate2_u[ELE_X_2 * ELE_Y_2] ;		// マップ情報を保持する
		int mapdate3[ELE_X_3 * ELE_Y_3] ;		// マップ情報を保持する

		int mapmode ;							// マップモード
//		int mapswitch ;							// マップスイッチ（フェーズ管理はここ見る）
		int f_move ;							// ムーブフラグ
		int c_scaf ;							// カウンター
		int count ;

} ;


	