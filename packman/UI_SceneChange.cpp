/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: UI_SceneChange.cpp
	CREATE	: NOGUCHI
	
	------ Explanation of file ---------------------------------------------------------------------
       
	UI_SceneChangeクラスの実処理記述部
	
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

#include "Common.h"

// ---------------------------------------------------------------------------------
//									コンストラクタ
// ---------------------------------------------------------------------------------
UI_SceneChange::UI_SceneChange( )
{

}

// ---------------------------------------------------------------------------------
//									デストラクタ
// ---------------------------------------------------------------------------------
UI_SceneChange::~UI_SceneChange( )
{

}

// ---------------------------------------------------------------------------------
//								フィールド初期化メソッド
// ---------------------------------------------------------------------------------
void UI_SceneChange::initStates()
{
	float otOffSet_X	= (float)(g_graphHandle[ e_Image_SceneChange_Bar ].fullSize.x / 2) ;	// --- 一時的なオフセット

	this->BarNum		= 1920 / g_graphHandle[ e_Image_SceneChange_Bar ].fullSize.x ;			// --- バー数
	this->BarMoveSpeed	= 40.0f ;																// --- バー移動速度( Y座標 )

	// --- チェックする際の初期位置セット
	BarInitPos[0].y = (0 - (float)(g_graphHandle[ e_Image_SceneChange_Bar ].fullSize.y / 2) -
								(float)(g_graphHandle[ e_Image_SceneChange_Icon ].fullSize.y / 2)) ;			// --- 初期位置_上
	BarInitPos[1].y = (float)g_winSize.y / 2 ;																	// --- 初期位置_中
	BarInitPos[2].y = (g_winSize.y + (float)(g_graphHandle[ e_Image_SceneChange_Bar ].fullSize.y / 2) +
										(float)(g_graphHandle[ e_Image_SceneChange_Icon ].fullSize.y / 2)) ;	// --- 初期位置_下
	// --- 初期位置
	for ( int i = 0 ; i < BarNum ; i++ )
	{
		this->BarPos[ i ].x = ((1920 / 6) * i) + otOffSet_X ;
		this->BarPos[ i ].y = BarInitPos[0].y ;
	}

	// --- カウント関連
	moveCnt.now		= 0 ;		// --- 現在カウンタ
	moveCnt.max[0]	= 10 ;		// --- この倍数に達したら隣のバーの移動を開始する
}

// ================================================================================================================ UI Action
// ---------------------------------------------------------------------------------
//
//
//							シーン遷移のアクションを行う
//
//						完了すると帰り値が1、そうでないなら0。
//						引数を異なるものにすると-1が帰ってくる。
//
//
// ---------------------------------------------------------------------------------
//				・引数１ - {0で上から中へのの下降} {1で中から中への下降}
// ---------------------------------------------------------------------------------
int UI_SceneChange::UIAction( int eleno )
{
	int OKRet = 1 ;

	switch ( eleno )
	{
		// -------------------
		//	下降処理( 上→中 )
		// -------------------
		case 0 :
			// --- 初動位置をセット
			if ( moveCnt.now == 0 )
			{
				OKRet = 0 ;

				for ( int i = 0 ; i < BarNum ; i++ )
					BarPos[ i ].y = BarInitPos[0].y ;
			}

			// --- 移動開始！
			for ( int i = 0 ; i < BarNum ; i++ )
			{
				// --- 自身の移動開始カウンタを通り過ぎていたら移動( 全て移動しきるとOKRetは1のまま。つまり移動完了ということになる )
				if ( moveCnt.now >= (moveCnt.max[0] * i) )
				{
					if ( CheckLimitAndPlus<float>( CLAPMODE_BELOW, &BarPos[ i ].y, BarMoveSpeed, BarInitPos[1].y, BarInitPos[1].y ) )
						OKRet = 0 ;
				}
			}

			moveCnt.now++ ;

			break ;

		// -------------------
		//	下降処理( 中→下 )
		// -------------------
		case 1 :
			// --- 初動位置をセット
			if ( moveCnt.now == 0 )
			{
				OKRet = 0 ;

				for ( int i = 0 ; i < BarNum ; i++ )				
					BarPos[ i ].y = BarInitPos[1].y ;
			}

			// --- 移動か開始！
			for ( int i = 0 ; i < BarNum ; i++ )
			{
				// --- 自身の移動開始カウンタを通り過ぎていたら移動( 全て移動しきるとOKRetは1のまま。つまり移動完了ということになる )
				if ( moveCnt.now > (moveCnt.max[0] * i) )
				{
					if ( CheckLimitAndPlus<float>( CLAPMODE_BELOW, &BarPos[ i ].y, BarMoveSpeed, BarInitPos[2].y, BarInitPos[2].y ) )
						OKRet = 0 ;
				}
			}

			moveCnt.now++ ;

			break ;

		// -------------------
		//		？？？？
		// -------------------
		default :

			return -1 ;
			break ;
	}

	// --- 移動完了でカウント0
	if ( OKRet == 1 )
		moveCnt.now = 0 ;

	return OKRet ;
}

// ================================================================================================================ UI Draw
// ---------------------------------------------------------------------------------
//
//
//							シーン遷移の描画を行う
//
//
// ---------------------------------------------------------------------------------
//					・引数１ - アイコンの表示位置( 0で下、1で上 )
// ---------------------------------------------------------------------------------
void UI_SceneChange::UIDraw( int switchNo )
{
	float zurashiPos_Y ;			// --- アイコン描画位置

	// -----------------------------------
	//				バー描画
	// -----------------------------------
	for ( int i = 0 ; i < BarNum ; i++ )
		DrawRotaGraph( (int)BarPos[ i ].x, (int)BarPos[ i ].y, 1.0, 0.0, g_graphHandle[ e_Image_SceneChange_Bar ].handle, TRUE ) ;

	// -----------------------------------
	//			  アイコン描画
	// -----------------------------------
	for ( int i = 0 ; i < BarNum ; i++ )
	{
		// --- アイコンの描画位置を決定
		zurashiPos_Y = (float)(g_graphHandle[ e_Image_SceneChange_Bar ].fullSize.y / 2) ;

		if ( switchNo == 1 )
			zurashiPos_Y *= -1 ;

		DrawRotaGraph( (int)BarPos[ i ].x, (int)BarPos[ i ].y + (int)zurashiPos_Y, 1.0, 0.0, g_graphHandle[ e_Image_SceneChange_Icon ].handle, TRUE ) ;
	}
}



