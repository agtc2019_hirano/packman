/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: UI_CountDownNumber.cpp
	CREATE	: NOGUCHI
	
	------ Explanation of file ---------------------------------------------------------------------
       
	UI_CountDownNumberクラスの実処理記述部
	
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

#include "Common.h"

// ---------------------------------------------------------------------------------
//									コンストラクタ
// ---------------------------------------------------------------------------------
UI_CountDownNumber::UI_CountDownNumber( )
{
	nowCount	= -1 ;
	cSize		= 0.0f ;		// --- サイズ
	cRot		= 0.0f ;		// --- 回転

	sizeMax		= 1.0f ;		// --- 最高サイズ

	sizeChange.max[0] = 20.0f ;	// --- MAX拡大までの回数

	fadeInterval.now	= 0 ;	// --- フェードアウトまでの待機時間
	fadeInterval.max[0] = 25 ;	// --- フェードアウトまでの待機時間_MAX

	fOut.now	= 15 ;			// --- フェードアウトカウンタ
	fOut.plus	= 1 ;			// --- フェードアウトカウンタ_減算分
	fOut.max[0] = fOut.now ;	// --- フェードアウトカウンタ_MAX
}

// ---------------------------------------------------------------------------------
//									デストラクタ
// ---------------------------------------------------------------------------------
UI_CountDownNumber::~UI_CountDownNumber( )
{

}

// ================================================================================================================ Count Action
// ---------------------------------------------------------------------------------
//
//
//						カウントダウンのアクションを行う
//
//								帰り値 TRUE で終了。
//
//
// ---------------------------------------------------------------------------------
BOOL UI_CountDownNumber::CountAction()
{
	// --- 切り替わりの瞬間にリセット
	if ( nowCount != Time[0].CountSecond() )
	{
		cSize				= 0.0f ;
		cRot				= 0.0f ;
		fOut.now			= fOut.max[0] ;
		fadeInterval.now	= 0 ;

		sizeChange.now = 0.0f ;

		if( nowCount < 2 )
		{
			// --- ゴング再生
			SOUND_INFO *otSP = &g_soundHandle[ e_Sound_CountdownSE ] ;	// --- カウントダウン
			PlaySoundMem( otSP->soundHandle, otSP->playType ) ;
		}else if( nowCount == 2 ){
			// --- ゴング再生
			SOUND_INFO *otSP = &g_soundHandle[ e_Sound_GongSE ] ;	// --- ゴング
			PlaySoundMem( otSP->soundHandle, otSP->playType ) ;
		}
	}

	nowCount = Time[0].CountSecond() ;				// カウントダウンタイマースタート
	g_camera.cameracount = nowCount ;
	
	// --- 拡大率がMAXになるまでインクリメント
	if ( sizeChange.now + 1 < sizeChange.max[0] )
		sizeChange.now++ ;
	else
		sizeChange.now = sizeChange.max[0] ;

	// --------------------------------------------
	//	数値格納
	// --------------------------------------------
	if ( sizeChange.now != sizeChange.max[0] )
	{
		cRot  = 20 * (sizeChange.now / sizeChange.max[0]) ;	
		cSize = sizeMax * (sizeChange.now / sizeChange.max[0]) ;
	}
	else
	{
		if ( fadeInterval.now + 1 < fadeInterval.max[0] )	
		{
			fadeInterval.now++ ;

			cRot	= 0.0f ;	// --- 登場しきったら回転率0
			cSize	+= 0.05f ;	// --- 登場しきったら徐々にサイズを上げる
		}

		else
		{
			cSize	+= 0.05f ;	// --- 登場しきったら徐々にサイズを上げる
			fOut.now -= fOut.plus ;	// --- 徐々に透明にしていく
		}
	}

	// --- カウントダウンが「3, 2, 1, GO!」を過ぎたら終了判定
	if ( nowCount > 3 )
		return TRUE ;

	return FALSE ;
}

// ================================================================================================================ Count Draw
// ---------------------------------------------------------------------------------
//
//
//							カウントダウンの描画を行う
//
//
// ---------------------------------------------------------------------------------
void UI_CountDownNumber::CountDraw()
{
	int		counter = 3 - nowCount ;		// --- 描画切り取り位置
	float	imagey ;						// ---		→短縮版

	if ( nowCount <= 3 )
	{
		imagey = (float)g_graphHandle[ e_Image_CountDown_Str ].chipSize.y * (3 - counter) ;		// 画像の描画したい左上y座標

		int newCountHandle = DerivationGraph( 0, (int)imagey, g_graphHandle[ e_Image_CountDown_Str ].chipSize.x, g_graphHandle[ e_Image_CountDown_Str ].chipSize.y,
								g_graphHandle[ e_Image_CountDown_Str ].handle ) ;

		// --- 本描画
		SetDrawBlendMode( DX_BLENDMODE_ALPHA, (int)( fOut.now * (255.0f / fOut.max[0]) ) ) ;
		DrawRotaGraph( g_winSize.x / 2, g_winSize.y / 2, cSize, cRot, newCountHandle, TRUE ) ;
		SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;

		DeleteGraph( newCountHandle ) ;
	}
}


