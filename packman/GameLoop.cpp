/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: GameLoop.cpp
	CREATE	: NOGUCHI
	
	------ Explanation of file -------------------------------------------------------------------------
       
	ゲームのメインループ管理部。ここから更にシーンごとに枝分かれする。
	なお、グローバル変数「g_gameMode」の数値をここ以外で変更したら末裔まで呪うものとする。
	
	ここではシューティングゲームの時に使用した「テーブルジャンプ」を扱っており、
	呼び出した関数の帰り値が -1 以外の時、シーン定数に対応するゲームモード変数の値変更処理が行われる。

	(例)
		returnNum = ( *Values_ReturnGameModeUp[ g_gameMode ] )() ;

			→	帰り値が定数「e_Mode_Game_Init」の場合、
				「g_gameMode = e_Mode_GameInit ;」
				という処理がその直後に施される。

	また、特定のパッド情報が１フレームのみ押されたことをチェックするには

		Padinput.Check1FramePressed( パッドNo, キー定数 )

	の帰り値を見ればいい。TRUE なら１フレーム目であることが保証される。

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

#include "Common.h"
#include <math.h>

// ---------------------------------
//		ゲームループ用の変数
// ---------------------------------
S_PLAYER_INPUTINFO	player_PSInfo ;			// --- プレイヤーの押した情報
S_PLAYER_INPUTINFO	*piP = &player_PSInfo ;	// --- ↑のポインタ(長いので、こっちの方が打ちやすいよ)

Player				*otPP ;					// --- プレイヤーのポインタ

BOOL				isUseKeyInput = TRUE ;	// --- キー入力を受け付けるかどうかのフラグ( これグローバルの方がいいか？ )

// ---------------------------------
//		テーブルジャンプ実装部
// ---------------------------------
int LoopFunc_Blank() ;						// --- ブランク				処理関数
int LoopFunc_TitleInit() ;					// --- タイトル_初期化		処理関数
int LoopFunc_Title() ;						// --- タイトル				処理関数
int LoopFunc_CharaSelectInit() ;			// --- キャラ選択_初期化	処理関数
int LoopFunc_CharaSelect() ;				// --- キャラ選択			処理関数
int LoopFunc_GameInit() ;					// --- ゲーム_初期化		処理関数
int LoopFunc_GameStart() ;					// --- ゲーム_スタート		処理関数
int LoopFunc_Game() ;						// --- ゲーム				処理関数
int LoopFunc_ResultInit() ;					// --- 結果発表_初期化		処理関数
int LoopFunc_Result() ;						// --- 結果発表				処理関数

int ( *Values_ReturnGameModeChange[] )() =	
	{	LoopFunc_Blank,
		LoopFunc_TitleInit,
		LoopFunc_Title,
		LoopFunc_CharaSelectInit,
		LoopFunc_CharaSelect,
		LoopFunc_GameInit,
		LoopFunc_GameStart,
		LoopFunc_Game,
		LoopFunc_ResultInit,
		LoopFunc_Result,
	} ;

// ============================================================================================================================================== Game Loop
// ---------------------------------------------------------------------------------------------------------------------------
//
//											全体のゲームループを司る
//									あれ？これもしかして関数化する必要ないのでは？？
//
// ---------------------------------------------------------------------------------------------------------------------------
int GameLoop()
{
	// パッド情報の更新
	Padinput.UpDate( ) ;	
	for ( char i = 0 ; i < Padinput.Get_PadNum() ; i++ )
		g_dataPadInput[i] = PadData::GetButton( i ) ;

	// --- ゲームモードに応じた関数を呼び出す。( 同時に帰り値も受け取る )
	int returnNum = ( *Values_ReturnGameModeChange[ g_gameMode ] )() ;

	// --- ゲームモードをメインループ内の最後で変更する為に、帰り値で渡す
	return returnNum ;
}

// ============================================================================================================================================== 【ブランク】
// ---------------------------------------------------------------------------------------------------------------------------
//
//										GameLoop関数_ブランク
//
// ---------------------------------------------------------------------------------------------------------------------------
//
//								帰り値 :	-1		- シーンそのまま判定
//											-1以外	- シーンをその定数と同じにする判定
//
// ---------------------------------------------------------------------------------------------------------------------------
int LoopFunc_Blank(  )
{
	// --- GameLoop用の変数初期化
	SecureZeroMemory( &player_PSInfo, sizeof( S_PLAYER_INPUTINFO ) ) ;

	return e_Mode_Title_Init ;
	return e_Mode_Game_Init ;
	return e_Mode_Result_Init ;
}

// ============================================================================================================================================== 【タイトル部】
// ---------------------------------------------------------------------------------------------------------------------------
//
//										GameLoop関数_タイトル初期化
//
// ---------------------------------------------------------------------------------------------------------------------------
//
//							帰り値 :	-1		- シーンそのまま判定
//										-1以外	- シーンをその定数と同じにする判定
//
// ---------------------------------------------------------------------------------------------------------------------------
int LoopFunc_TitleInit(  )
{
	const int cat = 1 ;		// --- 定数：ネコ
	const int pig = 2 ;		// --- 定数：ブタ

	SetShadowMapLightDirection( g_shadowMap.handle, VGet( 0.0f, -0.4f, 0.5f ) ) ;		// --- 影の向きをセット
	g_shadowMap.isUse = TRUE ;

	g_camera.pos		= VGet( 0.0f,		300.0f,		-1500.0f ) ;	// --- 座標セット
	g_camera.lookPos	= VGet( 0.0f,		0.0f,		0.0f ) ;		// --- 注視点セット
	g_camera.upDir		= VGet( 0.0f,		1.0f,		0.0f ) ;		// --- 上方向セット

	// --- GameLoop用の変数初期化
	SecureZeroMemory( piP, sizeof( S_PLAYER_INPUTINFO ) ) ;

	piP->setBlackCount_Max	= 100 ;		// --- フェードアウトにかかるカウンタ

	piP->any_flg[0]			= TRUE ;	// --- テキスト表示の ON/OFF フラグ
	piP->any_number[0]		= 20 ;		// --- [Push To Enter] 点滅速度
	piP->any_flg[2]			= TRUE ;	// --- キツネ動き　上下　フラグ

	piP->cSel_Icon[0]		= 0 ;
	piP->cSel_Icon[1]		= 2 ;

	// --- 音量セット
	for ( int i = 0 ; i < e_Sound_Total ; i++ )
		StopSoundMem( g_soundHandle[ i ].soundHandle ) ;	// --- 全てのサウンドを止める

	SetAllSoundVolume100() ;								// --- ボリューム 100セット

	// --- タイトルＢＧＭ再生
	SOUND_INFO *otSP = &g_soundHandle[ e_Sound_TitleBGM ] ;
	PlaySoundMem( otSP->soundHandle, otSP->playType ) ;

//	SetSoundVolumeNum( 100, &g_soundHandle[ e_Sound_TitleBGM ] ) ;				// --- ボリューム 100セット

	g_Object[ OBJID_SKYDOM ].dir.y = 1 ;

	// --- タイトルで使用するオブジェクトのセット
	g_Object[ OBJID_TITLEOB ].setModelAndAnim( g_mv1Handle[ e_Model_Animal_Fox_1P ], TRUE, VGet( 0.5f, 0.5f, 0.5f )  ) ;						// --- キツネ
	g_Object[ OBJID_TITLEOB + e_Title_Cat ].setModelAndAnim( g_mv1Handle[ e_Model_Animal_Cat_1P ], TRUE, VGet( 0.5f, 0.5f, 0.5f ) ) ;			// --- ネコ
	g_Object[ OBJID_TITLEOB + e_Title_Pig ].setModelAndAnim( g_mv1Handle[ e_Model_Animal_Pig_1P ], TRUE, VGet( 0.5f, 0.5f, 0.5f ) ) ;			// --- ブタ
	g_Object[ OBJID_TITLEOB + e_Title_BeachChair ].setModelAndAnim( g_mv1Handle[ e_Model_BeachChair ], TRUE, VGet( 0.5f, 0.5f, 0.5f ) ) ;		// --- ビーチチェアー
	g_Object[ OBJID_TITLEOB + e_Title_Table ].setModelAndAnim( g_mv1Handle[ e_Model_Table ], TRUE, VGet( 0.6f, 0.6f, 0.6f ) ) ;					// --- テーブル
	g_Object[ OBJID_TITLEOB + e_Title_Juice ].setModelAndAnim( g_mv1Handle[ e_Model_Juice ], TRUE, VGet( 0.5f, 0.5f, 0.5f ) ) ;					// --- ココナッツジュース
	g_Object[ OBJID_TITLEOB + e_Title_YashiWood_1 ].setModelAndAnim( g_mv1Handle[ e_Model_YashiWood ], TRUE, VGet( 0.6f, 0.7f, 0.7f ) ) ;		// --- ヤシの木（ココナッツ付き）with Cat
	g_Object[ OBJID_TITLEOB + e_Title_YashiWood_2 ].setModelAndAnim( g_mv1Handle[ e_Model_YashiWood ], TRUE, VGet( 0.6f, 0.7f, 0.7f ) ) ;		// --- ヤシの木（ココナッツ付き）not Cat
	g_Object[ OBJID_TITLEOB + e_Title_YashiWood_3 ].setModelAndAnim( g_mv1Handle[ e_Model_YashiWood ], TRUE, VGet( 0.6f, 0.7f, 0.7f ) ) ;		// --- ヤシの木（ココナッツ付き）not Cat
	g_Object[ OBJID_TITLEOB + e_Title_Ukiwa ].setModelAndAnim( g_mv1Handle[ e_Model_Ukiwa ], TRUE, VGet( 0.5f, 0.5f, 0.5f ) ) ;					// --- ビーチ
	g_Object[ OBJID_TITLEOB + e_Title_Beach ].setModelAndAnim( g_mv1Handle[ e_Model_Beach ], TRUE, VGet( 0.8f, 0.8f, 0.6f ) ) ;					// --- ビーチ
	g_Object[ OBJID_TITLEOB + e_Title_Nami ].setModelAndAnim( g_mv1Handle[ e_Model_Nami ], TRUE, VGet( 0.6f, 0.6f, 0.6f ) ) ;					// --- 波
	g_Object[ OBJID_TITLEOB + e_Title_Sea ].setModelAndAnim( g_mv1Handle[ e_Model_BSea ], TRUE, VGet( 0.6f, 0.6f, 0.6f ) ) ;					// --- ビーチの海サイズセット
	
	// --- タイトルポジションセット																								
	g_Object[ OBJID_TITLEOB + e_Title_Fox ].pos = VGet( -700.0f, -460.0f, -600.0f ) ;			// --- キツネ
	g_Object[ OBJID_TITLEOB + e_Title_Fox ].dir.y = 3.2f ;
	g_Object[ OBJID_TITLEOB + e_Title_Fox ].movepos = VGet( 0.0f, 0.0f, 0.0f ) ;
	g_Object[ OBJID_TITLEOB + e_Title_Cat ].pos = VGet( 100.0f, -200.0f, 300.0f ) ;				// --- ネコ
	g_Object[ OBJID_TITLEOB + e_Title_Cat ].dir.y = 2.5f ;
	g_Object[ OBJID_TITLEOB + e_Title_Pig ].pos = VGet( 500.0f, -210.0f, -650.0f ) ;			// --- ブタ
	g_Object[ OBJID_TITLEOB + e_Title_Pig ].dir.y = 0.8f ;
	g_Object[ OBJID_TITLEOB + e_Title_BeachChair ].pos = VGet( 500.0f, -210.0f, -650.0f ) ;		// --- ビーチチェアー
	g_Object[ OBJID_TITLEOB + e_Title_BeachChair ].size = VGet( 0.8f, 0.4f, 0.5f ) ;
	g_Object[ OBJID_TITLEOB + e_Title_BeachChair ].dir.y = 2.8f ;
	g_Object[ OBJID_TITLEOB + e_Title_Table ].pos = VGet( 350.0f, -100.0f, -450.0f ) ;			// --- テーブル
	g_Object[ OBJID_TITLEOB + e_Title_Table ].size = VGet( 1.8f, 1.5f, 1.8f ) ;
	g_Object[ OBJID_TITLEOB + e_Title_Juice ].pos = VGet( 360.0f, 140.0f, -460.0f ) ;			// --- ココナッツジュース
	g_Object[ OBJID_TITLEOB + e_Title_Juice ].size = VGet( 0.6f, 0.6f, 0.6f ) ;
	g_Object[ OBJID_TITLEOB + e_Title_YashiWood_1 ].pos = VGet( 330.0f, -200.0f, 380.0f ) ;		// --- ヤシの木（ココナッツ付き）with Cat
	g_Object[ OBJID_TITLEOB + e_Title_YashiWood_1 ].size = VGet( 0.4f, 0.38f, 0.4f ) ;
	g_Object[ OBJID_TITLEOB + e_Title_YashiWood_1 ].dir.y = 3.0f ;
	g_Object[ OBJID_TITLEOB + e_Title_YashiWood_2 ].pos = VGet( 1200.0f, -200.0f, 1000.0f ) ;	// --- ヤシの木（ココナッツ付き）not Cat
	g_Object[ OBJID_TITLEOB + e_Title_YashiWood_2 ].size = VGet( 0.3f, 0.3f, 0.3f ) ;
	g_Object[ OBJID_TITLEOB + e_Title_YashiWood_2 ].dir.y = 3.0f ;
	g_Object[ OBJID_TITLEOB + e_Title_YashiWood_3 ].pos = VGet( 1900.0f, -200.0f, 1200.0f ) ;	// --- ヤシの木（ココナッツ付き）not Cat
	g_Object[ OBJID_TITLEOB + e_Title_YashiWood_3 ].size = VGet( 0.25f, 0.25f, 0.25f ) ;
	g_Object[ OBJID_TITLEOB + e_Title_YashiWood_3 ].dir.y = 3.5f ;
	g_Object[ OBJID_TITLEOB + e_Title_Ukiwa ].pos = VGet(  -700.0f, -450.0f, -600.0f ) ;		// --- 浮き輪
	g_Object[ OBJID_TITLEOB + e_Title_Ukiwa ].size	= VGet( 0.8f, 1.0f, 0.8f ) ;				
	g_Object[ OBJID_TITLEOB + e_Title_Beach ].pos = VGet( 2200.0f, 0.0f, 0.0f ) ;				// --- ビーチ
	g_Object[ OBJID_TITLEOB + e_Title_Beach ].size	= VGet( 2.0f, 2.0f, 2.0f ) ;				
	g_Object[ OBJID_TITLEOB + e_Title_Sea ].pos = VGet( 500.0f, -100.0f, 0.0f ) ;				// --- ビーチの海
	g_Object[ OBJID_TITLEOB + e_Title_Sea ].size	= VGet( 5.0f, 1.0f, 5.0f ) ;				
	g_Object[ OBJID_TITLEOB + e_Title_Nami ].pos = VGet( 2200.0f, -175.0f, 0.0f ) ;				// --- 波
	g_Object[ OBJID_TITLEOB + e_Title_Nami ].movepos = VGet( 0.0f, 1.0f, 0.0f ) ;				
	g_Object[ OBJID_TITLEOB + e_Title_Nami ].size	= VGet( 2.0f, 2.0f, 2.0f ) ;				

	// --- 動物たちのアニメーションセット
	g_Object[ OBJID_TITLEOB + e_Title_Fox ].setAnim( e_PlayerTitle_Flow ) ;			// --- アニメーション：浮かぶ
	g_Object[ OBJID_TITLEOB + e_Title_Cat ].setAnim( e_PlayerTitle_Jump ) ;			// --- アニメーション：跳ぶ
	g_Object[ OBJID_TITLEOB + e_Title_Pig ].setAnim( e_PlayerTitle_Sleep ) ;		// --- アニメーション：寝る

	g_Object[ OBJID_TITLEOB + e_Title_Fox].nowanim = e_PlayerTitle_Flow ;
	g_Object[ OBJID_TITLEOB + e_Title_Cat ].nowanim = e_PlayerTitle_Jump ;
	g_Object[ OBJID_TITLEOB + e_Title_Pig ].nowanim = e_PlayerTitle_Sleep ;

	g_Object[ OBJID_TITLEOB + e_Title_Fox ].playTime = 0.0f ;
	g_Object[ OBJID_TITLEOB + e_Title_Cat ].playTime = 0.0f ;
	g_Object[ OBJID_TITLEOB + e_Title_Pig ].playTime = 0.0f ;

	UI_War.WarInit() ;
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//				モード切替
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	return e_Mode_Title ;
}

// ---------------------------------------------------------------------------------------------------------------------------
//
//												GameLoop関数_タイトル
//
// ---------------------------------------------------------------------------------------------------------------------------
//
//								帰り値 :	-1		- シーンそのまま判定
//											-1以外	- シーンをその定数と同じにする判定
//
// ---------------------------------------------------------------------------------------------------------------------------
int LoopFunc_Title(  )
{
	// --- 使用状況 ---	
	//
	//	any_counter[0]	-	テキスト点滅時のカウンター
	//
	//	any_flg[0]		-	テキスト表示の ON/OFF フラグ
	//	any_flg[1]		-	フェードアウト開始フラグ
	//	any_flg[2]		-	キツネ　動き　上下フラグ
	//
	//	any_number[0]	-	点滅速度の格納先
	//
	// ----------------

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//	暗転処理時間が終わったらモード切替
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	if ( piP->setBlackCount >= piP->setBlackCount_Max )
	{
		StopSoundMem( g_soundHandle[ e_Sound_TitleBGM ].soundHandle ) ;		// --- サウンド停止
		StopSoundMem( g_soundHandle[ e_Sound_SeaWaveSE ].soundHandle ) ;		// --- サウンド停止
		return e_Mode_CharaSelect_Init ;
	}

	// --- 一定フレームごとに点滅
	piP->any_counter[0]++ ;
	if ( 0 == piP->any_counter[0] % piP->any_number[0] )
		piP->any_flg[0] ^= 0x01 ;				// --- 01切替

	// --- 押下判定がTRUEだった際、フェードアウトカウントを上げる
	if ( piP->title_PushFlg )
	{
		piP->setBlackCount++ ;

		// --- 音量をフェードアウトに合わせて下げる
		SetSoundVolumePlusNum( -1, &g_soundHandle[ e_Sound_TitleBGM ] ) ;
		SetSoundVolumePlusNum( -1, &g_soundHandle[ e_Sound_SeaWaveSE ] ) ;
	}

	// --- 押下情報を取得したらフラグをTRUEにする
	BOOL isOTPush = FALSE ;
	if ( (Padinput.Check1FramePressed( 0, PAD_INPUT_B )) || (Padinput.Check1FramePressed( 1, PAD_INPUT_B )) )
		isOTPush = TRUE ;

	if ( (isUseKeyInput) && (g_keyBuf[ KEY_INPUT_SPACE ] == 1) )	
		isOTPush = TRUE ;

	if ( (isOTPush) && (!piP->title_PushFlg) )
	{
		SOUND_INFO *otSP = &g_soundHandle[ e_Sound_TitlePushSE ] ;
		PlaySoundMem( otSP->soundHandle, otSP->playType ) ;			// --- 効果音再生

		piP->title_PushFlg = TRUE ;				// --- 押下フラグ_ON
		piP->any_number[0] = 5 ;				// --- 点滅速度を早くする
	}

	// --- 海の音再生
	if( CheckSoundMem( g_soundHandle[ e_Sound_SeaWaveSE ].soundHandle ) == 0 )
	{
//		SetAllSoundVolume100() ;								// --- ボリューム 100セット

		SOUND_INFO *otSP = &g_soundHandle[ e_Sound_SeaWaveSE ] ;
		PlaySoundMem( otSP->soundHandle, otSP->playType ) ;
	}

	// -----------------------------------
	//		キツネの動き
	// -----------------------------------
	if ( piP->any_flg[2] == TRUE ){
		g_Object[ OBJID_TITLEOB + e_Title_Fox ].movepos.y = -0.2f ;		// --- キツネ沈む
	if ( g_Object[ OBJID_TITLEOB + e_Title_Fox ].pos.y < -475.0f )
		piP->any_flg[2] = FALSE ;
	}
	if ( piP->any_flg[2] == FALSE ){
		g_Object[ OBJID_TITLEOB + e_Title_Fox ].movepos.y = 0.2f ;		// --- キツネ浮く
		if ( g_Object[ OBJID_TITLEOB + e_Title_Fox ].pos.y > -460.0f )
			piP->any_flg[2] = TRUE ;
	}
	// --- キツネと浮き輪のポジションセット
	g_Object[ OBJID_TITLEOB + e_Title_Fox ].pos.y += g_Object[ OBJID_TITLEOB + e_Title_Fox ].movepos.y ;
	g_Object[ OBJID_TITLEOB + e_Title_Ukiwa ].pos.y += g_Object[ OBJID_TITLEOB + e_Title_Fox ].movepos.y ;

	// -----------------------------------
	//		波の動き
	// -----------------------------------
	// --- 波の縮小
	g_Object[ OBJID_TITLEOB + e_Title_Nami ].movepos.x = -0.01f ; 
	g_Object[ OBJID_TITLEOB + e_Title_Nami ].movepos.z = -0.01f ;
	if ( g_Object[ OBJID_TITLEOB + e_Title_Nami ].movepos.y <= 1.0f )
		g_Object[ OBJID_TITLEOB + e_Title_Nami ].movepos.y += 0.01f ;	// --- 不透明度上げる
	else{
		g_Object[ OBJID_TITLEOB + e_Title_Nami ].movepos.y = 1.0f ;		// --- 不透明
	}

	if ( g_Object[ OBJID_TITLEOB + e_Title_Nami ].size.x <= 0.5f ){
		g_Object[ OBJID_TITLEOB + e_Title_Nami ].pos = VGet( 2200.0f, -175.0f, 0.0f ) ;				// --- 波
		g_Object[ OBJID_TITLEOB + e_Title_Nami ].movepos = VGet( 0.0f, 0.1f, 0.0f ) ;				// --- 波拡縮値格納
		g_Object[ OBJID_TITLEOB + e_Title_Nami ].size	= VGet( 3.0f, 2.0f, 3.0f ) ;				// --- 波サイズセット
		MV1SetOpacityRate( g_Object[ OBJID_TITLEOB + e_Title_Nami ].modelHandle , 0.1f ) ;			// --- 不透明度セット
	}
	// --- 拡縮値と不透明度セット
	MV1SetOpacityRate( g_Object[ OBJID_TITLEOB + e_Title_Nami ].modelHandle , g_Object[ OBJID_TITLEOB + e_Title_Nami ].movepos.y ) ;			// --- 不透明度セット
	g_Object[ OBJID_TITLEOB + e_Title_Nami ].size.x += g_Object[ OBJID_TITLEOB + e_Title_Nami ].movepos.x ;			
	g_Object[ OBJID_TITLEOB + e_Title_Nami ].size.z += g_Object[ OBJID_TITLEOB + e_Title_Nami ].movepos.z ;

	// -----------------------------------
	//		タイトルアニメーション
	// -----------------------------------
	Object *obPP ;
	for ( int i = OBJID_TITLEOB ; i < OBJID_TITLEOB + 3 ; i++ )
	{
		obPP = &g_Object[ i ] ;			// --- ポインターセット
		if ( obPP->playTime <= obPP->anim[obPP->nowanim].animTotal )
		{
			if ( obPP->nowanim == e_PlayerTitle_Flow )			// --- 浮かぶモーションの再生速度
				obPP->playTime += 0.6f * g_gameSpeed ;
			if ( obPP->nowanim == e_PlayerTitle_Sleep )			// --- 寝るモーションの再生速度
				obPP->playTime += 0.6f * g_gameSpeed ;
			if ( obPP->nowanim == e_PlayerTitle_Jump )			// --- 跳ぶモーションの再生速度
				obPP->playTime += 0.6f * g_gameSpeed ;
		}
		else{
			if ( obPP->nowanim == e_PlayerTitle_Flow )			// --- 浮かぶモーションループさせる
				obPP->playTime = 0.0f ;					
			if ( obPP->nowanim == e_PlayerTitle_Sleep )			// --- 寝るモーションループさせる
				obPP->playTime = 0.0f ;					
			if ( obPP->nowanim == e_PlayerTitle_Jump )			// --- 跳ぶモーションはループさせる
				obPP->playTime = 0.0f ;					
		}
	}		

	return -1 ;
}

// ============================================================================================================================================== 【キャラ選択】
// ---------------------------------------------------------------------------------------------------------------------------
//
//											GameLoop関数_キャラ選択初期化
//
// ---------------------------------------------------------------------------------------------------------------------------
//
//								帰り値 :	-1		- シーンそのまま判定
//											-1以外	- シーンをその定数と同じにする判定
//
// ---------------------------------------------------------------------------------------------------------------------------
int LoopFunc_CharaSelectInit(  )
{
	SetLightDirection( VGet( 0.0f, -1.0f, 1.0f ) ) ;								// --- 光の方向を決定
	SetShadowMapLightDirection( g_shadowMap.handle, VGet( 0.0f, -0.4f, 0.5f ) ) ;	// --- 影の向きをセット
	g_shadowMap.isUse = FALSE ;

	g_camera.pos		= VGet( 0.0f, 300.0f, -800.0f ) ;							// --- 座標セット
	g_camera.lookPos	= VGet( 0.0f, 50.0f, 0.0f ) ;								// --- 注視点セット
	g_camera.upDir		= VGet( 0.0f, 1.0f, 0.0f ) ;								// --- 上方向セット

	// ----------------------------------
	//		 ゲームループ用変数
	// ----------------------------------
	memset( &player_PSInfo.cSel_CharaSetFlg	, FALSE	, sizeof( BOOL[2] ) ) ;			// --- キャラ選択ＯＫフラグetc...
	memset( &player_PSInfo.cSel_StartOKFlg	, FALSE	, sizeof( BOOL[2] ) ) ;			// --- スタートＯＫフラグetc...
	memset( &player_PSInfo.any_number		, 0		, sizeof( int[16] ) ) ;			// --- 現在のモードetc...
	memset( &player_PSInfo.any_select		, 0		, sizeof( int[16] ) ) ;			// --- 現在のOK選択肢etc...
	memset( &player_PSInfo.any_counter		, 0		, sizeof( int[16] ) ) ;			// --- 選択肢の色etc...
	memset( &player_PSInfo.any_flg			, 0		, sizeof( BOOL[16] ) ) ;		// --- カウントダウンフラグetc...

	piP->any_counter[1] = 255 ;						// --- 1P 2P 選択色

	piP->isBlackStart		= TRUE ;			// --- 暗転から開始する
	piP->setBlackCount		= 100 ;					// --- 暗転の現在カウント
	piP->setBlackCount_Max	= 100 ;					// --- 暗転にかかる時間

	piP->cSel_Icon[ PLAYER_ONE ] = 0 ;				// --- 現在カーソルが合わさっているアイコン(1P)
	piP->cSel_Icon[ PLAYER_TWO ] = 2 ;				// --- 現在カーソルが合わさっているアイコン(2P)

	piP->cSel_IconNum = 3 ;							// --- アイコンの数

	for ( int i = 0 ; i < 3 ; i++ )
		piP->cSel_IconState[ i ].changeNum = 2 ;	// --- カーソル移動時のふんわりサイズが大きくなる速度

	piP->cSel_WhiteBox.now		= 0 ;				// --- 白靄数値_現在値
	piP->cSel_WhiteBox.plus		= 8 ;				// --- 白靄数値_加算分数値
	piP->cSel_WhiteBox.max[0]	= 50 ;				// --- 白靄数値_MAX( 使う分 )
	piP->cSel_WhiteBox.max[1]	= 100 ;				// --- 白靄数値_最高到達点
	piP->cSel_WhiteBox.min[0]	= 0 ;				// --- 白靄数値_最低ライン

	// --- 雲の移動量
	piP->cSel_OKToBlackInterval	= 50 ;				// --- 暗転までのインターバル( キャラをこの間に移動させる )

	// --- 雲の移動量
	piP->cSel_CloudMovePow = (float)(rand() % 20) ;	// --- 現在地		

	// ----------------------------------
	//				雲セット
	// ----------------------------------	
	g_Object[ OBJID_CLOUD + 0 ].pos		= VGet( -300.0f, -200.0f, -250.0f ) ;	// --- 座標セット
	g_Object[ OBJID_CLOUD + 1 ].pos		= VGet( 300.0f, -200.0f, -250.0f ) ;	// --- 座標セット

	for ( int i = OBJID_CLOUD ; i < OBJID_CLOUD + 2 ; i++ )
	{
		g_Object[ i ].size		= SetVector( 0.8f ) ;
		g_Object[ i ].dir		= SetVector( 0.0f ) ;
		g_Object[ i ].dispFlg	= TRUE ;

		g_Object[ i ].sizePow.y	;
		g_Object[ i ].dirPow.y	= ((float)(rand() % 50) / 1000.0f) - 0.025f ;
	}

	// ----------------------------------
	//			プレイヤーセット
	// ----------------------------------	
	// --- プレイヤーは雲の上に配置
	VECTOR otDispSpot[2] = {	VGet( g_Object[ OBJID_CLOUD + 0 ].pos.x, g_Object[ OBJID_CLOUD + 0 ].pos.y + 120, g_Object[ OBJID_CLOUD + 0 ].pos.z ),
								VGet( g_Object[ OBJID_CLOUD + 1 ].pos.x, g_Object[ OBJID_CLOUD + 1 ].pos.y + 120, g_Object[ OBJID_CLOUD + 1 ].pos.z ) } ; 
	VECTOR otDispRot[2]	 = { VGet( 0.0f, 3.5f, 0.0f ), VGet( 0.0f, 0.5f, 0.0f ) } ; 

	for ( int i = 0 ; i < 2 ; i++ )
	{
		g_Player[ i ].setModelAndAnim( g_mv1Handle[ piP->cSel_Icon[ i ] ], FALSE, VGet( 0.3f, 0.3f, 0.3f ) ) ;

		g_Player[ i ].nowanim = e_PlayerWait ;
		g_Player[ i ].setAnim( e_PlayerWait ) ;

		g_Player[ i ].pos = otDispSpot[ i ] ;
		g_Player[ i ].dir = otDispRot[ i ] ;
	}

	// ----------------------------------
	//				その他
	// ----------------------------------	
	// --- エフェクトの初期化
	for ( int i = 0 ; i < EFFECT_MAX ; i++ )
		g_effect[ i ].initStates() ;

	// --- 音量セット
	for ( int i = 0 ; i < e_Sound_Total ; i++ )
		StopSoundMem( g_soundHandle[ i ].soundHandle ) ;	// --- 全てのサウンドを止める

	SetAllSoundVolume100() ;								// --- ボリューム 100セット

	// --- セレクトＢＧＭ再生
	SOUND_INFO *otSP = &g_soundHandle[ e_Sound_SelectBGM ] ;
	PlaySoundMem( otSP->soundHandle, otSP->playType ) ;

	// --- 黒くする
	DrawBox( 0, 0, g_winSize.x, g_winSize.y, GetColor( 0x00, 0x00, 0x00 ), TRUE ) ;	

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//				モード切替
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	return e_Mode_CharaSelect ;
}

// ---------------------------------------------------------------------------------------------------------------------------
//
//											GameLoop関数_キャラ選択
//
// ---------------------------------------------------------------------------------------------------------------------------
//
//								帰り値 :	-1		- シーンそのまま判定
//											-1以外	- シーンをその定数と同じにする判定
//
// ---------------------------------------------------------------------------------------------------------------------------
int LoopFunc_CharaSelect(  )
{
	// --- 使用状況 ---	
	//
	//	any_counter[1]	-	選択枠 - セット色
	//	any_counter[2]	-	アイコン枠 - 発光カウント_1P
	//	any_counter[3]	-	アイコン枠 - 発光カウント_2P
	//
	//	any_flg[1]		-	セット色カウントアップフラグ( 0でダウン、1でアップ )
	//	any_flg[2]		-	発光色カウントアップフラグ_1P( 1にしたらacounter[2]が255まで上昇し、0にまた戻る )
	//	any_flg[3]		-	発光色カウントアップフラグ_2P( 1にしたらacounter[3]が255まで上昇し、0にまた戻る )
	//	any_flg[5]		-	選択時のアニメーションが終了したかどうかのフラグ
	//	any_flg[6]		-	選択時のアニメーションが終了したかどうかのフラグ
	//	any_flg[8]		-	二人ともOKを押したときのフラグ
	//
	//	any_number[0]	-	現在のモード( 0でキャラ選択, 1でOK画面 )
	//
	//	any_select[0]	-	OKモード時に選択していル選択肢_プレイヤー１( 0でいいえ、1ではい )
	//	any_select[1]	-	OKモード時に選択していル選択肢_プレイヤー２( 0でいいえ、1ではい )
	//
	// ----------------

	PUSH_STATE	otPad[2] ;

	for ( int i = 0 ; i < 2 ; i++ )
		otPad[ i ] = Padinput.GetButton( i ) ; 

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//	暗転処理時間が終わったらモード切替
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	if ( (piP->setBlackCount >= piP->setBlackCount_Max) && (!piP->isBlackStart) )
	{
		StopSoundMem( g_soundHandle[ e_Sound_SelectBGM ].soundHandle ) ;		// --- サウンド停止
		return e_Mode_Game_Init ;
	}
 
	// ===========================================
	// --- プレイヤーの処理( キャラ選択部 )
	// ===========================================
	if ( piP->any_number[0] == 0 )
	{
		int checkKey_LR1P2P[][4] = { {KEY_INPUT_A, KEY_INPUT_D, KEY_INPUT_X, KEY_INPUT_C},	
										{KEY_INPUT_LEFT, KEY_INPUT_RIGHT, KEY_INPUT_M, KEY_INPUT_N} } ;
		for ( char i = 0 ; i < 2 ; i++ )
		{
			BOOL isMove_Left	= FALSE ;		// --- 左キーの押下フラグ
			BOOL isMove_Right	= FALSE ;		// --- 右キーの押下フラグ
			BOOL isPush_Maru	= FALSE ;		// --- 決定キーの押下フラグ
			BOOL isPush_Batsu	= FALSE ;		// --- 戻るキーの押下フラグ
			BOOL isSizeChange	= FALSE ;		// --- アイコンサイズを変更させるフラグ

			// --- 左キー
			if ( Padinput.Check1FramePressed( i, PAD_INPUT_LEFT ) )
				isMove_Left = TRUE ;
			if ( ((g_keyBuf[ checkKey_LR1P2P[i][0] ] == 1) || ((g_keyBuf[ checkKey_LR1P2P[i][0] ] % 15) == 1)) && (isUseKeyInput) )
				isMove_Left = TRUE ;

			// --- 右キー
			if ( Padinput.Check1FramePressed( i, PAD_INPUT_RIGHT ) )
				isMove_Right = TRUE ;
			if ( ((g_keyBuf[ checkKey_LR1P2P[i][1] ] == 1) || ((g_keyBuf[ checkKey_LR1P2P[i][1] ] % 15) == 1)) && (isUseKeyInput) )
				isMove_Right = TRUE ;

			// --- 選択( 〇 )
			if ( Padinput.Check1FramePressed( i, PAD_INPUT_B ) )
				isPush_Maru = TRUE ;
			if ( (g_keyBuf[ checkKey_LR1P2P[i][2] ] == 1) && (isUseKeyInput) )
				isPush_Maru = TRUE ;

			// --- 非選択(×)
			if ( Padinput.Check1FramePressed( i, PAD_INPUT_C ) )
				isPush_Batsu = TRUE ;
			if ( (g_keyBuf[ checkKey_LR1P2P[i][3] ] == 1) && (isUseKeyInput) )
				isPush_Batsu = TRUE ;

			// ------------------------
			// --- 左、右キーの処理
			c_ImgInfo *otSetP ;
			if ( (isMove_Left) && (!piP->cSel_CharaSetFlg[ i ]) )			// --- 左移動処理
			{
				if ( piP->cSel_Icon[ i ] >= 1 )
					piP->cSel_Icon[ i ]-- ;
				else
					piP->cSel_Icon[ i ] = piP->cSel_IconNum - 1 ;

				isSizeChange = TRUE ;

				// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
				// --- プレイヤーの見た目を変化
				// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
				int otSelNum = piP->cSel_Icon[ i ] + (i * 3) ;
				g_Player[ i ].setModelAndAnim( g_mv1Handle[ otSelNum ], TRUE, VGet( 0.3f, 0.3f, 0.3f ) ) ;
				g_Player[ i ].setAnim( e_PlayerWait ) ;

				SOUND_INFO *otSP = &g_soundHandle[ e_Sound_CursolSE ] ;
				PlaySoundMem( otSP->soundHandle, otSP->playType ) ;			// --- 効果音再生
			}

			else if ( (isMove_Right) && (!piP->cSel_CharaSetFlg[ i ]) )	// --- 右移動処理
			{
				if ( piP->cSel_Icon[ i ] < piP->cSel_IconNum - 1 )
					piP->cSel_Icon[ i ]++ ;
				else
					piP->cSel_Icon[ i ] = 0 ;

				isSizeChange = TRUE ;

				// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
				// --- プレイヤーの見た目を変化
				// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
				int otSelNum = piP->cSel_Icon[ i ] + (i * 3) ;
				g_Player[ i ].setModelAndAnim( g_mv1Handle[ otSelNum ], TRUE, VGet( 0.3f, 0.3f, 0.3f ) ) ;
				g_Player[ i ].setAnim( e_PlayerWait ) ;

				SOUND_INFO *otSP = &g_soundHandle[ e_Sound_CursolSE ] ;
				PlaySoundMem( otSP->soundHandle, otSP->playType ) ;			// --- 効果音再生
			}

			// ------------------------
			// --- 決定プッシュ処理
			if ( (isPush_Maru) && (!piP->cSel_CharaSetFlg[ i ]) )
			{
				SOUND_INFO *otSP = &g_soundHandle[ e_Sound_TitlePushSE ] ;
				PlaySoundMem( otSP->soundHandle, otSP->playType ) ;			// --- 効果音再生

				piP->cSel_CharaSetFlg[ i ] = TRUE ;		// --- セットフラグをTRUE
				piP->any_flg[ 2 + i ] = TRUE ;			// --- 発光開始

				g_Player[ i ].nowanim = e_PlayerSelectAction ;
				g_Player[ i ].setAnim( e_PlayerSelectAction ) ;
				g_Player[ i ].playTime = 0.0f ;

				isSizeChange = TRUE ;
			}

			// ------------------------
			// --- プッシュ処理_戻し
			if ( (isPush_Batsu) && (piP->cSel_CharaSetFlg[ i ]) )
			{
				SOUND_INFO *otSP = &g_soundHandle[ e_Sound_TitlePushSE ] ;
				PlaySoundMem( otSP->soundHandle, otSP->playType ) ;			// --- 効果音再生

				piP->cSel_CharaSetFlg[ i ]	= 0 ;
				piP->cSel_StartOKFlg[ i ]	= 0 ;
				piP->any_counter[ 2 + i ]	= 0 ;
				piP->any_flg[ 2 + i ]		= FALSE ;

				piP->any_flg[ 5 + i ]		= FALSE ;
			}

			// ------------------------
			// --- サイズ変更処理
			if ( isSizeChange )
			{
				otSetP = &player_PSInfo.cSel_IconState[ piP->cSel_Icon[ i ] ] ;

				otSetP->changeNum = abs( otSetP->changeNum ) ;	// --- 加算数値は常に正の値
				otSetP->sizeChange.x	= 100 ;					// --- サイズを通常のものにする
				otSetP->sizeChange.y	= 100 ;					// --- サイズを通常のものにする
				otSetP->isSizeChange	= TRUE ;				// --- サイズ変更フラグをONにする
			}
		}

		// --- ２人とも選択ＯＫであればモードを変更( OK? を出す )
		if ( (piP->cSel_CharaSetFlg[ 0 ] && piP->cSel_CharaSetFlg[ 1 ]) &&
				(piP->any_flg[5] && piP->any_flg[6]) )
		{
			piP->any_select[0] = 0 ;		// --- 現在のOK選択肢_P1
			piP->any_select[1] = 0 ;		// --- 現在のOK選択肢_P2
			piP->any_number[0] = 1 ;		// --- モード( キャラ選択、OK )
		}
	}

	// ===========================================
	// --- プレイヤーの処理( ＯＫ選択部 )
	// ===========================================
	if ( piP->any_number[0] == 1 )
	{
		//								- 1P -										- 2P -
		int checkKey_UD1P2P[][3] = { {KEY_INPUT_W, KEY_INPUT_S, KEY_INPUT_X},	{KEY_INPUT_UP, KEY_INPUT_DOWN, KEY_INPUT_M} } ;
		for ( int i = 0 ; i < 2 ; i++ )
		{
			BOOL isMove_Up		= FALSE ;
			BOOL isMove_Down	= FALSE ;

			if ( piP->cSel_StartOKFlg[ i ] )
				continue ;

			// 上キー
			if ( Padinput.Check1FramePressed( i, PAD_INPUT_UP ) ||
				g_keyBuf[ checkKey_UD1P2P[ i ][0] ] == 1 )
			{
				isMove_Up = TRUE ;

				SOUND_INFO *otSP = &g_soundHandle[ e_Sound_CursolSE ] ;
				PlaySoundMem( otSP->soundHandle, otSP->playType ) ;			// --- 効果音再生
			}

			// 下キー
			if ( Padinput.Check1FramePressed( i, PAD_INPUT_DOWN ) ||
				g_keyBuf[ checkKey_UD1P2P[ i ][1] ] == 1 )
			{
				isMove_Down = TRUE ;

				SOUND_INFO *otSP = &g_soundHandle[ e_Sound_CursolSE ] ;
				PlaySoundMem( otSP->soundHandle, otSP->playType ) ;			// --- 効果音再生
			}

			// 選択
			if ( (Padinput.Check1FramePressed( i, PAD_INPUT_B ) ||
				g_keyBuf[ checkKey_UD1P2P[ i ][2] ] == 1) && (!piP->cSel_StartOKFlg[ i ]) )
			{
				SOUND_INFO *otSP = &g_soundHandle[ e_Sound_TitlePushSE ] ;
				PlaySoundMem( otSP->soundHandle, otSP->playType ) ;			// --- 効果音再生

				piP->cSel_StartOKFlg[ i ] = TRUE ;	
			}

			// --- 最後に全ての処理を行う( 入れ替えのみ )
			if ( isMove_Up || isMove_Down )
				piP->any_select[ i ] ^= 1 ;
		}

		// --- 2つとも「はい」でスタート開始
		if ( (piP->cSel_StartOKFlg[0] && piP->any_select[0] == 1) && (piP->cSel_StartOKFlg[1] && piP->any_select[1] == 1) &&
				(!piP->any_flg[8]) )
		{
			piP->any_flg[8] = TRUE ;			// --- ２人ともOKプッシュフラグ
			piP->cSel_CloudMovePow = -8.0f ;	// --- 雲の初速セット
		}

		// --- 片方でも「いいえ」だったらキャラ選択状態に戻す
		if ( (piP->cSel_StartOKFlg[0] && piP->any_select[0] == 0) || (piP->cSel_StartOKFlg[1] && piP->any_select[1] == 0) )
		{
			for ( int i = 0 ; i < 2 ; i++ )
			{
				piP->cSel_CharaSetFlg[ i ]	= 0 ;
				piP->cSel_StartOKFlg[ i ]	= 0 ;
				piP->any_counter[ 2 + i ]	= 0 ;
				piP->any_flg[ 2 + i ]		= FALSE ;

				piP->any_flg[ 5 + i ]		= FALSE ;
			}

			piP->any_number[0] = 0 ;
		}

		// --- MINまでカウントを下げる。これは選択画面を全体的に白くする為に使用するよ
		CheckLimitAndPlus<float>( CLAPMODE_BELOW, &piP->cSel_WhiteBox.now, piP->cSel_WhiteBox.plus, piP->cSel_WhiteBox.max[0], piP->cSel_WhiteBox.max[0] ) ;
	}
	else if ( piP->cSel_WhiteBox.now != piP->cSel_WhiteBox.min[0] )
		// --- MAXまでカウントを上げる。
		CheckLimitAndPlus<float>( CLAPMODE_EXCEED, &piP->cSel_WhiteBox.now, -piP->cSel_WhiteBox.plus, piP->cSel_WhiteBox.min[0], piP->cSel_WhiteBox.min[0] ) ;

	// ===========================================
	// --- その他・・・
	// ===========================================
	// --- 暗転までのインターバル
	if ( (piP->any_flg[8]) && (!piP->cSel_IsBlackStart) )
	{
		piP->cSel_OKToBlackInterval-- ;

		// --- リミット到達で暗転開始
		if ( piP->cSel_OKToBlackInterval == 0 )
			piP->cSel_IsBlackStart = TRUE ;
	}

	// --- 暗転処理
	if ( piP->cSel_IsBlackStart )
	{
		piP->setBlackCount++ ;

		// --- 音量をフェードアウトに合わせて下げる
		SetSoundVolumePlusNum( -1, &g_soundHandle[ e_Sound_SelectBGM ] ) ;
	}

	// --- 開始時の暗転処理
	if ( piP->isBlackStart )
	{
		piP->setBlackCount -= 2 ;

		if ( piP->setBlackCount <= 0 )
			piP->isBlackStart = FALSE ;
	}

	// --------------------------
	// --- 選択肢色変更処理
	if ( !piP->any_flg[1] )
	{
		piP->any_counter[1] -= 2 ;
		if ( piP->any_counter[1] <= 200 )
		{
			piP->any_counter[1] = 200 ;
			piP->any_flg[1] = TRUE ;		// --- カウント"アップ"に移行
		}
	}
	else
	{
		piP->any_counter[1] += 2 ;
		if ( piP->any_counter[1] >= 255 )
		{
			piP->any_counter[1] = 255 ;
			piP->any_flg[1] = FALSE ;		// --- カウント"ダウン"に移行
		}
	}

	// --------------------------
	// --- アイコン部分変更処理
	// --- 発光色
	int changeNum = 30 ;				// --- これは上昇数値分
	for ( int i = 0 ; i < 2 ; i++ )
	{
		int nowI = 2 + i ;

		// --- 数値上昇中の処理
		if ( piP->any_flg[ nowI ] )
		{
			piP->any_counter[ nowI ] += changeNum ;
			if ( piP->any_counter[ nowI ] >= 255 )
			{
				piP->any_counter[ nowI ] = 255 ;
				piP->any_flg[ nowI ] = FALSE ;
			}
		}
		else if ( piP->any_counter[ nowI ] != 0 )
		{
			piP->any_counter[ nowI ] -= changeNum ;
			if ( piP->any_counter[ nowI ] <= 0 )
				piP->any_counter[ nowI ] = 0 ;
		}
	}

	// --- サイズ
	c_ImgInfo *otIP = &piP->cSel_IconState[ 0 ] ;
	for ( int i = 0 ; i < 3 ; i++ )
	{
		if ( otIP->isSizeChange )
		{
			// --- 1.15倍のサイズに達したら、選択肢から外れるまでそのサイズにする
			if ( otIP->sizeChange.x >= 115 )
				otIP->isSizeChange = FALSE ;

			// --- サイズをセットする( サイズ( 100 が通常 ) に変更分数値を加えている )
			otIP->sizeChange.x += otIP->changeNum ;
			otIP->sizeChange.y += otIP->changeNum ;
		}

		// --- 自身が誰にも選択されていないことを確認
		BOOL isSelCheck = FALSE ;
		for ( int j = 0 ; j < 2 ; j++ )
		{
			if ( piP->cSel_Icon[ j ] == i )
				isSelCheck = TRUE ;
		}

		// --- もし未選択だったらサイズを元に戻す
		if ( !isSelCheck )
		{
			otIP->sizeChange.x = 100 ;
			otIP->sizeChange.y = 100 ;
			otIP->isSizeChange = FALSE ;
		}

		otIP++ ;
	}

	// ===========================================
	//				雲アクション
	// ===========================================
	float plusNum ;

	for ( int i = OBJID_CLOUD ; i < OBJID_CLOUD + 2 ; i++ )
	{		
		// --- ２人ともOKを押していない状態
		if ( !piP->any_flg[8] )
		{
			// --- 下の二行でふわふわさせる！
			piP->cSel_CloudMovePow += 0.1f ;	// --- 速度をセット
			plusNum	= (float)sin( piP->cSel_CloudMovePow ) ;

			g_Object[ i ].dir.y += g_Object[ i ].dirPow.y ;
			g_Object[ i ].pos.y	+= plusNum ;
		}
		// --- OK!( 雲を上昇させる )
		else
		{
			g_Object[ i ].dir.y += g_Object[ i ].dirPow.y ;

			// --- 左の雲は左に
			if ( i == OBJID_CLOUD )
				g_Object[ i ].pos.x	+= -5.0f ;
			// --- 右の雲は右に
			else
				g_Object[ i ].pos.x	+= 5.0f ;

			g_Object[ i ].pos.y	+= piP->cSel_CloudMovePow ;
			g_Object[ i ].pos.z	+= -2.0f ;

			piP->cSel_CloudMovePow += 0.5f ;
		}
	}

	// ===========================================
	//			プレイヤーアクション
	// ===========================================
	Player	*otPP ;
	for ( int i = 0 ; i < 2 ; i++ )
	{
		// --- キャラ
		otPP = &g_Player[ i ] ;

		// --- 座標を雲と合わせる
		otPP->pos = VGet( g_Object[ OBJID_CLOUD + i ].pos.x, g_Object[ OBJID_CLOUD + i ].pos.y + 120, g_Object[ OBJID_CLOUD + i ].pos.z ) ;

		// --- 移動開始していた場合、dirをいじるよ( 前を向かせる )
		if ( piP->any_flg[8] )
		{
			float plusDir = 0.02f ;

			// --- 1Pの回転
			if ( (i == PLAYER_ONE) && (otPP->dir.y + plusDir < 4.0f) )
				otPP->dir.y += plusDir ;
			else if ( i == PLAYER_ONE )
				otPP->dir.y = 4.0f ;

			// --- 2Pの回転
			if ( (i == PLAYER_TWO) && (otPP->dir.y - plusDir > 0.0f) )
				otPP->dir.y -= plusDir ;
			else if ( i == PLAYER_TWO )
				otPP->dir.y = 0.0f ;

		}

		// --- セレクト待機アニメーション
		if ( piP->cSel_CharaSetFlg[ i ] != TRUE )
		{
			if ( otPP->nowanim != e_PlayerWait )
			{
				otPP->nowanim = e_PlayerWait ;
				otPP->setAnim( e_PlayerWait ) ;
			}
		}

		// --- アニメーション継続の場合
		if ( otPP->playTime <= otPP->anim[ otPP->nowanim ].animTotal )
		{
			if ( otPP->nowanim == e_PlayerWait )			// --- 待機モーションの再生速度
				otPP->playTime += 1.0f * g_gameSpeed ;
			if ( otPP->nowanim == e_PlayerSelectAction )	// --- 選ばれモーションは少し遅め！
				otPP->playTime += 0.5f * g_gameSpeed ;
		}
		// --- アニメーションが終了した場合
		else
		{
			if ( otPP->nowanim == e_PlayerWait )			// --- 待機モーションはループさせる
				otPP->playTime = 0.0f ;					
			if ( otPP->nowanim == e_PlayerSelectAction )	// --- 選ばれモーションは一度のみ
			{
				otPP->nowanim = e_PlayerWait ;		
				otPP->setAnim( e_PlayerWait ) ;

				otPP->playTime = 0.0f ;

				// --- フラグをTRUEにする(２人終了した時点でOK処理に移行させる)
				piP->any_flg[ 5 + i ] = TRUE ;
			}
		}
	}

	return -1 ;
}

// ============================================================================================================================================== 【メインのゲーム部】
// ---------------------------------------------------------------------------------------------------------------------------
//
//											GameLoop関数_ゲーム初期化
//
// ---------------------------------------------------------------------------------------------------------------------------
//
//							帰り値 :	-1		- シーンそのまま判定
//										-1以外	- シーンをその定数と同じにする判定
//
// ---------------------------------------------------------------------------------------------------------------------------
int LoopFunc_GameInit(  )
{
	SetLightDirection( VGet( 0.0f, -1.0f, 1.0f ) ) ;									// --- 光の方向を決定
	SetShadowMapLightDirection( g_shadowMap.handle, VGet( 0.0f, -0.4f, 0.5f ) ) ;		// --- 影の向きをセット
	g_shadowMap.isUse = TRUE ;

	// --- キャラ選択を潜り抜けた証拠があれば、キャラの情報を引き継ぐ
	int otNum[2] = { piP->cSel_Icon[0], piP->cSel_Icon[1] + 3 } ;

	g_Player[0].selmodel = otNum[0] ;
	g_Player[1].selmodel = otNum[1] ;

	g_Player[0].setModelAndAnim( g_mv1Handle[ otNum[0] ], FALSE, VGet( 0.3f, 0.3f, 0.3f ) ) ;		// --- プレイヤー情報_1P
	g_Player[1].setModelAndAnim( g_mv1Handle[ otNum[1] ], FALSE, VGet( 0.3f, 0.3f, 0.3f ) ) ;		// --- プレイヤー情報_2P

	// --- 構造体初期化
	SecureZeroMemory( &player_PSInfo, sizeof( S_PLAYER_INPUTINFO ) ) ;

	for ( int i = 0 ; i < 2 ; i++ )
		MV1SetupCollInfo( g_Player[i].modelHandle ) ;	// --- プレイヤーの衝突セットアップ

	// -------------------------------------
	//			プレイヤー関連
	// -------------------------------------
	for ( int i = 0 ; i < 2 ; i++ )
	{
		g_Player[ i ].dispFlg  = TRUE ;
		g_Player[ i ].playTime = 0.0f ;
		g_Player[ i ].setAnim( e_PlayerWait ) ;
		g_Player[ i ].dir.y = e_Down ;
		g_Player[ i ].moveInfo.moveSpeed = SetVector( 0.0f ) ;

		g_Player[ i ].dotscore	= 0 ;
		g_Player[ i ].pushscore = 0 ;
		g_Player[ i ].dotcharge = 0 ;
		
		g_Player[ i ].atkInfo.nowTime	= 0 ; 
		g_Player[ i ].atkInfo.isValid	= FALSE ;
		g_Player[ i ].atkInfo.isUse		= FALSE ;
	}

	// -------------------------------------
	//	 使用オブジェクトセット( ゲーム中 )
	// -------------------------------------
	VECTOR randPos ;

	// --- モデルセット
	for ( int i = OBJID_GAMEOB ; i < OBJID_GAMEOB + GAMEOB_MAX ; i++ )
		g_Object[ i ].setModelAndAnim( g_mv1Handle[ e_Model_Cloud ],	TRUE,	VGet( 0.5f, 0.5f, 0.5f )  ) ;		// --- 雲

	// --- 座標セット
	for ( int i = OBJID_GAMEOB ; i < OBJID_GAMEOB + GAMEOB_MAX ; i++ )
	{	
		randPos.x = (float)(rand() % 8000) - 4200.0f ;				// --- ランダム地点_X
		randPos.z = (float)(rand() % 6000) - 3000.0f ;				// --- ランダム地点_Y
		randPos.y = (float)(rand() % 600) - 500.0f - 600.0f ;		// --- ランダム地点_Z

		g_Object[ i ].pos = VGet( randPos.x, randPos.y, randPos.z ) ;
	}

	// --- 速度セット
	for ( int i = OBJID_GAMEOB ; i < OBJID_GAMEOB + GAMEOB_MAX ; i++ )
		g_Object[ i ].movepos.x = (float)(rand() % 100) / 100.0f * 3 ;

	// --- サイズセット
	for ( int i = OBJID_GAMEOB ; i < OBJID_GAMEOB + GAMEOB_MAX ; i++ )
		g_Object[ i ].size = VGet( 1.0f, 1.0f, 1.0f ) ;

	// -------------------------------------
	//			  登場演出関連
	// -------------------------------------
	VECTOR otCloudStartPos		= VGet( 0.0f, 2200.0f, 2000.0f ) ;		// --- 雲の共通スタート地点
	VECTOR otCloudStartPower	= VGet( 0.0f, -120.0f, -100.0f ) ;		// --- 雲の共通初速
	VECTOR otPlayStartPower		= VGet( 0.0f, 20.0f,	0.0f ) ;		// --- プレイヤーの共通初速

	g_Object[ OBJID_CLOUD + 0 ].pos		= VGet( 300.0f,		otCloudStartPos.y,	otCloudStartPos.z ) ;	// --- 座標セット
	g_Object[ OBJID_CLOUD + 1 ].pos		= VGet( 1500.0f,	otCloudStartPos.y,	otCloudStartPos.z ) ;	// --- 座標セット

	for ( int i = OBJID_CLOUD ; i < OBJID_CLOUD + 2 ; i++ )
	{
		g_Object[ i ].size		= SetVector( 1.0f ) ;
		g_Object[ i ].dir		= SetVector( 0.0f ) ;

		g_Object[ i ].dispFlg	= TRUE ;

		g_Object[ i ].dirPow.y	= ((float)(rand() % 100) / 1000.0f) - 0.05f ;		// --- 回転速度セット
	}

	piP->game_MovePower_Cloud[0]		= otCloudStartPower ;			// --- 初速セット( Y移動量のみ加算してきます )
	piP->game_MovePower_Cloud[1]		= otCloudStartPower ;			// --- 初速セット( Y移動量のみ加算してきます )

	g_Player[0].pos		= VGet( g_Object[ OBJID_CLOUD + 0 ].pos.x,	g_Object[ OBJID_CLOUD + 0 ].pos.y + 120.0f,	g_Object[ OBJID_CLOUD + 0 ].pos.z ) ;
	g_Player[1].pos		= VGet( g_Object[ OBJID_CLOUD + 1 ].pos.x,	g_Object[ OBJID_CLOUD + 1 ].pos.y + 120.0f,	g_Object[ OBJID_CLOUD + 1 ].pos.z ) ;
	g_Player[0].size	= SetVector( 1.0f ) ;
	g_Player[1].size	= SetVector( 1.0f ) ;

	g_Player[0].nowanim = e_PlayerWait ;
	g_Player[0].setAnim( e_PlayerWait ) ;
	g_Player[0].playTime = 0.0f ;

	g_Player[1].nowanim = e_PlayerWait ;
	g_Player[1].setAnim( e_PlayerWait ) ;
	g_Player[1].playTime = 0.0f ;

	piP->game_pFallPos[0] = VGet( 300.0f,	-150.0f, 0.0f ) ;				// --- プレイヤーが落下したい地点
	piP->game_pFallPos[1] = VGet( 1500.0f,	-150.0f, 0.0f ) ;				// --- プレイヤーが落下したい地点

	piP->game_MovePower_Player[0] = otPlayStartPower ;						// --- プレイヤーの初動_Y
	piP->game_MovePower_Player[1] = otPlayStartPower ;						// --- プレイヤーの初動_Y

	// -------------------------------------
	//				足場関連
	// -------------------------------------
	Map.f_move = 0 ;
	for ( int i = 0 ; i < ELE_X_1 * ELE_Y_1 ; i++ )
		Scaf1[ i ].f_move = 0 ;
	for ( int i = 0 ; i < ELE_X_2 * ELE_Y_2 ; i++ )
	{
		Scaf2_t[ i ].f_move = 0 ;
		Scaf2_u[ i ].f_move = 0 ;
	}
	for ( int i = 0 ; i < ELE_X_3 * ELE_Y_3 ; i++ )
		Scaf3[ i ].f_move = 0 ;

	Map.HideMap( e_stage2_top ) ;
	Map.HideMap( e_stage2_under ) ;
	Map.HideMap( e_stage3 ) ;

	// --- GameLoop用の変数初期化
	piP->game_IsCountStart			= FALSE ;	// --- カウントダウンを開始させるフラグ

	piP->game_FinishDisp.nowTime	= 0 ;		// --- 90秒経過後の経過フレーム
	piP->game_FinishDisp.totalTime	= 200 ;		// --- 「終了」を表示する長さ
	piP->game_FinishDisp.inTime		= 15 ;		// --- totalTimeの内、フェードインに使用する長さ
	piP->game_FinishDisp.outTime	= 60 ;		// --- totalTimeの内、フェードアウトに使用する長さ

	piP->game_ChangeResTime			= 220 ;		// --- 90秒経過後、リザルトに移るまでの時間

	piP->any_flg[0]		= 0 ;				// --- 1P、2Pのジャンプ完了フラグ	( 0x01 - 1P, 0x02 - 2P )
	piP->any_flg[1]		= 0 ;				// --- 1P、2Pの着地完了フラグ		( 0x01 - 1P, 0x02 - 2P )
	piP->any_counter[0]	= 40 ;				// --- 着地完了してからのインターバル

	piP->isBlackStart		= TRUE ;			// --- 暗転から開始する
	piP->setBlackCount		= 100 ;				// --- 暗転スタート値
	piP->setBlackCount_Max	= 100 ;				// --- 暗転MAX値

	// -------------------------------------
	//				その他
	// -------------------------------------
	for ( int i = 0 ; i < EFFECT_MAX ; i++ )
		g_effect[ i ].initStates() ;

	UI_SChange.initStates() ;

	// --- 音量セット
	for ( int i = 0 ; i < e_Sound_Total ; i++ )
		StopSoundMem( g_soundHandle[ i ].soundHandle ) ;	// --- 全てのサウンドを止める

	SetAllSoundVolume100() ;								// --- ボリューム 100セット

	// --- バトルＢＧＭ再生
	SOUND_INFO *otSP = &g_soundHandle[ e_Sound_BattleBGM ] ;
	PlaySoundMem( otSP->soundHandle, otSP->playType ) ;

	g_gameSpeed = 1.0f ;									// --- ゲームスピード初期化

	// --- 黒くする
	DrawBox( 0, 0, g_winSize.x, g_winSize.y, GetColor( 0x00, 0x00, 0x00 ), TRUE ) ;	

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//				モード切替
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	return e_Mode_Game_Start ;
}

// ---------------------------------------------------------------------------------------------------------------------------
//
//											GameLoop関数_ゲームスタート
//
// ---------------------------------------------------------------------------------------------------------------------------
//
//								帰り値 :	-1		- シーンそのまま判定
//											-1以外	- シーンをその定数と同じにする判定
//
// ---------------------------------------------------------------------------------------------------------------------------
int LoopFunc_GameStart(  )
{
	// --- 使用状況 ---	
	//
	//	any_counter[0]	-	着地完了後のインターバル
	//
	//	any_flg[0]		-	1P、2Pのジャンプ完了フラグ	( 0x01 - 1P, 0x02 - 2P )
	//	any_flg[1]		-	1P、2Pの着地完了フラグ		( 0x01 - 1P, 0x02 - 2P )
	//	any_number[0]	-	
	//
	//	any_select[0]	-	
	//
	// ----------------
	SOUND_INFO *otSP ;		// --- サウンド用

	// -------------------------------------
	//			オブジェクト処理
	// -------------------------------------
	for ( int i = OBJID_GAMEOB ; i < OBJID_GAMEOB + GAMEOB_MAX ; i++ )
		g_Object[ i ].pos.x += g_Object[ i ].movepos.x ;		// --- 雲を動かす

	// -------------------------------------
	//				暗転処理
	// -------------------------------------
	// --- 開始時の暗転処理
	if ( piP->isBlackStart )
	{
		piP->setBlackCount -= 5 ;

		if ( piP->setBlackCount <= 0 )
			piP->isBlackStart = FALSE ;
	}

	// --- この処理が終わらないと次の処理にはいかせない
	if ( piP->isBlackStart )
		return -1 ;

	// -------------------------------------
	//
	//			  スタート時演出
	//
	// -------------------------------------
	if ( !piP->game_IsCountStart )
	{
		for ( int i = PLAYER_ONE ; i <= PLAYER_TWO ; i++ )
		{
			otPP = &g_Player[ i ] ;	// --- ポインタセット

			// -------------------------------------------
			//			キャラたちのアクション
			// -------------------------------------------

			// --- 1Pがジャンプ完了していなかったらスルー
			if ( (i == PLAYER_TWO) && ((piP->any_flg[0] & 0x01) != 0x01) )
				continue ;

			// --- 雲移動
			g_Object[ OBJID_CLOUD + i ].pos = VAdd( g_Object[ OBJID_CLOUD + i ].pos, piP->game_MovePower_Cloud[ i ] ) ;
			piP->game_MovePower_Cloud[ i ].y += 5.0f ;

			g_Object[ OBJID_CLOUD + i ].dir.y += g_Object[ OBJID_CLOUD + i ].dirPow.y ;		// --- 回転

			// --- 着地が完了していたら次のキャラを見る
			if ( (piP->any_flg[1] & (i + 1)) == i + 1 )
				continue ;		

			// --- プレイヤーの座標を雲の上にする
			if ( otPP->nowanim == e_PlayerWait )
			{
				// --- 目標地点を超過しない場合のみ移動
				if ( otPP->pos.z + g_Object[ OBJID_CLOUD + i ].pos.z > piP->game_pFallPos[ i ].z )
					otPP->pos = VGet( g_Object[ OBJID_CLOUD + i ].pos.x, g_Object[ OBJID_CLOUD + i ].pos.y + 120.0f, g_Object[ OBJID_CLOUD + i ].pos.z ) ;
				else
				{
					// --- ジャンプ完了フラグをTRUEにする
					piP->any_flg[0] |= i + 1 ;

					otPP->nowanim = e_PlayerJump_In ;
					otPP->setAnim( e_PlayerJump_In ) ;
				}
			}
			// --- ジャンプ処理だった場合
			else
			{
				VECTOR moveRet = piP->game_MovePower_Player[ i ] ;		// --- 移動量

				if ( piP->game_MovePower_Player[ i ].y >= -60.0f )
					piP->game_MovePower_Player[ i ].y -= 4.0f ;				// --- 重力値を与える

				// --- 落下処理( ヒットチェックはしていない )
				if ( otPP->pos.y + moveRet.y <= piP->game_pFallPos[ i ].y )
				{
					// --- 着地完了でフラグを立てる
					moveRet.y = piP->game_pFallPos[ i ].y - otPP->pos.y ;
					piP->any_flg[1] |= i + 1 ;

					otPP->nowanim = e_PlayerJump_Out ;				// --- アニメセット
					otPP->setAnim( e_PlayerJump_Out ) ;				// --- アニメセット

					otSP = &g_soundHandle[ e_Sound_ChakuchiSE ] ;
					PlaySoundMem( otSP->soundHandle, otSP->playType ) ;			// --- 効果音再生
				}

				otPP->pos	= VAdd( otPP->pos, moveRet ) ;
			}
		}

		// --- 2人とも着地完了でインターバル
		if ( (piP->any_flg[1] & 0x03) == 0x03 )
		{
			piP->any_counter[0]-- ;

			if ( piP->any_counter[0] == 0 )
				piP->game_IsCountStart = TRUE ;
		}
	}

	// -------------------------------------
	//			カウントダウン開始
	// -------------------------------------
	else
	{
		// --- カウントダウン処理が終了したら次のシーンへ
		if ( TRUE == UI_CntDown.CountAction() )
		{
			g_Player[ PLAYER_ONE ].dir = SetVector( 0.0f ) ;			// --- 回転セット
			g_Player[ PLAYER_TWO ].dir = SetVector( 0.0f ) ;			// --- 回転セット

			// --- タイマー初期化
			for ( int i = 0 ; i < 3 ; i++ )
				Time[ i ].ResetTimer() ;

			// ~-~-~-~-~-~-~-~-~-~-
			//		デバッグ用
			// ~-~-~-~-~-~-~-~-~-~-
			if ( g_debInfo.mapSwitch <= 1 )	// --- 通常プレイもここを経由する
			{
				g_debInfo.mapSwitch = 0 ;
				Map.mapswitch	= 1 ;
				Map.mapcount	= 0 ;
				TimeUI[0].time	= 90 ;
				TimeUI[1].time	= 90 ;
				Time[2].m_timer = 0 ;
			}
			if ( g_debInfo.mapSwitch == 2 )	// --- デバッグ限定。第二ステージから始める為の準備
			{
				g_debInfo.mapSwitch = 0 ;
				Map.mapswitch	= 2 ;
				Map.mapcount	= 29 ;
				TimeUI[0].time	= 90 - 29 ;
				TimeUI[1].time	= 90 - 29 ;
				Time[2].m_timer = 29 ;
			}
			if ( g_debInfo.mapSwitch == 3 )	// --- デバッグ限定。第三ステージから始める為の準備
			{
				g_debInfo.mapSwitch = 0 ;
				Map.mapswitch	= 3 ;
				Map.mapcount	= 58 ;
				TimeUI[0].time	= 90 - 58 ;
				TimeUI[1].time	= 90 - 58 ;
				Time[2].m_timer = 58 ;
			}

			return e_Mode_Game ;		// モード切替
		}
	}

	// ==============================================
	//	プレイヤーのアクション( 関数化すべきか？ )
	// ==============================================
	for ( int i = PLAYER_ONE ; i <= PLAYER_TWO ; i++ )
	{
		otPP = &g_Player[ i ] ;

		if ( otPP->playTime < otPP->anim[ otPP->nowanim	].animTotal )
			otPP->playTime += 0.4f * g_gameSpeed ;
		else if ( otPP->nowanim == e_PlayerJump_In )	// --- 敗北エモート
		{
			otPP->nowanim = e_PlayerJump_Loop ;
			otPP->setAnim( e_PlayerJump_Loop ) ;
			otPP->playTime = 0.0f ;
		}
		else if ( otPP->nowanim == e_PlayerJump_Out )	// --- 勝利エモート
		{
			otPP->nowanim = e_PlayerWait ;
			otPP->setAnim( e_PlayerWait ) ;
			otPP->playTime = 0.0f ;
		}
		else												// --- その他継続モーション
			otPP->playTime = 0.0f ;	
	}	

	return -1 ;
}

// ---------------------------------------------------------------------------------------------------------------------------
//
//											GameLoop関数_ゲーム
//
// ---------------------------------------------------------------------------------------------------------------------------
//
//							帰り値 :	-1		- シーンそのまま判定
//										-1以外	- シーンをその定数と同じにする判定
//
// ---------------------------------------------------------------------------------------------------------------------------
int LoopFunc_Game(  )
{
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//	  モード切替( 終了の文字が出たら )
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	if ( 90 <= Time[ e_Timer3 ].CountSecond( ) )		// --- 90秒経過でタイムアップ処理
	{
		s_FinInfo *otFP = &piP->game_FinishDisp ;

		if ( !piP->game_IsFin )
		{
			piP->game_IsFin = TRUE ;		// --- 終了フラグをTRUEにする( 一度のみ )

			// --- 音量セット
			SetAllSoundVolume100() ;								// --- ボリューム 100セット

			// --- ホイッスル再生
			SOUND_INFO *otSP = &g_soundHandle[ e_Sound_TimeOutSE ] ;// --- ホイッスル
			PlaySoundMem( otSP->soundHandle, otSP->playType ) ;
		}

		otFP->nowTime++ ;					// --- 経過時間加算

		if ( otFP->nowTime <= otFP->totalTime )
			g_gameSpeed = 0.2f ;
		else
		{
			SetSoundVolumePlusNum( -1, &g_soundHandle[ e_Sound_BattleBGM ] ) ;
			g_gameSpeed = 0.05f ;
		}

		// --- 経過時間がリザルトに移行する長さに達したらモード変更( シーンチェンジも含めて )
		if ( otFP->nowTime >= piP->game_ChangeResTime )
		{
			if ( UI_SChange.UIAction( 0 ) )
			return e_Mode_Result_Init ;
		}
	}

	// ---------------------------------------------
	//			オブジェクトのアクション
	// ---------------------------------------------
	Scaf1[0].MapSelect() ;				// マップ選択
	Dot1stg[0].DotMapSelect() ;			// マップ選択

	// --- 1ステージ目のみアクションを行う
	if ( Map.mapswitch == 1 )
	{
		for ( int i = OBJID_GAMEOB ; i < OBJID_GAMEOB + GAMEOB_MAX ; i++ )
			g_Object[ i ].pos.x += g_Object[ i ].movepos.x ;		// --- 雲を動かす
	}

	// ---------------------------------------------
	//	  プレイヤーアクション、ヒットチェック等
	// ---------------------------------------------
	 g_Player[0].PlayerAction( PLAYER_ONE ) ;		// --- プレイヤーアクション( 1P )
	 g_Player[1].PlayerAction( PLAYER_TWO ) ;		// --- プレイヤーアクション( 2P )
	
	for( int i = 0 ; i < ELE_X_1 * ELE_Y_1 ; i++ )
		Dot1stg[i].DotHitCheck() ;			// --- ドットのヒットチェック

	for( int i = 0 ; i < ELE_X_2 * ELE_Y_2 ; i++ )
		Dot2stg_t[i].DotHitCheck() ;		// --- ドットのヒットチェック

	for( int i = 0 ; i < ELE_X_2 * ELE_Y_2 ; i++ )
		Dot2stg_u[i].DotHitCheck() ;		// --- ドットのヒットチェック

	for( int i = 0 ; i < ELE_X_3 * ELE_Y_3 ; i++ )
		Dot3stg[i].DotHitCheck() ;			// --- ドットのヒットチェック
	
	/* ------------------------------------
				エフェクト関連
	--------------------------------------- */
	Effect *oneTimeEP ;
	for ( int i = 0 ; i < EFFECT_MAX ; i++ )
	{
		oneTimeEP = &g_effect[i] ;

		if ( oneTimeEP->efInfo.aliveFlg )
			oneTimeEP->eAction() ;				// --- エフェクトアクション
	}

	// --- ブザー再生
	if( (CheckSoundMem( g_soundHandle[ e_Sound_BuzzerSE ].soundHandle ) == 0) &&
		(UI_War.f_disp == TRUE) )
	{
		SOUND_INFO *otSP = &g_soundHandle[ e_Sound_BuzzerSE ] ;
		PlaySoundMem( otSP->soundHandle, otSP->playType ) ;
	}


	/* ------------------------------------
				その他
	--------------------------------------- */
	SetWinLoseInfo() ;							// --- 勝敗ステータスのセット

	return -1 ;
}

// ============================================================================================================================================== 【リザルト部】
// ---------------------------------------------------------------------------------------------------------------------------
//
//											GameLoop関数_結果発表初期化
//
// ---------------------------------------------------------------------------------------------------------------------------
//
//								帰り値 :	-1		- シーンそのまま判定
//											-1以外	- シーンをその定数と同じにする判定
//
// ---------------------------------------------------------------------------------------------------------------------------
int LoopFunc_ResultInit(  )
{
	const int C_OT_ISLAND	= 0 ;	// --- 定義_島
	const int C_OT_YASHI_1	= 1 ;	// --- 定義_ヤシの木
	const int C_OT_YASHI_2	= 2 ;	// --- 定義_ヤシの木
	const int C_OT_YASHI_3	= 3 ;	// --- 定義_ヤシの木
	const int C_OT_YASHI_4	= 4 ;	// --- 定義_ヤシの木
	const int C_OT_YASHI_5	= 5 ;	// --- 定義_ヤシの木
	const int C_OT_CHAIR_1	= 6 ;	// --- 定義_イス
	const int C_OT_CHAIR_2	= 7 ;	// --- 定義_イス
	const int C_OT_TABLE	= 8 ;	// --- 定義_テーブル
	const int C_OT_JUICE	= 9 ;	// --- 定義_じゅ〜す
	const int C_OT_SEA		= 10 ;	// --- 定義_海

	SetLightDirection( VGet( 0.0f, -1.0f, 1.0f ) ) ;									// --- 光の方向を決定
	SetShadowMapLightDirection( g_shadowMap.handle, VGet( 0.0f, -0.1f, 0.5f ) ) ;		// --- 影の向きをセット
	g_shadowMap.isUse = TRUE ;

	// ----------------------------------------------------------
	//					オブジェクトの配置
	// ----------------------------------------------------------
	g_Object[ OBJID_RESULTOB + C_OT_ISLAND ].setModelAndAnim( g_mv1Handle[ e_Model_Island ],		FALSE,	VGet( 1.0f, 1.0f, 1.0f )  ) ;		// --- 島
	g_Object[ OBJID_RESULTOB + C_OT_YASHI_1 ].setModelAndAnim( g_mv1Handle[ e_Model_YashiWood ],	TRUE,	VGet( 0.6f, 0.6f, 0.7f )  ) ;		// --- やし
	g_Object[ OBJID_RESULTOB + C_OT_YASHI_2 ].setModelAndAnim( g_mv1Handle[ e_Model_YashiWood ],	TRUE,	VGet( 0.6f, 0.6f, 0.7f )  ) ;		// --- やし
	g_Object[ OBJID_RESULTOB + C_OT_YASHI_3 ].setModelAndAnim( g_mv1Handle[ e_Model_YashiWood ],	TRUE,	VGet( 0.6f, 0.6f, 0.7f )  ) ;		// --- やし
	g_Object[ OBJID_RESULTOB + C_OT_YASHI_4 ].setModelAndAnim( g_mv1Handle[ e_Model_YashiWood ],	TRUE,	VGet( 0.6f, 0.6f, 0.7f )  ) ;		// --- やし
	g_Object[ OBJID_RESULTOB + C_OT_YASHI_5 ].setModelAndAnim( g_mv1Handle[ e_Model_YashiWood ],	TRUE,	VGet( 0.6f, 0.6f, 0.7f )  ) ;		// --- やし
	g_Object[ OBJID_RESULTOB + C_OT_CHAIR_1 ].setModelAndAnim( g_mv1Handle[ e_Model_BeachChair ],	TRUE,	VGet( 0.5f, 0.5f, 0.5f )  ) ;		// --- イス
	g_Object[ OBJID_RESULTOB + C_OT_CHAIR_2 ].setModelAndAnim( g_mv1Handle[ e_Model_BeachChair ],	TRUE,	VGet( 0.5f, 0.5f, 0.5f )  ) ;		// --- イス
	g_Object[ OBJID_RESULTOB + C_OT_TABLE ].setModelAndAnim( g_mv1Handle[ e_Model_Table ],			TRUE,	VGet( 0.6f, 0.6f, 0.6f )  ) ;		// --- テーブル
	g_Object[ OBJID_RESULTOB + C_OT_JUICE ].setModelAndAnim( g_mv1Handle[ e_Model_Juice ],			TRUE,	VGet( 0.5f, 0.5f, 0.5f )  ) ;		// --- ジュース
	g_Object[ OBJID_RESULTOB + C_OT_SEA ].setModelAndAnim( g_mv1Handle[ e_Model_BSea ],				TRUE,	VGet( 0.5f, 0.5f, 0.5f )  ) ;		// --- 海

	g_Object[ OBJID_RESULTOB + C_OT_ISLAND ].pos	= VGet( 0.0f,		-440.0f,	0.0f ) ;			// --- 島
	g_Object[ OBJID_RESULTOB + C_OT_YASHI_1 ].pos	= VGet( -1200.0f,	-220.0f,	800.0f ) ;			// --- やし
	g_Object[ OBJID_RESULTOB + C_OT_YASHI_2 ].pos	= VGet( 1200.0f,	-170.0f,	0.0f ) ;			// --- やし
	g_Object[ OBJID_RESULTOB + C_OT_YASHI_3 ].pos	= VGet( 200.0f,		-150.0f,	1100.0f ) ;			// --- やし
	g_Object[ OBJID_RESULTOB + C_OT_YASHI_4 ].pos	= VGet( -1000.0f,	-200.0f,	-600.0f ) ;			// --- やし
	g_Object[ OBJID_RESULTOB + C_OT_YASHI_5 ].pos	= VGet( 1000.0f,	-200.0f,	-700.0f ) ;			// --- やし
	g_Object[ OBJID_RESULTOB + C_OT_CHAIR_1 ].pos	= VGet( -500.0f,	-45.0f,		700.0f ) ;			// --- イス
	g_Object[ OBJID_RESULTOB + C_OT_CHAIR_2 ].pos	= VGet( 300.0f,		-45.0f,		700.0f ) ;			// --- イス
	g_Object[ OBJID_RESULTOB + C_OT_TABLE ].pos		= VGet( -100.0f,	0.0f,		700.0f ) ;			// --- テーブル
	g_Object[ OBJID_RESULTOB + C_OT_JUICE ].pos		= VGet( -90.0f,		250.0f,		700.0f ) ;			// --- ジュース
	g_Object[ OBJID_RESULTOB + C_OT_SEA ].pos		= VGet( 1000.0f,	-300.0f,	1000.0f ) ;			// --- 海

	g_Object[ OBJID_RESULTOB + C_OT_ISLAND ].size	= VGet( 0.7f,	0.2f,	0.7f ) ;					// --- 島
	g_Object[ OBJID_RESULTOB + C_OT_YASHI_1 ].size	= VGet( 0.35f,	0.45f,	0.35f ) ;					// --- やし
	g_Object[ OBJID_RESULTOB + C_OT_YASHI_2 ].size	= VGet( 0.35f,	0.45f,	0.35f ) ;					// --- やし
	g_Object[ OBJID_RESULTOB + C_OT_YASHI_3 ].size	= VGet( 0.35f,	0.45f,	0.35f ) ;					// --- やし
	g_Object[ OBJID_RESULTOB + C_OT_YASHI_4 ].size	= VGet( 0.35f,	0.45f,	0.35f ) ;					// --- やし
	g_Object[ OBJID_RESULTOB + C_OT_YASHI_5 ].size	= VGet( 0.35f,	0.45f,	0.35f ) ;					// --- やし
	g_Object[ OBJID_RESULTOB + C_OT_CHAIR_1 ].size	= VGet( 0.8f,	0.4f,	0.5f ) ;					// --- イス
	g_Object[ OBJID_RESULTOB + C_OT_CHAIR_2 ].size	= VGet( 0.8f,	0.4f,	0.5f ) ;					// --- イス
	g_Object[ OBJID_RESULTOB + C_OT_TABLE ].size	= VGet( 1.8f,	1.5f,	1.8f ) ;					// --- テーブル
	g_Object[ OBJID_RESULTOB + C_OT_JUICE ].size	= VGet(	0.7f,	0.7f,	0.7f ) ;					// --- ジュース
	g_Object[ OBJID_RESULTOB + C_OT_SEA ].size		= VGet(	5.7f,	1.0f,	5.7f ) ;					// --- 海

	g_Object[ OBJID_RESULTOB + C_OT_CHAIR_1 ].dir	= VGet( 0.0f,	2.2f,	0.0f ) ;					// --- イス
	g_Object[ OBJID_RESULTOB + C_OT_CHAIR_2 ].dir	= VGet( 0.0f,	2.2f,	0.0f ) ;					// --- イス
	g_Object[ OBJID_RESULTOB + C_OT_JUICE ].dir		= VGet( 0.0f,	0.3f,	0.0f ) ;					// --- ジュース

	// --- 土台トス
	g_Object[ OBJID_DODAI + 0 ].pos	 = VGet( -300.0f,	-10.0f,	0.0f ) ;		// --- 土台_1P座標
	g_Object[ OBJID_DODAI + 1 ].pos	 = VGet( 300.0f,	-10.0f,	0.0f ) ;		// --- 土台_2P座標
	g_Object[ OBJID_DODAI + 0 ].size = VGet( 2.0f,		1.0f,	2.0f ) ;		// --- 土台_1Pサイズ 
	g_Object[ OBJID_DODAI + 1 ].size = VGet( 2.0f,		1.0f,	2.0f ) ;		// --- 土台_2Pサイズ

	// ----------------------------------------------------------
	//					プレイヤーなどのセット
	// ----------------------------------------------------------
	MV1SetMaterialEmiColor( g_Player[0].modelHandle,	0, GetColorF( 0.5f, 0.5f, 0.5f, 1.0f ) ) ;		// プレイヤー
	MV1SetMaterialEmiColor( g_Player[1].modelHandle,	0, GetColorF( 0.5f, 0.5f, 0.5f, 1.0f ) ) ;		// プレイヤー

	g_Player[ PLAYER_ONE ].pos	= VGet( -300.0f,	1200.0f,	0.0f ) ;		// --- プレイヤー_1P座標
	g_Player[ PLAYER_TWO ].pos	= VGet( 300.0f,		1200.0f,	0.0f ) ;		// --- プレイヤー_2P座標
	g_Player[ PLAYER_ONE ].dir	= VGet( 0.0f,		3.8f,		0.0f ) ;		// --- プレイヤー_1P向き
	g_Player[ PLAYER_TWO ].dir	= VGet( 0.0f,		0.2f,		0.0f ) ;		// --- プレイヤー_2P向き
	g_Player[ PLAYER_ONE ].size	= VGet( 1.0f,		1.0f,		1.0f ) ;		// --- プレイヤー_1Pサイズ
	g_Player[ PLAYER_TWO ].size	= VGet( 1.0f,		1.0f,		1.0f ) ;		// --- プレイヤー_2Pサイズ

	g_Player[ PLAYER_ONE ].setAnim( e_StageChange_2to3_End ) ;					// --- プレイヤー_1Pアニメ
	g_Player[ PLAYER_TWO ].setAnim( e_StageChange_2to3_End ) ;					// --- プレイヤー_2Pアニメ
	g_Player[ PLAYER_ONE ].nowanim = e_StageChange_2to3_End ;					// --- プレイヤー_1Pアニメ_情報
	g_Player[ PLAYER_TWO ].nowanim = e_StageChange_2to3_End ;					// --- プレイヤー_2Pアニメ_情報

	MV1SetOpacityRate( g_Player[ PLAYER_ONE ].modelHandle , 1.0f ) ;			// --- 不透明度セット
	MV1SetOpacityRate( g_Player[ PLAYER_TWO ].modelHandle , 1.0f ) ;			// --- 不透明度セット

	// ----------------------------------------------------------
	//							その他
	// ----------------------------------------------------------
	MV1SetTextureGraphHandle( g_mv1Handle[ e_Model_SkyDom ].modelHandle, 0, g_graphHandle[ e_Image_Skysun ].handle, FALSE ) ;		// --- 朝背景

	g_camera.pos		= VGet( 0.0f,	350.0f,	-700.0f ) ;			// --- 座標セット
	g_camera.lookPos	= VGet( 0.0f,	300.0f,	0.0f ) ;			// --- 注視点セット
	g_camera.upDir		= VGet( 0.0f,	1.0f,	0.0f ) ;			// --- 上方向セット

	g_gameSpeed			= 1.0f ;

	// --- エフェクトの初期化
	for ( int i = 0 ; i < EFFECT_MAX ; i++ )
		g_effect[ i ].initStates() ;

	// --- 使用変数の初期化
	memset( &player_PSInfo.any_counter	, 0		, sizeof( int[16] ) ) ;		// --- どぅるるる時間etc...
	memset( &player_PSInfo.any_number	, 0		, sizeof( int[16] ) ) ;		// --- 発表前のランダム数値etc...
	memset( &player_PSInfo.any_flg		, FALSE	, sizeof( BOOL[16] ) ) ;	// --- ドット数での勝敗決定が必要になったフラグetc...

	piP->any_counter[0] = 0 ;		// --- タイマー用

	piP->res_fallSpeed[0]	 = 10.0f ;	// --- 1Pの落下速度
	piP->res_fallSpeed[1]	= 0.0f ;	// --- 2Pの落下速度

	piP->res_isCameraMove	= FALSE ;	// --- カメラ移動_完了フラグ
	piP->res_rad[0]			= 180.0f ;	// --- 1P_ラジアンセット
	piP->res_rad[1]			= 0.0f ;	// --- 2P_ラジアンセット
	piP->res_rad[2]			= -90.0f ;	// --- カメラ_ラジアンセット

	piP->res_flashRotPower	= 0.005f ;	// --- 背景のフラッシュ_回転加算分数値
	piP->res_flashRot		= 0.0f ;	// --- 背景のフラッシュ_回転率

	piP->res_flashSize.now		= 1.0f ;	// --- 背景のフラッシュ_サイズ
	piP->res_flashSize.max[0]	= 1.3f ;	// --- 背景のフラッシュ_サイズ_MAX
	piP->res_flashSize.min[0]	= 0.7f ;	// --- 背景のフラッシュ_サイズ_MIN
	piP->res_flashSize.plus		= 0.02f ;	// --- 背景のフラッシュ_サイズ変動数値

	// --- 勝敗用のスコアを取得( 八百長 )
	// ==--==--==--==--==--==--==--==--==--==--
/*
	int otScore[ 2 ] = { 4, 33 } ;
	int otDot[ 2 ]	 = { 2, 15 } ;

	for ( int i = 0 ; i < 2 ; i++ )
	{
		g_Player[ i ].pushscore = otScore[ i ] ;
		g_Player[ i ].dotscore = otDot[ i ] ;
	}
*/
	// ==--==--==--==--==--==--==--==--==--==--

	int winRes = SetWinLoseInfo() ;
	for ( int i = 0 ; i < 2 ; i++ )
	{
		if ( g_Player[ i ].isWin_now )
			piP->res_WinnerID = i ;		// --- 勝者のIDを取得
	}

	// --- 同点対策
	if ( winRes == -1 )
		g_Player[ piP->res_WinnerID ].dotscore += 1 ;

	// --- 音量セット
	for ( int i = 0 ; i < e_Sound_Total ; i++ )
		StopSoundMem( g_soundHandle[ i ].soundHandle ) ;	// --- 全てのサウンドを止める

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//				モード切替
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	return e_Mode_Result ;
}

// ---------------------------------------------------------------------------------------------------------------------------
//
//											GameLoop関数_結果発表
//
// ---------------------------------------------------------------------------------------------------------------------------
//
//								帰り値 :	-1		- シーンそのまま判定
//											-1以外	- シーンをその定数と同じにする判定
//
// ---------------------------------------------------------------------------------------------------------------------------
int LoopFunc_Result(  )
{
	// --- 使用状況 ---	
	//
	//	any_counter[0]	-	どぅるるる、インターバルの時間
	//	any_counter[1]	-	どぅるるる、インターバルの時間_MAX
	//	any_counter[2]	-	どぅるるる、インターバルの時間(2)
	//	any_counter[3]	-	どぅるるる、インターバルの時間(2)_MAX
	//
	//	any_flg[0]		-	様々なフラグ( 数字変更サイズ終了etc... )
	//	any_flg[1]		-	キャラ登場時_着地完了フラグ_1P
	//	any_flg[2]		-	キャラ登場時_着地完了フラグ_2P
	//
	//	any_number[0]	-	発表前のランダム数値_1P
	//	any_number[1]	-	発表前のランダム数値_2P
	//
	//	any_select[0]	-	
	//
	// ----------------
	
	int		fadeTime	 = 10 ;
	int		intervalTime = 80 ;

	Player *otPP ;
	otPP = &g_Player[0] ;

	SOUND_INFO *otSP ;		// --- サウンド用

	/* ------------------------------------
				エフェクト関連
	--------------------------------------- */
	Effect *oneTimeEP ;
	for ( int i = 0 ; i < EFFECT_MAX ; i++ )
	{
		oneTimeEP = &g_effect[i] ;

		if ( oneTimeEP->efInfo.aliveFlg )
			oneTimeEP->eAction() ;				// --- エフェクトアクション
	}

	switch ( piP->res_Mode )
	{
		// ----------------------------------------
		//
		//					暗転時
		//
		// ----------------------------------------
		case e_RSMode_BlackChange :

			// --- シーンチェンジが終了したら処理を開始
			if ( UI_SChange.UIAction( 1 ) )
			{
				piP->any_counter[0] = 0 ;				// --- フェードタイムセット
				piP->any_counter[1] = fadeTime ;		// --- フェードタイムセット_MAX

				piP->any_flg[0] = 0 ;

				piP->res_Mode++ ;
			}

			break ;

		// ----------------------------------------
		//
		//				キャラ出現シーン
		//
		// ----------------------------------------
		case e_RSMode_Chara_SpawnIn :

			switch ( piP->any_flg[0] )
			{
				// -----------------------------------
				// --- ２人の落下処理＆着地処理
				// -----------------------------------
				case 0 :

					// --- プレイヤー２人のアクション
					for ( int i = PLAYER_ONE ; i <= PLAYER_TWO ; i++ )
					{
						// --- 落下中の処理
						if ( g_Player[ i ].pos.y > 0.0f )
						{
							g_Player[ i ].pos.y		-= piP->res_fallSpeed[ i ] ;
							g_Player[ i ].playTime	= 0.0f ;

							// --- 着地処理
							if ( g_Player[ i ].pos.y <= 0.0f )
							{
								otSP = &g_soundHandle[ e_Sound_ChakuchiSE ] ;
								PlaySoundMem( otSP->soundHandle, otSP->playType ) ;			// --- 効果音再生
								g_Player[ i ].pos.y = 0.0f ;		// --- 座標セット
							}
							else
							{
								if ( piP->res_fallSpeed[ i ] != 0.0f )
									piP->res_fallSpeed[ i ] += 5.0f ;	// --- 落下速度を徐々に早くする
							}
						}
						// --- 着地後の処理
						else
						{
							// --- この処理が1P側だった場合、2Pの落下を開始させる
							if ( (i == PLAYER_ONE) && (piP->res_fallSpeed[ PLAYER_TWO ] == 0) )
								piP->res_fallSpeed[ PLAYER_TWO ] = 10.0f ;

							// --- アニメーションを再生する( アニメ再生中、なおかつアニメが落下中である場合 )
							if ( (g_Player[ i ].playTime + 0.8f * g_gameSpeed <= g_Player[ i ].anim[ g_Player[ i ].nowanim ].animTotal) &&
								 (g_Player[ i ].nowanim == e_StageChange_2to3_End))
								g_Player[ i ].playTime += 0.8f * g_gameSpeed ;
							// --- 落下終了時の処理
							else if ( g_Player[ i ].nowanim == e_StageChange_2to3_End )
							{
								g_Player[ i ].nowanim	= e_PlayerWait ;
								g_Player[ i ].setAnim( e_PlayerWait ) ;
								g_Player[ i ].playTime	= 0.0f ;

								piP->any_flg[ i + 1 ]	= TRUE ;
							}
						}
					}

					// --- ２人とも着地が完了したら次のモードへ
					if ( piP->any_flg[1] && piP->any_flg[2] )
					{
						piP->any_counter[0] = 50 ;
						piP->any_flg[0]++ ;
					}

					break ;

				// -----------------------------------
				// --- ２人とも着地完了したときの処理
				// -----------------------------------
				case 1 :

					// --- 心臓音が流れてないか確認
					if( CheckSoundMem( g_soundHandle[ e_Sound_heartSE ].soundHandle ) == 0 )
					{
						// --- 音量セット
						for ( int i = 0 ; i < e_Sound_Total ; i++ )
							StopSoundMem( g_soundHandle[ i ].soundHandle ) ;	// --- 全てのサウンドを止める

						SetAllSoundVolume100() ;								// --- ボリューム 100セット

						otSP = &g_soundHandle[ e_Sound_heartSE ] ;
						PlaySoundMem( otSP->soundHandle, otSP->playType ) ;
					}

					if ( piP->any_counter[0] >= 0 )
						piP->any_counter[0]-- ;
					else
					{
						g_Player[ PLAYER_ONE ].dir = VGet( 0.0f,	3.8f,	0.0f ) ;		// --- プレイヤー_1P向き
						g_Player[ PLAYER_TWO ].dir = VGet( 0.0f,	0.2f,	0.0f ) ;		// --- プレイヤー_2P向き

						piP->any_counter[0] = 0 ;				// --- フェードタイムセット
						piP->any_counter[1] = fadeTime ;		// --- フェードタイムセット_MAX
						piP->any_counter[2] = intervalTime ;	// --- 文字表示タイムセット
						piP->any_counter[3] = intervalTime ;	// --- 文字表示タイムセット_MAX

						piP->any_flg[0]		= 0 ;				// --- モード管理

						piP->res_Mode++ ;

					}

					break ;
			}

			break ;

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 押し出し
		// ----------------------------------------
		//
		//			「落とした数」タグ処理
		//
		// ----------------------------------------
		case e_RSMode_PresenBefore_Str_Push :

			switch ( piP->any_flg[0] )
			{
				// -------------------
				//	フェードイン処理
				// -------------------
				case 0 :

					// --- カウントダウン。0になったら次のモードへ
					piP->any_counter[0]++ ;

					if ( piP->any_counter[0] >= piP->any_counter[1] )
						piP->any_flg[0]++ ;

					break ;

				// ----------------------
				//		文字描画処理
				// ----------------------
				case 1 :

					// --- カウントダウン。0になったら次のモードへ
					piP->any_counter[2]-- ;
					
					if ( piP->any_counter[2] <= 0 )
						piP->any_flg[0]++ ;

					break ;

				// -------------------
				//	フェードアウト処理
				// -------------------
				case 2 :

					// --- カウントダウン。0になったら次のモードへ
					piP->any_counter[0]-- ;

					if ( piP->any_counter[0] <= 0 )
					{
						piP->any_counter[0] = 0 ;				// --- フェードタイム
						piP->any_counter[1] = fadeTime ;		// --- フェードタイム_MAX

						piP->any_flg[0]		= 0 ;				// --- モード

						piP->res_Mode++ ;
					}

					break ;
			}

			// --- バーサイズ決定
			piP->res_barSChange = (float)piP->any_counter[0] / (float)piP->any_counter[1] ;

			break ;

		// ----------------------------------------
		//
		//		押し出しスコア発表前(どぅるる)
		//
		// ----------------------------------------
		case e_RSMode_PresenBefore_Push :

			switch ( piP->any_flg[0] )
			{
				// -------------------
				//	フェードイン処理
				// -------------------
				case 0 :

					// --- フェードインが終了したら次のモードへ
					piP->any_counter[0]++ ;

					if ( piP->any_counter[0] >= piP->any_counter[1] )
					{
						piP->any_counter[2] = 95 ;			// --- どぅるるる時間
						piP->any_flg[0]++ ;
					}

					// --- バーサイズ決定
					piP->res_barSChange = (float)piP->any_counter[0] / (float)piP->any_counter[1] ;

					break ;

				// -------------------
				//	 どぅるるる処理
				// -------------------
				case 1 :

					// ---ドラムロールが流れていないか確認
					if( CheckSoundMem(g_soundHandle[ e_Sound_DrumrollSE ].soundHandle) == 0 )
					{
						// --- 音量セット
						for ( int i = 0 ; i < e_Sound_Total ; i++ )
							StopSoundMem( g_soundHandle[ i ].soundHandle ) ;	// --- 全てのサウンドを止める

						SetAllSoundVolume100() ;								// --- ボリューム 100セット

						otSP = &g_soundHandle[ e_Sound_DrumrollSE ] ;
						PlaySoundMem( otSP->soundHandle, otSP->playType ) ;
					}

					piP->any_number[0] = rand() % 99 + 1 ;		// --- 1Pのランダム数値生成
					piP->any_number[1] = rand() % 99 + 1 ;		// --- 2Pのランダム数値生成

					// --- どぅるるる時間が終了したら次のモードへ
					piP->any_counter[2]-- ;
					if ( piP->any_counter[2] <= 0 )
					{
						piP->any_flg[0]			= 0 ;						// --- モード

						piP->res_strSize.now	= 0.0f ;					// --- 数字サイズ
						piP->res_strSize.max[0] = 1.0f ;					// --- 数字サイズ_MAX
						piP->res_strSize.min[0] = piP->res_strSize.now ;	// --- 数字サイズ_MIN( 大元 )
						piP->res_strSize.plus	= 0.1f ;					// --- 数字サイズ_PLUS

						piP->res_Mode++ ;

						// --- 音量セット
						for ( int i = 0 ; i < e_Sound_Total ; i++ )
							StopSoundMem( g_soundHandle[ i ].soundHandle ) ;	// --- 全てのサウンドを止める

						SetAllSoundVolume100() ;								// --- ボリューム 100セット

						otSP = &g_soundHandle[ e_Sound_rollfinishSE ] ;			// --- SE(シンバル)を流す
						PlaySoundMem( otSP->soundHandle, otSP->playType ) ;
					}

					break ;
			}

			break ;

		// ----------------------------------------
		//
		//		押し出しスコア発表後(でぇぇん)
		//
		// ----------------------------------------
		case e_RSMode_PresenAfter_Push :
	
			// --- 数字サイズ変更終了がまだである場合の処理
			switch ( piP->any_flg[0] )
			{
				// -------------------
				//	 でぇぇん処理
				// -------------------
				case 0 :
					// --- 0.0f 〜 MAX 〜 0.0f の動きを行う
					if ( piP->res_strSize.plus > 0 )
					{
						if ( !CheckLimitAndPlus<float>( CLAPMODE_BELOW, &piP->res_strSize.now, piP->res_strSize.plus, piP->res_strSize.max[0], piP->res_strSize.max[0] ) )
							piP->res_strSize.plus *= -1 ;
					}
					else
					{
						if ( !CheckLimitAndPlus<float>( CLAPMODE_EXCEED, &piP->res_strSize.now, piP->res_strSize.plus, piP->res_strSize.min[0], piP->res_strSize.min[0] ) )
						{
							piP->res_strSize.plus *= -1 ;

							piP->any_counter[0] = intervalTime ;	// --- インターバルセット
							piP->any_counter[1] = intervalTime ;	// --- インターバルセット_MAX

							piP->any_flg[0]++ ;
						}
					}

					break ;

				// -------------------
				//	 インターバル
				// -------------------
				case 1 :
					// --- カウントダウン。0になったら次のモードへ
					piP->any_counter[0]-- ;
					if ( piP->any_counter[0] <= 0 )
					{
						piP->any_counter[0] = fadeTime ;	// --- フェードタイムセット
						piP->any_counter[1] = fadeTime ;	// --- フェードタイムセット_MAX

						piP->any_flg[0]++ ;

					}

					break ;

				// -------------------
				//	 引っ込み処理
				// -------------------
				case 2 :

					// --- カウントダウン終了で移行処理に
					piP->any_counter[0]-- ;

					if ( piP->any_counter[0] <= 0 )
					{
						// --- もし押し出しスコアが同率ならドットを比較するモードに移行
						if ( g_Player[0].pushscore == g_Player[1].pushscore )
						{
							piP->any_counter[0] = 0 ;				// --- フェードタイムセット
							piP->any_counter[1] = fadeTime ;		// --- フェードタイムセット_MAX
							piP->any_counter[2] = intervalTime ;	// --- インターバルセット
							piP->any_counter[3] = intervalTime ;	// --- インターバルセット_MAX

							piP->any_flg[0]		= 0 ;				// --- モード

							piP->res_Mode++ ;
						}

						// --- そうでないなら、勝利敗北のシーンに移行
						else
						{
							for ( int i = 0 ; i < 2 ; i++ )
							{
								g_Player[ i ].playTime = 0.0f ;

								if ( i == piP->res_WinnerID )
								{
									g_Player[ i ].nowanim = e_PlayerWinEmote_In ;		// --- 勝利エモートセット
									g_Player[ i ].setAnim( e_PlayerWinEmote_In ) ;		// --- 勝利エモートセット
								}
								else
								{
									g_Player[ i ].nowanim = e_PlayerLoseEmote_In ;		// --- 敗北エモートセット
									g_Player[ i ].setAnim( e_PlayerLoseEmote_In ) ;		// --- 敗北エモートセット
								}
							}
							piP->res_Mode		= e_RSMode_DispString ;
						}					
					}

					// --- バーサイズ決定
					piP->res_barSChange = (float)piP->any_counter[0] / (float)piP->any_counter[1] ;

					break ;
			}

			break ;

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ドット
		// ----------------------------------------
		//
		//		「獲得ココナッツ数」タグ処理
		//
		// ----------------------------------------
		case e_RSMode_PresenBefore_Str_Dot :

			switch ( piP->any_flg[0] )
			{
				// -------------------
				//	  インターバル
				// -------------------
				case 0 :

					// --- カウントダウン。0になったら次のモードへ
					piP->any_counter[0]++ ;

					if ( piP->any_counter[0] >= 30 )
					{
						piP->any_counter[0] = 0 ;
						piP->any_flg[0]++ ;
					}

					break ;

				// -------------------
				//	フェードイン処理
				// -------------------
				case 1 :

					// --- カウントダウン。0になったら次のモードへ
					piP->any_counter[0]++ ;

					if ( piP->any_counter[0] >= piP->any_counter[1] )
					{
						piP->any_flg[0]++ ;

						g_gameSpeed = 1.0f ;									// --- ゲームスピード初期化
					}

					break ;

				// ----------------------
				//		文字描画処理
				// ----------------------
				case 2 :

					// --- カウントダウン。0になったら次のモードへ
					piP->any_counter[2]-- ;
					
					if ( piP->any_counter[2] <= 0 )
						piP->any_flg[0]++ ;

					break ;

				// -------------------
				//	フェードアウト処理
				// -------------------
				case 3 :

					// --- カウントダウン。0になったら次のモードへ
					piP->any_counter[0]-- ;

					if ( piP->any_counter[0] <= 0 )
					{
						piP->any_counter[0] = 0 ;				// --- フェードタイム
						piP->any_counter[1] = fadeTime ;		// --- フェードタイム_MAX

						piP->any_flg[0]		= 0 ;				// --- モード

						piP->res_Mode++ ;
					}

					break ;
			}

			// --- バーサイズ決定
			if ( piP->any_flg[0] > 0 )
				piP->res_barSChange = (float)piP->any_counter[0] / (float)piP->any_counter[1] ;

			break ;

		// ----------------------------------------
		//
		//		ドットスコア発表前(どぅるる)
		//
		// ----------------------------------------
		case e_RSMode_PresenBefore_Dot :

			switch ( piP->any_flg[0] )
			{
				// -------------------
				//	フェードイン処理
				// -------------------
				case 0 :

					// --- フェードインが終了したら次のモードへ
					piP->any_counter[0]++ ;

					if ( piP->any_counter[0] >= piP->any_counter[1] )
					{
						piP->any_counter[2] = 95 ;			// --- どぅるるる時間
						piP->any_flg[0]++ ;
					}

					// --- バーサイズ決定
					piP->res_barSChange = (float)piP->any_counter[0] / (float)piP->any_counter[1] ;

					break ;

				// -------------------
				//	 どぅるるる処理
				// -------------------
				case 1 :

					// ---ドラムロールが流れていないか確認
					if( CheckSoundMem(g_soundHandle[ e_Sound_DrumrollSE ].soundHandle) == 0 )
					{
						// --- 音量セット
						for ( int i = 0 ; i < e_Sound_Total ; i++ )
							StopSoundMem( g_soundHandle[ i ].soundHandle ) ;	// --- 全てのサウンドを止める

						SetAllSoundVolume100() ;								// --- ボリューム 100セット

						otSP = &g_soundHandle[ e_Sound_DrumrollSE ] ;
						PlaySoundMem( otSP->soundHandle, otSP->playType ) ;
					}

					piP->any_number[0] = rand() % 99 + 1 ;		// --- 1Pのランダム数値生成
					piP->any_number[1] = rand() % 99 + 1 ;		// --- 2Pのランダム数値生成

					// --- どぅるるる時間が終了したら次のモードへ
					piP->any_counter[2]-- ;
					if ( piP->any_counter[2] <= 0 )
					{
						piP->any_flg[0]			= 0 ;						// --- モード

						piP->res_strSize.now	= 0.0f ;					// --- 数字サイズ
						piP->res_strSize.max[0] = 1.0f ;					// --- 数字サイズ_MAX
						piP->res_strSize.min[0] = piP->res_strSize.now ;	// --- 数字サイズ_MIN( 大元 )
						piP->res_strSize.plus	= 0.1f ;					// --- 数字サイズ_PLUS

						piP->res_Mode++ ;

						// --- 音量セット
						for ( int i = 0 ; i < e_Sound_Total ; i++ )
							StopSoundMem( g_soundHandle[ i ].soundHandle ) ;	// --- 全てのサウンドを止める

						SetAllSoundVolume100() ;								// --- ボリューム 100セット

						otSP = &g_soundHandle[ e_Sound_rollfinishSE ] ;			// --- SE(シンバル)を流す
						PlaySoundMem( otSP->soundHandle, otSP->playType ) ;
					}

					break ;
			}
			break ;

		// ----------------------------------------
		//		ドットスコア発表前(どぅるる)
		// ----------------------------------------
		case e_RSMode_PresenAfter_Dot :

			// --- 数字サイズ変更終了がまだである場合の処理
			switch ( piP->any_flg[0] )
			{
				// -------------------
				//	 でぇぇん処理
				// -------------------
				case 0 :
					// --- 0.0f 〜 MAX 〜 0.0f の動きを行う
					if ( piP->res_strSize.plus > 0 )
					{
						if ( !CheckLimitAndPlus<float>( CLAPMODE_BELOW, &piP->res_strSize.now, piP->res_strSize.plus, piP->res_strSize.max[0], piP->res_strSize.max[0] ) )
							piP->res_strSize.plus *= -1 ;
					}
					else
					{
						if ( !CheckLimitAndPlus<float>( CLAPMODE_EXCEED, &piP->res_strSize.now, piP->res_strSize.plus, piP->res_strSize.min[0], piP->res_strSize.min[0] ) )
						{
							piP->res_strSize.plus *= -1 ;

							piP->any_counter[0] = intervalTime ;	// --- インターバルセット
							piP->any_counter[1] = intervalTime ;	// --- インターバルセット_MAX

							piP->any_flg[0]++ ;
						}
					}

					break ;

				// -------------------
				//	 インターバル
				// -------------------
				case 1 :
					// --- カウントダウン。0になったら次のモードへ
					piP->any_counter[0]-- ;
					if ( piP->any_counter[0] <= 0 )
					{
						piP->any_counter[0] = fadeTime ;	// --- フェードタイムセット
						piP->any_counter[1] = fadeTime ;	// --- フェードタイムセット_MAX

						piP->any_flg[0]++ ;

						// --- バーサイズ決定
						piP->res_barSChange = (float)piP->any_counter[0] / (float)piP->any_counter[1] ;

					}

					break ;

				// -------------------
				//	 引っ込み処理
				// -------------------
				case 2 :

					// --- カウントダウン終了で移行処理に
					piP->any_counter[0]-- ;

					if ( piP->any_counter[0] <= 0 )
					{
						for ( int i = 0 ; i < 2 ; i++ )
						{
							g_Player[ i ].playTime = 0.0f ;

							if ( i == piP->res_WinnerID )
							{
								g_Player[ i ].nowanim = e_PlayerWinEmote_In ;		// --- 勝利エモートセット
								g_Player[ i ].setAnim( e_PlayerWinEmote_In ) ;		// --- 勝利エモートセット
							}
							else
							{
								g_Player[ i ].nowanim = e_PlayerLoseEmote_In ;		// --- 敗北エモートセット
								g_Player[ i ].setAnim( e_PlayerLoseEmote_In ) ;		// --- 敗北エモートセット
							}
						}

						piP->any_counter[0]		= 0 ;			// --- 回転カウンタ
						piP->res_isCameraMove	= FALSE ;		// --- カメラ移動終了フラグ

						piP->res_Mode		= e_RSMode_DispString ;
					}

					break ;
			}

			break ;

		// ----------------------------------------
		//				勝敗文字表示
		// ----------------------------------------
		case e_RSMode_DispString :

			// --- キャラ移動処理( ぐるっと )
			if ( !piP->res_isCameraMove )
			{
/*
				for ( int i = 0 ; i < 2 ; i++ )
				{
					if ( piP->res_WinnerID == PLAYER_ONE )
						piP->res_rad[ i ] += 5.0f ;				// --- 1P勝利で時計回り
					else
						piP->res_rad[ i ] -= 5.0f ;				// --- 2P勝利で反時計回り

					g_Player[ i ].pos.x = (float)(cos( piP->res_rad[ i ] * DX_PI / 180 ) * 300) ;		// --- 回転処理_スピード決め 
					g_Player[ i ].pos.z = (float)(sin( piP->res_rad[ i ] * DX_PI / 180 ) * 300) ;		// --- 回転処理_スピード決め
				}
*/				
				if ( piP->res_WinnerID == PLAYER_ONE )
					piP->res_rad[ 2 ] -= 5.0f ;				// --- 1P勝利で時計回り
				else
					piP->res_rad[ 2 ] += 5.0f ;				// --- 2P勝利で反時計回り

				g_camera.pos.x = (float)(cos( piP->res_rad[2] * DX_PI / 180 ) * 700) ;					// --- 回転処理_カメラ 
				g_camera.pos.z = (float)(sin( piP->res_rad[2] * DX_PI / 180 ) * 700) ;					// --- 回転処理_カメラ

				for ( int i = 0 ; i < 2 ; i++ )
				{
					if ( piP->res_WinnerID == PLAYER_ONE )
						g_Player[ i ].dir.y += 0.06f ;			// --- 1P勝利で反時計回り
					else
						g_Player[ i ].dir.y -= 0.06f ;			// --- 2P勝利で反時計回り
				}

				piP->any_counter[0]++ ;

				// --- 移動終了で完了フラグをTRUEにする
				if ( piP->any_counter[0] >= 12 )
				{
					// ---拍手が流れていないか確認
					if( CheckSoundMem( g_soundHandle[ e_Sound_applauseSE ].soundHandle ) == 0 ) 
					{
						SetAllSoundVolume100() ;								// --- ボリューム 100セット

						otSP = &g_soundHandle[ e_Sound_applauseSE ] ;
						PlaySoundMem( otSP->soundHandle, otSP->playType ) ;
					}

					piP->res_isCameraMove = TRUE ;
				}
			}
			// --- カメラの回転が終わった後の処理
			else
			{
				// --- エフェクト表示
				EffectGenerator( VGet( (float)g_winSize.x, (float)g_winSize.y, 0.0f), e_Effect_Result_Coconuts ) ;	// --- リザルト時のココナッツ

				// --- 光を回転させる
				piP->res_flashRot += piP->res_flashRotPower ;

				// --- 光のサイズを変更させる
				if ( piP->res_flashSize.plus > 0 )
				{
					if ( !CheckLimitAndPlus<float>( CLAPMODE_BELOW, &piP->res_flashSize.now, piP->res_flashSize.plus,
														piP->res_flashSize.max[0] ,piP->res_flashSize.max[0] ) )
						piP->res_flashSize.plus *= -1 ;
				}
				else
				{
					if ( !CheckLimitAndPlus<float>( CLAPMODE_EXCEED, &piP->res_flashSize.now, piP->res_flashSize.plus,
														piP->res_flashSize.min[0] ,piP->res_flashSize.min[0] ) )
						piP->res_flashSize.plus *= -1 ;
				}
			}

			break ;
	}

	// ==============================================
	//	プレイヤーのアクション( 関数化すべきか？ )
	// ==============================================
	for ( int i = PLAYER_ONE ; i <= PLAYER_TWO ; i++ )
	{
		otPP = &g_Player[ i ] ;

		// --- 落下中の処理はスルー( 上記で行っているため )
		if ( otPP->nowanim == e_StageChange_2to3_End )
			continue ;

		if ( otPP->playTime < otPP->anim[ otPP->nowanim	].animTotal )
			otPP->playTime += 0.4f * g_gameSpeed ;
		else if ( otPP->nowanim == e_PlayerLoseEmote_In )	// --- 敗北エモート
		{
			otPP->nowanim = e_PlayerLoseEmote_Loop ;
			otPP->setAnim( e_PlayerLoseEmote_Loop ) ;
			otPP->playTime = 0.0f ;
		}
		else if ( otPP->nowanim == e_PlayerWinEmote_In )	// --- 勝利エモート
		{
			otPP->nowanim = e_PlayerWinEmote_Loop ;
			otPP->setAnim( e_PlayerWinEmote_Loop ) ;
			otPP->playTime = 0.0f ;
		}
		else												// --- その他継続モーション
			otPP->playTime = 0.0f ;	
	}

	return -1 ;
}



