/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: MapGeneration.h
	CREATE	: HIRANO・HIRANO
	
	------ Explanation of file ---------------------------------------------------------------------
       
	マップの生成クラスの宣言

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

class MapGeneration
{
	public :
		MapGeneration( ) ;		// コンストラクタ
		~MapGeneration( ) ;		// デストラクタ

		void initStates() ;			// 初期化メソッド

		int MapStart( ) ;			// ゲームスタート時のマップ表示
		int LoadMap( int, int * ) ;		// マップの読み込み
		int AnimMap1( ) ;				// マップ1のアニメーション
		int AnimMap2_t( ) ;				// マップ2_tのアニメーション
		int AnimMap2_u( ) ;				// マップ2_uのアニメーション
		int AfterMapInit( ) ;			// マップの切り替え後の初期セット
		int MapChangeInitSet1to2( ) ;	// マップ切り替え時の初期セットをする
		int MapChangeInitSet2to3( ) ;	// マップ切り替え時の初期セットをする
		int MapMigration( ) ;			// マップ移行をする
		void HideMap( int ) ;			// マップを非表示にする

		int f_move ;					// 足場動くフラグ
		int c_collapse ;				// ステージ切り替えフラグ
		int p_move ;
		int mapcount ;					// 足場の経過時間
		int mapswitch ;					// マップフェーズ
		float darken_screen ;			// 画面暗転カウント
		float size ;					// 画面のサイズ

		Scaffold *scafP ;				// セット用ポインタ(Scaffold)
		Scaffold *scafAnimP ;			// セット用ポインタ(Scaffold_Anim)
		Object	 *objP ;				// セット用ポインタ(Object)

} ;





