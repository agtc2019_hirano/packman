/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: CameraMovement.h
	CREATE	: NOGUCHI

	------ Explanation of file ---------------------------------------------------------------------
       
	カメラの動きを制御するクラス

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

class CameraMovement
{
	// ----------
	//	public
	// ----------
	public :
		// --- 以下メソッド
		CameraMovement() ;			// --- コンストラクタ
		~CameraMovement() ;			// --- デストラクタ

		int	initStates() ;																	// --- 初期化メソッド
		int	cAction() ;																		// --- カメラのアクション
		int circle_AroundObject( const float arg_circleNum, const float arg_distance ) ;	// --- カメラを現在注視点を中心にして回すメソッド

		int CamInitGame() ;			// --- ゲーム初期セット時のカメラポジ
		int CamFirst() ;			// --- ゲームスタート時のカメラポジ
		int CamSecond() ;			// --- カウント２時のカメラポジ
		int CamThird() ;			// --- カウント１時のカメラポジ
		int CamLast() ;				// --- カウントGO時のカメラポジ
		int CamLanding() ;			// --- カメラ落下
		int CamShake( int ) ;		// --- カメラを揺らす

		int CamPlayGame() ;			// --- ゲームプレイ時のカメラポジ

		// --- 以下フィールド
		VECTOR	pos ;							// --- カメラの情報_位置
		VECTOR	lookPos ;						// --- カメラの情報_注視点
		VECTOR	upDir ;							// --- カメラの情報_上方向

		int		chaseObjID ;					// --- 追跡するオブジェクトのID
		int		cameracount ;					// --- カメラカウント
		bool	camflg ;
		float	rotTan ;						// --- 角度を表す変数。オブジェクトの周りを綺麗に回る時に使う
		float	TRotate, HRotate, VRotate ;		// --- テスト
} ;
