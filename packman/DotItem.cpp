/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: DotItem.cpp
	
	------ Explanation of file ---------------------------------------------------------------------
       
	ドットアイテムクラス（アイテムクラス継承）

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

#include "Common.h"


DotItem::DotItem()
{
	initStates() ;
	DotInit() ;
}

DotItem::~DotItem()
{
}

void DotItem::DotInit()
{
	dotonly = TRUE ;
	size = SetVector( 0.35f ) ;
	dispFlg = FALSE ;
	dotrevnot = TRUE ;
	nowmap = Map.mapswitch ;
}

/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
			ヒットチェック処理
=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
BOOL DotItem::DotHitCheck()
{
	for( int i = 0 ; i < 2 ; i++ )
	{
		if(g_Player[i].superflg == TRUE)
		{
			dispFlg = FALSE ;
			return 0 ;
		}

		if( (HitCheck_Capsule_Capsule( pos , pos , 60.0f ,
			VGet(g_Player[i].pos.x,g_Player[i].pos.y + 50,g_Player[i].pos.z) ,
			VGet(g_Player[i].pos.x,g_Player[i].pos.y - 50,g_Player[i].pos.z) , 100.0f ) == TRUE)
			&& (dispFlg == TRUE) )
		{
			dispFlg = FALSE ;
			dotrevcnt = GetNowCount() ;
//			printf("aaaaaaa%d\n" , i) ;
			g_Player[i].PlayerCatch() ;
			dotrevnot = FALSE ;
		}
	}
	
	DotRevive() ;

	dir.y += (0.025f * g_gameSpeed) ;
	if( dir.y >= 4.0f )
		dir.y = 0.0f ;

	if( pos.y < -800.0f )
		dotonly = FALSE ;

	return FALSE ;
}

int DotItem::DotMapSelect()
{
	if( Map.mapswitch != nowmap )
	{
		dotonly = TRUE ;
		nowmap = Map.mapswitch ;
	}

	switch(Map.mapswitch)
	{
		// === ステージ1ドット生成
		case 1 :
			DotGenerate( e_stage1 ) ;
			break ;

		// === ステージ2ドット生成
		case 2 :
			DotGenerate( e_stage2_top ) ;
			DotGenerate( e_stage2_under ) ;
			DotDelete( e_stage1 ) ;
			break ;

		// === ステージ3ドット生成
		case 3 :
			DotGenerate( e_stage3 ) ;
			DotDelete( e_stage1 ) ;
			DotDelete( e_stage2_top ) ;
			DotDelete( e_stage2_under ) ;
			break ;

		// === 全ドット削除
		default :
			DotDelete( e_stage1 ) ;
			DotDelete( e_stage2_top ) ;
			DotDelete( e_stage2_under ) ;
			DotDelete( e_stage3 ) ;
			break ;
	}

	return 0 ;
}

/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
			　ドット生成処理
=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
int DotItem::DotGenerate( int mapno )
{
	switch( mapno )
	{
		case e_stage1 :
			for( int i = 0 ; i < ELE_X_1 * ELE_Y_1 ; i++ )
			{
				if(dotonly == TRUE)								// --- 最初の一回だけ処理
				{
					Dot1stg[i].dispFlg = Scaf1[i].dispFlg ;
				}

				Dot1stg[i].pos.x = Scaf1[i].pos.x ;
				Dot1stg[i].pos.y = Scaf1[i].pos.y + 250 ;
				Dot1stg[i].pos.z = Scaf1[i].pos.z ;
			}
//			printf("dotonly = %d\n" , dotonly) ;
			dotonly = FALSE ;
			break ;

		case e_stage2_top :
			for( int i = 0 ; i < ELE_X_2 * ELE_Y_2 ; i++ )
			{
				if(dotonly == TRUE)
				{
					Dot2stg_t[i].dispFlg = Scaf2_t[i].dispFlg ;
				}

				Dot2stg_t[i].pos.x = Scaf2_t[i].pos.x ;
				Dot2stg_t[i].pos.y = Scaf2_t[i].pos.y + 250 ;
				Dot2stg_t[i].pos.z = Scaf2_t[i].pos.z ;
			}
//			printf("dotonly = %d\n" , dotonly) ;
//			dotonly = FALSE ;
			break ;

		case e_stage2_under :
			for( int i = 0 ; i < ELE_X_2 * ELE_Y_2 ; i++ )
			{
				if(dotonly == TRUE)
				{
					Dot2stg_u[i].dispFlg = Scaf2_u[i].dispFlg ;
				}

				Dot2stg_u[i].pos.x = Scaf2_u[i].pos.x ;
				Dot2stg_u[i].pos.y = Scaf2_u[i].pos.y + 250 ;
				Dot2stg_u[i].pos.z = Scaf2_u[i].pos.z ;
			}
//			printf("dotonly = %d\n" , dotonly) ;
			dotonly = FALSE ;
			break ;

		case e_stage3 :
			for( int i = 0 ; i < ELE_X_3 * ELE_Y_3 ; i++ )
			{
				if(dotonly == TRUE)
				{
					Dot3stg[i].dispFlg = Scaf3[i].dispFlg ;
				}

				Dot3stg[i].pos.x = Scaf3[i].pos.x ;
				Dot3stg[i].pos.y = Scaf3[i].pos.y + 250 ;
				Dot3stg[i].pos.z = Scaf3[i].pos.z ;
			}
//			printf("dotonly = %d\n" , dotonly) ;
			dotonly = FALSE ;
			break ;
	}

	return 0 ;
}

/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
				削除処理
=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
int DotItem::DotDelete( int mapno )
{
	switch( mapno )
	{
		case e_stage1 :
			for( int i = 0 ; i < ELE_X_1 * ELE_Y_1 ; i++ )
			{
				Dot1stg[i].dispFlg = FALSE ;
				Dot1stg[i].dotrevnot = TRUE ;
			}
			break ;

		case e_stage2_top :
			for( int i = 0 ; i < ELE_X_2 * ELE_Y_2 ; i++ )
			{
				Dot2stg_t[i].dispFlg = FALSE ;
				Dot2stg_t[i].dotrevnot = TRUE ;
			}
			break ;

		case e_stage2_under :
			for( int i = 0 ; i < ELE_X_2 * ELE_Y_2 ; i++ )
			{
				Dot2stg_u[i].dispFlg = FALSE ;
				Dot2stg_u[i].dotrevnot = TRUE ;
			}
			break ;

		case e_stage3 :
			for( int i = 0 ; i < ELE_X_3 * ELE_Y_3 ; i++ )
			{
				Dot3stg[i].dispFlg = FALSE ;
				Dot3stg[i].dotrevnot = TRUE ;
			}
			break ;

		default :
			dispFlg = FALSE ;
			break ;

	}

	return 0 ;
}

/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
				復活処理
=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
BOOL DotItem::DotRevive()
{
	if( (GetNowCount() - dotrevcnt > 5000 ) && (dispFlg == FALSE) && (dotrevnot == FALSE) )
	{
		dispFlg = TRUE ;
		dotrevnot = TRUE ;
		return TRUE ;
	}

	return FALSE ;
}


