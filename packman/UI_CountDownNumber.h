/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: UI_CountDownNumber.h
	CREATE	: NOGUCHI
	
	------ Explanation of file ---------------------------------------------------------------------
       
	UI_CountDownNumberクラスの定義部
	
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

class UI_CountDownNumber : public UserInterface
{
	public :
		UI_CountDownNumber( ) ;			// --- コンストラクタ
		~UI_CountDownNumber( ) ;		// --- デストラクタ

		void UIDraw( int ){} ;			// --- 描画( これはダミー )
		BOOL CountAction() ;			// --- 本アクション
		void CountDraw() ;				// --- 本描画

		int		nowCount ;				// --- 現在のカウント進行
		float	cSize ;					// --- サイズ
		float	cRot ;					// --- 回転

		float	sizeMax ;				// --- 最も数値が大きくなるサイズ

		c_countGroup<float>	sizeChange ;	// --- サイズ変更のカウンタ
		c_countGroup<int>	fadeInterval ;	// --- フェードアウトを開始するまでのカウンタ
		c_countGroup<float>	fOut ;			// --- フェードアウト用のカウンタ
} ;
