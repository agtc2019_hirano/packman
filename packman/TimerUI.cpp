/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: TimerUI.cpp
	CREATE	: HIRANO
	
	------ Explanation of file ---------------------------------------------------------------------
       
	TimerUIクラス

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

#include "Common.h"

TimerUI::TimerUI( ){
	time				= 0 ;
	time_beforeOneSec	= 90 ;
	changeSize			= 0.8f ;	// この数値が規定値
	defaultSize			= changeSize ;

	redFlashLimit		= 10 ;		// 残りN秒からカウントが赤く光りだす
}
TimerUI::~TimerUI( ){
}

/* ------------------------------------------------------------
					TimeMain関数
===============================================================
	タイマーＵＩの表示
	引数：なし
	戻り値：int
--------------------------------------------------------------- */
void TimerUI::UIDraw( int eleno )
{			 
	// --------------------------------------------
	// --- 表示位置を指定
	// --------------------------------------------
	c_fPoint	UIDispPos[2] ;
	float		otChipSize_half = (float)g_graphHandle[ e_Image_Time1 ].chipSize.x / 2 * 0.7f ;			// --- 桁同士の隙間

	UIDispPos[0] = c_fPoint( (float)g_winSize.x / 2 + otChipSize_half, GAMEUI_POS_Y ) ;
	UIDispPos[1] = c_fPoint( (float)g_winSize.x / 2 - otChipSize_half, GAMEUI_POS_Y ) ;

	// --------------------------------------------
	// --- 必要な数値データを算出
	// --------------------------------------------
	int s1_time, s2_time ;	// 計算結果入れる

	time = 90 - Time[e_Timer3].CountSecond( ) ;		// タイムを９０からのカウントダウンに設定

	s2_time = time / 10 ;		// 10の位の計算
	s1_time = time % 10 ;		// 1の位の計算

	// --------------------------------------------
	// --- 時間の切り替わりを取得し、サイズを変更
	// --------------------------------------------
	if ( time_beforeOneSec != time )
	{
		// --- カウントダウンが残り10でなければ通常の処理
		if ( time > redFlashLimit )
			changeSize = defaultSize + 0.7f ;
		else
			changeSize = defaultSize + 1.0f ;
	}

	time_beforeOneSec = time ;

	CheckLimitAndPlus<float>( CLAPMODE_EXCEED, &changeSize, -0.07f * g_gameSpeed, defaultSize, defaultSize ) ;	// --- 2.0f を 1.3fまで減算させる

	// --------------------------------------------
	// --- 描画ステータスのセット
	// --------------------------------------------
	switch( eleno )
	{
		case 0 :		// 1の位のタイムＵＩ
			cord.x = UIDispPos[0].x ;												// 描画するｘ座標 396
			cord.y = UIDispPos[0].y ;												// 描画するｙ座標
			draw.x = (float)g_graphHandle[ e_Image_Time1 ].chipSize.x * s1_time ;	// 画像の描画したい左上ｘ座標
			draw.y = 0 ;															// 画像の描画したい左上ｙ座標
			image_w = g_graphHandle[ e_Image_Time1 ].chipSize.x ;					// 表示する幅
			image_h = g_graphHandle[ e_Image_Time1 ].chipSize.y ;					// 表示する高さ
			break ;

		case 1 :		// 10の位のタイムＵＩ
			cord.x = UIDispPos[1].x ;												// 描画するｘ座標 332
			cord.y = UIDispPos[1].y ;												// 描画するｙ座標
			draw.x = (float)g_graphHandle[ e_Image_Time1 ].chipSize.x * s2_time ;	// 画像の描画したい左上ｘ座標
			draw.y = 0 ;															// 画像の描画したい左上ｙ座標
			image_w = g_graphHandle[ e_Image_Time1 ].chipSize.x ;					// 表示する幅
			image_h = g_graphHandle[ e_Image_Time1 ].chipSize.x ;					// 表示する高さ
			break ;
	}

	// --------------------------------------------
	// --- 実描画
	// --------------------------------------------
	if ( ((TimeUI[0].f_disp == TRUE) && (eleno == 0)) || ((TimeUI[1].f_disp == TRUE) && (eleno == 1)) )
	{
		// --- 表示する数値に応じて、画像の切り取りを行う( 変形させたいため )
		int newTimeHandle = DerivationGraph( (int)draw.x, (int)draw.y, image_w, image_h, g_graphHandle[e_Image_Time1].handle ) ;
		DrawRotaGraph( (int)cord.x, (int)cord.y, changeSize, 0.0f, newTimeHandle, TRUE ) ;

		DeleteGraph( newTimeHandle ) ;
	}

	// ------------------------------------------------
	// --- 残りN秒でカウントが赤くなる
	// ------------------------------------------------
	if ( time <= redFlashLimit )
	{
		int newFlaHandle ;
		newFlaHandle = DerivationGraph( (int)draw.x, 0, image_w, image_h, g_graphHandle[ e_Image_Time1_RedBlend ].handle ) ;	// --- 赤発光画像

		// --- 発光用のカウントを取得
		// --- MIN - 1.8, MAX - 2.5 だった場合、割合で【0〜1】を取るのが難しいため、それぞれ1.8を引いた数値で計算する
		float nowFlash = changeSize + 0.5f - defaultSize ;
		float maxFlash = 1.0f ;

		// --- 発光用の画像を描画
		SetDrawBlendMode( DX_BLENDMODE_ADD, (int)(255.0f * (nowFlash / maxFlash)) ) ;
		DrawRotaGraph( (int)cord.x, (int)cord.y, changeSize, 0.0f, newFlaHandle, TRUE ) ;

		SetDrawBlendMode( DX_BLENDMODE_NOBLEND, 0 ) ;

		DeleteGraph( newFlaHandle ) ;
	}
}


