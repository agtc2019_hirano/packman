/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: Player.cpp
	
	------ Explanation of file ---------------------------------------------------------------------
       
	プレイヤークラス

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

#include "Common.h"
#include <math.h>

// ---------------------------------------------------------------------------------
//								   コンストラクタ
// ---------------------------------------------------------------------------------
Player::Player( )
{
	initStates() ;
	initStates_Player() ;
}

// ---------------------------------------------------------------------------------
//									デストラクタ
// ---------------------------------------------------------------------------------
Player::~Player( )
{

}

// ========================================================================================================== Init States
// ---------------------------------------------------------------------------------
//
//							ステータス初期化メソッド
//
// ---------------------------------------------------------------------------------
void Player::initStates_Player(  )
{
	mode		= 0 ;				// --- モード初期化
	pos.y		= 1000 ;			// --- 初期座標(Y)
	nowanim		= 0 ;				// --- 現在の指定アニメーション
	clearmodel	= 0.0f ;			// --- モデルの透明度
	reviveflg	= FALSE ;			// --- 復活フラグ
	animendflg	= TRUE ;			// --- アニメ終了フラグ
	flashtime	= 0 ;				// --- 点滅タイマー
	pushscore	= 0 ;				// --- 押し出しによるスコア
	speed		= 0.0f ;			// --- ステージ切り替え時プレイヤー落下速度
	land		= 0 ;				// --- 着地アニメーションフラグ
	timeCount.max[0] = 0 ;			// --- スーパー状態最大カウント
	timeCount.plus = 0 ;			// --- スーパー状態カウンター
	timeCount.now = 0 ;				// --- 現在のカウント数

	effectcnt = 0 ;

	selmodel	= 0 ;				// --- モデル情報は後々格納

	fallDotMinus = 3 ;				// --- 落ちた時にドットを失う量

	nowmap = 1 ;
	
	deathHeight			= -700.0f ;	// --- 死ぬ高さ

	this->fallSpeed		= 30.0f ;	// --- 落下速度
	this->isInvincible	= FALSE ;	// --- 無敵かどうかのフラグ
	this->isAnimChange	= FALSE ;	// --- アニメ切替フラグ

	// --- 移動速度関連
	moveInfo.moveSpeed						= SetVector( 0.0f ) ;	// --- 現在速度
	moveInfo.moveMax[ e_Speed_Normal ]		= 9.0f ;				// --- 最高速度_通常時
	moveInfo.moveMax[ e_Speed_Super ]		= 14.0f ;				// --- 最高速度_スーパー時

	moveInfo.power[ e_Speed_Normal ].accele		= 3.0f ;		// --- 加速度_通常時
	moveInfo.power[ e_Speed_Super ].accele		= 2.0f ;		// --- 加速度_スーパー時
	moveInfo.power[ e_Speed_Normal ].fliction	= 1.0f ;		// --- 減速度_通常時　
	moveInfo.power[ e_Speed_Super ].fliction	= 3.5f ;		// --- 減速度_スーパー時　

	// --- 移動制御関連( 氷上 )
	move_onIce.start.now	= 0.0f ;			// --- 現在の出だしブロック分数値
	move_onIce.start.plus	= 0.02f ;			// --- 徐々に移動を解放する分の初動数値
	move_onIce.start.max[0]	= 1.0f ;			// --- 終地点
	move_onIce.fliction		= 0.1f ;			// --- 減速度ブロック数値

	move_onIce.init.now		= move_onIce.start.now ;	// --- ばっくっぷを取るよ
	move_onIce.init.plus	= move_onIce.start.plus ;	// --- ばっくっぷを取るよ

	isSlide		= FALSE ;								// --- 滑りフラグ

	// --- エミッションパゥワ
	emiPower.now	= 0.3f ;							// --- 現在数値
	emiPower.min[0] = emiPower.now ;					// --- 元の値
	emiPower.max[0]	= 0.5f ;							// --- 往復数値_下限
	emiPower.max[1]	= 1.2f ;							// --- 往復数値_上限限
	emiPower.plus	= 0.05f ;							// --- 点滅速度

	if ( playerno == PLAYER_ONE )						// --- 現在勝利フラグ
		isWin_now	= TRUE ;							
	else
		isWin_now	= FALSE ;							

	size_fallWidth	= 60.0f ;							// --- 床ヒットチェックの時に使用する、プレイヤーの横幅( 大きいと落ちやすくなる )
	dotChargeMax	= 15 ;								// --- スーパー化するのに必要なDotの数

	// --- 復活時間関連
	floatCnt		= c_countGroup<float>() ;	// --- 初期化	
	floatCnt.max[0]	= 20.0f ;					// --- 着地までにかかる時間セット

	// --- 本当はinitValues.cpp 内で攻撃関連のステータスセットを行う。
	SecureZeroMemory( &this->atkInfo, sizeof( PACK_ATK ) ) ;			// --- 攻撃関連
	this->atkInfo.atkR		= 120.0f ;									// --- 半径
	this->atkInfo.validTime = 25 ;										// --- 攻撃判定が有効である時間
}

/* ------------------------------------------------------------
					PlayerId関数
===============================================================
	プレイヤーを識別する
	引数：なし
	戻り値：int
--------------------------------------------------------------- */
int Player::PlayerId( int arg_id )
{
	playerno = arg_id ;

	return 0 ;
}

//------------------------------------------------------------------------------------------------------------------------ Player Action
/* ------------------------------------------------------------
					PlayerAction関数
===============================================================
	押されたボタンの検知・処理
	引数：なし
	戻り値：int(現時点のアニメーション)
--------------------------------------------------------------- */
int Player::PlayerAction( int eleno )
{
	MV1RefreshCollInfo( this->modelHandle ) ;	// --- コリジョン情報を常に更新する

	isAnimChange	= FALSE ;
	movepos			= VGet( 0.0f, 0.0f, 0.0f ) ;
	isSlide			= FALSE ;

	if ( Map.mapswitch >= e_Stage_3_OnIce )		// --- 第三ステージであるならば滑る( 本当は床とのヒットチェックの際に決定付けをしたい )
		isSlide = TRUE ;

	// ========================================================================
	//	↓この一行ありでキーボード対応。消すとパッドに対応する( 十字移動 )
	// ========================================================================
//	key = GetJoypadInputState(DX_INPUT_KEY_PAD1) ;		// キーボード入力取得

	pad = PadData::GetButton( eleno ) ;					// パッドの入力情報を取得

	{
		BOOL isMoveCheck = FALSE ;		// --- 移動ＯＫフラグ(一時的な変数)

		if( ((pad.pushKey & PAD_INPUT_DOWN) || (g_keyBuf[KEY_INPUT_S] >= 1) 
						|| (key & PAD_INPUT_DOWN)) && (animendflg == TRUE) ){			// ===== 下移動（本番はパッドだけの情報とanimendflgだけでOK）
			isMoveCheck = TRUE ;
		}
		if( ((pad.pushKey & PAD_INPUT_LEFT) || (g_keyBuf[KEY_INPUT_A] >= 1) 
						|| (key & PAD_INPUT_LEFT)) && (animendflg == TRUE) ){			// ===== 左移動（本番はパッドだけの情報とanimendflgだけでOK）
			isMoveCheck = TRUE ;
		}
		if( ((pad.pushKey & PAD_INPUT_UP) || (g_keyBuf[KEY_INPUT_W] >= 1) 
						|| (key & PAD_INPUT_UP)) && (animendflg == TRUE) ){				// ===== 上移動（本番はパッドだけの情報とanimendflgだけでOK）
			isMoveCheck = TRUE ;
		}
		if( ((pad.pushKey & PAD_INPUT_RIGHT) || (g_keyBuf[KEY_INPUT_D] >= 1) 
						|| (key & PAD_INPUT_RIGHT)) && (animendflg == TRUE) ){			// ===== 右移動（本番はパッドだけの情報とanimendflgだけでOK）
			isMoveCheck = TRUE ;
		}

		// --- 移動フラグONであるのでれば...
		if ( isMoveCheck )
		{
			if ( eleno == 0 )		// 引数（プレイヤー番号）
			{
				keyflg_1 = 1 ;		// キーフラグ１立てる

			}else{
				keyflg_1 = 0 ;		// キーフラグ０
			}

			// --- 落下中でなければ
			if ( (this->mode == e_pMode_Wait) || (this->mode == e_pMode_Move) )
			{
				if ( mode != e_pMode_Move )
				{
					nowanim = e_PlayerWalk ;
					setAnim( e_PlayerWalk ) ;

					mode = e_pMode_Move ;	// 「移動」モード変更
				}
			}
		}
	}

	// ----------------------------------------------- 押し(Xキー)
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//	押し処理( キーボード操作ではXが1P, Mが2P )
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	// --- モード次第ではこの処理をさせない
	if ( this->mode <= e_pMode_Move )
	{
		BOOL isPushCheck = FALSE ;		// --- プッシュＯＫフラグ(一時的な変数)

		if ( (eleno == 0 && g_keyBuf[ KEY_INPUT_X ] == 1) ||
				(eleno == 1 && g_keyBuf[ KEY_INPUT_M ] == 1) && (animendflg) )
			isPushCheck = TRUE ;

		if((pad.pushKey & ( PAD_INPUT_B )) && (animendflg) && (pad.pushTime == 1))
			isPushCheck = TRUE ;

		// --- プッシュフラグONであるのでれば...
		if ( isPushCheck )
		{
			mode = e_pMode_Push ;	// 「押し」モード変更

			this->nowanim	= e_PlayerPush_In ;
			this->playTime	= 0.0f ;

			this->atkInfo.nowTime	= 0 ;
			this->atkInfo.isUse		= TRUE ;
			this->isAnimChange		= TRUE ;
		}
	}

	// -----------------------------------------------------------
	//					攻撃判定とのヒットチェック
	// -----------------------------------------------------------
	if ( !this->isInvincible )
	{
		int retNumtoHit = HitCheck_pAttack() ;
		
		if ( retNumtoHit != -1 )
		{
			// --- 被弾、なおかつ既にのけぞり状態でなければのけぞらせる
			if ( (this->nowanim != e_PlayerPress_In) && (this->nowanim != e_PlayerPress_Out) )
			{
				this->mode			= e_pMode_Press ;
				this->nowanim		= e_PlayerPress_In ;
				this->playTime		= 0.0f ;
				this->dir.y			= (float)retNumtoHit ;
				this->isAnimChange	= TRUE ;
				ResetVector( moveInfo.moveSpeed ) ;

				notmyid = playerno ^ 0x01 ;

				// --- 相手がスーパー状態だと即死する
				if ( g_Player[ notmyid ].superflg == TRUE )
				{
					pAttackZone_HitPowerSet( notmyid ) ;
					nokezoriFirstPower = 150 ;
				}
				// --- 当たり判定の生成リミットに応じて飛ばされ距離をセット( 出始めが一番のけぞる )
				else
					pAttackZone_HitPowerSet( notmyid ) ;

				EffectGenerator( this->pos, e_Effect_Push_Star ) ;	// --- 星生成(エフェクト)
			}
		}
	}

	if ( this->atkInfo.isUse )
		pAttackZone_Func() ;				// --- 攻撃判定処理

	// -----------------------------------------------------------
	//				死亡後に点滅させ、復活させる処理
	// -----------------------------------------------------------
	if((GetNowCount() - flashtime > 100) && (reviveflg == TRUE))
	{
		flashtime = GetNowCount() ;
		clearflg ^= 1 ;						// --- 反転

		flashcnt++ ;

		if(flashcnt > 15){

			if ( !superflg )				// --- スーパー時でなければ
				isInvincible	= FALSE ;	// --- 無敵化解除を行う

			reviveflg		= FALSE ;		// --- 十五回繰り返したら元通り
			flashcnt		= 0 ;
			clearflg		= 0 ;
		}
	}

	// -----------------------------------------------------------
	//						モード管理部
	// -----------------------------------------------------------
	switch( mode )
	{
		case e_pMode_Wait_Init :			// --- 待機処理_初期化
			mode++ ;
			break ;

		case e_pMode_Wait :
			nowanim = e_PlayerWait ;

			PlayerMove() ;					// --- アクション処理
			break ;

		case e_pMode_Move :
			if( superflg == FALSE )
				nowanim = e_PlayerWalk ;
			else{
//				nowanim = e_SuparPacMan_Walk ;
//				dir.x = 0.0f ;
			}
			PlayerMove() ;					// --- アクション処理
			break ;

		case e_pMode_Push :
			{
				float humiDis = 0.0f ;

				if( superflg == FALSE )
					humiDis = 14.0f ;	// --- 踏み込み_移動量
				else
					humiDis = 24.0f ;	// --- 踏み込み_移動量

				if ( this->nowanim == e_PlayerPush_Out )
					humiDis = 1.0f ;

				VECTOR ot_HumikomiDir[4] = { { 0, 0, -humiDis }, { -humiDis, 0, 0 }, { 0, 0, humiDis }, { humiDis, 0, 0 } } ;
			
				this->movepos = VAdd( this->movepos, ot_HumikomiDir[ (int)this->dir.y ] ) ;

				if( animendflg == TRUE ){
					nowanim = PlayerPush() ;	// --- 押し処理
				}
			}

			break ;

		case e_pMode_Press :
			{
				float nokeDis = 0.0f ;
				
				if ( nowanim == e_PlayerPress_In )
					nokeDis = nokezoriFirstPower ;		// --- のけぞり移動量( のけぞりパワーは pAttackZone_HitPowerSet() でセットしている。 )
				if ( nowanim == e_PlayerPress_Out )
					nokeDis = nokezoriFirstPower / 2 ;	// --- のけぞり移動量

				VECTOR ot_NokezoriDir[4] = { { 0, 0, nokeDis }, { nokeDis, 0, 0 }, { 0, 0, -nokeDis }, { -nokeDis, 0, 0 } } ;

				this->movepos = VAdd( this->movepos, ot_NokezoriDir[ (int)this->dir.y ] ) ;
				this->moveInfo.moveSpeed = this->movepos ;										// --- 滑るようにする

				if( animendflg == TRUE ){
					nowanim = PlayerPress() ;	// --- 押され処理
				}
			}

			break ;

		case e_pMode_Fall_Init :			// --- 落下処理_初期化
			mode++ ;
			break ;

		case e_pMode_Fall :
			PlayerFall() ;					// --- 落下処理
			break ;

		case e_pMode_Death :
			PlayerDeath() ;					// --- 死亡処理
			break ;

		case e_pMode_Revive :
			PlayerRevive() ;				// --- 復活処理
			break ;

		case e_pMode_Super :
			PlayerSuper() ;					// --- スーパーパックマン処理
			break ;

		case e_pMode_StageChange_1to2_Init :			// --- １−２：ステージ切り替え初期セット
			if ( nowanim != e_StageChange_1to2 )
				this->isAnimChange = TRUE ;				// --- アニメフラグ立てる
			playTime = 0.2f ;
			//	setAnim( e_StageChange_1to2 ) ;
			nowanim = e_StageChange_1to2 ;				// --- fallアニメセット
			mode = e_pMode_StageChange_1to2 ;			// --- モードインクリメント
			break ;

		case e_pMode_StageChange_1to2 :			// --- １−２：ステージ切り替え
			if ( Map.c_collapse != 0 )			// --- 切り替え中のフラグ立ってたら
				movepos.y = -15.0f ;			// --- 落下速度
			pos.y += movepos.y ;				// --- ｙ座標に足しこむ
			if ( Map.mapswitch == 2 )			// --- マップが切り替わったらモードチェンジ
				mode = e_pMode_Wait ;			// --- 待機
			break ;

		case e_pMode_StageChange_2to3_Init :		// --- ２−３：ステージ切り替え初期セット
			playTime = 0.2f ;
			if ( eleno == PLAYER_ONE )				// --- １ｐ
				movepos.x = -20.0f ;				// --- 左に飛ばす
			else
				movepos.x = 20.0f ;					// --- 右に飛ばす
			movepos.y = 30.0f ;						// --- プレイヤー上昇
			movepos.z = 5.0f ;						// --- 奥に移動値セット
			if ( nowanim != e_StageChange_2to3 )	
				this->isAnimChange = TRUE ;			// --- ステージが切り替わったらアニメフラグ立てる
			//	setAnim( e_StageChange_2to3 ) ;
			nowanim = e_StageChange_2to3 ;			// --- 回転アニメセット
			mode = e_pMode_StageChange_2to3 ;		// --- モードチェンジ
			break ;

		case e_pMode_StageChange_2to3 :			// --- ２−３：ステージ切り替え
			pos.x += movepos.x ;				// --- ｘ座標にｘ移動値足す
			pos.y += movepos.y ;				// --- ｙ座標にｙ移動値足す
			pos.z += movepos.z ;				// --- ｚ座標にｚ移動値足す
			break ;

		case e_pMode_StageChange_2to3_End :			// --- ２−３：ステージ切り替え終わり
//			if ( (nowanim != e_StageChange_2to3_End) && (Map.p_move == 1) ){
			if ( (nowanim != e_StageChange_2to3_End) && (land == 1) ){
				this->isAnimChange = TRUE ;				// --- 着地フラグ立ったらアニメフラグ立てる
				printf( "落ちるアニメセット\n" ) ;
				nowanim = e_StageChange_2to3_End ;		// --- 着地アニメセット
				land = FALSE ;							// --- 着地フラグ落とす
			}
			break ;
	}

	if( superflg == TRUE )
	{
		if ( (this->selmodel == 0) || (this->selmodel == 3) )		// --- fox
			timeCount.max[0] = 100 ;
		else if( (this->selmodel == 1) || (this->selmodel == 4) )	// --- cat
			timeCount.max[0] = 250 ;
		else if( (this->selmodel == 2) || (this->selmodel == 5) )// --- pig
			timeCount.max[0] = 100 ;

//		nowanim = e_SuparPacMan_Walk ;
		isAnimChange = TRUE ;
		this->atkInfo.validTime = 255 ;			// --- 攻撃判定が有効である時間
		if( (size.x < 1.25f) &&
			(size.y < 1.25f) &&
			(size.z < 1.25f) )
		{
			size.x += 0.01f ;
			size.y += 0.01f ;
			size.z += 0.01f ;
		}
	}else if( (size.x > 1.00f) &&
			(size.y > 1.00f) &&
			(size.z > 1.00f) )
	{
		size.x -= 0.01f ;
		size.y -= 0.01f ;
		size.z -= 0.01f ;
	}


	PlayerAnim() ;								// --- プレイヤーのアニメーション処理
	
	// -----------------------------
	//	  エミッションチェンジ
	// -----------------------------
	ChangeEmiPower() ;

	// -----------------------------
	//			実移動処理
	// -----------------------------
	movepos = VScale( movepos, g_gameSpeed ) ;	// --- 移動量にゲームスピードを加える( スローモーション処理 )
	movepos = HitCheck_ReturnMove( movepos ) ;	// --- ヒットチェックを行い、正しい移動量を帰す

	pos = VAdd( pos, movepos ) ;

	return nowanim ;
}

// =============================================================================================================== P Attack Zone HitPower Set
/* ------------------------------------------------------------
		攻撃判定に接触した際の吹っ飛びパワーを決める関数
===============================================================
	引数：なし
	戻り値：int( 成功で1 失敗で0 )
--------------------------------------------------------------- */
int Player::pAttackZone_HitPowerSet( const int arg_pNo )
{
	// -------------------------------------------------------------
	//		[0]  ->      [10]      ->      [50]
	//		開始 -> 当たり判定付与 -> 生成リミット
	//
	//		10 〜 50 の時が当たり判定有効時間なので、
	//		10(最大POWER)、50(最小POWER)にしたい。
	// -------------------------------------------------------------
	Player	*calcPP			= &g_Player[ arg_pNo ] ;	// --- g_Player[ i ]って長くない？
	float	minPowerCnt		= 0 ;						// --- 最小のパワーになる時のカウンタ( つまり、nowTimeがLimitに達するとき )
	float	nowPowerCnt		= 0 ;						// --- 現在のパワー分( 始動のインターバルを除いた数値にしたい )
	float	maxFirstPower	= 30.0f ;					// --- 最も出始めだった時のパワー

	minPowerCnt = (float)(calcPP->atkInfo.validTime) - (calcPP->anim[ e_PlayerPush_In ].animTotal * 0.75f) ;
	nowPowerCnt = (float)(calcPP->atkInfo.nowTime)	 - (calcPP->anim[ e_PlayerPush_In ].animTotal * 0.75f) ;

	if ( minPowerCnt < nowPowerCnt )
		return 0 ;

	nokezoriFirstPower = maxFirstPower - (maxFirstPower * (nowPowerCnt / minPowerCnt)) ;
	if ( nokezoriFirstPower < (maxFirstPower / 3) )
		nokezoriFirstPower = maxFirstPower / 3 ;			// --- 最大パワーの[1/3]は保証する

	return 1 ;
}

// =============================================================================================================== P Attack Zone Func
/* ------------------------------------------------------------
					攻撃判定の処理を行う関数
===============================================================
	引数：なし
	戻り値：int( 成功で1 失敗で0 )
--------------------------------------------------------------- */
int Player::pAttackZone_Func()
{
	if ( !this->atkInfo.isUse )
		return 0 ;

	this->atkInfo.nowTime += 1.0f * g_gameSpeed ;

	// --- 始動アニメーションの[3/4]が経過した時点で当たり判定を付与する
	if ( this->atkInfo.nowTime > (int)(this->anim[ e_PlayerPush_In ].animTotal * 0.75) )
	{
		// --- 向きに応じた場所に攻撃判定をセット
		float AtP = 140.0f ;
		float otVecAtk[][2] = { { 0.0f, -AtP }, { -AtP, 0.0f }, { 0.0f, AtP }, { AtP, 0.0f } } ;

		// --- プレイヤーの位置に合わせて移動する
		this->atkInfo.atkPos	= VGet( pos.x + otVecAtk[ (int)this->dir.y ][0], pos.y + 200.0f, pos.z + otVecAtk[ (int)this->dir.y ][1] ) ;

		if ( !this->atkInfo.isValid )
			this->atkInfo.isValid = TRUE ;	
	}
	if ( (this->atkInfo.nowTime >= this->atkInfo.validTime) || (this->mode != e_pMode_Push) )
	{
		this->atkInfo.isValid	= FALSE ;
		this->atkInfo.isUse		= FALSE ;
	}

	return 1 ;
}

//------------------------------------------------------------------------------------------------------------------------ Player Action After Stage 1
/* ------------------------------------------------------------
					PlayerAction_AfterStage1関数
===============================================================
	フェーズ切替後のパックマンアクション
	引数：なし
	戻り値：なし
--------------------------------------------------------------- */
int Player::PlayerAction_AfterStage1()
{
	moveInfo.moveSpeed.x = 0 ;		// 摩擦値を０にする
	moveInfo.moveSpeed.y = 0 ;		// 摩擦値を０にする
	moveInfo.moveSpeed.z = 0 ;		// 摩擦値を０にする
	movepos.x = 0.0f ;
	movepos.y = 0.0f ;
	movepos.z = 0.0f ;
	playTime = 0.0f ;

	land = TRUE ;	// プレイヤー１の移動値リセット

//	mode = e_pMode_Wait_Init ;
	return 0 ;
}

//------------------------------------------------------------------------------------------------------------------------ Player Action Invisible 1 to 2
/* ------------------------------------------------------------
					pAction_Invisible1to2関数
===============================================================
	フェーズ切替時のパックマンアクション
	引数：なし
	戻り値：なし
--------------------------------------------------------------- */
int Player::PlayerAction_Invisible1to2()
{
//	movepos.y = 5.0f ;
	mode =  e_pMode_StageChange_1to2_Init ;

	return 0 ;
}

//------------------------------------------------------------------------------------------------------------------------ Player Action Invisible 2 to 3
/* ------------------------------------------------------------
					pAction_Invisible2to3関数
===============================================================
	フェーズ切替時のパックマンアクション
	引数：なし
	戻り値：なし

--------------------------------------------------------------- */
int Player::PlayerAction_Invisible2to3()
{
	movepos.x = 0.0f ;
	movepos.y = 0.0f ;
	movepos.z = 0.0f ;
	playTime = 0.0f ;
	mode = e_pMode_StageChange_2to3_Init ;
	return 0 ;
}

/* ------------------------------------------------------------
					PlayerAnim関数
===============================================================
	アニメーション・移動(押し出し・のけぞりのみ)
	引数：なし
	戻り値：int
--------------------------------------------------------------- */
int Player::PlayerAnim()
{
	float otPlaySpeed = 0.0f ;

	if ( playTime < anim[nowanim].animTotal ){		// --- アニメ再生

		// --- 滑り時のアニメーションは少し早めに
		if ( (this->nowanim == e_PlayerWalk) && (this->move_onIce.start.now > 0.0f) && (this->move_onIce.start.now < 0.75f) )
			otPlaySpeed = 3.0f * g_gameSpeed ;					
		else
			otPlaySpeed = 1.5f * g_gameSpeed ;

		// --- 各アニメーション速度セット
		if ( nowanim == e_PlayerWait )															// --- 待機_In
			otPlaySpeed = 0.8f * g_gameSpeed ;
		if ( (nowanim == e_PlayerPush_In) || (nowanim == e_PlayerPush_Out) )					// --- プッシュ時
			otPlaySpeed = 0.8f * g_gameSpeed ;
		if ( nowanim == e_PlayerPress_In )														// --- 押され時_In
			otPlaySpeed = 1.2f * g_gameSpeed ;
		if ( nowanim == e_StageChange_2to3_End )												// --- 押され時_Out
			otPlaySpeed = 0.8f * g_gameSpeed ;
		if ( (nowanim == e_PlayerSpecialAttack_In) || (nowanim == e_PlayerSpecialAttack_Loop) || (nowanim == e_PlayerSpecialAttack_Out) ){
			if ( (this->selmodel == 0) || (this->selmodel == 3) )		// --- fox
				otPlaySpeed = 2.0f * g_gameSpeed ;													// --- スペシャル_Loop
			else if( (this->selmodel == 1) || (this->selmodel == 4) )	// --- cat
				otPlaySpeed = 1.0f * g_gameSpeed ;													// --- スペシャル_Loop
			else if( (this->selmodel == 2) || (this->selmodel == 5) )// --- pig
				otPlaySpeed = 2.0f * g_gameSpeed ;													// --- スペシャル_Loop
		}

		// --- ステージ切り替え：落下のアニメーション
		if ( (nowanim == e_StageChange_1to2) )		// --- ステージ切り替え時アニメ速度
			otPlaySpeed = 1.0f * g_gameSpeed ;

		// --- ステージ切り替え：回転のアニメーション
		if ( nowanim == e_StageChange_2to3 )
			otPlaySpeed = 2.0f * g_gameSpeed ;

		// --- ステージ切り替え：着地のアニメーション
		if ( nowanim == e_StageChange_2to3_End )
			otPlaySpeed = 0.5f * g_gameSpeed ;

	// --- 特定のアニメの移行処理
	}else if(nowanim == e_PlayerPush_In){			// --- 押し出し時のアニメーション(始)
		playTime = 0.0f ;
		nowanim = e_PlayerPush_Out ;
		isAnimChange = TRUE ;
	}else if(nowanim == e_PlayerPush_Out){			// --- 押し出し時のアニメーション(終)
		playTime = 0.0f ;
		nowanim = e_PlayerWait ;

		if ( mode != e_pMode_Fall )
			mode = e_pMode_Wait_Init ;

		animendflg = TRUE ;
		isAnimChange = TRUE ;

		moveInfo.moveSpeed = SetVector( 0.0f ) ;

	}else if(nowanim == e_PlayerPress_In){			// --- 押し出された時のアニメーション(始)
		playTime = 0.0f ;
		nowanim = e_PlayerPress_Out ;
		isAnimChange = TRUE ;
	}else if(nowanim == e_PlayerPress_Out){			// --- 押し出された時のアニメーション(終)
		playTime = 0.0f ;
		nowanim = e_PlayerWait ;

		if ( mode != e_pMode_Fall )
			mode = e_pMode_Wait_Init ;
		
		animendflg = TRUE ;
		isAnimChange = TRUE ;
	}else if(nowanim == e_PlayerSpecialAttack_In){			// --- スペシャルアタック(始)
		playTime = 0.0f ;
		nowanim = e_PlayerSpecialAttack_Loop ;
		superAttacktime = GetNowCount() ;
		isAnimChange = TRUE ;
	}else if( nowanim == e_PlayerSpecialAttack_Loop ){		// --- スペシャルアタック(中)
		playTime = 0.0f ;

		//if((GetNowCount() - superAttacktime) > timeCount.max[0] )
		//	nowanim = e_PlayerSpecialAttack_Out ;
		//else
		//	nowanim = e_PlayerSpecialAttack_Loop ;

		isAnimChange = TRUE ;
	}else if(nowanim == e_PlayerSpecialAttack_Out){		// ---スペシャルアタック(終)
		nowanim = e_PlayerWait ;

		if ( mode != e_pMode_Fall )
			mode = e_pMode_Wait_Init ;
		
		animendflg = TRUE ;
		isAnimChange = TRUE ;

		// --- 滑り方向をセット
		moveInfo.moveSpeed = movepos ;				// --- 滑り値を保存

	}else if(nowanim == e_StageChange_1to2){		// --- ステージ切り替え時：落下アニメーション
		nowanim = e_PlayerWait ;
		mode = e_pMode_Wait_Init ;
		animendflg = TRUE ;
		isAnimChange = TRUE ;
	}else if(nowanim == e_StageChange_2to3){	// --- ステージ切り替え後：回転アニメーション
		nowanim = e_PlayerWait ;
		mode = e_pMode_Wait_Init ;
		animendflg = TRUE ;
		isAnimChange = TRUE ;
	}else if( nowanim == e_StageChange_2to3_End ){	// --- ステージ切り替え後：着地アニメーション
		nowanim = e_PlayerWait ;
		mode = e_pMode_Wait_Init ;
		animendflg = TRUE ;
		isAnimChange = TRUE ;
		printf( "animok^^^^^\n" ) ;
	}else{
		playTime = 0.0f ;

		if ( mode != e_pMode_Fall )
			mode = e_pMode_Wait_Init ;

		animendflg = TRUE ;
		isAnimChange = TRUE ;
	}

	if( (superflg == TRUE) && ( nowanim == e_PlayerSpecialAttack_Loop ) ){
		if((GetNowCount() - superAttacktime) > timeCount.max[0] )
			nowanim = e_PlayerSpecialAttack_Out ;
		else
			nowanim = e_PlayerSpecialAttack_Loop ;
	}


	// --- playTimeに新たなアニメーションタイムを加算する
	playTime += otPlaySpeed ;	

	// --- アニメーションのセット
	if ( isAnimChange )
		setAnim( nowanim ) ;

//	printf( "%d\n" , nowanim ) ;

	// --------------------------------点滅
	if(clearflg == 0)
		MV1SetOpacityRate( modelHandle , 1.0f ) ;		// --- 不透明
	else
		MV1SetOpacityRate( modelHandle , 0.15f ) ;		// --- 半透明

	// --------------------------------スーパーから戻す処理(ステージ変更時)
	if( (mode == e_pMode_StageChange_1to2) || (nowmap != Map.mapswitch) )
	{
//		size = VGet(1.00f,1.00f,1.00f) ;

		nowmap = Map.mapswitch ;

		dir.x = 0.0f ;

		this->atkInfo.validTime = 25 ;										// --- 攻撃判定が有効である時間

		Dot1stg[0].DotMapSelect() ;

		isInvincible	= FALSE ;	// --- 無敵化解除
		superflg = FALSE ;
	}

	// --------------------------------スーパーから戻す処理(時間経過時)
	if( ((GetNowCount() > superendtime ) && (superflg == TRUE)) && (animendflg == TRUE) )
	{
//		size = VGet(1.00f,1.00f,1.00f) ;

		nowmap = Map.mapswitch ;

		dir.x = 0.0f ;

		this->atkInfo.validTime = 25 ;										// --- 攻撃判定が有効である時間

		isInvincible	= FALSE ;	// --- 無敵化解除
		superflg = FALSE ;

		SOUND_INFO *otSP = &g_soundHandle[ e_Sound_ReturnSE ] ;			// --- スペシャルから戻る音
		PlaySoundMem( otSP->soundHandle, otSP->playType ) ;
		SetSoundVolumeNum( 100, otSP ) ;								// --- ボリューム 100セット

//		printf("もとどーり\n") ;

		Dot1stg[0].dotonly = TRUE ;
		Dot1stg[0].DotMapSelect() ;
	}

	// ----------------------------------------------------------------------------------
	// --------------------------------スーパーから戻す処理(ﾀｲﾑｱｯﾌﾟ)	後で直します
	// ----------------------------------------------------------------------------------
	if ( (90 <= Time[ e_Timer3 ].CountSecond( )) && this->superflg ) 
	{
		nowmap = Map.mapswitch ;

		size = VGet(1.00f,1.00f,1.00f) ;

		dir.x = 0.0f ;

		this->atkInfo.validTime = 25 ;										// --- 攻撃判定が有効である時間

		Dot1stg[0].DotMapSelect() ;

		isInvincible	= FALSE ;	// --- 無敵化解除
		superflg = FALSE ;	
	}

	if( (GetNowCount() - effectcnt) > 250 )
	{
		effectcnt = GetNowCount()  ;

		if( superflg == TRUE )
			EffectGenerator( this->pos, e_Effect_Super_Shine , 50 ) ;	// --- 星生成(エフェクト)
	}

	return 0 ;
}

/* ------------------------------------------------------------
					PlayerWait関数
===============================================================
	待機アニメーション処理
	引数：なし
	戻り値：int(待機アニメの識別子)
--------------------------------------------------------------- */
// -----------------------待機時
int Player::PlayerWait()
{
//	printf( "PlayerWait\n" );

	return e_PlayerWait ;
}

/* ------------------------------------------------------------
					PlayerMove関数
===============================================================
	歩きアニメーション・移動処理
	引数：なし
	戻り値：int(歩きアニメの識別子)
--------------------------------------------------------------- */
// -----------------------移動時
int Player::PlayerMove( )
{
//	printf( "PlayerMove\n" );

	// ----------------------------------------------
	//	CheckLimitAndPlus() は
	//
	//	if ( 0.0f >= pos.x + speed )
	//		pos.x += speed ;
	//	else
	//		pos = 0.0f ;
	//
	//	のように、リミットを超えない限り加算を行いたい
	//	時のif文を短くできる。
	// ----------------------------------------------

	// ------------------------
	// --- プッシュチェック
	// ------------------------
	c_4Direction<BOOL> isPush ;
	if( (pad.pushKey & PAD_INPUT_DOWN) || ((g_keyBuf[KEY_INPUT_S] >= 1) && (keyflg_1 == 1))			// 下移動（パッドとキーボード入力に対応）
					|| ((key & PAD_INPUT_DOWN) && (keyflg_1 == 0)) )
		isPush.down = TRUE ;

	else if( (pad.pushKey & PAD_INPUT_UP) || ((g_keyBuf[KEY_INPUT_W] >= 1) && (keyflg_1 == 1)) 		// 上移動（パッドとキーボード入力に対応）
					|| ((key & PAD_INPUT_UP) && (keyflg_1 == 0)) )
		isPush.up	= TRUE ;

	if( (pad.pushKey & PAD_INPUT_LEFT) || ((g_keyBuf[KEY_INPUT_A] >= 1) && (keyflg_1 == 1))			// 左移動（パッドとキーボード入力に対応） 
					|| ((key & PAD_INPUT_LEFT) && (keyflg_1 == 0)) )
		isPush.left = TRUE ;

	else if( (pad.pushKey & PAD_INPUT_RIGHT) || ((g_keyBuf[KEY_INPUT_D] >= 1) && (keyflg_1 == 1)) 	// 右移動（パッドとキーボード入力に対応）
					|| ((key & PAD_INPUT_RIGHT) && (keyflg_1 == 0)) )
		isPush.right = TRUE ;

	// -----------------------------------------
	// --- 出だしを遅くする処理(氷ステージ用)
	// -----------------------------------------
	PMOVE_ONICE *mIce = &move_onIce ;

	if ( isSlide )
	{
		// --- キーがどれか押されている場合
		if ( !isPush.CheckAllZero() )
		{
			// --- リミットまで徐々に加算する( 最初は少しだけ、後半はかなりの伸びを見せる(?) )
			mIce->start.plus_RetMaxOver( mIce->start.plus ) ;

			// --- もしくは、前回の移動方向と逆に移動しようとした時にリセットをかける
			if ( mIce->start.now >= mIce->start.max[0] )
			{
				if ( ((isPush.right) && (moveInfo.moveSpeed.x < 0)) ||
					((isPush.left) && (moveInfo.moveSpeed.x > 0)) ||
					((isPush.up) && (moveInfo.moveSpeed.z < 0)) ||
					((isPush.down) && (moveInfo.moveSpeed.z > 0)) )
				{
					mIce->start.now		= mIce->init.now ;
					mIce->start.plus	= mIce->init.plus ;
				}
			}
		}
		// --- キーが押されていないと移動量に制限を加える( 初期化 )
		else
		{
			mIce->start.now		= mIce->init.now ;
			mIce->start.plus	= mIce->init.plus ;
		}
	}

	// ----------------------------------------------------------------------
	// -----------------------------------------
	// --- 移動処理
	// -----------------------------------------
	float *moveMax = &moveInfo.moveMax[ (int)superflg ] ;		// --- 最高速度
	float otAccele = moveInfo.power[ (int)superflg ].accele ;	// --- 加速度

	if ( isSlide )
		otAccele *= mIce->start.now ;							// --- 出だしは遅くなる

	// --------------
	// --- 手前移動処理
	if ( isPush.down )
	{
		// --- 最大速度に達するまで加速する
		CheckLimitAndPlus( CLAPMODE_EXCEED, &moveInfo.moveSpeed.z, -otAccele, -(*moveMax), -(*moveMax) ) ;

		dir.y = e_Down ;	// ---向き変更(手前)
	}

	// --------------
	// --- 左移動処理
	if ( isPush.left )
	{
		// --- 最大速度に達するまで加速する
		CheckLimitAndPlus( CLAPMODE_EXCEED, &moveInfo.moveSpeed.x, -otAccele, -(*moveMax), -(*moveMax) ) ;

		dir.y = e_Left ;	// ---向き変更(左)
	}

	// --------------
	// --- 奥移動処理
	if ( isPush.up )
	{
		// --- 最大速度に達するまで加速する
		CheckLimitAndPlus( CLAPMODE_BELOW, &moveInfo.moveSpeed.z, otAccele, (*moveMax), (*moveMax) ) ;

		dir.y = e_Up ;		// ---向き変更(奥)
	}

	// --------------
	// --- 右移動処理
	if ( isPush.right )
	{
		// --- 最大速度に達するまで加速する
		CheckLimitAndPlus( CLAPMODE_BELOW, &moveInfo.moveSpeed.x, otAccele, (*moveMax), (*moveMax) ) ;

		dir.y = e_Right ;	// ---向き変更(右)
	}

	// ----------------------------------------------------------------------
	// ----------------------------------------------
	// --- 減衰処理(押されていない場合に減速する)
	// ----------------------------------------------
	float downPower = moveInfo.power[ (int)superflg ].fliction ;

	if ( isSlide )
		downPower *= move_onIce.fliction ;			// --- 氷分の数値を加算

	// ----------------
	// --- 上下キー
	if ( !isPush.down && !isPush.up )			// --- 上、下キーどちらも押されていない場合
	{
		if ( moveInfo.moveSpeed.z < 0 )
			CheckLimitAndPlus( CLAPMODE_BELOW, &moveInfo.moveSpeed.z, downPower, 0.0f, 0.0f ) ;
		else if ( moveInfo.moveSpeed.z > 0 )
			CheckLimitAndPlus( CLAPMODE_EXCEED, &moveInfo.moveSpeed.z, -downPower, 0.0f, 0.0f ) ;
	}

	// ----------------
	// --- 左右キー
	if ( !isPush.left && !isPush.right )		// --- 左、右キーどちらも押されていない場合
	{
		if ( moveInfo.moveSpeed.x < 0 )
			CheckLimitAndPlus( CLAPMODE_BELOW, &moveInfo.moveSpeed.x, downPower, 0.0f, 0.0f ) ;
		else if ( moveInfo.moveSpeed.x > 0 )
			CheckLimitAndPlus( CLAPMODE_EXCEED, &moveInfo.moveSpeed.x, -downPower, 0.0f, 0.0f ) ;
	}

	// ------------------------------ キーを押してない時
	if((g_keyBuf == 0) && (pad.pushKey == 0) && (animendflg)){
		if ( mode != e_pMode_Fall )
			mode = e_pMode_Wait_Init ;	// 「待機」モード変更
	}

	// --- 移動量をセット
	if (HitCheck_Player_To_Player() == TRUE)
		movepos = moveInfo.moveSpeed ;

	return e_PlayerWalk ;
}

/* ------------------------------------------------------------
					PlayerPush関数
===============================================================
	押し出しアニメーション処理処理
	引数：なし
	戻り値：int(押し出しアニメーション[始]の識別子)
--------------------------------------------------------------- */
int Player::PlayerPush()
{
//	printf( "PlayerPush\n" ) ;

	animendflg = FALSE ;
	if(animendflg == FALSE){
		playTime = 0.0f ;
	}

	if(superflg == TRUE)
		return e_PlayerSpecialAttack_In ;

//	mode = 2 ;

	return e_PlayerPush_In ;
}

/* ------------------------------------------------------------
					PlayerPress関数
===============================================================
	のけぞりアニメーション処理
	引数：なし
	戻り値：int(のけぞりアニメーション[始]の処理)
--------------------------------------------------------------- */
int Player::PlayerPress()
{
	animendflg = FALSE ;
	if(animendflg == FALSE){
		playTime = 0.0f ;
	}

//	mode = 3 ;

	return e_PlayerPress_In ;
}

/* ------------------------------------------------------------
					PlayerFall関数
===============================================================
	落下時の処理
	引数：なし
	戻り値：int
--------------------------------------------------------------- */
int Player::PlayerFall()
{
	movepos.y -= this->fallSpeed ;

	// もし落下判定地点まで落ちたら
	if( (pos.y < deathHeight) && (Map.c_collapse == 0)){
		mode = e_pMode_Death ;		// 「死亡」モード変更
	}

	return 0 ;
}

/* ------------------------------------------------------------
					PlayerDeath関数
===============================================================
	死亡時の処理
	引数：なし
	戻り値：int
--------------------------------------------------------------- */
int Player::PlayerDeath()
{
	int		dropNum				= fallDotMinus ;	// --- 落とすココナッツの数( 計算に使う方 )
	int		otCoconutNum		= 0 ;				// --- 落とすココナッツの数( 結果 )
	VECTOR	otRespawnPos[][2]	= {	
									//	1Pの復活地点							2Pの復活地点
									{	VGet( 0.0f,		100.0f,		-400.0f ),	VGet( 1800.0f,	100.0f,		-400.0f ) },
									{	VGet( 1400.0f,	100.0f,		0.0f ),		VGet( 0.0f,		100.0f,		-1500.0f ) },
									{	VGet( 0.0f,		100.0f,		-400.0f ),	VGet( 1800.0f,	100.0f,		-400.0f ) },
								  } ;

	// --- 自身の落下ポイント加算( これ分かりづらい )
	this->pushscore++ ;

	// --- 相手がスーパー状態だと、落とすココナッツの数は減るようにする
	for ( int i = PLAYER_ONE ; i <= PLAYER_TWO ; i++ )
	{	
		// --- 自身は除く
		if ( playerno == i )
			continue ;

		if ( g_Player[ i ].superflg )
		{
			dropNum = 1 ;
			break ;
		}
	}

	// --- チャージ分を減少させる
	CheckLimitAndPlus<int>( CLAPMODE_EXCEED, &dotcharge, -dropNum, 0, 0 ) ;

	// --- 獲得ココナッツ数をMAX減らせる場合
	if ( dotscore - dropNum >= 0 )
	{
		otCoconutNum	= dropNum ;
		dotscore		-= dropNum ;
	}
	// --- MAXの数分減らせない場合
	else
	{
		otCoconutNum	= dotscore ;
		dotscore		= 0 ;
	}

	// --- 落とすココナッツのエフェクトを生成
	EffectGenerator( VGet( this->pos.x, this->pos.y + 700, this->pos.z ), e_Effect_Drop_Coconuts, otCoconutNum ) ;

	// --- ステージ、プレイヤーに応じて復活場所をセット
	this->pos = otRespawnPos[ Map.mapswitch - 1 ][ this->playerno ] ;
	this->dir = VGet( 0.0, 0.0f, 0.0f ) ;

	// --- ステータスのセット
	isInvincible	= TRUE ;
	reviveflg		= TRUE ;
	mode			= e_pMode_Revive ;		// --- モードを復活にセット
	nowanim			= e_PlayerWait ;		// --- 待機アニメにセット
	playTime		= 0.0f ;

	ResetVector ( moveInfo.moveSpeed ) ;	// --- 移動量初期化

	setAnim( nowanim ) ;

//	mode = 5 ;

	return 0 ;
}

/* ------------------------------------------------------------
					PlayerRevive関数
===============================================================
	復活時の処理
	引数：なし
	戻り値：int
--------------------------------------------------------------- */
int Player::PlayerRevive()
{

	return 0 ;
}

/* ------------------------------------------------------------
					PlayerCatch関数
===============================================================
	ドット取得時の処理(DotItemで呼出)
	引数：なし
	戻り値：int
--------------------------------------------------------------- */
int Player::PlayerCatch()
{
	dotcharge++ ;
	if( dotcharge >= dotChargeMax )
	{
		dotcharge = 0 ;
		supertime = GetNowCount() ;
		superendtime = ( supertime + 10000 ) ;
		PlayerSuper() ;
	}

	dotscore++ ;

//	printf( "%d\n" , dotcharge ) ;

//	mode = 7 ;

	SOUND_INFO *otSP = &g_soundHandle[ e_Sound_DoteatSE ] ;	// --- ドット取得音
	PlaySoundMem( otSP->soundHandle, otSP->playType ) ;

	return 0 ;
}

/* ------------------------------------------------------------
					PlayerSuper関数
===============================================================
	強化状態になった時の処理
	引数：なし
	戻り値：int
--------------------------------------------------------------- */
int Player::PlayerSuper()
{
	superflg = TRUE ;
	isInvincible = TRUE ;	// --- 無敵化

//	setModelAndAnim( g_mv1Handle[ e_Model_SuperPacMan ], FALSE, VGet( 0.3f, 0.3f, 0.3f ) ) ;

//	size = VGet(1.25f,1.25f,1.25f) ;

	SOUND_INFO *otSP = &g_soundHandle[ e_Sound_PowerUpSE ] ;		// --- パワーアップ音
	PlaySoundMem( otSP->soundHandle, otSP->playType ) ;

	SetSoundVolumeNum( 100, otSP ) ;								// --- ボリューム 100セット

	return 0 ;
}

/*
TBLJP PlyActTbl[ ] = {
	Player::PlayerAction ,		// 0
	PlayerAnim ,				// 1
	PlayerWait ,				// 2
	PlayerMove ,				// 3
	PlayerPush ,				// 4
	PlayerPress ,				// 5
	PlayerFall ,				// 6
	PlayerDeath ,				// 7
	PlayerRevive ,				// 8
	PlayerCatch ,				// 9
	PlayerSuper ,				//10
} ;


void PlayerActionChange( )
{
	switch( mode )
	{
		case 0 :
			break ;

		case 1 :
			break ;

		case 2 :
			break ;

		case 3 :
			break ;

	}
}
*/

// ========================================================================================================== Hit Check P Attack
// ---------------------------------------------------------------------------------
//
//				プレイヤーの攻撃判定とヒットチェックを行うメソッド
//				モデルと球なので、そんなに難しいことはしていない。
//
// ---------------------------------------------------------------------------------
//						帰り値 : 0〜3	- 被弾時にのけぞる方向を表す
//								 -1		- 何も無し
// ---------------------------------------------------------------------------------
int Player::HitCheck_pAttack()
{
	int	isHit	= FALSE ;						// --- 被弾した、というフラグ
	int	otID	= this->playerno ;				// --- プレイヤーのID( なんで1から？ )
	MV1_COLL_RESULT_POLY_DIM	HitDim ;		// --- モデルと球のヒットチェック結果格納変数
	Player *otPac ;								// --- 相手の情報を攻撃情報を保有するためのポインタ

	for ( int i = 0 ; i < 2 ; i++ )
	{
		if ( i == otID )					// --- 自身は除く
			continue ;

		// --- 情報の更新
		otPac = & g_Player[i] ;
		MV1SetScale( otPac->modelHandle, otPac->size ) ;
		MV1SetPosition( otPac->modelHandle, otPac->pos ) ;
		MV1SetRotationXYZ( otPac->modelHandle, VGet( 1.57f * otPac->dir.x, 1.57f * otPac->dir.y, 1.57f * otPac->dir.z ) ) ;
		MV1RefreshCollInfo( otPac->modelHandle ) ;

		if ( !g_Player[i].atkInfo.isValid )		// --- 攻撃判定有効フラグがFALSEの場合、無視する
			continue ;

		// --- モデルと攻撃判定の球でヒットチェック
		HitDim = MV1CollCheck_Sphere( g_Player[ otID ].modelHandle, -1, otPac->atkInfo.atkPos, otPac->atkInfo.atkR ) ;

		if( HitDim.HitNum != 0 )
		{
			isHit = TRUE ;

			if( CheckSoundMem( g_soundHandle[e_Sound_BattlePushSE].soundHandle ) == 0 )
			{
				SOUND_INFO *otSP = &g_soundHandle[ e_Sound_BattlePushSE ] ;	// --- 押し出し音
				PlaySoundMem( otSP->soundHandle, otSP->playType ) ;
				SetSoundVolumeNum( 100, otSP ) ;								// --- ボリューム 100セット
			}
			break ;
		}
		else
			MV1CollResultPolyDimTerminate( HitDim );	// --- ヒットなしの場合、動的に取得した変数を消去
	}

	if ( isHit )
	{
		// --- 現在の向きを逆にした数値を帰す
		if ( otPac->dir.y >= 2 )
			return (int)otPac->dir.y - 2 ; 
		else
			return (int)otPac->dir.y + 2 ; 
	}

	return -1 ;
}

// ========================================================================================================== HitCheck Player To Player
// ---------------------------------------------------------------------------------
//
//					プレイヤー同士のヒットチェックをするメソッド
//					二線分間の距離を見る
//
// ---------------------------------------------------------------------------------
//						帰り値 :	TRUE  - ぶつかっていない
//									FALSE - プレイヤーとぶつかっている
// ---------------------------------------------------------------------------------
int Player::HitCheck_Player_To_Player( )
{
	int modelID = this->playerno ;		// --- 自分のID保存
	Player *otherPlayer ;				// --- 相手の情報格納
	VECTOR LineMyse, LineMyse_Under, LineOp_Top, LineOp_Under ;		// --- 線分情報格納
	VECTOR LengthCK[4] ;				// --- 重なりチェック用
	float length ;						// --- 二線分間の距離格納
	float depty_length ;				// --- 二線分間の距離格納
	const float offset = 100.0f ;		// --- オフセット
	const float distance = 200.0f ;		// --- ヒットチェック距離
	int c_length = 0 ;

	// --- 初期セット
	LineMyse = VGet( 0.0f, 0.0f, 0.0f ) ;
	LineMyse_Under = VGet( 0.0f, 0.0f, 0.0f ) ;
	LineOp_Top = VGet( 0.0f, 0.0f, 0.0f ) ;
	for ( int i = 0 ; i < 4 ; i++ )
		LengthCK[i] = VGet( 0.0f, 0.0f, 0.0f ) ;

	for ( int con = 0 ; con < 4 ; con++ )
	{
		switch ( con )
		{
			case 0 :		// --- 下向き
				LengthCK[con] = this->pos ;
				LengthCK[con].z = this->pos.z - offset ;
				break ;

			case 1 :		// --- 左向き
				LengthCK[con] = this->pos ;
				LengthCK[con].x = this->pos.x - offset ;
				break ;

			case 2 :		// --- 上向き
				LengthCK[con] = this->pos ;
				LengthCK[con].z = this->pos.z + offset ;
				break ;

			case 3 :		// --- 右向き
				LengthCK[con] = this->pos ;
				LengthCK[con].x = this->pos.x + offset ;
				break ;
		}
		if ( con == (int)this->dir.y )
		{
			LineMyse = LengthCK[con] ;		// --- 向いているときの点座標
		}
	}

	if ( this->playerno == 0 )		// --- 1Pだったら
	{
		otherPlayer = &g_Player[1] ;						// --- 2P情報の更新
		LineOp_Under = otherPlayer->pos ;					// --- 相手の下の座標
		LineOp_Top = otherPlayer->pos ;						// --- 相手の座標入れる
		LineOp_Top.y = otherPlayer->pos.y + 200.0f ;		// --- ｙ座標は200足す
	}else{
		otherPlayer = &g_Player[0] ;						// --- 1P情報の更新
		LineOp_Under = otherPlayer->pos ;					// --- 相手の下の座標
		LineOp_Top = otherPlayer->pos ;						// --- 相手の座標入れる
		LineOp_Top.y = otherPlayer->pos.y + 200.0f ;		// --- ｙ座標は200足す
	}

	// 二線分と最近点の距離を求めlengthに格納
	length = Segment_Point_MinLength( LineOp_Top, LineOp_Under, LineMyse ) ;
	for ( int ele = 0 ; ele < 4 ; ele++ )
	{
		// --- 自分と相手が重なったかのチェック
		depty_length = Segment_Point_MinLength( LineOp_Top, LineOp_Under, LengthCK[ele] ) ;
		if ( depty_length < distance )
			c_length ++ ;		// --- インクリメント
	}
	// --- 100.0fより小さい相手と重なっていないとFALSE
	if ( (length < distance) && (c_length < 4) )
	{
		moveInfo.moveSpeed = SetVector( 0.0f ) ;	// --- 氷ステージ用慣性無視

		return FALSE ;		// 当たってるよ
	}
	else
		return TRUE ;		// 動けるよ

	return -1 ;
}


// ========================================================================================================== Change Emi Power
// ---------------------------------------------------------------------------------
//
//						「emiPower」をいじるためだけの関数
//				なお、加算値、MAX値などはInitStates()でやってるよ。
//			
// ---------------------------------------------------------------------------------
int Player::ChangeEmiPower()
{
	// --- スーパー状態であるならばMAX[0]〜MAX[1]の間をうろうろさせる
	if ( this->superflg )
	{
		// --- 上昇処理
		if ( emiPower.plus > 0 )
		{
			if ( !CheckLimitAndPlus<float>( CLAPMODE_BELOW, &emiPower.now, emiPower.plus, emiPower.max[1], emiPower.max[1] ) )
				emiPower.plus *= -1.0f ;
		}
		// --- 下降処理
		else
		{
			if ( !CheckLimitAndPlus<float>( CLAPMODE_EXCEED, &emiPower.now, emiPower.plus, emiPower.max[0], emiPower.max[0] ) )
				emiPower.plus *= -1.0f ;
		}

	}
	// --- 非スーパー状態であるならば、MIN[0]まで下げる
	else if ( emiPower.now != emiPower.min[0] )
	{
		if ( emiPower.plus > 0 )
			emiPower.plus *= -1.0f ;

		CheckLimitAndPlus<float>( CLAPMODE_EXCEED, &emiPower.now, emiPower.plus, emiPower.min[0], emiPower.min[0] ) ;
	}

	MV1SetMaterialEmiColor( this->modelHandle, 0, GetColorF( emiPower.now, emiPower.now, emiPower.now, 1.0f ) ) ;		// --- フラッシュセット

	return 0 ;
}

// ========================================================================================================== Hit Check Return Move
// ---------------------------------------------------------------------------------
//
//					ヒットチェック、実移動を処理するメソッド
//				壁部分、床部分ごとにメソッドを分けるべきか悩んでいる。
//
// ---------------------------------------------------------------------------------
//						帰り値 :	TRUE  - 成功
//									FALSE - 失敗
// ---------------------------------------------------------------------------------
VECTOR Player::HitCheck_ReturnMove( VECTOR a_moveVec )
{
	// ======================
	//		変数宣言地帯
	// ======================
	int footNum = 0 ;											// --- ヒットした足元オブジェクトの数
	int HitObjNumber[ COLLOBJ_MAX ] ;							// --- ヒットしたオブジェクトのナンバー( デバッグ用である )
	int	FootObjNumber[ PLAYER_MAX_HITCOLL ] ;					// --- 足元チェックを行うオブジェクトの番号を格納( 周辺取得時に使用 )
	int	hitNum = 0 ;											// --- ヒットしたオブジェクトの数
	MV1_COLL_RESULT_POLY_DIM	HitDim[ COLLOBJ_MAX ] ;			// --- プレイヤーの周囲に存在するポリゴンを検出した際の結果を格納する
	MV1_COLL_RESULT_POLY		*Kabe[ PLAYER_MAX_HITCOLL ] ;	// --- 壁
	MV1_COLL_RESULT_POLY		*Yuka[ PLAYER_MAX_HITCOLL ] ;	// --- 床
	MV1_COLL_RESULT_POLY		*Poly ;							// --- ポリゴンの構造体にアクセスするために使用するポインタ
	int	KabeNum = 0 ;											// --- 察知した壁の数
	int	YukaNum = 0 ;											// --- 察知した床の数
	VECTOR	oldPos = pos ;										// --- 現在位置
	VECTOR	nowPos = VAdd( pos, a_moveVec ) ;					// --- 移動後の位置

	// ====================================================================================== SEARCH
	// ====================================================
	//						周辺探索
	// ====================================================
	// プレイヤーの周囲にあるコリジョンモデルのポリゴンを取得する
	// ( 検出する範囲は移動距離も考慮する )

	Scaffold *otScaf = 0 ;

	for ( int i = 0 ; i < COLLOBJ_MAX ; i++ )
	{
		if ( i < (ELE_X_1 * ELE_Y_1) )											// --- 足場(Stage1)セット
			otScaf = &Scaf1[ i ] ;

		else if ( i < ((ELE_X_1 * ELE_Y_1) + (ELE_X_2 * ELE_Y_2)) )				// --- 足場(Stage2_t)セット
			otScaf = &Scaf2_t[ i - (ELE_X_1 * ELE_Y_1) ] ;
		
		else if ( i < ((ELE_X_1 * ELE_Y_1) + ((ELE_X_2 * ELE_Y_2) * 2)) )		// --- 足場(Stage2_u)セット
			otScaf = &Scaf2_u[ i - ((ELE_X_1 * ELE_Y_1) + (ELE_X_2 * ELE_Y_2)) ] ;

		else																	// --- 足場(Stage3)セット
			otScaf = &Scaf3[ i - (ELE_X_1 * ELE_Y_1 + ((ELE_X_2 * ELE_Y_2) * 2)) ] ;

		// --- 位置を再読み込み( ここじゃないと何故かおかしくなる・・・何故？ )
		otScaf->setStatesFromParent() ;
		MV1RefreshCollInfo( otScaf->col.handle ) ;

		// --- もし当たり判定使用フラグがFALSEなら、このオブジェクトは無視する
		if ( !otScaf->col.isValid )
			continue ;

		HitDim[ hitNum ] = MV1CollCheck_Sphere( otScaf->col.handle, -1, oldPos, 600.0f ) ;

		// --- 当たったら次の床ポリチェックにかける
		if( HitDim[ hitNum ].HitNum != 0 )
		{
			// --- 現在踏んでいる足場の要素数を格納
			HitObjNumber[ footNum ] = i ;
			footNum++ ;

			hitNum++;
		}
		else
			MV1CollResultPolyDimTerminate( HitDim[ hitNum ] );
	}

	// --- 検知オブジェクト数が1に満たない場合落下判定を付与
	if ( hitNum <= 0 )
	{
		if ( mode < e_pMode_Fall_Init )
			this->mode = e_pMode_Fall_Init ;

		pos = nowPos ;
		return a_moveVec ;
	}

	// ====================================================================================== FLOOR OR WALL?( 僕はWALLがすき )
	// ====================================================
	//					壁床振り分け
	// ====================================================

	// --- 検出されたポリゴンが壁ポリゴン( ＸＺ平面に垂直なポリゴン )か床ポリゴン( ＸＺ平面に垂直ではないポリゴン )かを判断する
	for ( int i = 0 ; i < hitNum ; i++ )
	{
		// --- 検出されたポリゴンの数だけ繰り返し
		for( int j = 0 ; j < HitDim[ i ].HitNum ; j ++ )
		{
			// --- ＸＺ平面に垂直かどうかはポリゴンの法線のＹ成分が０に限りなく近いかどうかで判断する
			if( HitDim[ i ].Dim[ j ].Normal.y < 0.000001f && HitDim[ i ].Dim[ j ].Normal.y > -0.000001f )
			{
				// --- 壁ポリゴンと判断された場合でも、プレイヤーのＹ座標＋1.0ｆより高いポリゴンのみ当たり判定を行う
				if( HitDim[ i ].Dim[ j ].Position[ 0 ].y > oldPos.y + 1.0f ||
					HitDim[ i ].Dim[ j ].Position[ 1 ].y > oldPos.y + 1.0f ||
					HitDim[ i ].Dim[ j ].Position[ 2 ].y > oldPos.y + 1.0f )
				{
					// --- ポリゴンの数が列挙できる限界数に達していなかったらポリゴンを配列に追加
					if( KabeNum < PLAYER_MAX_HITCOLL )
					{
						Kabe[ KabeNum ] = &HitDim[ i ].Dim[ j ] ;	// --- ポリゴンの構造体のアドレスを壁ポリゴンポインタ配列に保存する

						KabeNum ++ ;	// --- 壁ポリゴンの数を加算する
					}
				}
			}
			else
			{
				// --- ポリゴンの数が列挙できる限界数に達していなかったらポリゴンを配列に追加
				if( YukaNum < PLAYER_MAX_HITCOLL )
				{
					FootObjNumber[ YukaNum ] = HitObjNumber[ i ] ;	// --- 足元オブジェクトの要素数を取得( 併合移動処理に使う )

					Yuka[ YukaNum ] = &HitDim[ i ].Dim[ j ] ;		// --- ポリゴンの構造体のアドレスを床ポリゴンポインタ配列に保存する

					YukaNum++ ;			//  -- 床ポリゴンの数を加算する
				}
			}
		}
	}

	// ====================================================================================== FLOOR
	// ====================================================
	//			床ポリゴンとのヒットチェック
	//				(MoveVec.Y のみ変更)
	// ====================================================
	BOOL	hitFootFlg = FALSE ;

	if( YukaNum != 0 )
	{
		BOOL	footMoveFlg		= FALSE ;		// --- 動いている足場に乗った時のフラグ
		float	MaxY_Movement	= -100000 ;		// --- 最上層位置_動いている足場(この初期数より低いことは...無い？)
		float	MaxY			= 0 ;			// --- 最上層位置
		int		mostHitScaf		= 0 ;			// --- 最もヒット数が多かった足場のナンバー( そのループの "i" を格納 )

		float	sizeP			= this->size_fallWidth ;	// --- 足元の中心からの距離分( 数値を大きくすると足元の判定が広くなる(落ちづらさUP) )
		float	checkDis		= 100.0f ;					// --- 足元チェックの最大距離( 下に伸ばす距離 )
		float	nowMax			= -100000 ;					// --- 最高地点格納変数。この初期数より高くはなるよね？
		int		booGoukeiNum	= 0 ;						// --- BOOL 型の合計数を格納するシーンで使用

		Scaffold	*lastScaf	= 0 ;						// --- 足場のポインタ( 最後に使用 )
		int			hitMax		= 0 ;						// --- 最大ヒット数格納変数

		float		checkDis_Death	= nowPos.y - deathHeight ;				// --- 志望ポイントまでの探索距離
		BOOL		isKakuteiDeath	= TRUE ;								// --- 死亡が確定しているとTRUEになる。
		SOUND_INFO	*otSP			= &g_soundHandle[ e_Sound_FallSE ] ;	// --- 落下音

		HITRESULT_LINE oneFHR_LL ;			// --- 足元チェック_リザルト_左上			 		
		HITRESULT_LINE oneFHR_LR ;			// --- 足元チェック_リザルト_左右
		HITRESULT_LINE oneFHR_RL ;			// --- 足元チェック_リザルト_右左			 		
		HITRESULT_LINE oneFHR_RR ;			// --- 足元チェック_リザルト_右右

		HITRESULT_LINE oneMHR ;				// --- 足元チェック_リザルト_中央
		HITRESULT_LINE oneMHR_LL ;			// --- 足元チェック_リザルト_左
		HITRESULT_LINE oneMHR_RR ;			// --- 足元チェック_リザルト_右
		HITRESULT_LINE oneMHR_UU ;			// --- 足元チェック_リザルト_奥
		HITRESULT_LINE oneMHR_DD ;			// --- 足元チェック_リザルト_手前
		HITRESULT_LINE oneMHR_UL ;			// --- 足元チェック_リザルト_左上
		HITRESULT_LINE oneMHR_UR ;			// --- 足元チェック_リザルト_右上
		HITRESULT_LINE oneMHR_DL ;			// --- 足元チェック_リザルト_左下
		HITRESULT_LINE oneMHR_DR ;			// --- 足元チェック_リザルト_右下

		VECTOR lookLine[][2] = {	
							{VGet( nowPos.x, nowPos.y + 59, nowPos.z ),					VGet( nowPos.x, nowPos.y - 50, nowPos.z )},					// 通常
							{VGet( nowPos.x - sizeP, nowPos.y + 59, nowPos.z ),			VGet( nowPos.x - sizeP, nowPos.y - 50, nowPos.z )},			// LL(左)
							{VGet( nowPos.x + sizeP, nowPos.y + 59, nowPos.z ),			VGet( nowPos.x + sizeP, nowPos.y + 59, nowPos.z )},			// RR(右)
							{VGet( nowPos.x, nowPos.y + 59, nowPos.z + sizeP ),			VGet( nowPos.x, nowPos.y - 50, nowPos.z + sizeP )},			// UU(上)
							{VGet( nowPos.x, nowPos.y + 59, nowPos.z - sizeP ),			VGet( nowPos.x, nowPos.y - 50, nowPos.z - sizeP )},			// DD(下)
							{VGet( nowPos.x - sizeP, nowPos.y + 59, nowPos.z + sizeP ), VGet( nowPos.x - sizeP, nowPos.y - 50, nowPos.z + sizeP )},	// UL(左上)
							{VGet( nowPos.x + sizeP, nowPos.y + 59, nowPos.z + sizeP ), VGet( nowPos.x + sizeP, nowPos.y - 50, nowPos.z + sizeP )},	// UR(右上)
							{VGet( nowPos.x - sizeP, nowPos.y + 59, nowPos.z - sizeP ), VGet( nowPos.x - sizeP, nowPos.y - 50, nowPos.z - sizeP )},	// DL(左下)
							{VGet( nowPos.x + sizeP, nowPos.y + 59, nowPos.z - sizeP ), VGet( nowPos.x + sizeP, nowPos.y - 50, nowPos.z - sizeP )},	// DR(右下)
		} ;

		for( int i = 0 ; i < YukaNum ; i ++ )
		{
			Poly = Yuka[ i ] ;

			// ====================================================
			//		音を鳴らすために、死ぬか死なないかを判定
			// ====================================================
			// --- ヒット情報をチェック
			if ( isKakuteiDeath )
			{
				oneFHR_LL = HitCheck_Line_Triangle( VGet( nowPos.x - sizeP, nowPos.y + 59, nowPos.z - sizeP ),
				 				VGet( nowPos.x - sizeP, nowPos.y - checkDis_Death, nowPos.z - sizeP ), Poly->Position[0], Poly->Position[1], Poly->Position[2] ) ;
				oneFHR_LR = HitCheck_Line_Triangle( VGet( nowPos.x - sizeP, nowPos.y + 59, nowPos.z + sizeP ),
				 				VGet( nowPos.x - sizeP, nowPos.y - checkDis_Death, nowPos.z + sizeP ), Poly->Position[0], Poly->Position[1], Poly->Position[2] ) ;
				oneFHR_RL = HitCheck_Line_Triangle( VGet( nowPos.x + sizeP, nowPos.y + 59, nowPos.z - sizeP ),
					 			VGet( nowPos.x + sizeP, nowPos.y - checkDis_Death, nowPos.z - sizeP ), Poly->Position[0], Poly->Position[1], Poly->Position[2] ) ;
				oneFHR_RR = HitCheck_Line_Triangle( VGet( nowPos.x + sizeP, nowPos.y + 59, nowPos.z + sizeP ),
								VGet( nowPos.x + sizeP, nowPos.y - checkDis_Death, nowPos.z + sizeP ), Poly->Position[0], Poly->Position[1], Poly->Position[2] ) ;

				// --- 何かと接触確定した場合、音は鳴らさない
				if ( (oneFHR_LL.HitFlag) || (oneFHR_LR.HitFlag) || (oneFHR_RL.HitFlag) || (oneFHR_RR.HitFlag) )
					isKakuteiDeath = FALSE ;
			}

			// ====================================================
			//				落ちるかどうかの判断を行う
			// ====================================================
			// --- ヒット情報をチェック
			oneFHR_LL = HitCheck_Line_Triangle( VGet( nowPos.x - sizeP, nowPos.y + 59, nowPos.z - sizeP ),
							VGet( nowPos.x - sizeP, nowPos.y - checkDis, nowPos.z - sizeP ), Poly->Position[0], Poly->Position[1], Poly->Position[2] ) ;
			oneFHR_LR = HitCheck_Line_Triangle( VGet( nowPos.x - sizeP, nowPos.y + 59, nowPos.z + sizeP ),
							VGet( nowPos.x - sizeP, nowPos.y - checkDis, nowPos.z + sizeP ), Poly->Position[0], Poly->Position[1], Poly->Position[2] ) ;
			oneFHR_RL = HitCheck_Line_Triangle( VGet( nowPos.x + sizeP, nowPos.y + 59, nowPos.z - sizeP ),
							VGet( nowPos.x + sizeP, nowPos.y - checkDis, nowPos.z - sizeP ), Poly->Position[0], Poly->Position[1], Poly->Position[2] ) ;
			oneFHR_RR = HitCheck_Line_Triangle( VGet( nowPos.x + sizeP, nowPos.y + 59, nowPos.z + sizeP ),
											VGet( nowPos.x + sizeP, nowPos.y - checkDis, nowPos.z + sizeP ), Poly->Position[0], Poly->Position[1], Poly->Position[2] ) ;

			// --- 当たらなかったら次のループへ
			if ( (!oneFHR_LL.HitFlag) && (!oneFHR_LR.HitFlag) && (!oneFHR_RL.HitFlag) && (!oneFHR_RR.HitFlag) )
				continue ;

			// --- 最も高い点を見つける
			if ( (nowMax < oneFHR_LL.Position.y) && (oneFHR_LL.HitFlag) )
				nowMax = oneFHR_LL.Position.y ;
			if ( (nowMax < oneFHR_LR.Position.y) && (oneFHR_LR.HitFlag) )
				nowMax = oneFHR_LR.Position.y ;
			if ( (nowMax < oneFHR_RL.Position.y) && (oneFHR_RL.HitFlag) )
				nowMax = oneFHR_RL.Position.y ;		
			if ( (nowMax < oneFHR_RR.Position.y) && (oneFHR_RR.HitFlag) )
				nowMax = oneFHR_RR.Position.y ;
			
			// --- 既に当たったポリゴンがあり、且つ今まで検出した床ポリゴンより低い場合は何もしない
			if( hitFootFlg == TRUE && MaxY > nowMax )
				continue ;

			hitFootFlg = TRUE ;			// --- ポリゴンに当たったフラグを立てる

			MaxY = nowMax ;				// --- 接触したＹ座標を保存する

			// ====================================================
			//				床オブジェとの併合移動処理
			// ====================================================
			if ( footMoveFlg )
				continue ;

			// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
			// --- プレイヤーの中心と周辺８つに判定があるように設計
			//
			//                  *---*---*
			//                  |   |   |
			//                  *---@---*
			//                  |   |   |
			//                  *---*---*
			//
			//	合計9箇所の足元ブロックを見て、ヒットした回数が
			//	最も多かったブロックの移動量を反映させる。
			//
			//	( 例 )	Xmove - 30  のブロックと 1箇所接地
			//			Xmove - -30 のブロックと 3箇所接地
			//				の場合はXの移動量が「-30」となる
			//
			//	ここ頭悪いね・・・でもこうするしか無かったの許して
			//
			// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
			oneMHR		= HitCheck_Line_Triangle( lookLine[0][0], lookLine[0][1], Poly->Position[0], Poly->Position[1], Poly->Position[2] ) ;
			oneMHR_LL	= HitCheck_Line_Triangle( lookLine[1][0], lookLine[1][1], Poly->Position[0], Poly->Position[1], Poly->Position[2] ) ;
			oneMHR_RR	= HitCheck_Line_Triangle( lookLine[2][0], lookLine[2][1], Poly->Position[0], Poly->Position[1], Poly->Position[2] ) ;
			oneMHR_UU	= HitCheck_Line_Triangle( lookLine[3][0], lookLine[3][1], Poly->Position[0], Poly->Position[1], Poly->Position[2] ) ;
			oneMHR_DD	= HitCheck_Line_Triangle( lookLine[4][0], lookLine[4][1], Poly->Position[0], Poly->Position[1], Poly->Position[2] ) ;
			oneMHR_UL	= HitCheck_Line_Triangle( lookLine[5][0], lookLine[5][1], Poly->Position[0], Poly->Position[1], Poly->Position[2] ) ;
			oneMHR_UR	= HitCheck_Line_Triangle( lookLine[6][0], lookLine[6][1], Poly->Position[0], Poly->Position[1], Poly->Position[2] ) ;
			oneMHR_DL	= HitCheck_Line_Triangle( lookLine[7][0], lookLine[7][1], Poly->Position[0], Poly->Position[1], Poly->Position[2] ) ;
			oneMHR_DR	= HitCheck_Line_Triangle( lookLine[8][0], lookLine[8][1], Poly->Position[0], Poly->Position[1], Poly->Position[2] ) ;

			// --- 何一つ当たらなかったら AND 最上層位置より低い場合スルー
			if ( (!oneMHR_LL.HitFlag) && (!oneMHR_RR.HitFlag) && (!oneMHR_UU.HitFlag) && (!oneMHR_DD.HitFlag) &&
				 (!oneMHR_UL.HitFlag) && (!oneMHR_UR.HitFlag) && (!oneMHR_DL.HitFlag) && (!oneMHR_DR.HitFlag) &&
				 (!oneMHR.HitFlag) && (MaxY_Movement > oneMHR.Position.y) )
				continue ;

			footMoveFlg = TRUE ;
			MaxY_Movement = oneMHR.Position.y ;

			// --- ヒット件数をサーチ
			BOOL otSearch[9] = { oneMHR_LL.HitFlag, oneMHR_RR.HitFlag, oneMHR_UU.HitFlag, oneMHR_DD.HitFlag,
								 oneMHR_UL.HitFlag, oneMHR_UR.HitFlag, oneMHR_DL.HitFlag, oneMHR_DR.HitFlag,
								 oneMHR.HitFlag} ;
			booGoukeiNum = CountArrayInBool( otSearch ) ;

			// --- ヒット数の多さを更新した場合、新たな数値として格納する
			if ( booGoukeiNum >= mostHitScaf )
				mostHitScaf = i ;
		}

		// ------------------
		// --- 着地処理
		// ------------------
		if ( hitFootFlg )
		{
			// --- 新たに座標をセット
			if ( mode <= e_pMode_Move )
				nowPos.y = MaxY ;
			if ( mode == e_pMode_Fall )
			{
				nowPos.y = MaxY ;
				mode = e_pMode_Wait_Init ;			// --- モードを「待機」にセット
			}
			// --- ステージ切り替え後のプレイヤーアニメーション
			if ( land == TRUE )
			{
				if (nowanim != e_StageChange_2to3_End){
					mode = e_pMode_StageChange_2to3_End ;	// --- 着地アニメじゃなかったら着地モードにチェンジ
					printf( "flgon\n" ) ;
				}
			}
		}

		// ------------------
		// --- 落ち処理
		// ------------------
		if ( !hitFootFlg )
		{
			if ( mode < e_pMode_Fall_Init )
			{
				// --- 確定で死亡する場合、音を鳴らすよ
				if ( (isKakuteiDeath) && (this->pos.y <= 0.0f) )
					PlaySoundMem( otSP->soundHandle, otSP->playType ) ;

				mode = e_pMode_Fall_Init ;		// --- モードを「落下初期化」にセット
			}
		}

		hitMax = FootObjNumber[ mostHitScaf ] ;		// --- 最も接触部分が多かった足場の要素数を取得

		if ( hitMax < (ELE_X_1 * ELE_Y_1) )													// --- 足場(Stage1)
			lastScaf = &Scaf1[ hitMax ] ;
		else if ( hitMax < ((ELE_X_1 * ELE_Y_1) + (ELE_X_2 * ELE_Y_2)) )					// --- 足場(Stage2_t)
			lastScaf = &Scaf2_t[ hitMax - (ELE_X_1 * ELE_Y_1) ] ;	
		else if ( hitMax < ((ELE_X_1 * ELE_Y_1) + ((ELE_X_2 * ELE_Y_2) * 2)) )				// --- 足場(Stage2_u)
			lastScaf = &Scaf2_u[ hitMax - (ELE_X_1 * ELE_Y_1 + ELE_X_2 * ELE_Y_2) ] ;		
		else																				// --- 足場(Stage3)
			lastScaf = &Scaf3[ hitMax - (ELE_X_1 * ELE_Y_1 + ((ELE_X_2 * ELE_Y_2) * 2)) ] ;

		nowPos = VAdd ( nowPos, lastScaf->movepos ) ;
	}

	// ====================================================================================== WALL
	// ====================================================
	//			壁ポリゴンとのヒットチェック
	//				(MoveVec.X Z のみ変更)
	// ====================================================
	if ( KabeNum != 0 )
	{
		float pW = 70.0f ;		// --- カプセルサイズの半径
		float pH = 50.0f ;		// --- カプセルサイズの中心点からの距離(縦頂点)

		VECTOR zurasiPos = SetVector( 0.0f ) ;		// --- ずらし座標セット

		for ( int i = 0 ; i < KabeNum ; i++ )
		{
			Poly = Kabe[i] ;

			// --- 見ているベクトルの線分と壁をヒットチェック
			if ( !HitCheck_Capsule_Triangle( VGet( nowPos.x, nowPos.y + 60.0f, nowPos.z ), 
															VGet( nowPos.x, nowPos.y + 60.0f + pH, nowPos.z ), pW,
															Poly->Position[0], Poly->Position[1], Poly->Position[2] ) )
				continue ;
			
			// --- 法線の方向にプレイヤーを動かし、ヒットしない地点まで移動させる( 確定で一度処理を行う為、do~while()を使用する )
			do
			{
				zurasiPos.x += (Poly->Normal.x / 10) ;
				zurasiPos.z += (Poly->Normal.z / 10) ;

				// --- 0.1ずつ移動させた時の座標でチェックを行い、当たらなかった時の座標を新しいものにする
				if ( HitCheck_Capsule_Triangle( VGet( nowPos.x + zurasiPos.x, nowPos.y + 60.0f, nowPos.z + zurasiPos.z ), 
																VGet( nowPos.x + zurasiPos.x, nowPos.y + 60.0f + pH, nowPos.z + zurasiPos.z ), pW,
																Poly->Position[0], Poly->Position[1], Poly->Position[2] ) )
					continue ;

				nowPos = VAdd( nowPos, zurasiPos ) ;
				break ;
			}
			while ( 1 ) ;
		}
	}

	// --- 実移動
	VECTOR retNum = VGet( nowPos.x - pos.x, nowPos.y - pos.y, nowPos.z - pos.z ) ;

	// --- HitDim[] は動的に領域を得たので、元気にさようならしようね
	for ( int i = 0 ; i < hitNum ; i++ )
		MV1CollResultPolyDimTerminate( HitDim[ i ] ) ;

	return retNum ;
}



