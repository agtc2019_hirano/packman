/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: ScoreUI.h
	CREATE	: HIRANO
	
	------ Explanation of file ---------------------------------------------------------------------
       
	ScoreUIクラスの宣言

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

class ScoreUI : public UserInterface
{
	public :
		ScoreUI( ) ;		// コンストラクタ
		~ScoreUI( ) ;		// デストラクタ		
		
		void initStates() ;			// --- ステータス初期

		void UIDraw( int ) ;		// プレイヤーの１P・２P表示

		int plyScore_BeforeOneSec ;			// --- プレイヤーの１フレーム前のスコア
		c_countGroup<float> sizeChange ;	// --- サイズを変更する為のカウンタ

		BOOL	isSizeChangeNum ;			// --- 数字のサイズを変えるときにTRUEになる
		BOOL	isLastSpurt ;				// --- ラストスパート( 残り30秒 )を過ぎたかどうかのフラグ
} ;


	