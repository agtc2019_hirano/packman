/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: PadDate.h
	CREATE	: HIRANO
	
	------ Explanation of file ---------------------------------------------------------------------
       
	PadDateクラスの宣言

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

typedef struct
{
	int pushKey ;		// --- プッシュしているキー情報
	int pushTime ;		// --- その押した長さ

} PUSH_STATE ;

// --------------------------------------
//
//			パッドデータクラス
//
// --------------------------------------
class PadData
{
	public :
		PadData( ) ;		// コンストラクタ
		~PadData( ) ;		// デストラクタ

		static void UpDate( );		// ゲームパッドの入力の状態更新

		static const PUSH_STATE& GetButton( const int& t_padNum ) ;		// ボタンの入力状態取得

		static inline void SetPadNum( );	// パッドの接続数		
		static PUSH_STATE m_button[4];		// ゲームパッドの入力状態格納用変数

		static inline void SetPushTime( char a_padNum, int a_pushKey );		// 前回のキーをチェックして「押した長さ」をセットする
		inline char Get_PadNum() { return m_padNum ; }

		BOOL	Check1FramePressed( const int a_pNum, const int a_checkKey ) ;			// 押されているボタンが１フレーム目であるかどうかを帰す

private:
		static int m_trigger[4];			// ゲームパッドのトリガーの入力状態収納用変数

		static char m_padNum;				// 繋がってるゲームパッドの数

} ;

/* ------------------------------------------------------------
					SetPadNum関数
===============================================================
	パッドの接続数を取得
	引数：なし
	戻り値：なし
--------------------------------------------------------------- */
inline void PadData::SetPadNum( )
{
	if ( GetJoypadNum() > 0 )
	{
		m_padNum = static_cast<int>( GetJoypadNum() );
	}
	else
	{
		m_padNum = -1;
	}
}

/* ------------------------------------------------------------
					SetPushTime関数
===============================================================
	パッドの接続数を取得
	引数：なし
	戻り値：なし
--------------------------------------------------------------- */
inline void PadData::SetPushTime( char a_padNum, int a_pushKey )
{
	if ( m_button[ a_padNum ].pushKey == a_pushKey )	// --- 前回と一致で加算
		m_button[ a_padNum ].pushTime++ ;
	else if ( 0 == a_pushKey )							// --- 未操作 - 0
		m_button[ a_padNum ].pushTime = 0 ;
	else												// --- 前回と不一致で初期化
		m_button[ a_padNum ].pushTime = 1 ;
}

	