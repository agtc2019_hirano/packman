/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: Common.cpp
	
	------ Explanation of file ---------------------------------------------------------------------
       
	共通での処理、または汎用性がある関数や変数の実体宣言部

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
#include <windows.h>
#include <stdio.h>	// --- コンソール用

#include "Common.h"

/* ---------------------------------------------------------------------------------------

       グローバル変数＆オブジェクトの実体宣言

 --------------------------------------------------------------------------------------- */
//ConsoleWindow		g_cWin;											// --- コンソールウィンドウ
POINT				g_winSize ;										// --- 現在のウィンドウサイズの大きさ
c_countGroup<float>	g_loadCnt ;										// --- ロード完了のパーセンテージを格納した変数

PUSH_STATE			g_dataPadInput[4] ;								// --- パッドの入力情報( 一応要素は4つ作っておく )
SHADOW_INFO			g_shadowMap ;									// --- 影のハンドル

CameraMovement		g_camera ;										// --- カメラオブジェクト

float				g_gameSpeed ;									// --- ゲームスピード変数( デフォルトが 1.0 )

int					g_gameMode ;									// --- ゲームモード管理変数
int					g_keyBuf[ 256 ] ;								// --- キーの入力情報( 常時更新 )
MODEL_ANIM_INFO		g_mv1Handle[ e_Model_Total ] ;					// --- モデルハンドルの格納変数
GRAPH_INFO			g_graphHandle[ e_Image_Total ] ;				// --- 画像のハンドル
SOUND_INFO			g_soundHandle[ e_Sound_Total ] ;				// --- サウンドのハンドル格納構造体

GRAPH_INFO			g_graphHandle_LoadView[ e_imgLoadView_Total ] ;	// --- ロード画面用の画像ハンドル格納変数

Object g_Object[ USEOBJ_MAX ] ;										// --- 使用するオブジェクトを格納する配列

PadData Padinput ;													// --- パッド入力情報
Player  g_Player[2] ;												// --- プレイヤーオブジェクト
Player  *p_Player ;													// --- プレイヤーのポインタ

DotItem Dot1stg[ELE_X_1 * ELE_Y_1] ;								// --- ドットオブジェ
DotItem Dot2stg_t[ELE_X_2 * ELE_Y_2] ;								// --- ドットオブジェ
DotItem Dot2stg_u[ELE_X_2 * ELE_Y_2] ;								// --- ドットオブジェ
DotItem Dot3stg[ELE_X_3 * ELE_Y_3] ;								// --- ドットオブジェ

MapGeneration Map ;													// --- マップ生成
Scaffold Scaf1[ELE_X_1 * ELE_Y_1] ;									// --- 足場１
Scaffold Scaf2_t[ELE_X_2 * ELE_Y_2] ;								// --- 足場２上部
Scaffold Scaf2_u[ELE_X_2 * ELE_Y_2] ;								// --- 足場２下部
Scaffold Scaf3[ELE_X_3 * ELE_Y_3] ;									// --- 足場３

Timer Time[e_TimerTotal] ;											// --- タイマー

TimerUI					TimeUI[2] ;									// --- タイマーのUI
PlayerId				PlayerUI[2] ;								// --- プレイヤーの１P・２P表示
ScoreUI					ScoreBoard[2] ;								// --- スコアの１P・２P表示
UI_DotGaugeAndPlayerID	UI_DGauge_ID[2] ;							// --- ドットゲージ_プレイヤーIDのUI
UI_CountDownNumber		UI_CntDown ;								// --- カウントダウン用UI
UI_Warning				UI_War ;									// --- ワーニングUI
UI_SceneChange			UI_SChange ;								// --- シーンチェンジUI

int map1[ELE_X_1 * ELE_Y_1] = {	// ステージ1データ
	0,1,1,1,1,1,0,
	1,1,1,0,1,1,1,
	1,1,0,0,0,1,1,
	1,1,1,0,1,1,1,
	0,1,1,1,1,1,0
} ;

int map2_top[ELE_X_2 * ELE_Y_2] = {	// ステージ2上部データ
	1,1,1,1,1,1,1,1,1,1,1,1,1,
	1,1,1,0,1,0,1,1,0,1,1,1,1,
	1,0,1,1,1,1,0,1,1,0,1,0,1
} ;
int map2_under[ELE_X_2 * ELE_Y_2] = {	// ステージ2下部データ
	1,0,1,1,1,1,0,1,1,1,1,1,1,
	1,1,1,0,1,1,0,1,0,1,1,0,1,
	1,1,1,1,1,1,1,1,1,1,1,1,1
} ;
//int map_2[] = {
//	0,1,1,1,1,0,
//	1,1,1,0,1,1,
//	1,1,0,0,0,1,
//	1,1,1,0,1,1,
//	0,1,1,1,1,0
//} ;

int map3[ELE_X_3 * ELE_Y_3] = {	// ステージ3データ
	0,1,1,1,1,1,0,
	1,1,1,1,1,1,1,
	1,1,1,1,1,1,1,
	1,1,1,1,1,1,1,
	0,1,1,1,1,1,0
} ;

Effect		g_effect[ EFFECT_MAX ] ;					// --- 使用するエフェクトの格納先

DEBUGMODE_INFO	g_debInfo ;								// --- デバッグステータス


/* ---------------------------------------------------------------------------------------

       グローバル関数の実装部

 --------------------------------------------------------------------------------------- */



