/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	FILE	: Effect.cpp
	CREATE	: NOGUCHI
	
	------ Explanation of file ---------------------------------------------------------------------
       
	エフェクトクラス。

	このオブジェクトを実際にゲーム内で呼び起こすには「EffectGenerator()」を使う必要がある。
	座標、画像の定数を指定してあげよう。自動的にゼロサーチも行ってくれるぞ。

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

#include "Common.h"

// ---------------------------------------------------------------------------------
//									コンストラクタ
// ---------------------------------------------------------------------------------
Effect::Effect( )
{
	initStates() ;
}

// ---------------------------------------------------------------------------------
//									デストラクタ
// ---------------------------------------------------------------------------------
Effect::~Effect( )
{

}

// ============================================================================================================== Init States
// ---------------------------------------------------------------------------------
//
//							フィールドを初期化するメソッド
//
// ---------------------------------------------------------------------------------
int Effect::initStates()
{
	ResetVector( this->pos ) ;					// --- 座標

	this->handle	= -1 ;						// --- 画像ハンドル
	this->ltSpot	= c_fPoint( 0.0f ) ;		// --- 出力左上位置

	SecureZeroMemory( &this->efInfo, sizeof( EFFECT_INFO ) ) ;	// --- エフェクト情報

	memset( this->freeVar_f, 0, sizeof( float[16] ) ) ;			// --- 自由に使っていい配列( float )
	memset( this->freeVar_i, 0, sizeof( int[16] ) ) ;			// --- 自由に使っていい配列( int )

	this->isUse2D      = FALSE ;
	this->isUse3DModel = FALSE ;
	this->use3DModelID = -1 ;

	return 0 ;
}

// ============================================================================================================== Set Handle
// ---------------------------------------------------------------------------------
//
//							画像ハンドルのセッター
//					 成功で 0、失敗で -1 が帰ってくる。
//
// ---------------------------------------------------------------------------------
int Effect::setHandle( int arg_no )
{
	// --- 0より小さい値は受け付けない
	if ( arg_no < 0 )
		return -1 ;

	this->handle = arg_no ;

	return 0 ;
}

// ============================================================================================================== Set Effect ID
// ---------------------------------------------------------------------------------
//
//							エフェクトIDのセッター
//					 成功で 0、失敗で -1 が帰ってくる。
//
// ---------------------------------------------------------------------------------
int Effect::setEffectID( int arg_ID )
{
	// --- エフェクトIDがマイナス値だとエラー判定。
	if ( arg_ID <= -1 )
		return -1 ;

	this->effectID = arg_ID ;

	return 0 ;
}

// ============================================================================================================== E Action And Draw
// ---------------------------------------------------------------------------------
//
//						アクション関数 & 描画関数
//			
//			と謳っているが、実のところエフェクトのメインループ的なものである。
//				エフェクト呼び出し側が分かりやすいようこうしている。
//
// ---------------------------------------------------------------------------------
int Effect::eAction()
{
	// --- ゲームスピードを加える
	VECTOR newMoveVec = VGet( efInfo.moveVec.x * g_gameSpeed, efInfo.moveVec.y * g_gameSpeed, efInfo.moveVec.z * g_gameSpeed ) ;

	// --- エフェクトのIDごとに処理を変える
	switch ( this->effectID )
	{
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ // ---------------- e_Effect_Star
		//					エフェクト_星(テスト)					//
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
		case e_Effect_Star :
			this->pos = VAdd( this->pos,  g_Player[0].movepos ) ;
			this->pos = VAdd( this->pos, newMoveVec ) ;
			this->efInfo.moveVec.y -= 0.5f * g_gameSpeed ;
			this->efInfo.rotate.z += 1.0f * g_gameSpeed ;

			break ;

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ // ---------------- e_Effect_Push_Star
		//			エフェクト_プッシュ時の星エフェクト				//
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
		case e_Effect_Push_Star :
			this->pos = VAdd( this->pos, newMoveVec ) ;
			this->efInfo.rotate.z += 0.4f * g_gameSpeed ;
			break ;

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ // ---------------- e_Effect_Push_Star
		//			エフェクト_スーパー時の煌きエフェクト				//
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
		case e_Effect_Super_Shine :
			this->pos = VAdd( this->pos, newMoveVec ) ;
			this->efInfo.rotate.z += 0.4f * g_gameSpeed ;
			break ;

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ // ---------------- e_Effect_Drop_Coconuts
		//			エフェクト_落としたココナッツエフェクト			//
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
		case e_Effect_Drop_Coconuts :
			this->pos = VAdd( this->pos, newMoveVec ) ;
			this->efInfo.rotate.x += 0.05f * g_gameSpeed ;
			this->efInfo.rotate.y += 0.05f * g_gameSpeed ;
			this->efInfo.rotate.z += 0.05f * g_gameSpeed ;
			
			this->efInfo.moveVec.y -= 2.0f * g_gameSpeed ;

			// --- 位置、回転、サイズを反映させる
			g_Object[ this->use3DModelID ].pos	= this->pos ;
			g_Object[ this->use3DModelID ].dir	= VGet( this->efInfo.rotate.x, this->efInfo.rotate.y, this->efInfo.rotate.z ) ;
			g_Object[ this->use3DModelID ].size	= VGet( this->efInfo.size, this->efInfo.size, this->efInfo.size ) ;

			break ;

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ // ---------------- e_Effect_Result_Coconuts
		//					リザルトココナッツ						//
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
		case e_Effect_Result_Coconuts :
			if ( this->efInfo.moveVec.y < 70.0f )		// --- ココナッツが落ちる速度調整
				this->efInfo.moveVec.y += 1.0f   ;
			// --- 位置を足しこむ
			this->ltSpot.x += this->efInfo.moveVec.x ;
			this->ltSpot.y += this->efInfo.moveVec.y ;
			break ;
	}

	// --- 生成してからのカウントを進め、生存時間を越したら消滅する
	this->nowTime += 1 * g_gameSpeed ;

	if ( this->nowTime >= this->efInfo.aliveTime )
		this->destroy() ;

	return 0 ;
}

// ============================================================================================================== E Draw
// ---------------------------------------------------------------------------------
//
//							描画を専門に行う。

//						この関数を呼び出す前に
//			「eInfo.aliveFlg」がTRUEであるかどうかをif()で確認してください。
//					 
// ---------------------------------------------------------------------------------
int Effect::eDraw()
{
	// --- 2D画像の場合( 通常 )
	if ( (!this->isUse3DModel) && (!this->isUse2D) )
		DrawBillboard3D( this->pos, 0.5f, 0.5f, this->efInfo.size, this->efInfo.rotate.z, g_graphHandle[ this->handle ].handle, TRUE ) ;

	// --- 3Dモデルの場合
	else if ( !this->isUse2D )
	{
		MV1SetPosition( g_Object[ this->use3DModelID ].modelHandle,		g_Object[ this->use3DModelID ].pos ) ;
		MV1SetRotationXYZ( g_Object[ this->use3DModelID ].modelHandle,	g_Object[ this->use3DModelID ].dir ) ;
		MV1SetScale( g_Object[ this->use3DModelID ].modelHandle,		g_Object[ this->use3DModelID ].size ) ;
		DrawModelEasy( &g_Object[ this->use3DModelID ] ) ;
	}
	// --- リザルト時のココナッツ表示
	else
		DrawRotaGraph( (int)this->ltSpot.x, (int)this->ltSpot.y, (double)this->efInfo.size, (double)this->efInfo.rotate.z, g_graphHandle[ this->handle ].handle, TRUE, (int)this->efInfo.rotate.x, (int)this->efInfo.rotate.y ) ;

	return 1 ;
}

// ============================================================================================================== Destroy
// ---------------------------------------------------------------------------------
//
//								消滅処理
//					 
// ---------------------------------------------------------------------------------
int Effect::destroy()
{
	if ( this->isUse3DModel )
	{
		g_Object[ this->use3DModelID ].dispFlg = FALSE ;
		g_Object[ this->use3DModelID ].initStates() ;
	}

	this->initStates() ;					// --- あらゆるステータスを初期化する

	return 0 ;
}
